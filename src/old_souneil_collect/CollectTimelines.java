package old_souneil_collect;
import twitter4j.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import old_souneil_collect.OldDBManager;

public class CollectTimelines {
	/**
	 * @param args
	 */
	private OldDBManager dbm = null;
	private BufferedWriter TweetDump = null;
	private BufferedWriter URLDump = null;
	private BufferedWriter HashtagDump = null;
	private BufferedWriter MentionDump = null;
	private ArrayList<Long> RSID = null;
	
    public static void main(String args[]) {
    	CollectTimelines ct = new CollectTimelines();
    	ct.collectTimeLines();
    }
    
    public CollectTimelines(){
    	dbm = new OldDBManager();
    	try {
			TweetDump = new BufferedWriter(new FileWriter("RSTweetDump.txt"));
			HashtagDump = new BufferedWriter(new FileWriter("RSHashtagDump.txt"));
			URLDump = new BufferedWriter(new FileWriter("RSURLDump.txt"));
			MentionDump = new BufferedWriter(new FileWriter("RSMentionDump.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	RSID = dbm.selectRumorSpreaders();
    }
    
    public void collectTimeLines(){
    	Long id;
    	for(int i = 0; i < RSID.size(); i++){
    		id = RSID.get(i);
    		getTweets(id);
//    		break;
    	}
    }

	private void getTweets(Long id) {
		// TODO Auto-generated method stub
		Map<String ,RateLimitStatus> rateLimitStatus = null;
		RateLimitStatus rlstatus = null, rlstatus2 = null;
		try{
            Twitter twitter = new TwitterFactory().getInstance();
			Paging paging = null;
			List<Status> statuses = null;
			String text;
			HashtagEntity[] ht = null;
			URLEntity[] urls = null;
			UserMentionEntity[] mt = null;

	    	for(int page_id = 1; page_id <= 16; page_id++){
	    		while(true){
	    			rateLimitStatus = twitter.getRateLimitStatus();
//	    			rlstatus = twitter.getRateLimitStatus();
					rlstatus = rateLimitStatus.get("/statuses/user_timeline");
					rlstatus2 = rateLimitStatus.get("/application/rate_limit_status");
	                System.out.println(" Limit: " + rlstatus.getLimit());
	                System.out.println(" Remaining: " + rlstatus.getRemaining());
	                System.out.println(" ResetTimeInSeconds: " + rlstatus.getResetTimeInSeconds());
	                System.out.println(" SecondsUntilReset: " + rlstatus.getSecondsUntilReset());

	                if(rlstatus2.getRemaining() <= 0 || rlstatus.getRemaining() <= 0){
	                	int tts = -1;
	                	if(rlstatus2.getSecondsUntilReset() > rlstatus.getSecondsUntilReset()){
	                		tts = rlstatus2.getSecondsUntilReset();
	                	} else{ 
	                		tts = rlstatus.getSecondsUntilReset();
	                	}
	                	Thread.sleep((tts + 10)* 1000);
	                } else {
	                	break;
	                }
	    		}
                paging = new Paging(page_id, 200);
	           // gets Twitter instance with default credentials
	            statuses = twitter.getUserTimeline(id, paging);
	            for (Status status : statuses) {
					text = status.getText().replace('\"', ' ');
					text = status.getText().replace('\\', ' ');
					TweetDump.write("\"" + status.getId() + "\",\"" + text + "\",\"" + id + "\",\"" + status.isRetweet() + "\",\"" + status.getCreatedAt() + "\"EOL\n");
					ht = status.getHashtagEntities();
					for(HashtagEntity e : ht){
						HashtagDump.write("\"" + status.getId() + "\",\"" + e.getText() + "\"EOL\n");
					}
					urls = status.getURLEntities();
					for(URLEntity e: urls){
						URLDump.write("\"" + status.getId() + "\",\"" + e.getURL() + "\"EOL\n");
					}
					mt = status.getUserMentionEntities();
					for(UserMentionEntity e: mt){
						MentionDump.write("\"" + status.getId() + "\",\"" + id + "\",\"" + e.getId() + "\"EOL\n");
					}
	            }
	    	}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
