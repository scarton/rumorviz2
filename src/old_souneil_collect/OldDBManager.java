
package old_souneil_collect;
//
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;

public class OldDBManager {
	/**
	 * @param args
	 */
	private Connection con = null;
	private PreparedStatement ps1 = null;
	private PreparedStatement ps2 = null;
	private PreparedStatement ps3 = null;
	private PreparedStatement ps4 = null;
	private Statement s = null;
	private ResultSet rs = null;

	public OldDBManager() {
		OpenConnect();
	}
	
	public void OpenConnect() {
		String url = "jdbc:mysql://" + Constants.host + ":3306/" + Constants.db;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");

			con = DriverManager.getConnection(url, Constants.id, Constants.pw);
			ps1 = ps2 = ps3 = null;
			rs = null;
			s = null;
		} catch (ClassNotFoundException e) {
			System.out.println("Do not find driver");
		} catch (SQLException ex) {
			System.out.println(ex);
			System.out.println("Uncomplete Connection");
		}
		if(con == null)
			System.out.println("connection failed");
//		System.out.println("logging anyway?");
	} // getConnection
	
	public void initBuffer() {
		
		try
		{
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps1 != null) {
				if(batch_counter > 0 && batch_counter % BATCH_SIZE != 0)
					ps1.executeBatch();
				ps1.close();				
				ps1 = null;
			}
			if(ps2 != null){
				ps2.close();
				ps2 = null;
			}
			if(ps3 != null){
				ps3.close();
				ps3 = null;
			}
			if(ps4 != null){
				ps4.close();
				ps4 = null;
			}

			if (s != null)
			{
				s.close();
				s = null;
			}

		}
		
		catch (Exception e)
		{
			System.out.println ( e);
		}
	}
	
	public void insertRumorTweet(long tweet_id, String text, long author, boolean retweet, int rumor_ID){
		String queryString="INSERT INTO "+Constants.RumorTweets+"(Tweet_ID,Text,Author,Retweet,Rumor_ID) VALUES ( ?,?,?,?,? )";
		int result;
		
		try {
			if(ps1 == null){
				ps1 = con.prepareStatement(queryString);
			}
			ps1.setLong(1, tweet_id);
			ps1.setString(2, text);
			ps1.setLong(3, author);
			ps1.setBoolean(4, retweet);
			ps1.setInt(5, rumor_ID);
			result = ps1.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void insertURL(long tweet_id, String url){
		String queryString="INSERT INTO "+Constants.RumorURL+"(Tweet_ID,url) VALUES ( ?,? )";
		int result;
		
		try {
			if(ps2 == null){
				ps2 = con.prepareStatement(queryString);				
			}
			ps2.setLong(1, tweet_id);
			ps2.setString(2, url);
			
			result = ps2.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void insertMention(long tweet_id, long author, long dest){
		String queryString="INSERT INTO "+Constants.RumorMentions +"(Tweet_ID,Author_ID,Mentioned_ID) VALUES ( ?,?,? )";
		int result;
		
		try {
			if(ps3 == null){
				ps3 = con.prepareStatement(queryString);				
			}
			
			ps3.setLong(1, tweet_id);
			ps3.setLong(2, author);
			ps3.setLong(3, dest);
			result = ps3.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void insertHashtag(long tweet_id, String hashtag){
		String queryString="INSERT INTO "+Constants.RumorHashtag+"(Tweet_ID,Hashtag) VALUES ( ?,? )";
		int result;
		
		try {
			if(ps4 == null){
				ps4 = con.prepareStatement(queryString);				
			}
			ps4.setLong(1, tweet_id);
			ps4.setString(2, hashtag);
			
			result = ps4.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void insertRandomUser(long UID, int type){
		String queryString="INSERT INTO "+Constants.RandomUser+"(Author,Type) VALUES ( ?,? )";
		int result;
		
		try {
			if(ps4 == null){
				ps4 = con.prepareStatement(queryString);				
			}
			ps4.setLong(1, UID);
			ps4.setInt(2, type);
			
			result = ps4.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void insertMeteorTweeter(String S_Name, long UID){
		String queryString="INSERT INTO "+Constants.Meteor_Tweeter+"(screen_name,uid) VALUES ( ?,? )";
		int result;
		
		try {
			if(ps4 == null){
				ps4 = con.prepareStatement(queryString);				
			}
			ps4.setString(1, S_Name);
			ps4.setLong(2, UID);
			
			result = ps4.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public Hashtable<Long, Integer> selectRandomUser(int type){
		Statement statement = null;
		ResultSet rs = null;
		String query;
		Long id = (long) -1;
		int tp = -1;
		Hashtable<Long,Integer> RandomUserList = new Hashtable<Long, Integer>();
		
		try {
			statement = con.createStatement();
			query = "select distinct Author, Type from " + Constants.RandomUser;
			if(type > 0){
				query += " where Type=" + type;
			}
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				id = rs.getLong(1);
				tp = rs.getInt(2);
				RandomUserList.put(id, new Integer(tp));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return RandomUserList;
	}
	
	public ArrayList<Long> selectRumorSpreaders(){
		Statement statement = null;
		ResultSet rs = null;
		String query;
		Long id = (long) -1;
		int tp = -1;
		ArrayList<Long> RumorSpreaders = new ArrayList<Long>();
		
		try {
			statement = con.createStatement();
			query = "select distinct Author from " + Constants.RumorSpreader;
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				id = rs.getLong(1);
				RumorSpreaders.add(id);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return RumorSpreaders;
	}
	
	public HashMap<Long,Object> selectMeteorTweeter(){
		Statement statement = null;
		ResultSet rs = null;
		String query;
		long id;
		HashMap<Long,Object> Tweeters = new HashMap<Long,Object>();
		Object exist = new Object();
		
		try {
			statement = con.createStatement();
			
			query = "select distinct uid from " + Constants.Meteor_Tweeter;
			rs = statement.executeQuery(query);
			while(rs.next()){
				id = rs.getLong(1);
				Tweeters.put(id, exist);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Tweeters;
	}
	
	public HashMap<String,Object> selectMeteorPeople(){
		Statement statement = null;
		ResultSet rs = null;
		String query;
		String id = null;
		int tp = -1;
		HashMap<String,Object> RumorSpreaders = new HashMap<String,Object>();
		HashMap<String,Object> ExistingSpreaders = new HashMap<String,Object>();
		Object exist = new Object();
		
		try {
			statement = con.createStatement();
			
			query = "select distinct Author from " + Constants.Meteor_Followers;
			rs = statement.executeQuery(query);
			while(rs.next()){
				id = rs.getString(1);
				ExistingSpreaders.put(id, exist);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		return RumorSpreaders;
		return ExistingSpreaders;
	}
	
	public HashMap<String,Object> selectPeopleOfRumor(int Rumor_ID){
		Statement statement = null;
		ResultSet rs = null;
		String query;
		String id = null;
		int tp = -1;
		HashMap<String,Object> ExistingSpreaders = new HashMap<String,Object>();
		Object exist = new Object();
		
		try {
			statement = con.createStatement();
			
			query = "select distinct screen_name from " + Constants.BostonRumor_Tweeter + " where Rumor_ID=" + Rumor_ID;
			rs = statement.executeQuery(query);
			while(rs.next()){
				id = rs.getString(1);
				ExistingSpreaders.put(id, exist);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		return RumorSpreaders;
		return ExistingSpreaders;
	}
	
	public ArrayList<Long> selectPeopleByIDOrder(){
		return selectPeopleByIDOrder(true);
	}

	public ArrayList<Long> selectPeopleByIDOrder(boolean follower){
		Statement statement = null;
		ResultSet rs = null;
		String query;
		long id = -1;
		int tp = -1;
		HashSet<Long> oldPeople = new HashSet<Long>();
		ArrayList<Long> People = new ArrayList<Long>();
		Object exist = new Object();
		
		try {
			statement = con.createStatement();
		//	query = "select distinct Author from Tweets where not exists (select * from Follower where Follower.Author=Tweets.Author)";
			if (follower)
				query = "select distinct Author from Follower";
			else
				query = "select distinct Author from Followee";

			rs = statement.executeQuery(query);
			
			while(rs.next()){
				id = rs.getLong(1);
				oldPeople.add(id);
			}

			query = "select distinct Author from Tweets where Rumor_ID <= 5";
                        rs = statement.executeQuery(query);

                        while(rs.next()){
                                id = rs.getLong(1);
                                oldPeople.add(id);
                        }

			System.out.println(oldPeople.size() + " distinct authors detected for which we have already collected follower information.");
			query = "select distinct Author from Tweets where Rumor_ID >= 12 and Rumor_ID <= 17";
			
			rs = statement.executeQuery(query);
			while (rs.next()){
				id = rs.getLong(1);
				if (!oldPeople.contains(id)) {
					People.add(id);
				}
			}
			System.out.println(People.size() + " authors detected for which we have not collected follower information.");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		return RumorSpreaders;
		return People;
	}
	
	public void insertMeteorTweeterInfo(long uid, int followers_count,
			int followees_count, String description) {
		// TODO Auto-generated method stub
		String queryString="UPDATE "+Constants.Meteor_Tweeter+" SET followers=" 
				+ followers_count + ", followees=" + followees_count + 
				", description=\"" + description + "\" where uid=" + uid;
		int result;
		
		try {
			ps4 = con.prepareStatement(queryString);
			result = ps4.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	static final int BATCH_SIZE = 1000;
	static int batch_counter = 0;
	
	public void updateTweeterInfo(long uid, int followers_count,
			int followees_count, String description, String u_name) {
		// TODO Auto-generated method stub
		String queryString="UPDATE "+Constants.BostonRumor_Tweeter+" SET uid=?, followers=?, followees=?, description=? where screen_name=?";
		
		try {
			if(ps1 == null){
				ps1 = con.prepareStatement(queryString);
			}
			ps1.setLong(1, uid);
			ps1.setInt(2, followers_count);
			ps1.setInt(3, followees_count);
			ps1.setString(4, description);
			ps1.setString(5, u_name);
			ps1.addBatch();
			if(batch_counter % BATCH_SIZE == BATCH_SIZE - 1)
				ps1.executeBatch();
			batch_counter++;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public ArrayList<String> selectTweetOfPerson(long uid){
		Statement statement = null;
		ResultSet rs = null;
		String query;
		ArrayList<String> ret = new ArrayList<String>();
		
		try {
			statement = con.createStatement();
			query = "select screen_name,followers,followees,description from " + Constants.Meteor_Tweeters + " where uid=" + uid;
			rs = statement.executeQuery(query);
			
			rs.next();
			String screen_name = rs.getString(1);
			ret.add(String.format("%d (%s) - followees: %d", uid, screen_name, rs.getInt(3)));
/*			ret.add(String.format("%d (%s) - follwers: %d, followees: %d, desc: %s", uid, screen_name, rs.getInt(2), rs.getInt(3), rs.getString(4)));
			
			query = "select Type, Text from " + Constants.Meteor_TweetsAll + " where Author=\"" + screen_name + "\" group by Tweet_ID"; 
			rs = statement.executeQuery(query);
			while(rs.next()){
				ret.add(String.format("%d - %s", rs.getInt(1), rs.getString(2)));
			}
*/
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}

	public ArrayList<Long> selectTweetIDs(int rumor_id) {
		// TODO Auto-generated method stub
		String TableName, query;
		Statement statement = null;
		ResultSet rs = null;
		long tid = -1;
		ArrayList<Long> tids = new ArrayList<Long>();
		
		if(rumor_id == 0)
			TableName = Constants.Meteor_Tweets;
		else if (rumor_id == 1)
			TableName = Constants.AP_Tweets;
		else
			TableName = "Tweets";
		
		try {
			statement = con.createStatement();

			if(rumor_id == 0 || rumor_id == 1)
				query = "select distinct Tweet_ID from " + TableName;
			else
				query = "select distinct Tweet_ID from Tweets where Embed is null";
			//	query = "select Tweet_ID from (select * from Tweets where Type=1 or Type=-1 or Type=2) as T where Embed is null";
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				tid = rs.getLong(1);
				tids.add(tid);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tids;
	}

	public void storeEmbed(Long tid, String embed, int rumor_id) {
		// TODO Auto-generated method stub
		String TableName, query;
		
		if(rumor_id == 0){
			TableName = Constants.Meteor_Tweets_Seed;
		} else if(rumor_id == 1) {
			TableName = Constants.AP_Tweets;
		} else {
			TableName = "Tweets";
		}
		
		query ="UPDATE "+TableName+" SET Embed=? WHERE Tweet_ID=?";
		
		try {
			if(ps2 == null){
				ps2 = con.prepareStatement(query);			
			}
			ps2.setString(1, embed);
			ps2.setLong(2, tid);
			ps2.executeUpdate();
			
			if(rumor_id == 0){
				TableName = Constants.Meteor_Trackback;
				query ="UPDATE "+TableName+" SET Embed=? WHERE Tweet_ID=?";
				
				ps2 = con.prepareStatement(query);
				ps2.setString(1, embed);
				ps2.setLong(2, tid);
				
				ps2.executeUpdate();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
