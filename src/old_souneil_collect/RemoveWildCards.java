package old_souneil_collect;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class RemoveWildCards {

	/**
	 * @param args
	 */
	private BufferedReader input = null;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RemoveWildCards rwc = new RemoveWildCards();
		rwc.removeChars();
	}
	
	private void removeChars() {
		// TODO Auto-generated method stub
		try {
			String line;
			String [] segs = new String[5];
			int comma_loc[] = new int[4];
			
			while((line = input.readLine()) != null){
				if(line.endsWith("EOL") == false){
					while(line.endsWith("EOL") == false){
						String temp;
						if((temp = input.readLine()) != null)
							line = line + temp;
						else
							System.exit(0);
					}
				}
//				System.out.println("test - " + line);
				line = line.replace('\"', ' ');
				line = line.replace('\\', ' ');
				
				comma_loc[0] = line.indexOf(',');
				comma_loc[3] = line.lastIndexOf(',', line.lastIndexOf(','));
				comma_loc[2] = line.lastIndexOf(',', comma_loc[3]-1);
				comma_loc[1] = line.lastIndexOf(',', comma_loc[2]-1);
//				System.out.println("comma locs: " + comma_loc[0] + ", " + comma_loc[1] + ", " + comma_loc[2] + ", " + comma_loc[3]);
				segs[0] = line.substring(0,comma_loc[0]);
				segs[0] = segs[0].trim();
				segs[1] = line.substring(comma_loc[0]+1, comma_loc[1]);
				segs[1] = segs[1].trim();
				segs[2] = line.substring(comma_loc[1]+1, comma_loc[2]);
				segs[2] = segs[2].trim();
				segs[3] = line.substring(comma_loc[2]+1, comma_loc[3]);
				segs[3] = segs[3].trim();
				segs[4] = line.substring(comma_loc[3]+1, line.lastIndexOf("EOL"));
				segs[4] = segs[4].trim();
				
				System.out.println("\"" + segs[0] + "\",\"" + segs[1] + "\",\"" + segs[2] + "\",\"" + segs[3] + "\",\"" + segs[4] + "\"EOL");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public RemoveWildCards(){
		try {
			input = new BufferedReader(new FileReader("RSTweetDump.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
