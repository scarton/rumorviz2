package old_souneil_collect;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import old_souneil_collect.OldDBManager;
import twitter4j.HashtagEntity;
import twitter4j.IDs;
import twitter4j.OEmbed;
import twitter4j.OEmbedRequest;
import twitter4j.Paging;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.UserMentionEntity;
import twitter4j.conf.ConfigurationBuilder;

public class CollectTweetEmbeds2 {
	private OldDBManager dbm = null;
	private BufferedWriter FollowerDump = null;
	private BufferedWriter FolloweeDump = null;
	public int mode = 4;			// 1 if collecting followers, 2 if collecting followees, 3 if collecting author info
	
	private static int Rumor_ID = 21;
	public OAuth[] tokens = null;
	private int token_count = 18, TokenID=-1;
	
	/**
	 * @param args
	 */
	class OAuth {
		public String CK;			// ConsumerKey
		public String CS;			// ConsumerSecret
		public String AT;			// AccessToken
		public String ATS;			// AccessTokenSecret
		
		public OAuth (String CK, String CS, String AT, String ATS){
			this.CK = CK;
			this.CS = CS;
			this.AT = AT;
			this.ATS = ATS;
		}
	};	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int TokenID = Integer.parseInt(args[0]);

		CollectTweetEmbeds2 cf = new CollectTweetEmbeds2(TokenID);
		if(cf.mode == 1){
			cf.getFollowers(TokenID);			
			cf.closeDump();
		} else if(cf.mode == 2){
			cf.getFollowee(TokenID);			
			cf.closeDump();
		} else if(cf.mode == 3){
			cf.getIds();
		} else if(cf.mode == 4){
			cf.GetEmbeds(TokenID);
		}
	}

	private void getFollowee(int TokenID) {
		// TODO Auto-generated method stub
		Map<String ,RateLimitStatus> rateLimitStatus = null;
		ArrayList<Long> People = null;
		RateLimitStatus rlstatus = null, rlstatus2 = null;
		try{
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true)
			  .setOAuthConsumerKey(tokens[TokenID].CK)
			  .setOAuthConsumerSecret(tokens[TokenID].CS)
			  .setOAuthAccessToken(tokens[TokenID].AT)
			  .setOAuthAccessTokenSecret(tokens[TokenID].ATS);
			TwitterFactory tf = new TwitterFactory(cb.build());
            Twitter twitter = tf.getInstance();
            IDs ids;
   
//            People = dbm.selectMeteorTweeter();
            People = dbm.selectPeopleByIDOrder();
//            Iterator<Long> it = People.keySet().iterator();
            for(int idx = TokenID*1391; idx < Math.min((TokenID+1)*1391, People.size()); idx++){
            	try{
	            	long uid = People.get(idx);
		    		while(true){
		    			rateLimitStatus = twitter.getRateLimitStatus();
//		    			rlstatus = twitter.getRateLimitStatus();
						rlstatus = rateLimitStatus.get("/friends/ids");
						rlstatus2 = rateLimitStatus.get("/application/rate_limit_status");
/*						
    	                System.out.println(" Limit: " + rlstatus.getLimit());
    	                System.out.println(" Remaining: " + rlstatus.getRemaining());
    	                System.out.println(" ResetTimeInSeconds: " + rlstatus.getResetTimeInSeconds());
    	                System.out.println(" SecondsUntilReset: " + rlstatus.getSecondsUntilReset());
*/
    	                if(rlstatus2.getRemaining() <= 0 || rlstatus.getRemaining() <= 0){
			              	int tts = -1;
			               	if(rlstatus2.getSecondsUntilReset() > rlstatus.getSecondsUntilReset()){
			               		tts = rlstatus2.getSecondsUntilReset();
			               	} else{ 
			               		tts = rlstatus.getSecondsUntilReset();
			               	}
			               	System.out.println("going to sleep....");
			               	Thread.sleep((tts + 10)* 1000);
			               	System.out.println("woke up!!!");
			            } else {
			               	break;
			            }
			   		}
//		    		System.out.println("handling " + uid);
		    		long cursor = -1;
		  	      	do {
		  	      		ids = twitter.getFriendsIDs(uid, cursor);
		  	      		System.out.println("uid - " + uid + ", cursor - " + cursor);
		  	      		for (long id : ids.getIDs()) {
	//	  	      			System.out.println(id);
	//	  	      			User user = twitter.showUser(id);
	//	  	      			System.out.println(user.getName());
		  	      			FolloweeDump.write("\"" + uid + "\",\"" + id + "\"EOL\n");
		  	      		}
		  	      	} while ((cursor = ids.getNextCursor()) != 0);
            	} catch (TwitterException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
            }
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CollectTweetEmbeds2(int TokenID){
    	dbm = new OldDBManager();
    	tokens = new OAuth[token_count];
    	this.TokenID =TokenID;  
    	
    	tokens[0] = new OAuth("S0HrvMBtFWrnccV9mXKKeQ", "EVkctWVhzv1CdvT4Tkezf1pA6LU8xpk0udfgdWpNAk", "10434312-vEHrNtZYqpT7z2iPXqPqq5JEJlRsPulHLTLZEEk3K", "4ifpNIckQkyBLKWw6rwJ8bMuqTYZs4W6NLiIy1Yi4");
    	tokens[1] = new OAuth("P3pcHqmYsZE1Vf6DO2dw", "9VY9ON3e6GihALfjVncLZnjmy3cGMj2du94mus", "36915251-5sX4dabA8F9Vkb8BD1cvgOtrEBHhcRJlJDCKmx3oI", "1uGl5VSoMtxR8k3RLcdggcMtBgoykdKHMHhkpdhj2Ud5S");				// Mine
    	tokens[2] = new OAuth("IYOKWSM9z7VySguX0ZGhQw","dPKmjXnVgKT7iWJlnhwigZlQt260hyI4RznBG9wDY","171026401-b4o1Xlifrrm1C3Otf3vAgi26XmtNkCWQpNOt8r5z","sVOhZBCIRPD2iG4s4KQMDL72UvYZ72gfyApCaWIdIxXps");			// Sue
    	tokens[3] = new OAuth("7x1mC5bIAw78E9KMUm2kA","NiuRmf0qucxP91nfADvaAQBtVyMfS64lvWKWQvbI","2261399071-dKcRjQcN3N9BgthXUeSQ8nXwOlDx0hDaiRRfXlZ","ikselUt7EQjyG6bsfUic1BZxYERPj4iXMU1RlynTthxg3");			// bspark915
    	tokens[4] = new OAuth("jFjRWNPiEcdqCH566e6LKQ","suCjds8bmc7EYXJBVrdj4JeKN59Nqzrm9XfHYc6Kc","2261397656-udH6KRtRsZNpLvxDDved82XB73N5KtJSCOFKoPG","q5MBGAcfjvzQhYO0YyFvKh3k4YGAaafx7lrDARCDZPOZ2");			// ParkSouneil
    	tokens[5] = new OAuth("rN6Xr0mUIQ4aEvs8sA8kNg","lnb9BtR28isB8YKAyRGttC6VhKgHVHiz6wviOn1Y","2261408167-b8XtemCHWTcz95ESFy7jpOe97Bk5SK5muIyRZ9x","gce9nkW0sD9vsxsUyKcc4xPJBpjC2OzGmcXVPvNWgdkCT");			// RumorUmich
    	tokens[6] = new OAuth("7aiab5Ke3QHdFOrXgaJ1Q","U6VNiPDMs1W0sXaJOedXlyj149xF6ku0ImmD092ru94","369421172-ZPHN62QE5II9SIaFBMog8tYRI3m39z1rha1tccLU","9UzOKZb5w5Y1mCNF5GxN2hHfr9Joh2QQl63ab8VC48B9L");				// seungwoo_kang
    	tokens[7] = new OAuth("tsw1k2uRR2BYPFI043Yag","jIKySRddIApTLDMSTpfYMN6o8riXvpEyHUzls2C4Y","2261551902-pSmcPQSBSFqYpzoc3WSWyg05vR9Qh3PQaaqcRwV","cxDxDhrtqkbbbYNC4A2HHVKZbm5AmInpT7gxCci7ysKBd");				// CollectorOne - rumor.collector1@gmail.com
    	tokens[8] = new OAuth("2l7qBaInEFQZjE4al5b4A","yV3qztv7R6wLxTqY1dkfJjImd60umqaEzl9FH9LDn0","2261568800-Suj6w0jD9uA7rhHFxcWvBLRyRos5SlrNOeIFGUL","5MlUuX1oRLWrgNzT7JEASKlsZ8ZC6gHa3sliv7WcoTH5B");				// RCollectorTwo - rumor.collector2@gmail.com
    	tokens[9] = new OAuth("LYik5tgT6MzbwI3KcFqzQ","Duw2MZ3m3g5IhJEgooH7z6MhdEYZtADvyPzxbHHRw","2261572188-OPBA6naTx1d8kpy52AHeCzt7gfc84jiFie7U39X","IhtcpWilvpwEsILhI6lGC8s91rQiy2BTOES8xG2SDhIlZ");				// CollectorThree - rumor.collector3@gmail.com    	

    	tokens[10] = new OAuth("5n3VGqXkpfjWYEKn2UDA", "pxHlF4Nwqajavu5acW6RkhNqQoC5ltzSJAY5pst14", "2261871864-6MiZdFwhliRRUvaygF1gFhAgttJACjD2lXvtNEh", "f5vFM69jFHOhVEBpHnxFzQUxogMzTUKnm32BpODBCfSE7");				// SCSPResearch - scsp.research.1.@gmail.com
    	tokens[11] = new OAuth("tQ7NwhxOV5AkmdMJUPRMQ","PAebgkLlWskpU4GAXAH9rv1wgWw1ERfrdTNAda985o","2261939359-J9JjBuFVFcJYY9SKzm7FhoxHrQnncKRg8kDgyGS","yF24RWpe8uLhbKrHXh9wWehLz400dfcrR2by38dQOb8cJ");			// SCSPResearch2 - scsp.research.2.@gmail.com
    	tokens[12] = new OAuth("QwR8OsXCdTvQREkcWQ8pLw","vUgAUXtpJsiHoYtkReMv9wFYm7JIt8hcZTw0kXDdU","2261947464-sK1MafX8npJILchV9de0sXTXNSEKwjPvNgLMBr6","DlcXC61FNoxQw5Tzf5I3dBAbqzv9h76jr631aV0iTL69Y");			// AXAOResearch - axao.research@gmail.com
    	tokens[13] = new OAuth("aewI8zMyJgJwuy9ui7UaQ","3xY3lDnogDZhJi4UH5ubhegu2icjNMnsNM3kT1WkyAk","2261953154-d8LQO7njHXkTDt5F2BE9yAgMmvE8g5wzXikNHYG","GuO60I8cVk4Gg54cAbGENqz4OvHBjdgsCD3pOAbEhjzhv");			// MNLPResearch - mnlp.research@gmail.com
    	tokens[14] = new OAuth("X8WDvUwt11a75Y6nhhFw","9EbXxgbNEBqYNtlQo3M6n2nSSJ63xqx4MlZMA1GSQ","2261957124-ZfQGYkrxQEIYEwYcWfig7YipzIm0qxifBL7Wn93","yIGnuBZBljhvvWmTK5p9NZojmI0jzxhkEAF7qa0zj6Oyc");			// EWKSResearch - ewks.research@gmail.com
    	tokens[15] = new OAuth("9cTtQDPpFM3ousv7JdcSCw","iEfa3VWZzHEtMcvMIAxZ7p1afeVA0eYmdO2JwbEiIA","2261969496-FH9NQ8cG40IJgnE6oAUg8eLFxMQZhJ9Ff2V59rc","Eg3PZ39eNJT8HvB2FgOUnQjZ8shO7uDQqn6lQeq6t1n8Q");				// FPFPResearch - fpfp.research@gmail.com
    	tokens[16] = new OAuth("mXEMPOCUy9311TmsTSqrA","2tVy5MFRygylxtOuP6FXwEFV8kAf4YdlCqKBtNDg","2261978755-5T8jXXXZhxbv7HSajRwzx4EbipcMqbZOblOeUdN","iGJmh98cWPHaUROwBSHzvNGVzCKgW3xK7D0CWUDlenGH3");				// AOFPResearch - aofp.research@gmail.com
    	tokens[17] = new OAuth("Ye6RAO571iSElVQtQW42w","4TDG1Ut8AMJExWpZP7z18geIIzExT1UGTZEWFM8","2261980194-2ThPuE3Nt94nsm8AE4GDsr4JFJjEu1yyI845XS8","p9S05MZxB5jdD511p9BVWN4eEBoOIf35UYUwpr5y0gZz9");				// MPOKResearch - mpok.research@gmail.com
//    	tokens[18] = new OAuth("yRJehhPmgOmXRV6OFtLI5Q","v9G9SoZNILMUgSQP0R1VNAPQ2b7LLVuhy3LSgZkuk","516011372-C0qM9naZQtp2oHXkI6iKHkVYHTzjB3JotZnUNaM3","UiwKaoEuYIKPO83kWz0jGfzCny6lZIKSs8DDbIh6X");				// 17426278493 - i.ate.god@gmail.com    	

    	try {
    		if(mode == 1){
    			FollowerDump = new BufferedWriter(new FileWriter("FollowerDump" + TokenID + ".txt"));    			
    		} else if(mode == 2){
    			FolloweeDump = new BufferedWriter(new FileWriter("FolloweeDump" + TokenID + ".txt"));    			
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeDump(){
		try {
			if(mode == 1){
				FollowerDump.close();				
			} else if(mode == 2){
				FolloweeDump.close();				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getIds(){
		Map<String ,RateLimitStatus> rateLimitStatus = null;
//		HashMap<String, Object> People = dbm.selectMeteorPeople();
		HashMap<String, Object> People = dbm.selectPeopleOfRumor(Rumor_ID);
		RateLimitStatus rlstatus = null, rlstatus2 = null;
		try{
            Twitter twitter = new TwitterFactory().getInstance();
            Iterator<String> it = People.keySet().iterator();
            while(it.hasNext()){
            	try{
	            	String uid = it.next();
		    		while(true){
		    			rateLimitStatus = twitter.getRateLimitStatus();
		//    			rlstatus = twitter.getRateLimitStatus();
						rlstatus = rateLimitStatus.get("/statuses/user_timeline");
						rlstatus2 = rateLimitStatus.get("/application/rate_limit_status");
/*			            System.out.println("Limit: " + rlstatus.getLimit());
			            System.out.println("Remaining: " + rlstatus.getRemaining());
			            System.out.println("ResetTimeInSeconds: " + rlstatus.getResetTimeInSeconds());
			            System.out.println("SecondsUntilReset: " + rlstatus.getSecondsUntilReset());
*/			    		
			            if(rlstatus2.getRemaining() <= 0 || rlstatus.getRemaining() <= 0){
			              	int tts = -1;
			               	if(rlstatus2.getSecondsUntilReset() > rlstatus.getSecondsUntilReset()){
			               		tts = rlstatus2.getSecondsUntilReset();
			               	} else{ 
			               		tts = rlstatus.getSecondsUntilReset();
			               	}
			               	System.out.println("going to sleep....");
			               	Thread.sleep((tts + 10)* 1000);
			               	System.out.println("woke up!!!");
			            } else {
			              	break;
			            }
			   		}
		            User user = twitter.showUser(uid);
		    		System.out.println(uid + ", " + user.getId());		    		
//		            dbm.insertMeteorTweeter(uid, user.getId());
		    		int followers_count = user.getFollowersCount();
		    		int followees_count = user.getFriendsCount();
		    		String description = user.getDescription();
//		    		dbm.insertMeteorTweeterInfo(user.getId(), followers_count, followees_count, description);
		    		dbm.updateTweeterInfo(user.getId(), followers_count, followees_count, description, uid);
            	} catch (TwitterException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
            }
            dbm.initBuffer();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void getFollowers(int TokenID){
		Map<String ,RateLimitStatus> rateLimitStatus = null;
		ArrayList<Long> People = null;
		RateLimitStatus rlstatus = null, rlstatus2 = null;
		System.out.println("TokenID: " + TokenID);
		try{
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true)
			  .setOAuthConsumerKey(tokens[TokenID].CK)
			  .setOAuthConsumerSecret(tokens[TokenID].CS)
			  .setOAuthAccessToken(tokens[TokenID].AT)
			  .setOAuthAccessTokenSecret(tokens[TokenID].ATS);
			TwitterFactory tf = new TwitterFactory(cb.build());
            Twitter twitter = tf.getInstance();
            IDs ids;
            
//            People = dbm.selectMeteorPeople();
            People = dbm.selectPeopleByIDOrder();
		int interval = (int) (People.size()/18.0);
            for(int idx = TokenID*interval; idx < Math.min((TokenID+1)*interval, People.size()); idx++){
//            	System.out.println("From: " + (TokenID*2500) + ", To: " + Math.min((TokenID+1)*2500, People.size()));
            	try{
	            	Long uid = People.get(idx);
		    		while(true){
		    			rateLimitStatus = twitter.getRateLimitStatus();
		//    			rlstatus = twitter.getRateLimitStatus();
						rlstatus = rateLimitStatus.get("/followers/ids");
						rlstatus2 = rateLimitStatus.get("/application/rate_limit_status");
/*						System.out.println("Limit: " + rlstatus.getLimit());
			            System.out.println("Remaining: " + rlstatus.getRemaining());
			            System.out.println("ResetTimeInSeconds: " + rlstatus.getResetTimeInSeconds());
			            System.out.println("SecondsUntilReset: " + rlstatus.getSecondsUntilReset());
*/			    		
			            if(rlstatus2.getRemaining() <= 0 || rlstatus.getRemaining() <= 0){
			              	int tts = -1;
			               	if(rlstatus2.getSecondsUntilReset() > rlstatus.getSecondsUntilReset()){
			               		tts = rlstatus2.getSecondsUntilReset();
			               	} else{ 
			               		tts = rlstatus.getSecondsUntilReset();
			               	}
			               	System.out.println("going to sleep....");
			               	Thread.sleep((tts + 10)* 1000);
			               	System.out.println("woke up!!!");
			            } else {
			               	break;
			            }
		
			   		}
//		    		System.out.println("handling " + uid);
		    		long cursor = -1;
		  	      	do {
		  	      		ids = twitter.getFollowersIDs(uid, cursor);
		  	      		System.out.println("uid - " + uid + ", cursor - " + cursor);
		  	      		for (long id : ids.getIDs()) {
	//	  	      			System.out.println(id);
	//	  	      			User user = twitter.showUser(id);
	//	  	      			System.out.println(user.getName());
	
	//	  	      			FollowerDump.write("\"" + uid + "\",\"" + id + "\",\"" + user.getName() + "\"EOL\n");
		  	      			FollowerDump.write("\"" + uid + "\",\"" + id + "\"EOL\n");
		  	      		}
		  	      	} while ((cursor = ids.getNextCursor()) != 0);
            	} catch (TwitterException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
            }
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void GetEmbeds(int TokenID){
		Map<String ,RateLimitStatus> rateLimitStatus = null;
		ArrayList<Long> TweetIDs = null;
		RateLimitStatus rlstatus = null, rlstatus2 = null;
		try{
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true)
			  .setOAuthConsumerKey(tokens[TokenID].CK)
			  .setOAuthConsumerSecret(tokens[TokenID].CS)
			  .setOAuthAccessToken(tokens[TokenID].AT)
			  .setOAuthAccessTokenSecret(tokens[TokenID].ATS);
			TwitterFactory tf = new TwitterFactory(cb.build());
	        Twitter twitter = tf.getInstance();

	        TweetIDs = dbm.selectTweetIDs(-1);						// third parameter: 0 if Russian Meteor, 1 if AP_tweets, else if all others (tables in BostonSet_Rumors)
		System.out.println(TweetIDs.size() + " IDs retrieved from DB that don't have HTML Embed info associated with them."); 
		int interval = (int) Math.ceil(TweetIDs.size()/18.0);
            for(int idx = TokenID*interval; idx < Math.min((TokenID+1)*interval, TweetIDs.size()); idx++){
//	        for(int i = 0; i < TweetIDs.size(); i++){
//	        for(int idx = TokenID*10; idx < Math.min((TokenID+1)*10, TweetIDs.size()); idx++){
	        	try{
	            	long uid = TweetIDs.get(idx);
		    		while(true){
		    			try{
			    			rateLimitStatus = twitter.getRateLimitStatus();
	//		    			rlstatus = twitter.getRateLimitStatus();
							rlstatus = rateLimitStatus.get("/statuses/oembed");
							rlstatus2 = rateLimitStatus.get("/application/rate_limit_status");
	/*
							System.out.println(" Limit: " + rlstatus.getLimit());
			                System.out.println(" Remaining: " + rlstatus.getRemaining());
			                System.out.println(" ResetTimeInSeconds: " + rlstatus.getResetTimeInSeconds());
			                System.out.println(" SecondsUntilReset: " + rlstatus.getSecondsUntilReset());
	*/
			                if(rlstatus2.getRemaining() <= 0 || rlstatus.getRemaining() <= 0){
				              	int tts = -1;
				               	if(rlstatus2.getSecondsUntilReset() > rlstatus.getSecondsUntilReset()){
				               		tts = rlstatus2.getSecondsUntilReset();
				               	} else{ 
				               		tts = rlstatus.getSecondsUntilReset();
				               	}
				               	System.out.println("going to sleep....");
				               	Thread.sleep((tts + 10)* 1000);
				               	System.out.println("woke up!!!");
				            } else {
				               	break;
				            }
		    			} catch (TwitterException e) {
			    			// TODO Auto-generated catch block		
		    				if(e.exceededRateLimitation() == true){
				               	System.out.println("going to sleep....");
				               	Thread.sleep(300* 1000);				// sleep for 5 minutes
				               	System.out.println("woke up!!!");
		    				}
			    			e.printStackTrace();
			    		}
			   		}
		    		
	  	      		OEmbedRequest omr = new OEmbedRequest(uid, null);
	  	      		omr.setAlign(OEmbedRequest.Align.LEFT);
	  	      		omr.setOmitScript(true);
	  	      		omr.setMaxWidth(250);
	  	      		OEmbed oEmbed = twitter.getOEmbed(omr);
	  	      		if(oEmbed.getHtml() == null) continue;
	  	      		System.out.print("uid - " + uid + ", html - " + oEmbed.getHtml());
	  	      		dbm.storeEmbed(uid, oEmbed.getHtml(), -1);		// third parameter: 0 if Russian Meteor, 1 if AP_tweets, else if all others (tables in BostonSet_Rumors)
//	  	      		Embeds.put(uid, oEmbed.getHtml());	
	        	} catch (TwitterException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	        }
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
