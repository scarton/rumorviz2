package struct;

public class LongInt
{
	public Integer i;
	public Long l;
	public LongInt(Integer i, Long l)
	{
		super();
		this.i = i;
		this.l = l;
	}
	public Integer getInt()
	{
		return i;
	}
	public void setInt(Integer i)
	{
		this.i = i;
	}
	public Long getLong()
	{
		return l;
	}
	public void setLong(Long l)
	{
		this.l = l;
	}
	
	
}
