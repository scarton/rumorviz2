package precompute;

import db.DBManager;

public class DropPrecomputationTables
{
	public static void main(String[] args) throws Exception
	{
		System.out.println("Dropping precomputation tables...");
		DBManager db = new DBManager();
		db.delete.dropPrecomputationTables();
		System.out.println("Tables dropped.");

	}
}
