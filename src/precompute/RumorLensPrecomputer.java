package precompute;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import precompute.StateManager.Interaction;
import precompute.StateManager.State;
import precompute.StateManager.TweetType;
import precompute.StateManager.UserType;
import db.Constants;
import db.DBManager;
import entity.Author;
import entity.AuthorInfo;
import entity.CompleteTweet;
import entity.TimeStamp;

/**
 * This class does precomputation of stuff to produce the RumorLens visualizations.
 * 
 * For each rumor ID selected, it takes the contents of the tweets, tweeter_info, 
 * followers, followees tables and makes entries in the sankey_state_updates,  
 * neighborhood_tweets  and tweeter_activity tables for that rumor.
 * 
 * Sankey_State_Updates stores the impact of each tweet in spreading the rumor
 * 
 * neighborhood_tweets stores records of activity by followers and followed people
 * of authors of tweets that spread the rumor.
 * 
 *  
 * @author Sam
 *
 */

public class RumorLensPrecomputer
{
	private int precompute_rumor_id;
	private DBManager readDatabase;
	private DBManager writeDatabase; //So we can write the precomputation data to a different database than we read the raw info from, if need be
	private HashMap<Long,HashSet<Long>> followerMap; //Maps Twitter names to sets of follower IDs
	private HashMap<Long,Author> authorInfos;
	
	//private static final int[] rumor_IDs = {20} ; //Indicates that we are dealing with the Russian Meteor rumor set. 

	private static final int[] rumor_IDs = {12,13,14,16,31} ; //Indicates that we are dealing with the Russian Meteor rumor set. 
	//private static final int population = 1451046; //Total number of twitter users associated with this rumor over all timespan
	private static final int start = 0; //start and end pertain to how many tweets we read into the tweet timeline
	//private static final int end = 1361920704; 
	
	private static final boolean firstNTweets = false;
	private static final int N = 600;
	private static final int off = 600;

	
	private ArrayList<CompleteTweet> tweets;
	
	private StateManager manager; //Tracks the evolving state transition diagram
	
	public static void test()
	{
		for (int rumor : rumor_IDs)
		{ 
			try
			{
			System.out.println("Initializing MultiStatePrecomputer for rumor: " + rumor);
			RumorLensPrecomputer sp = new RumorLensPrecomputer(rumor);
			System.out.println("Precomputing states...");
			sp.precomputeStates();
			System.out.println("Done with rumor " + rumor+"\n\n");
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				continue;
			}
		}
	}
	
	
	public RumorLensPrecomputer(int rumor_type) throws Exception
	{
		readDatabase = new DBManager(Constants.Server.resnick1);
		writeDatabase = new DBManager(Constants.Server.resnick1_2);
		
		initialize(rumor_type, readDatabase, writeDatabase);
		
	}
	
	
	public RumorLensPrecomputer(int rumor_id, DBManager readDB, DBManager writeDB) throws Exception
	{
		initialize(rumor_id, readDB,writeDB);
	}
	
	
	private void initialize(int rumor_id, DBManager readDB, DBManager writeDB) throws Exception
	{
		this.precompute_rumor_id = rumor_id;
		System.out.println("Initializing read DBManager...");
		readDatabase = readDB;
		System.out.println("Initializing write DBManager...");
		writeDatabase = writeDB;
		
		writeDatabase.create.createPrecomputationTablesIfTheyDoNotExist();
		System.out.println("Getting followers");
		followerMap = readDatabase.get.selectAllFollower(precompute_rumor_id);
		System.out.println(followerMap.size() + " followers found.");
		
		System.out.println("Getting basic author author information");
		authorInfos = readDatabase.get.getBasicAuthorInformation(precompute_rumor_id);
	
		System.out.println(authorInfos.size() + " author infos found.");
		
		System.out.println("Copying tweets from Cheng DB into Sam DB");
		writeDatabase.copyTweetsBetweenDatabases(rumor_id);
		
	}
	


	public void precomputeStates() throws Exception
	{
		System.out.println("Retrieving tweet timeline from database...");
		if (firstNTweets)
		{
			System.out.println("Getting top " + N + " tweets from database...");

			tweets = readDatabase.get.getTopTweetsFromTimeline(precompute_rumor_id,N);

		}
		else
		{
			System.out.println("Retrieving tweet timeline from database...");

			tweets = readDatabase.get.getCompleteTweetTimeline(precompute_rumor_id);
		}
		
//		correctTweetDatesIfNecessary(tweets);
		System.out.println(tweets.size() + " tweets found.");
		
		System.out.println("Identifying all active users in tweet set");
		HashMap<Long,LinkedList<CompleteTweet>> activeUsers = new HashMap<Long,LinkedList<CompleteTweet>>();
		for (CompleteTweet tweet : tweets)
		{
			if (!activeUsers.containsKey(tweet.getAuthorID())) 
				activeUsers.put(tweet.getAuthorID(), new LinkedList<CompleteTweet>());
			 
			activeUsers.get(tweet.getAuthorID()).add(tweet);		
		}
		System.out.println(activeUsers.size() + " active users found...");
		
		System.out.println("Precomputing state transitions...");
		
		manager = new StateManager(followerMap, activeUsers); //Initialize a new state manager that will
		//keep track the movement of users between states of interaction with the rumor
		
		//Send each tweet into the StateManager, which will figure out the tweet's impact
		for (int i = 0; i < tweets.size(); i++)
		{
			//manager.clearUpdates(); //Definitely don't forget to do this			
			CompleteTweet tweet = tweets.get(i);
			TweetType tweetType = tweet.getType() == 1 ? TweetType.rumor:TweetType.debunk;
			
			manager.ingestTweet(tweet, tweetType); //have the state manager ingest the tweet and calculate its impact
			
		}
		
		manager.generateConservativeNeighborhoodTweets(5);
		
		System.out.println("Done precomputing.");
		System.out.println(manager.getMovementSnapshots().size() + " Sankey state movement updates generated.");
		System.out.println(manager.getAuthorActivities().size() + " author activity summaries generated.");
		System.out.println(manager.getNeighborhoodTweets().size() + " neighborhood tweet records generated.");
		System.out.println(manager.totCount + " nonunique neighborhoods collected.");
		System.out.println(manager.dupCount + " duplications encountered while collecting neighborhood tweets.");
		System.out.println(manager.noFollowerCount + " tweets for which author followers could not be found");
		System.out.println(manager.blankMapCount + " Passive movement maps blank");
		System.out.println(manager.yesTransitionCount + " instances of a tweet causing a valid transition");
		System.out.println(manager.noTransitionCount + " instances of tweets causing invalid transition");
		System.out.println("Writing sankey state results to DB");
		
		
		//Write a snapshot of the tweet's impact to DB
		for (StateMovementSnapshot snapshot : manager.getMovementSnapshots())
		{
			for (StateManager.UserType type : StateManager.UserType.values())
			{			
				writeDatabase.insert.writeSankeyMovementMap(snapshot.getTweet().getTweetID(), snapshot.getTweet().getDate(), snapshot.getMoveMap().get(type), precompute_rumor_id, type);
			}
		}
		
		System.out.println("Writing neighborhood tweets to write DB");
		writeDatabase.insert.writeNeighborhoodTweetBatches(manager.getNeighborhoodTweets(), precompute_rumor_id);
		System.out.println("Writing author activities to write DB");
		writeDatabase.insert.writeAuthorActivityBatch(manager.getAuthorActivities(), precompute_rumor_id);
		if (!readDatabase.sameDB(writeDatabase))
		{
			System.out.println("Write DB is different from read DB, so write tweets and author infos also");
			System.out.println("Writing tweets to DB...");
			writeDatabase.insert.writeCompleteTweetBatch(tweets);
			System.out.println("Writing basic author information to DB");
			writeDatabase.insert.writeBasicAuthorInformationBatch(authorInfos, precompute_rumor_id);
		}
		else
		{
			System.out.println("Write DB is same as read DB, so not writing tweets or basic author info");
		}
		
		System.out.println("Done writing. Bye.");
	}

	
	/**
	 * This goes through and corrects tweets' formats from Cheng's date format if necessary
	 * @param tweets2
	 */
	private SimpleDateFormat chengDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

	private void correctTweetDatesIfNecessary(ArrayList<CompleteTweet> tweets2) throws Exception
	{
		System.out.println("Correcting tweet dates if necessary");
		Date date = null;
		try
		{
			date = chengDateFormat.parse(Long.toString(tweets2.get(0).getDate()));
		}
		catch (Exception ex)
		{
			System.err.println("Not converting from Cheng time to epoch time, as incoming tweet dates don't seem to be formatted her way");
			return;
		}
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    int year = cal.get(Calendar.YEAR);
	    if (year > 1900 && year < 2100)
	    {
	    	System.out.println("Tweet dates apparently need to be corrected from Cheng time to epoch time. Doing so now.");
	    	for (CompleteTweet tweet:tweets2)
	    	{
	    		//DB expects seconds rather than milliseconds, hence the division by 1000 here.
	    		tweet.setDate(chengDateFormat.parse(Long.toString(tweet.getDate())).getTime()/1000);
	    	}
	    }
		
	}
		

	
}
