package precompute;

public class StateUpdateME {
	public long tweet_id;
	public int [] transitions;
/*	public static final String [] transitionNames = {"Start_RumorExp1", "Start_RumorTwt", "Start_DebunkExp1", 
			"Start_DebunkTwt", "RumorExp1_RumorExp2", "RumorExp1_BothExp", 
			"RumorExp1_RumorTwt", "RumorExp1_DebunkTwt", "RumorExp2_RumorExp3m", "RumorExp2_BothExp", 
			"RumorExp2_RumorTwt", "RumorExp2_DebunkTwt", "RumorExp3m_BothExp", "RumorExp3m_RumorTwt", 
			"RumorExp3m_DebunkTwt", "DebunkExp1_DebunkExp2", "DebunkExp1_BothExp", "DebunkExp1_RumorTwt",
			"DebunkExp1_DebunkTwt", "DebunkExp2_DebunkExp3m", "DebunkExp2_BothExp", "DebunkExp2_RumorTwt",
			"DebunkExp2_DebunkTwt", "DebunkExp3m_BothExp", "DebunkExp3m_RumorTwt", "DebunkExp3m_DebunkTwt",
			"BothExp_RumorTwt", "BothExp_DebunkTwt", "RumorTwt_BothTwt", "DebunkTwt_BothTwt"};
*/

  	public static final String [] transitionNames = {"Start_RumorExp1", "Start_RumorTwt1", "Start_DebunkExp1", "Start_DebunkTwt1", 
  		"RumorExp1_RumorExp2", "RumorExp1_BothExp", "RumorExp1_RumorTwt1", "RumorExp1_DebunkTwt1", 
  		"RumorExp2_RumorExp3m", "RumorExp2_BothExp", "RumorExp2_RumorTwt1",	"RumorExp2_DebunkTwt1", 
  		"RumorExp3m_BothExp", "RumorExp3m_RumorTwt1", "RumorExp3m_DebunkTwt1", 
  		"DebunkExp1_DebunkExp2", "DebunkExp1_BothExp", "DebunkExp1_RumorTwt1", "DebunkExp1_DebunkTwt1", 
		"DebunkExp2_DebunkExp3m", "DebunkExp2_BothExp", "DebunkExp2_RumorTwt1", "DebunkExp2_DebunkTwt1", 
		"DebunkExp3m_BothExp", "DebunkExp3m_RumorTwt1", "DebunkExp3m_DebunkTwt1", 
		"BothExp_RumorTwt1", "BothExp_DebunkTwt1", 
		"RumorTwt1_RumorTwt2", "RumorTwt1_BothTwt", "RumorTwt2_RumorTwt3m", "RumorTwt2_BothTwt", "RumorTwt3m_BothTwt", 
		"DebunkTwt1_DebunkTwt2", "DebunkTwt1_BothTwt", "DebunkTwt2_DebunkTwt3m", "DebunkTwt2_BothTwt", "DebunkTwt3m_BothTwt"};

 
	public StateUpdateME(){
		transitions = new int[transitionNames.length];
	}
	
	public StateUpdateME(long tweet_id){
		transitions = new int[transitionNames.length];
		this.tweet_id = tweet_id;
	}
	
	public void setTransition(int transition_index, int value){
		transitions[transition_index] = value;
	}

	public long getTweet_id()
	{
		return tweet_id;
	}

	public void setTweet_id(long tweet_id)
	{
		this.tweet_id = tweet_id;
	}
	
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < transitionNames.length; i ++)
		{
			builder.append(transitionNames[i] + " : " + transitions[i] + " | ");
		}
		return builder.toString();
	}
}
