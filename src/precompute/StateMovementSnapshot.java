package precompute;

import java.util.LinkedHashMap;

import entity.CompleteTweet;
import entity.TimeStamp;
import precompute.StateManager.State;
import precompute.StateManager.UserType;

/**
 * Just a holder for a tweet and its impact
 * @author Sam
 *
 */
public class StateMovementSnapshot
{
	private CompleteTweet tweet;
	private LinkedHashMap<UserType, LinkedHashMap<State, LinkedHashMap<State, Integer>>> moveMap;
	public StateMovementSnapshot(CompleteTweet tweet, LinkedHashMap<UserType, LinkedHashMap<State, LinkedHashMap<State, Integer>>> moveMap)
	{
		super();
		this.tweet = tweet;
		this.moveMap = moveMap;
	}
	public CompleteTweet getTweet()
	{
		return tweet;
	}
	public void setTweet(CompleteTweet tweet)
	{
		this.tweet = tweet;
	}
	public LinkedHashMap<UserType, LinkedHashMap<State, LinkedHashMap<State, Integer>>> getMoveMap()
	{
		return moveMap;
	}
	public void setMoveMap(LinkedHashMap<UserType, LinkedHashMap<State, LinkedHashMap<State, Integer>>> moveMap)
	{
		this.moveMap = moveMap;
	}
	

}
