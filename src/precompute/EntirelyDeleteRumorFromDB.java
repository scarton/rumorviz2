package precompute;

import db.Constants;
import db.DBManager;

public class EntirelyDeleteRumorFromDB
{
	public static int rumor_id = 20;
	
	public static void main(String[] args) throws Exception
	{
		DBManager manager = new DBManager(Constants.Server.google);
		
		
		System.out.println("Deleting rows for rumor #" + rumor_id);
		
		System.out.println("Deleting sankey updates");
		manager.delete.deleteSankeyStateUpdates(rumor_id);
		
		System.out.println("Deleting authors");
		manager.delete.deleteAuthors(rumor_id);
		
		System.out.println("Deleting author activities");
		manager.delete.deleteAuthorActivities(rumor_id);
		
		System.out.println("Deleting neighborhood tweets");
		manager.delete.deleteNeighborhoodTweets(rumor_id);
		
		System.out.println("Deleting tweets");
		manager.delete.deleteTweets(rumor_id);
		
		System.out.println("Done");
	
	}
}
