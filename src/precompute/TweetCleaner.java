package precompute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.*;

import db.DBManager;
import entity.TimeStamp;

public class TweetCleaner {

	/**
	 * @param args
	 */
	private DBManager dbm;
	private ArrayList<TimeStamp> TweetHistory;
	private HashMap<String, Object> PrintTrace;
	
	private String [] codes = {"&#39;", "&quot;", "&amp;", "&gt;", "&lt;"};
	private String [] chars = {"'", "\"", "&", ">", "<"};
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TweetCleaner tc = new TweetCleaner();
//		tc.viewHTMLCodesInTweets();
		tc.replaceCodes();
	}

	public void viewHTMLCodesInTweets(){
		Pattern r = Pattern.compile("&.{2,6};");
		PrintTrace = new HashMap<String,Object>();
		Object exist = new Object();
		for(int i = 0; i < TweetHistory.size(); i++){
			String text = TweetHistory.get(i).Tweet;
			int count = 0;
			Matcher mc = r.matcher(text);
			while(mc.find()){
				if(PrintTrace.get(text.substring(mc.start(), mc.end())) == null){
					System.out.print(text.substring(mc.start(), mc.end()) + " ");
					count++;
					PrintTrace.put(text.substring(mc.start(), mc.end()), exist);
				}
			}
			if(count > 0)
				System.out.println();
		}
	}
	
	public void replaceCodes(){
		for(int i = 0; i < TweetHistory.size(); i++){
			String text = TweetHistory.get(i).Tweet;
			long tid = -1; 
			for(int j = 0; j < codes.length; j++){
				if(text.indexOf(codes[j]) != -1){
					text = text.replaceAll(codes[j], chars[j]);
					tid = TweetHistory.get(i).tweet_id;
				}
			}
			if(tid != -1){
				System.out.println(text);
				dbm.update.updateText(tid, text);
			}
		}
	}
	
	public TweetCleaner(){
		dbm = new DBManager();
		TweetHistory = dbm.get.getTweetTimeline(-1, -1, 0);
	}
}
