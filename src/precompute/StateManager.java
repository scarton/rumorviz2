package precompute;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.TreeMap;

import entity.AuthorActivitySummary;
import entity.CompleteTweet;
import entity.NeighborhoodTweet;
import entity.NeighborhoodTweet.Relationship;
import entity.TimeStamp;
import precompute.StateManager.State;
import precompute.StateManager.TweetType;

/**
 * This is a class which is meant to encode a state transition diagram that represents
 * the movement of Twitter users between states of interaction with a tweets that either 
 * propagate or debunk a rumor.
 * 
 * So for example, a Twitter user might start of in the start state, indicating that they
 * have neither been exposed to or propagated the rumor or the debunk. If someone they follow
 * tweets or retweets an instance of the rumor, than that user moves to the "exposed once" state.
 * 
 * The class differentiates between "active" users who at some point tweet or retweet something,
 * and "passive" users who are just exposed to stuff
 * @author Sam
 *
 */
public class StateManager
{
	public enum State //Types of states of interaction with rumor
	{
		start (0, "Start"),			//Not exposed to rumor or debunk, has not tweeted or retweeted either
		rumor_exp_1 (1, "RumorExp1"),	//Exposed to rumor exactly once
		rumor_exp_2 (2, "RumorExp2"),
		rumor_exp_3m (3, "RumorExp3m"),	//Exposed to rumor 3 times or more
		rumor_twt_1 (4,"RumorTwt1"),	//Has retweeted rumor exactly once
		rumor_twt_2 (5,"RumorTwt2"),
		rumor_twt_3m (6,"RumorTwt3m"),
		debunk_exp_1 (7,"DebunkExp1"),	//Exposed to debunk exactly once
		debunk_exp_2 (8,"DebunkExp2"),
		debunk_exp_3m (9,"DebunkExp3m"),	//Exposed to debunk 3 times or more
		debunk_twt_1 (10,"DebunkTwt1"),	//Has retweeted debunk exactly once
		debunk_twt_2 (11,"DebunkTwt2"),
		debunk_twt_3m (12,"DebunkTwt3m"),
		both_exp (13,"BothExp"),		//Has been exposed to both rumor and debunk any number of times
		both_twt (14,"BothTwt");		//Has tweeted or retweeted both rumor and debunk any number of times
		
		public final int index; //This may not be necessary
		public final String column; //MySQL column name that this state corresponds to
		
		State (int index, String column)
		{
			this.index = index;
			this.column = column;
		}
	}
	public enum UserType //Types of users
	{
		passive (0), 
		active (1);
		
		public final int flag; //What these values map to in the DB
		UserType (int flag)
		{
			this.flag = flag;
		}
		
	}
	
	public enum TweetType //Tweet types
	{
		rumor, debunk
	}
	
	public enum Interaction //Ways a user can have interacted with given tweet (either exposed to it or tweeted it)
	{
		exp, twt;
	}
	
	//Map of what type of tweet leads from each state to its neighbors
	public static LinkedHashMap<State,LinkedHashMap<TweetType, LinkedHashMap<Interaction, State>>> transitionMap = enumerateTransitionMap(); 	
	
	//Edge map of state transition diagram
	public static LinkedHashMap<State,LinkedHashSet<State>> possibleTransitions = enumeratePossibleTransitions(); 
	
	private HashMap<Long,State> memberMap; //Records exactly who is in what state. We don't bother storing everyone who is in start state,
											//we just assume anyone not in any other state is in the start state. Updated with each new tweet.	
	
	private LinkedList<StateMovementSnapshot> movementMaps;
	private LinkedHashMap<Long, AuthorActivitySummary> authorActivities;
	private LinkedHashSet<NeighborhoodTweet> neighborhoodTweets;

	//Background information we need to properly manage state transitions. Sent in to the constructor
	private HashMap<Long, HashSet<Long>> followerMap;
	private HashMap<Long, LinkedList<CompleteTweet>> authorTweets;
	public Integer dupCount;
	public Integer totCount;
	public Integer noFollowerCount;
	public Integer blankMapCount;

	public Integer yesTransitionCount;

	public Integer noTransitionCount;
	
	public StateManager(HashMap<Long, HashSet<Long>> followerMap, HashMap<Long, LinkedList<CompleteTweet>> activeUsers)
	{
		this.followerMap = followerMap;
		this.authorTweets = activeUsers;
		
		memberMap = new HashMap<Long,State>();	
		movementMaps = new LinkedList<StateMovementSnapshot>();
		authorActivities = new LinkedHashMap<Long, AuthorActivitySummary>();
		for (Long authorID : activeUsers.keySet())
		{
			authorActivities.put(authorID, new AuthorActivitySummary(authorID));
		}
		neighborhoodTweets = new LinkedHashSet<NeighborhoodTweet>();
		
		dupCount =0;
		totCount = 0;
		noFollowerCount = 0;
		blankMapCount = 0;
		yesTransitionCount = 0;
		noTransitionCount = 0;
	}

	/**
	 * List the allowable transitions and return them as a LinkedHashMap of States to LinkedHashSets of states. Enumerates this by condensing transitionMap
	 * @return
	 */
	private static LinkedHashMap<State,LinkedHashSet<State>> enumeratePossibleTransitions()
	{
		LinkedHashMap<State,LinkedHashSet<State>> transitions = new LinkedHashMap<State,LinkedHashSet<State>>();
		LinkedHashMap<State, LinkedHashMap<TweetType, LinkedHashMap<Interaction, State>>> transitionMap = enumerateTransitionMap();
		
		for (State state : State.values())
		{
			transitions.put(state,new LinkedHashSet<State>());
		}
		
		for (State source : transitionMap.keySet())
		{
			for (TweetType tweet : transitionMap.get(source).keySet())
			{
				for (Interaction relate : transitionMap.get(source).get(tweet).keySet())
				{
					transitions.get(source).add(transitionMap.get(source).get(tweet).get(relate));
				}
			}
		}
		
		return transitions;
	}
	
	public static LinkedHashMap<StateManager.UserType, LinkedHashMap<State,LinkedHashMap<State,Integer>>> newBlankMovementMap()
	{
		LinkedHashMap <StateManager.UserType, LinkedHashMap<State,LinkedHashMap<State,Integer>>>moveMap = new LinkedHashMap<StateManager.UserType, LinkedHashMap<State,LinkedHashMap<State,Integer>>>();
		for (UserType type : UserType.values())
		{	
			moveMap.put(type,new LinkedHashMap<State,LinkedHashMap<State,Integer>>());
			for (State source : possibleTransitions.keySet())
			{
				moveMap.get(type).put(source, new LinkedHashMap<StateManager.State, Integer>());
				for (State dest: possibleTransitions.get(source))
				{
					moveMap.get(type).get(source).put(dest, 0);
				}
			}
		}
		return moveMap;
	}

	public State getCurrentState(Long authorID)
	{
		if (memberMap.containsKey(authorID)) 
			return memberMap.get(authorID);
		else
			return State.start;
	}

	/**
	 * Moves a user from one state to another. Updates counts and updates. 
	 * @param destination
	 * @param userType 
	 */
	public void moveUserToAppropriateState(Long authorID, UserType userType, TweetType tweetType, Interaction interaction,
			LinkedHashMap<StateManager.UserType, LinkedHashMap<State,LinkedHashMap<State,Integer>>> moveMap)
	{
		State source = getCurrentState(authorID);
		
		if (transitionMap.get(source).containsKey(tweetType) && transitionMap.get(source).get(tweetType).containsKey(interaction))
		{
			State destination = transitionMap.get(source).get(tweetType).get(interaction);
			memberMap.put(authorID, destination);
			moveMap.get(userType).get(source).put(destination, moveMap.get(userType).get(source).get(destination)+1);
			
			//We count active folks as passive also. 
			if (userType.equals(UserType.active))
			{
				
				moveMap.get(UserType.passive).get(source).put(destination, moveMap.get(UserType.passive).get(source).get(destination)+1);
			}

			yesTransitionCount++;
		}
		else
		{
			noTransitionCount++;
		}
	}
	
	/**
	 * Enumerates the state transitions in the diagram, as well as what circumstances lead to each transition. Basically a schematic of the diagram. 
	 * @return
	 */
	private static LinkedHashMap<State, LinkedHashMap<TweetType, LinkedHashMap<Interaction, State>>> enumerateTransitionMap()
	{
		LinkedHashMap<State, LinkedHashMap<TweetType, LinkedHashMap<Interaction, State>>> transitionMap = new LinkedHashMap<StateManager.State, LinkedHashMap<TweetType,LinkedHashMap<Interaction,State>>>();
		
		//Initialize inner data structures
		for(State state : State.values())
		{
			transitionMap.put(state, new LinkedHashMap<StateManager.TweetType, LinkedHashMap<Interaction,State>>());
			for (TweetType tweet : TweetType.values())
			{
				transitionMap.get(state).put(tweet, new LinkedHashMap<StateManager.Interaction, StateManager.State>());
			}
		}
		
		//Transitions from the start state
		transitionMap.get(State.start).get(TweetType.rumor).put(Interaction.exp,State.rumor_exp_1);//For example, when in State.start, being exposed (Relate.exp) to a rumor (Tweet.rumor) leads to State.rumor_exp_1
		transitionMap.get(State.start).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.start).get(TweetType.debunk).put(Interaction.exp,State.debunk_exp_1);
		transitionMap.get(State.start).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		
		//transitionMap from a state of rumor exposure or tweeting
		transitionMap.get(State.rumor_exp_1).get(TweetType.rumor).put(Interaction.exp,State.rumor_exp_2);
		transitionMap.get(State.rumor_exp_1).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.rumor_exp_1).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		transitionMap.get(State.rumor_exp_1).get(TweetType.debunk).put(Interaction.exp,State.both_exp);
		
		transitionMap.get(State.rumor_exp_2).get(TweetType.rumor).put(Interaction.exp,State.rumor_exp_3m);
		transitionMap.get(State.rumor_exp_2).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.rumor_exp_2).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		transitionMap.get(State.rumor_exp_2).get(TweetType.debunk).put(Interaction.exp,State.both_exp);
		
		//transitionMap.get(State.rumor_exp_3m).get(TweetType.rumor).put(Interaction.exp,State.rumor_exp_3m);
		transitionMap.get(State.rumor_exp_3m).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.rumor_exp_3m).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		transitionMap.get(State.rumor_exp_3m).get(TweetType.debunk).put(Interaction.exp,State.both_exp);

		transitionMap.get(State.rumor_twt_1).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_2);
		transitionMap.get(State.rumor_twt_1).get(TweetType.debunk).put(Interaction.twt,State.both_twt);
		
		transitionMap.get(State.rumor_twt_2).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_3m);
		transitionMap.get(State.rumor_twt_2).get(TweetType.debunk).put(Interaction.twt,State.both_twt);

		//transitionMap.get(State.rumor_twt_3m).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_3m);
		transitionMap.get(State.rumor_twt_3m).get(TweetType.debunk).put(Interaction.twt,State.both_twt);	
		
		//transitionMap from a state of debunk exposure or tweeting
		transitionMap.get(State.debunk_exp_1).get(TweetType.debunk).put(Interaction.exp,State.debunk_exp_2);
		transitionMap.get(State.debunk_exp_1).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		transitionMap.get(State.debunk_exp_1).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.debunk_exp_1).get(TweetType.rumor).put(Interaction.exp,State.both_exp);
		
		transitionMap.get(State.debunk_exp_2).get(TweetType.debunk).put(Interaction.exp,State.debunk_exp_3m);
		transitionMap.get(State.debunk_exp_2).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		transitionMap.get(State.debunk_exp_2).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.debunk_exp_2).get(TweetType.rumor).put(Interaction.exp,State.both_exp);
		
		//transitionMap.get(State.debunk_exp_3m).get(TweetType.debunk).put(Interaction.exp,State.debunk_exp_3m);
		transitionMap.get(State.debunk_exp_3m).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		transitionMap.get(State.debunk_exp_3m).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.debunk_exp_3m).get(TweetType.rumor).put(Interaction.exp,State.both_exp);

		transitionMap.get(State.debunk_twt_1).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_2);
		transitionMap.get(State.debunk_twt_1).get(TweetType.rumor).put(Interaction.twt,State.both_twt);
		
		transitionMap.get(State.debunk_twt_2).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_3m);
		transitionMap.get(State.debunk_twt_2).get(TweetType.rumor).put(Interaction.twt,State.both_twt);

		//transitionMap.get(State.debunk_twt_3m).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_3m);
		transitionMap.get(State.debunk_twt_3m).get(TweetType.rumor).put(Interaction.twt,State.both_twt);
		
		
		transitionMap.get(State.both_exp).get(TweetType.rumor).put(Interaction.twt,State.rumor_twt_1);
		transitionMap.get(State.both_exp).get(TweetType.debunk).put(Interaction.twt,State.debunk_twt_1);
		//transitionMap.get(State.both_exp).get(TweetType.rumor).put(Interaction.exp,State.both_exp);
		//transitionMap.get(State.both_exp).get(TweetType.debunk).put(Interaction.exp,State.both_exp);

		//transitionMap.get(State.both_twt).get(TweetType.rumor).put(Interaction.twt,State.both_twt);
		//transitionMap.get(State.both_twt).get(TweetType.debunk).put(Interaction.twt,State.both_twt);

		
		return transitionMap;
	}



	public LinkedList<StateMovementSnapshot> getMovementSnapshots()
	{
		
		return movementMaps;
	}

	/**
	 * This method takes a tweet, figures out what its impact was, and 
	 * records that impact as a snapshot
	 * @param tweet
	 * @param tweetType
	 */
	public void ingestTweet(CompleteTweet tweet, TweetType tweetType)
	{
		
		//Data structure that records how many people moved between each pair of states as
		//a result of this tweet. Serialized in the sankey_state_updates table
		LinkedHashMap<StateManager.UserType, LinkedHashMap<State,LinkedHashMap<State,Integer>>> moveMap = newBlankMovementMap();
		
		//Manage how the tweet affects the movement of its author
		Long authorID = tweet.getAuthorID();
		if (authorID == null) return;		

		moveUserToAppropriateState(authorID, UserType.active, tweetType, Interaction.twt, moveMap);
				
		//For every other tweet by the same author in the dataset, add a neighborhood relationship to this tweet
		for (CompleteTweet authorTweet : authorTweets.get(authorID))
		{
			if (!authorTweet.equals(tweet))
			{
				addNeighborhoodTweet(new NeighborhoodTweet(tweet.getTweetID(), authorTweet.getTweetID(), authorTweet.getDate(), NeighborhoodTweet.Relationship.author));
			}
		}
		
		//Manage how the tweet affects movement of people exposed to it		
		AuthorActivitySummary.increment(authorActivities.get(authorID).getNumTweets(),tweetType);
		
		
		if (followerMap.containsKey(authorID))
		{
			//For each tweet, look at all the followers of the author of that tweet
			for (Long followerID :  followerMap.get(authorID))
			{
				UserType followerType = authorTweets.containsKey(followerID) ? UserType.active : UserType.passive;
				
				moveUserToAppropriateState(followerID, followerType,tweetType,Interaction.exp, moveMap);
			
				//If the follower is one of our active authors, then add some stuff to neighborhoodTweets for them
				if (authorTweets.containsKey(followerID))
				{
					AuthorActivitySummary.increment(authorActivities.get(followerID).getNumTimesExposed(), tweetType);

					
					for (CompleteTweet followerTweet : authorTweets.get(followerID))
					{
						
						
						
						//This is if I don't mind having each tweet connect to EVERY later tweet by each follower
						if (followerTweet.getDate() > tweet.getDate())
						{
							
							//If tweet was the latest 
							addNeighborhoodTweet(new NeighborhoodTweet(tweet.getTweetID(), followerTweet.getTweetID(), followerTweet.getDate(), NeighborhoodTweet.Relationship.follower));
							addNeighborhoodTweet(new NeighborhoodTweet(followerTweet.getTweetID(), tweet.getTweetID(), tweet.getDate(), NeighborhoodTweet.Relationship.followed));
						}

					}
				}
			}
			
			if (followerMap.get(authorID).isEmpty())
			{
				noFollowerCount++;
			}
		}
		else
		{
			noFollowerCount++;
		}

		if (emptyMovementMap(moveMap))
		{
			blankMapCount++;
		}
		movementMaps.add(new StateMovementSnapshot(tweet,moveMap));
	
	}
	
	/**
	 * This function generates neighborhood relationships between tweets. 
	 * 
	 * For a tweet A and tweet B, it generates a link under the following circumstances
	 * 1) Author of tweet A follows author of tweet B
	 * 2) Tweet B came after tweet A
	 * 3) The author of tweet B did not tweet a tweet of the same type as tweet A, between tweet A and tweet B
	 * 
	 * It is intended to trace influence between tweets.
	 */
	public void generateNeighborhoodTweets()
	{	
		
		System.out.println("Generating neighborhood tweets for " + authorTweets.size() + " distinct authors.");
		CompleteTweet authorTweet = null;
		CompleteTweet followerTweet = null;
		int count = 0;
		for (Long authorID : authorTweets.keySet())
		{
			count++;
			if (count % 1000 == 0) System.out.println("\t"+count + " author neighborhoods generated...");
			LinkedList<CompleteTweet> tweets  = authorTweets.get(authorID);
			if (followerMap.containsKey(authorID))
			{
				for (Long followerID : followerMap.get(authorID))
				{
				
					
					if (authorTweets.containsKey(followerID))
					{
						LinkedList<CompleteTweet> followerTweets = authorTweets.get(followerID);
						
						ListIterator<CompleteTweet> aIt = tweets.listIterator();
						ListIterator<CompleteTweet> fIt = followerTweets.listIterator();
						
						followerTweet = fIt.next(); 

						while (aIt.hasNext())
						{
							authorTweet = aIt.next();
	
							while (fIt.hasNext() && followerTweet.getDate() < authorTweet.getDate())
							{
								followerTweet = fIt.next();
							}
							
							if (followerTweet.getDate() > authorTweet.getDate())
							{
								addNeighborhoodTweet(new NeighborhoodTweet(authorTweet.getTweetID(), followerTweet.getTweetID(), followerTweet.getDate(), NeighborhoodTweet.Relationship.follower));
								addNeighborhoodTweet(new NeighborhoodTweet(followerTweet.getTweetID(), authorTweet.getTweetID(), authorTweet.getDate(), NeighborhoodTweet.Relationship.followed));
							}
							
						}
						
					}
				}
			}
		}
		
		System.out.println("Done generating neighborhood tweets.");
	}

	
	/**
	 * This function generates neighborhood tweets using a fairly conservative set of rules based
	 * on strong assumptions about what tweets are allowed to influence each other
	 * 
	 * Tweet Ta by author A influences tweet Tb by author B iff:
	 * 
	 *   1) Author B follows author A
	 *   2) Tweet Tb was tweeted after tweet Ta, but not more than 24 hours later
	 *   3) Tweet Ta was the last tweet by author A before tweet Tb
	 *   4) Tweet Tb was the first tweet by author B after tweet Ta
	 *   5) Tweet Ta is one of the N most recent tweets by people who author B follows, leading up to tweet Tb
	 *   	where N is passed as a parameter to the function. I'll start by trying N = 10
	 */
	public void generateConservativeNeighborhoodTweets(int N)
	{	
		
		System.out.println("Generating neighborhood tweets for " + authorTweets.size() + " distinct authors.");
		CompleteTweet authorTweet = null;
		CompleteTweet followerTweet = null;
		CompleteTweet currentTweet = null;
		boolean currentAuthor = false;
		CompleteTweet previousTweet = null;
		boolean previousAuthor = false;
		long dayGap = 24*3600*1000;
		
		HashMap<CompleteTweet,PriorityQueue<CompleteTweet>> followerTweetMap = new HashMap<CompleteTweet, PriorityQueue<CompleteTweet>>();
		
		int count = 0;
		//For each author
		for (Long authorID : authorTweets.keySet())
		{
			//System.out.println("Author: " + authorID);
			count++;
			if (count % 1000 == 0) System.out.println("\t"+count + " author neighborhoods generated...");
			LinkedList<CompleteTweet> tweets  = authorTweets.get(authorID);
			if (followerMap.containsKey(authorID))
			{
				//For each follower
				for (Long followerID : followerMap.get(authorID))
				{
				
					//If that follower is an author
					if (authorTweets.containsKey(followerID))
					{					
						
						 authorTweet = null;
						 followerTweet = null;
						 currentTweet = null;
						 currentAuthor = false;
						 previousTweet = null;
						 previousAuthor = false;
						
						//System.out.println("\tFollower: " + followerID); 

						LinkedList<CompleteTweet> followerTweets = authorTweets.get(followerID);
						
						ListIterator<CompleteTweet> aIt = tweets.listIterator();
						ListIterator<CompleteTweet> fIt = followerTweets.listIterator();
						
						while (aIt.hasNext() || fIt.hasNext())
						{
							if (aIt.hasNext() && fIt.hasNext())
							{
								followerTweet = safeNext(fIt,null); 
								authorTweet = safeNext(aIt,null);
		
								//Pull back the one that the further forward
								if (followerTweet.getDate() > authorTweet.getDate())
								{
									followerTweet = fIt.previous();
									currentTweet = authorTweet;
									currentAuthor = true;
								}
								else 
								{
									authorTweet = aIt.previous();
									currentTweet = followerTweet;
									currentAuthor = false;
								}
							}
							else if (aIt.hasNext())
							{
								currentTweet = aIt.next();
								currentAuthor = true;
							}
							else
							{
								currentTweet = fIt.next();
								currentAuthor = false;
							}
							//System.out.println();
							//System.out.println("\t\tPrevious tweet: " +previousTweet);
							//System.out.println("\t\tCurrent tweet: " +currentTweet);
							
							
							
							//if previous tweet was by author and current tweet is by follower, add a link between them as long as they are no more than 24 hours apart
							if (previousTweet != null && !currentAuthor && previousAuthor && (currentTweet.getDate() - previousTweet.getDate() < dayGap))
							{
								//System.out.println("\t\t\tMatch!");
								//System.out.println("\t\t\tGap: " + (currentTweet.getDate() - previousTweet.getDate()) + " | Day gap: " + dayGap);
								//Add this author tweet to a list of tweets that influenced the follower's tweet
								if (!followerTweetMap.containsKey(currentTweet))
									followerTweetMap.put(currentTweet, new PriorityQueue<CompleteTweet>());
								followerTweetMap.get(currentTweet).add(previousTweet);
								//if that list tries to get bigger than N, shave off the oldest tweet
								if (followerTweetMap.get(currentTweet).size() > N)
									followerTweetMap.get(currentTweet).poll();
								
							/*	
								addNeighborhoodTweet(new NeighborhoodTweet(previousTweet.getTweetID(), currentTweet.getTweetID(), currentTweet.getDate(), NeighborhoodTweet.Relationship.follower));
								addNeighborhoodTweet(new NeighborhoodTweet(currentTweet.getTweetID(), previousTweet.getTweetID(), previousTweet.getDate(), NeighborhoodTweet.Relationship.followed));
								*/
							}
							
							previousTweet = currentTweet;
							previousAuthor = currentAuthor;
							
						}
						
					}
				}
			}
		}
		
		//For every follower tweet we identified, add a bidirectional link for each tweet we allowed to influence it
		for (CompleteTweet fTweet : followerTweetMap.keySet())
		{
			//System.out.println("Follower tweet: " + fTweet);
			for (CompleteTweet aTweet : followerTweetMap.get(fTweet))
			{
				//System.out.println("\tAuthor tweet: "  + aTweet);
				addNeighborhoodTweet(new NeighborhoodTweet(aTweet.getTweetID(), fTweet.getTweetID(), fTweet.getDate(), NeighborhoodTweet.Relationship.follower));
				addNeighborhoodTweet(new NeighborhoodTweet(fTweet.getTweetID(), aTweet.getTweetID(), aTweet.getDate(), NeighborhoodTweet.Relationship.followed));
			}
		}
		
		//System.out.println("Done generating neighborhood tweets.");
	}
	
	private <T> T safePrevious(ListIterator<T> it)
	{
		if (it.hasPrevious())
			return it.previous();
		else
		{
			return null;
		}
	}
	
	private <T> T safeNext(ListIterator<T> it, T def)
	{
		if (it.hasNext())
			return it.next();
		else
		{
			return def;
		}
	}
	
	
	private boolean emptyMovementMap(LinkedHashMap<UserType, LinkedHashMap<State, LinkedHashMap<State, Integer>>> moveMap)
	{
		boolean empty = true;
		for (State startState : moveMap.get(UserType.passive).keySet())
		{
			for (State endState : moveMap.get(UserType.passive).get(startState).keySet())
			{
				if (moveMap.get(UserType.passive).get(startState).get(endState) > 0)
					empty = false;
			}
		}
		return empty;
	}

	private void addNeighborhoodTweet(NeighborhoodTweet tweet)
	{
		if (neighborhoodTweets.contains(tweet))
		{
			dupCount++;
			
			//System.out.println("Warning: duplicate tweet found:");
			//System.out.println(tweet);
		}
		totCount++;
		neighborhoodTweets.add(tweet);
		
	}

	public LinkedHashMap<Long, AuthorActivitySummary> getAuthorActivities()
	{
		return authorActivities;
	}

	public LinkedHashSet<NeighborhoodTweet> getNeighborhoodTweets()
	{
		return neighborhoodTweets;
	}

	/**
	 * Test out some of the functionality
	 * @param args
	 */
	public static void main(String[] args)
	{
		ArrayList<CompleteTweet> tList = new ArrayList<CompleteTweet>();
		tList.add(new CompleteTweet(1,"Author tweet 1",1,0,1,false,"author",null,1));		
		tList.add(new CompleteTweet(2,"Follower tweet 1",2,6*3600*1000,1,false,"follower",null,1));
		tList.add(new CompleteTweet(1,"Author tweet 2",3,7*3600*1000,1,false,"author",null,1));		
		tList.add(new CompleteTweet(1,"Author tweet 3",4,8*3600*1000,1,false,"author",null,1));	
		tList.add(new CompleteTweet(2,"Follower tweet 2",5,9*3600*1000,1,false,"follower",null,1));
		tList.add(new CompleteTweet(2,"Follower tweet 3",6,10*3600*1000,1,false,"follower",null,1));
		tList.add(new CompleteTweet(1,"Author tweet 4",7,11*3600*1000,1,false,"author",null,1));	
		tList.add(new CompleteTweet(2,"Follower tweet 4",8,36*3600*1000,1,false,"follower",null,1));
		tList.add(new CompleteTweet(3,"Author 2 tweet 1",9,37*3600*1000,1,false,"author 1",null,1));		
		tList.add(new CompleteTweet(4,"Author 3 tweet 1",10,38*3600*1000,1,false,"author 2",null,1));		
		tList.add(new CompleteTweet(5,"Author 4 tweet 1",11,39*3600*1000,1,false,"author 3",null,1));		
		tList.add(new CompleteTweet(6,"Author 5 tweet 1",12,40*3600*1000,1,false,"author 4",null,1));		
		tList.add(new CompleteTweet(2,"Follower tweet 5",13,41*3600*1000,1,false,"follower",null,1));
		
		/**
		 * 1,2
		 * 4,5
		 * 10,13
		 * 11,13
		 * 12,13
		 */
		HashMap<Long, LinkedList<CompleteTweet>> activeUsers = new HashMap<Long, LinkedList<CompleteTweet>>();
		for (CompleteTweet tweet : tList)
		{
			if (!activeUsers.containsKey(tweet.getAuthorID()))
					activeUsers.put(tweet.getAuthorID(),new LinkedList<CompleteTweet>());
			activeUsers.get(tweet.getAuthorID()).add(tweet);
		}


		HashMap<Long, HashSet<Long>> followerMap = new HashMap<Long, HashSet<Long>>();
		HashSet<Long> follower = new HashSet<Long>();
		follower.add(new Long(2));
		
		followerMap.put(new Long(1), follower);
		followerMap.put(new Long(3), follower);
		followerMap.put(new Long(4), follower);
		followerMap.put(new Long(5), follower);
		followerMap.put(new Long(6), follower);

		
		
		
		StateManager manager = new StateManager(followerMap, activeUsers);
		
		manager.generateConservativeNeighborhoodTweets(3);

		for (NeighborhoodTweet tweet : manager.getNeighborhoodTweets())
		{
			if (tweet.getRelationship() == Relationship.follower) System.out.println(tweet);
		}
		
		
		
	}


}
