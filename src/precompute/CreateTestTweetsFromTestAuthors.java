package precompute;

import java.util.ArrayList;

import db.DBManager;
import entity.Author;

public class CreateTestTweetsFromTestAuthors
{
	private static int rumorID = 50;
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("Creating spoof tweets");
		DBManager db = new DBManager();
		db.create.createTweetsTableIfItDoesNotExist();
		ArrayList<Author> authors = db.get.getAuthorList(rumorID);
		long tweetID =0;
		long date = 946080000*1000;
		long dIncrement = 86400000;
		for (Author author: authors)
		{
			for (int i = 0; i < author.getScreenName().length(); i++)
			{
				char ch = author.getScreenName().charAt(i);
				if (ch == 'r')
				{
					db.insert.insertTweet(author.getID(), "Placeholder tweet text", tweetID, date+dIncrement*tweetID, 1, false, author.getScreenName(), "Placeholder tweet embed", rumorID);
					tweetID++;
				}
				else if (ch == 'd')
				{
					db.insert.insertTweet(author.getID(), "Placeholder tweet text", tweetID, date+dIncrement*tweetID, 2, false, author.getScreenName(), "Placeholder tweet embed", rumorID);
					tweetID++;
				}
				else
				{
					System.out.println("\tWarning: unrecognized character found at position " + i + " of screen name: " + author.getScreenName());
				}
			}
		}
		System.out.println("Done. " + tweetID + " tweets inserted.");
	}
}
