package precompute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import db.DBManager;

public class AuthorInfoCalculator {

	/**
	 * @param args
	 */
	private DBManager dbm = null;
	private HashMap<Long, Integer> spreader_mention_count = null;
	private HashMap<Long, Integer> corrector_mention_count = null;
	private HashMap<Long, HashSet<Long>> FollowerTable = null;
	private HashMap<Long,String> Tweeter = null;
	private HashMap<Long,Integer> Rumor_Count = null;
	private HashMap<Long,Integer> Correction_Count = null;
	private int rumor_type = -1;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AuthorInfoCalculator aic = new AuthorInfoCalculator(1);
		aic.calculateTR();
		aic.calculateTD();
		aic.calculateRE();
		aic.calculateDE();

		aic.updateResults();
	}
	
	public AuthorInfoCalculator(int rumor_type){
		// load following relationship
		this.rumor_type = rumor_type;  
		dbm = new DBManager();
		FollowerTable = dbm.get.selectAllFollower(rumor_type);
		Tweeter = dbm.TweeterIDToName(rumor_type);
		Rumor_Count = new HashMap<Long,Integer>();
		Correction_Count = new HashMap<Long,Integer>();
	}
	
	private void updateResults() {
		// TODO Auto-generated method stub
		int re_count, de_count, tr_count, td_count;
		
		Iterator<Long> it = Tweeter.keySet().iterator();
		
		while(it.hasNext()){
			long id = it.next();
			if(Rumor_Count.get(id) == null){
				re_count = 0;
			} else {
				re_count = Rumor_Count.get(id);				
			}
			
			if(Correction_Count.get(id) == null){
				de_count = 0;
			} else {
				de_count = Correction_Count.get(id);				
			}
			String name;
			if(rumor_type == 0){
				name = Tweeter.get(id);
			} else {
				name = Long.toString(id);
			}

			if(spreader_mention_count.get(name) == null){
				tr_count = 0;
			} else {
				tr_count = spreader_mention_count.get(name);
//				System.out.println("not once? tr - " + tr_count);
			}
			if(corrector_mention_count.get(name) == null){
				td_count = 0;
			} else {
				td_count = corrector_mention_count.get(name);
//				System.out.println("not once? td - " + td_count);
			}
			
			dbm.update.updateTweeterInfo(rumor_type, Tweeter.get(id), re_count, de_count, tr_count, td_count);
		}
	}

	
	public void calculateRE(){
		//  update tweeters who follow spreaders
		Iterator<Long> it = spreader_mention_count.keySet().iterator();
		Long id;
		int mention_count = 0;
		
		while(it.hasNext()){
			id = it.next();
			mention_count = spreader_mention_count.get(id);
			System.out.println("loading spreader " + id);
			ArrayList<Long> followers = new ArrayList<Long>(FollowerTable.get(id));
			
			if(followers == null){
				continue;
			}
			for(int j = 0; j < followers.size(); j++){
				
				if(rumor_type == 0){
					if(Tweeter.get(followers.get(j)) == null){
						continue;
					}
				} else {
					if(FollowerTable.get(followers.get(j).toString()) == null){
						continue;
					}
				}
				
				if(Rumor_Count.get(followers.get(j)) == null){
					Rumor_Count.put(followers.get(j), new Integer(mention_count));
				} else {
					Integer count = Rumor_Count.get(followers.get(j));
					Rumor_Count.put(followers.get(j), new Integer(count+mention_count));
				}
			}
		}
	}
	
	public void calculateDE(){
		//  update tweeters who follow spreaders
		Iterator<Long> it = corrector_mention_count.keySet().iterator();
		Long id;
		int mention_count = 0;
		
		while(it.hasNext()){
			id = it.next();
			mention_count = corrector_mention_count.get(id);
			System.out.println("loading corrector " + id);
			ArrayList<Long> followers = new ArrayList<Long>(FollowerTable.get(id));
		
			if(followers == null){
				continue;
			}
			for(int j = 0; j < followers.size(); j++){
				
				if(rumor_type == 0){
					if(Tweeter.get(followers.get(j)) == null){
						continue;
					}
				} else {
					if(FollowerTable.get(followers.get(j).toString()) == null){
						continue;
					}
				}
				
				if(Correction_Count.get(followers.get(j)) == null){
					Correction_Count.put(followers.get(j), new Integer(mention_count));
				} else {
					Integer count = Correction_Count.get(followers.get(j));
					Correction_Count.put(followers.get(j), new Integer(count+mention_count));
				}
			}
		}
	}
	
	public void calculateTR(){
		//	select spreaders and count their tweets
		spreader_mention_count = new HashMap<Long,Integer>();
		ArrayList<Long> spreader = dbm.get.selectSpreader(-1, -1, rumor_type);
		
		Long authorID;
		int count = 0;	
		
		for(int i = 0; i < spreader.size(); i++){
			authorID = spreader.get(i);
			
			if(spreader_mention_count.get(authorID) != null){
				count = spreader_mention_count.get(authorID);
				spreader_mention_count.put(authorID, count+1);
			} else {
				spreader_mention_count.put(authorID, 1);
			}
		}
	}
	
	public void calculateTD(){
		//	select debunkers and count their tweets
		corrector_mention_count = new HashMap<Long,Integer>();
		ArrayList<Long> debunker = dbm.get.selectDebunker(-1, -1, rumor_type);
		
		Long line;
		int count = 0;
		
		for(int i = 0; i < debunker.size(); i++){
			line = debunker.get(i);
			
			if(corrector_mention_count.get(line) != null){
				count = corrector_mention_count.get(line);
				corrector_mention_count.put(line, count+1);
			} else {
				corrector_mention_count.put(line, 1);				
			}
		}
	}
}
