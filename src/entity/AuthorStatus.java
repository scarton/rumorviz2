package entity;


public class AuthorStatus
{
	public Long authorID;
	public Integer rumorID;
	public Boolean unknown;
	public Boolean missingFollowers;
	public Boolean missingFollowed;

	public Integer followersFound;
	public Integer followedFound;
	
	public Integer numFollowers;
	public Integer numFollowed;
	public AuthorStatus(Long authorID, Integer rumorID, Boolean unknown, Boolean missingFollowers, Boolean missingFollowed, Integer followersFound,
			Integer followedFound, Integer numFollowers, Integer numFollowed)
	{
		super();
		this.authorID = authorID;
		this.rumorID = rumorID;
		this.unknown = unknown;
		this.missingFollowers = missingFollowers;
		this.missingFollowed = missingFollowed;
		this.followersFound = followersFound;
		this.followedFound = followedFound;
		this.numFollowers = numFollowers;
		this.numFollowed = numFollowed;
	}
	

	

	
	
	
}
