package entity;

public class TimeStamp {

	public long date;
	public String Author;
	public long Author_ID;
	public int type;
	public long tweet_id;
	public String Tweet;

	
	public TimeStamp(Long Author, String authorName, long date, int type, long tweet_id, String tweet) {
		this.Author_ID = Author;
		this.Author = authorName;
		this.date = date;
		this.type = type;
		this.tweet_id = tweet_id;
		this.Tweet = tweet;
	}


	@Override
	public String toString()
	{
		return "TimeStamp [date=" + date + ", Author=" + Author + ", Author_ID=" + Author_ID + ", type=" + type + ", tweet_id=" + tweet_id + ", Tweet=" + Tweet
				+ "]";
	}
	
	
}
