package entity;

import java.util.Date;

public class Rumor
{
	 public Integer id;
	 public String name;
	 public String description;
	 public Integer valid_count;
	 public Integer error_count;
	 public Integer private_count;
	 public Integer too_many_followers_count;
	 public Integer too_many_followed_count;
	 public Integer uncompleted_count;
	 public Date first_lookup_date;
	 public Integer completed_count;
	
	


	public Rumor(Integer id, String name, String description, Integer valid_count, Integer error_count, Integer private_count,
			Integer too_many_followers_count, Integer too_many_followed_count, Integer uncompleted_count, Date first_lookup_date, Integer completed_count)
	{
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.valid_count = valid_count;
		this.error_count = error_count;
		this.private_count = private_count;
		this.too_many_followers_count = too_many_followers_count;
		this.too_many_followed_count = too_many_followed_count;
		this.uncompleted_count = uncompleted_count;
		this.first_lookup_date = first_lookup_date;
		this.completed_count = completed_count;
	}

	public Integer getCompleted_count()
	{
		return completed_count;
	}

	public void setCompleted_count(Integer completed_count)
	{
		this.completed_count = completed_count;
	}

	public Date getFirst_lookup_date()
	{
		return first_lookup_date;
	}

	public void setFirst_lookup_date(Date first_lookup_date)
	{
		this.first_lookup_date = first_lookup_date;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getValid_count()
	{
		return valid_count;
	}

	public void setValid_count(Integer valid_count)
	{
		this.valid_count = valid_count;
	}

	public Integer getError_count()
	{
		return error_count;
	}

	public void setError_count(Integer error_count)
	{
		this.error_count = error_count;
	}

	public Integer getPrivate_count()
	{
		return private_count;
	}

	public void setPrivate_count(Integer private_count)
	{
		this.private_count = private_count;
	}

	public Integer getToo_many_followers_count()
	{
		return too_many_followers_count;
	}

	public void setToo_many_followers_count(Integer too_many_followers_count)
	{
		this.too_many_followers_count = too_many_followers_count;
	}

	public Integer getToo_many_followed_count()
	{
		return too_many_followed_count;
	}

	public void setToo_many_followed_count(Integer too_many_followed_count)
	{
		this.too_many_followed_count = too_many_followed_count;
	}

	public Integer getUncompleted_count()
	{
		return uncompleted_count;
	}

	public void setUncompleted_count(Integer uncompleted_count)
	{
		this.uncompleted_count = uncompleted_count;
	}
	 
	 
}
