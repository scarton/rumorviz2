package entity;

public class RumorStatus
{
	public Integer rumorID;
	public Long date;
	public Boolean fullyLabeled;
	public Boolean allAuthorsFound;
	public Boolean precomputed;
	public Integer numAuthors;
	public Integer foundAuthors;
	
	public RumorStatus(Integer rumorID, Long date, Boolean fullyLabeled, Boolean allAuthorsFound, Boolean precomputed, Integer numAuthors, Integer foundAuthors)
	{
		super();
		this.rumorID = rumorID;
		this.date = date;
		this.fullyLabeled = fullyLabeled;
		this.allAuthorsFound = allAuthorsFound;
		this.precomputed = precomputed;
		this.numAuthors = numAuthors;
		this.foundAuthors = foundAuthors;
	}
	

}
