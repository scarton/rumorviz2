package entity;

public class ExperimentalCondition
{
	private Integer condition;
	private String hitID;
	private String expID;
	private Integer id;
	
	public ExperimentalCondition(Integer condition, String hitID, String expID)
	{
		super();
		this.condition = condition;
		this.hitID = hitID;
		this.expID = expID;
		this.id = null;
	}
	
	public ExperimentalCondition(Integer condition, String hitID, String expID,Integer id)
	{
		super();
		this.condition = condition;
		this.hitID = hitID;
		this.expID = expID;
		this.id = id;
	}
	
	public Integer getCondition()
	{
		return condition;
	}
	public void setCondition(Integer condition)
	{
		this.condition = condition;
	}
	public String getHitID()
	{
		return hitID;
	}
	public void setHitID(String hitID)
	{
		this.hitID = hitID;
	}
	public String getExpID()
	{
		return expID;
	}
	public void setExpID(String expID)
	{
		this.expID = expID;
	}

	
	
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return "ExperimentalCondition [condition=" + condition + ", hitID=" + hitID + ", expID=" + expID + "]";
	}

	public Object toCSVRow(String string)
	{
		return hitID + "," + expID + ","+condition+",\""+string+"\"";
	}

	
	
}
