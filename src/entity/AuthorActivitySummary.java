package entity;

import java.util.LinkedHashMap;

import precompute.StateManager;
import precompute.StateManager.TweetType;

public class AuthorActivitySummary
{
	private Long authorID;
	private LinkedHashMap<StateManager.TweetType, Integer> numTweets;
	private LinkedHashMap<StateManager.TweetType, Integer> numTimesExposed;

	
	
	
	public AuthorActivitySummary(Long authorID)
	{
		this.authorID = authorID;
		numTweets = newEmptyCountMap();
		numTimesExposed = newEmptyCountMap();
/*		numFollowersWhoTweeted = newEmptyCountMap();
		numFollowedWhoWereExposed = newEmptyCountMap();*/
	}



	private static LinkedHashMap<TweetType, Integer> newEmptyCountMap()
	{
		LinkedHashMap<StateManager.TweetType, Integer> countMap = new LinkedHashMap<StateManager.TweetType, Integer>();
		for (TweetType type: TweetType.values())
		{
			countMap.put(type, 0);
		}
		return countMap;
	}


	public static void increment(LinkedHashMap<StateManager.TweetType, Integer> countMap, TweetType type)
	{
		countMap.put(type,countMap.get(type)+1);
	}

	public Long getAuthorID()
	{
		return authorID;
	}



	public void setAuthorID(Long authorID)
	{
		this.authorID = authorID;
	}



	public LinkedHashMap<StateManager.TweetType, Integer> getNumTweets()
	{
		return numTweets;
	}



	public void setNumTweets(LinkedHashMap<StateManager.TweetType, Integer> numTweets)
	{
		this.numTweets = numTweets;
	}



	public LinkedHashMap<StateManager.TweetType, Integer> getNumTimesExposed()
	{
		return numTimesExposed;
	}



	public void setNumTimesExposed(LinkedHashMap<StateManager.TweetType, Integer> numTimesExposed)
	{
		this.numTimesExposed = numTimesExposed;
	}


	public String toString()
	{
		String rString = this.authorID.toString() +"\n" ;
		
		for (StateManager.TweetType type : numTweets.keySet())
		{
			rString += "\t"+numTweets.get(type) +" " + type.toString() + " tweets\n";
		}
		for (StateManager.TweetType type : numTimesExposed.keySet())
		{
			rString += "\t"+numTimesExposed.get(type) +" " + type.toString() + " exposed\n";
		}
		
		return rString;
		
	}
	

}
