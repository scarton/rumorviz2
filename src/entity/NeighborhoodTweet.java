package entity;

import entity.NeighborhoodTweet.Relationship;

public class NeighborhoodTweet
{
	private Long originalTweetID;
	private Long newTweetID;
	private Long date;
	public static enum Relationship{author, follower, followed};
	private Relationship relationship;
	
	public NeighborhoodTweet(Long originalTweetID, Long newTweetID, Long date, Relationship relationship)
	{
		super();
		this.originalTweetID = originalTweetID;
		this.newTweetID = newTweetID;
		this.date = date;
		this.relationship = relationship;
	}

	public Long getOriginalTweetID()
	{
		return originalTweetID;
	}

	public Long getNewTweetID()
	{
		return newTweetID;
	}

	public Long getDate()
	{
		return date;
	}

	public Relationship getRelationship()
	{
		return relationship;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((newTweetID == null) ? 0 : newTweetID.hashCode());
		result = prime * result + ((originalTweetID == null) ? 0 : originalTweetID.hashCode());
		result = prime * result + ((relationship == null) ? 0 : relationship.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NeighborhoodTweet other = (NeighborhoodTweet) obj;
		if (newTweetID == null)
		{
			if (other.newTweetID != null)
				return false;
		}
		else if (!newTweetID.equals(other.newTweetID))
			return false;
		if (originalTweetID == null)
		{
			if (other.originalTweetID != null)
				return false;
		}
		else if (!originalTweetID.equals(other.originalTweetID))
			return false;
		if (relationship != other.relationship)
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "NeighborhoodTweet [originalTweetID=" + originalTweetID + ", newTweetID=" + newTweetID + ", date=" + date + ", relationship=" + relationship
				+ "]";
	}

	public static int relationshipToInt(Relationship rel)
	{
		if (rel.equals(Relationship.author))
			return 0;
		else if (rel.equals(Relationship.follower))
			return 1;
		else
			return 2;
	}

	public static Relationship intToRelationship(int num)
	{
		if (num == 0)
			return Relationship.author;
		else if (num == 1)
			return Relationship.follower;
		else
			return Relationship.followed;
	}
}
