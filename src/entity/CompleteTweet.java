package entity;

/**
 * This class is an alternative to TimeStamp. It loads every piece of available
 * information from the tweet table where TimeStamp only holds some fields. 
 * 
 * This one gets used for precomputation in case we want to rewrite the tweets
 * elsewhere than the DB we read them from
 * @author Sam
 *
 */
public class CompleteTweet implements Comparable
{
	private long authorID;
	private String text;
	private long tweetID;
	private long date;
	private int type;
	private boolean retweet;
	private String authorName;
	private String embed; //HTML representation of the tweet for presenting it in Twitter style
		//when the app goes live these should be generated dynamically using an Twitter key,
		//but for development purposes we just collect them when we collect the data
	private int rumorID;
	
	public CompleteTweet(long authorID, String text, long tweetID, long date, int type, boolean retweet, String authorName, String embed, int rumorID)
	{
		super();
		this.authorID = authorID;
		this.text = text;
		this.tweetID = tweetID;
		this.date = date;
		this.type = type;
		this.retweet = retweet;
		this.authorName = authorName;
		this.embed = embed;
		this.rumorID = rumorID;
	}
	public long getAuthorID()
	{
		return authorID;
	}
	public void setAuthorID(long authorID)
	{
		this.authorID = authorID;
	}
	public String getText()
	{
		return text;
	}
	public void setText(String text)
	{
		this.text = text;
	}
	public long getTweetID()
	{
		return tweetID;
	}
	public void setTweetID(long tweetID)
	{
		this.tweetID = tweetID;
	}
	public long getDate()
	{
		return date;
	}
	public void setDate(long date)
	{
		this.date = date;
	}
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public boolean isRetweet()
	{
		return retweet;
	}
	public void setRetweet(boolean retweet)
	{
		this.retweet = retweet;
	}
	public String getAuthorName()
	{
		return authorName;
	}
	public void setAuthorName(String authorName)
	{
		this.authorName = authorName;
	}
	public String getEmbed()
	{
		return embed;
	}
	public void setEmbed(String embed)
	{
		this.embed = embed;
	}
	public int getRumorID()
	{
		return rumorID;
	}
	public void setRumorID(int rumorID)
	{
		this.rumorID = rumorID;
	}
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (tweetID ^ (tweetID >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompleteTweet other = (CompleteTweet) obj;
		if (tweetID != other.tweetID)
			return false;
		return true;
	}
	@Override
	//Comparison between tweets is based on their dates
	public int compareTo(Object arg0)
	{
		return new Long(this.date).compareTo(new Long(((CompleteTweet) arg0).getDate()));
		//return 0;
	}
	@Override
	public String toString()
	{
		return "CompleteTweet [authorID=" + authorID + ", text=" + text + ", tweetID=" + tweetID + ", date=" + date + ", type=" + type + ", authorName="
				+ authorName + ", rumorID=" + rumorID + "]";
	}

	
	
}
