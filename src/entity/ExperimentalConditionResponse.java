package entity;
/**
 * This is a wrapper around an ExperimentalCondition object (or lack thereof)
 * that gets requested by fetch_data.jsp
 * @author Sam
 *
 */
public class ExperimentalConditionResponse
{
	public enum Error{
		bad_hit_id("Invalid HIT id. Please visit this page from an Amazon Mechanical Turk HIT"),
		none_found("Error: Either your IP address could not be linked to a previous instance, or there are no free instances remaining.<br>"
				+ "If you think this message is in error, please email scarton[at]umich[dot]edu");

		public String message;
		
		Error(String msg)
		{
			this.message = msg;
		}
	}
	
	private ExperimentalCondition condition;
	private Error error;
	public ExperimentalConditionResponse(ExperimentalCondition condition, Error error)
	{
		super();
		this.condition = condition;
		this.error = error;
	}
	public ExperimentalCondition getCondition()
	{
		return condition;
	}
	public void setCondition(ExperimentalCondition condition)
	{
		this.condition = condition;
	}
	public Error getError()
	{
		return error;
	}
	public void setError(Error error)
	{
		this.error = error;
	}
	
	
}
