package entity;

import java.util.HashMap;

import twitter4j.User;

public class Author
{
	private Long ID;
	private String screenName;
	private Integer numFollowers;
	private Integer numFollowed;
	private String description;
	private Boolean completed;
	private Long followersCursor;
	private Long followedCursor;
	private Boolean isPrivate; //if this person's profile is protected
	private HashMap<String,Object> other;
	private Boolean error; //if we got an error when trying to retrieve info for this person
	private Boolean tooManyFollowers; //flags indicating whether this person has too many links for us to look up in any reasonable time
	private Boolean tooManyFollowed; 
	private Boolean valid; //Catch-all for whether we should use this author in precomputation.  
	
/*	private long[] followerIDs;
	private long[] followedIDs;*/
	

	public Author(Long iD, String screenName, Integer numFollowers, Integer numFollowed, String description)
	{
		super();
		ID = iD;
		this.screenName = screenName;
		this.numFollowers = numFollowers;
		this.numFollowed = numFollowed;
		this.description = description;
		this.completed = null;
		this.followersCursor = null;
		this.followedCursor = null;
		this.isPrivate = null;
		this.other = new HashMap<String, Object>();
		this.error = false;
		this.tooManyFollowers = false;
		this.tooManyFollowed = false;
		this.valid = true;
		
	}
	
	
	
	public Author(Long iD, String screenName, Integer numFollowers, Integer numFollowed, String description,
			Boolean completed, Long followersCursor, Long followedCursor,Boolean isPrivate, 
			Boolean error,Boolean tooManyFollowers, Boolean tooManyFollowed, Boolean valid)
	{
		super();
		ID = iD;
		this.screenName = screenName;
		this.numFollowers = numFollowers;
		this.numFollowed = numFollowed;
		this.description = description;
		this.completed = completed;
		this.followersCursor = followersCursor;
		this.followedCursor = followedCursor;
		this.other = new HashMap<String, Object>();
		this.isPrivate = isPrivate;
		this.error = error;
		this.tooManyFollowers = tooManyFollowers;
		this.tooManyFollowed = tooManyFollowed;
		this.valid = valid;
	}

	public Boolean getValid()
	{
		return valid;
	}



	public void setValid(Boolean valid)
	{
		this.valid = valid;
	}



	public Boolean getTooManyFollowers()
	{
		return tooManyFollowers;
	}



	public void setTooManyFollowers(Boolean tooManyFollowers)
	{
		this.tooManyFollowers = tooManyFollowers;
	}



	public Boolean getTooManyFollowed()
	{
		return tooManyFollowed;
	}



	public void setTooManyFollowed(Boolean tooManyFollowed)
	{
		this.tooManyFollowed = tooManyFollowed;
	}



	public HashMap<String, Object> getOther()
	{
		return other;
	}



	public void setOther(HashMap<String, Object> other)
	{
		this.other = other;
	}



	public Long getFollowedCursor()
	{
		return followedCursor;
	}



	public void setFollowedCursor(Long followedCursor)
	{
		this.followedCursor = followedCursor;
	}



	public Boolean getCompleted()
	{
		return completed;
	}



	public void setCompleted(Boolean completed)
	{
		this.completed = completed;
	}



	public Long getFollowersCursor()
	{
		return followersCursor;
	}



	public void setFollowersCursor(Long followersCursor)
	{
		this.followersCursor = followersCursor;
	}



	public Long getID()
	{
		return ID;
	}
	public void setID(Long iD)
	{
		ID = iD;
	}
	public String getScreenName()
	{
		return screenName;
	}
	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}
	public Integer getNumFollowers()
	{
		return numFollowers;
	}
	public void setNumFollowers(Integer numFollowers)
	{
		this.numFollowers = numFollowers;
	}
	public Integer getNumFollowed()
	{
		return numFollowed;
	}
	public void setNumFollowed(Integer numFollowed)
	{
		this.numFollowed = numFollowed;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}



	public Boolean getIsPrivate()
	{
		return isPrivate;
	}



	public void setIsPrivate(Boolean isPrivate)
	{
		this.isPrivate = isPrivate;
	}

	public Boolean getError()
	{
		return error;
	}



	public void setError(Boolean error)
	{
		this.error = error;
	}

}
