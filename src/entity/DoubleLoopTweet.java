package entity;

/**
 * The double loop outputs data that looks like the following:

1       324173951133687808      Why would someone start a rumor about an 8 year old girl dying just to connect it to the shooting when an actual 8 year old boy died #boston
1       324174446808154112      RT @Ryan_Litton: Why would someone start a rumor about an 8 year old girl dying just to connect it to the shooting when an actual 8 year ...
1       324209178992328705      RT @Ryan_Litton: Why would someone start a rumor about an 8 year old girl dying just to connect it to the shooting when an actual 8 year ...
1       324181177655230464      RT @Ryan_Litton: Why would someone start a rumor about an 8 year old girl dying just to connect it to the shooting when an actual 8 year ...
1       323947536647876609      I just saw someone post a picture of an 8 year old girl who "died" at the Boston marathon, it was a 8 year old boy that died. Not girl.

 * This class is meant to encapsulate one line of this
 * @author Sam
 *
 */
public class DoubleLoopTweet
{
	private Integer classification;
	private Long ID;
	private String text;
	
	private Long authorID; //This information isn't in the file that is outputted by the Double Loop but we can find it out from the raw data file
	private Long time;
	
	public DoubleLoopTweet(Integer classification, Long iD, String text)
	{
		this.classification = classification;
		ID = iD;
		this.text = text;
		this.authorID = null;
		this.time = null;
	}
	
	public DoubleLoopTweet(Integer classification, Long iD, String text, Long time)
	{
		this.classification = classification;
		ID = iD;
		this.text = text;
		this.authorID = null;
		this.time = time;
	}
	
	public Integer getClassification()
	{
		return classification;
	}
	public void setClassification(Integer classification)
	{
		this.classification = classification;
	}
	public Long getID()
	{
		return ID;
	}
	public void setID(Long iD)
	{
		ID = iD;
	}
	public String getText()
	{
		return text;
	}
	public void setText(String text)
	{
		this.text = text;
	}
	public static DoubleLoopTweet parseDLT(String line)
	{
		String[] parts = line.split("\t");
		return new DoubleLoopTweet(Integer.parseInt(parts[0]),Long.parseLong(parts[1]),parts[2]);
	}
	public Long getAuthorID()
	{
		return authorID;
	}
	public void setAuthorID(Long authorID)
	{
		this.authorID = authorID;
	}

	public Long getTime()
	{
		return time;
	}

	public void setTime(Long time)
	{
		this.time = time;
	}
	
	
}
