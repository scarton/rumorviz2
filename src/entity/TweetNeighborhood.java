package entity;

import java.util.HashMap;
import java.util.LinkedList;

import entity.NeighborhoodTweet.Relationship;

/**
 * For a given author, this class represents other tweets by that author,
 * as well as tweets by their followers and authors that they follow.
 * @author Sam
 *
 */
public class TweetNeighborhood {

	public Long authorID;
	private HashMap<Relationship,LinkedList<Long>> neighborhood;
		
	public TweetNeighborhood(Long author_id)
	{
		authorID = author_id;
		
		neighborhood = new HashMap<NeighborhoodTweet.Relationship, LinkedList<Long>>();
		for (Relationship relationship : NeighborhoodTweet.Relationship.values())
		{
			neighborhood.put(relationship,new LinkedList<Long>());
		}
	}

	public boolean containsKey(Object arg0)
	{
		return neighborhood.containsKey(arg0);
	}

	public LinkedList<Long> get(Object arg0)
	{
		return neighborhood.get(arg0);
	}

	public LinkedList<Long> put(Relationship arg0, LinkedList<Long> arg1)
	{
		return neighborhood.put(arg0, arg1);
	}

	public int size()
	{
		return neighborhood.size();
	}
	
	public String getString(Object arg0)
	{
		LinkedList<Long> ids = neighborhood.get(arg0);
		boolean first= true;
		StringBuilder builder = new StringBuilder();
		
		for (Long id : ids)
		{
			if (first)
				first = false;
			else
				builder.append(",");
			
			builder.append(id);
		}
		return builder.toString();
	}

}
