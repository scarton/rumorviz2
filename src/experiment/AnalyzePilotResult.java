package experiment;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;


import db.Constants;
import db.DBManager;
import entity.ExperimentalCondition;

/**
 * Analyze result of pilot study
 * @author Sam
 * 
 * args[0]: path of data file downloaded from amazon
 * args[1]: comma-delimited answers file where each line is:
 * 	[question name], [correct answer]
 * 
 * It outputs:
 * 1) Summary information
 * 2) A file with experimental conditions of the form:
 * [hit_id],[trial_id],[condition_id],[condition_name],[ip address]
 * 
 * 3) A file with a row for each worker with:
 * [workerId],[condition_id],[trial_id],[condition_type],[individual_question_score],[avg_individual_time],[audience_score],[average_audience_time],[total_score],
 * 		[vert. monitor resolution],[horiz monitor resolution],[window status],,,[q1_correct],[q2_correct],...
 */
public class AnalyzePilotResult
{
	private static boolean onlyFirstX = false;
	private static int X = 12;
	
	private static boolean onlyBestY = false;
	private static int Y = 12;
	
	private static boolean outputDBTable = false;
	private static String dbTableOutputName = "db_experiment_table.csv";
	private static String individualResultsName = "individual_results.csv";
	private static String questionResultsName = "question_results.csv";
	private  static HashMap<Integer, String> conditionNameMap = enumerateConditionNameMap();
	
	private static String hitID = "3f615fit6fl5z";
	
	private static SimpleDateFormat jsFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
	
	private static String[] oHeaderArray = {"sample_id","worker_id","hit_id","condition_id","condition_name","trial_id",
		"i_score","i_num","i_avg_time","a_score","a_num","a_avg_time","t_score","t_num","t_avg_time","t_time",
		"v_res","h_res","window","browser",
		"q1ic","q2ic","q4ic","q5ic","q6ic","q7ic",
		"q8ac","q9ac","q10ac","q11ac","q12ac","q13ac","q14ac","q15ac","q16ac","q17ac","q18ac"};
	
	private static String[] qHeaderArray = {"question_id","user_id","trial_id","correct","treatment","question_type","elapsed_time"};
	
	public static void main(String[] args) throws Exception
	{
		String dataFileName = args[0];
		String answersFileName = args[1];
		String outputDirectory = args[2];
		
		DBManager manager = new DBManager(Constants.Server.google);
		
		
		//Read experimental conditions
		HashMap<String,ExperimentalCondition> trialIDMap = manager.get.getTrialIDExperimentalConditionMap(hitID);
		System.out.println(trialIDMap);
		
		
		//Read answers into a hashmap connecting question name to correct answer
		HashMap<String,String> answerMap = new HashMap<String, String>();
		HashMap<String,String> questionMap = new HashMap<String, String>();

		CSVParser answersParser = new CSVParser(new FileReader(answersFileName),CSVFormat.DEFAULT);
		List<CSVRecord> answersRecords = answersParser.getRecords();
		answersParser.close();

		System.out.println("Answers:");
		for (CSVRecord record : answersRecords)
		{
			answerMap.put(record.get(0), record.get(1).trim());
			System.out.println(record.get(0) + ": " + record.get(1).trim() + " | " + record.get(2).trim());
			questionMap.put(record.get(0), record.get(2).trim());

		}
		
		
		//Read data
		CSVParser dataParser = new CSVParser(new FileReader(dataFileName),CSVFormat.DEFAULT.withHeader());
		List<CSVRecord> dataRecords = dataParser.getRecords();
		dataParser.close();

		
		//Analyze data
		System.out.println("Headers:");
		Map<String,Integer> headerMap = dataParser.getHeaderMap();
		System.out.println(headerMap);
		System.out.println("Records trial IDs:");
		HashMap<String,Integer> foundTrials = new HashMap<String,Integer>();
		HashMap<String,Integer> foundWorkers = new HashMap<String,Integer>();
		HashMap<Integer,ConditionResult> resultMap = new HashMap<Integer, AnalyzePilotResult.ConditionResult>();
		
		LinkedHashMap<String,Integer> oHeaderMap = createOutputHeaderMap(oHeaderArray);
		
		LinkedHashMap<String,Integer> qHeaderMap = createOutputHeaderMap(oHeaderArray);
		
		System.out.println("Writing individual results to : " + outputDirectory + "/"+individualResultsName);
		BufferedWriter oWriter = new BufferedWriter(new FileWriter(outputDirectory+"/"+individualResultsName));
		CSVPrinter oPrinter = new CSVPrinter(oWriter,CSVFormat.DEFAULT.withHeader(oHeaderArray));
		
		System.out.println("Writing question-by-question results to : " + outputDirectory + "/"+individualResultsName);
		BufferedWriter qWriter = new BufferedWriter(new FileWriter(outputDirectory+"/"+questionResultsName));
		CSVPrinter qPrinter = new CSVPrinter(qWriter,CSVFormat.DEFAULT.withHeader(qHeaderArray));

		int count = 0;
		for (CSVRecord record : dataRecords)
		{
			//New record we'll populate and output
			HashMap<String,String> oRecord = new HashMap<String,String>();
			
			System.out.println("=========================================================");
			
			String trialID = record.get("Answer.trial_id");
			String workerID = record.get("WorkerId");
			count++;
			System.out.println("#"+count);
			System.out.println("trialID: "+trialID);
			System.out.println("workerID: "+workerID);

			boolean error = false;
			
			//Check for various kinds of errors
			if (!trialIDMap.containsKey(trialID))
			{
				System.out.println("ERROR: Invalid trial ID found: " + trialID);
				System.out.println("Subject #"+count);
				System.out.println(record);
				error = true;
			}
			
			if (foundTrials.containsKey(trialID))
			{
				System.out.println("ERROR: Duplicate trial ID found: " + trialID);
				System.out.println("Result "+count + " has same trial ID as result " + foundTrials.get(trialID));
				System.out.println(record);
				error = true;
			}
			else
			{
				foundTrials.put(trialID,count);
			}
			
			if (foundWorkers.containsKey(workerID))
			{
				System.out.println("ERROR: Duplicate worker ID found: " + workerID);
				System.out.println("Result "+count + " has same worker ID as result " + foundWorkers.get(workerID));
				System.out.println(record);
				error = true;
			}
			else
			{
				foundWorkers.put(workerID,count);
			}
			
			if (error)
			{
				System.out.println("Not counting this row because of errors.");
				continue;
			}
			
			ExperimentalCondition condition = trialIDMap.get(trialID);
			String name = conditionNameMap.get(condition.getCondition());
			ConditionResult result;
			if (!resultMap.containsKey(condition.getCondition()))
			{
				result = new ConditionResult(0,name);
				resultMap.put(condition.getCondition(), result);
			}
			else
				result = resultMap.get(condition.getCondition());
						
			
			if (onlyFirstX && result.count >= X) continue;
			
			result.incrementCount();
			
			//Sort the questions by when they were answered so we can figure out how long it took to answer each one
			TreeMap<Long,String> dateMap = new TreeMap<Long, String>();
			for (String header : headerMap.keySet())
			{
				String dateHeader = header.substring(0,header.length()-1)+"time";
				if (header.startsWith("Answer.q") && (header.endsWith("i") || header.endsWith("a")) && !record.get(header).isEmpty() && !record.get(dateHeader).isEmpty())
				{
					String dateString = record.get(dateHeader);
					/*System.out.print(dateString+": ");
					System.out.println(jsDateStringToJavaDate(dateString));*/
					try
					{
						Date date = jsDateStringToJavaDate(dateString);
						dateMap.put(date.getTime(), header);
					}
					catch (Exception ex)
					{
						System.err.println("Erroneous header: " + header);
						System.err.println("Erroneous answer: " + record.get(header));
						System.err.println("Erroneous time header: " + dateHeader);
						System.err.println("Erroneous date string: " + dateString);
						System.err.println("Erroneous record: ");
						for (String header2: headerMap.keySet())
						{
							System.err.println("\t"+header2 + ": " + record.get(header2));
						}

						throw ex;
					}
				}
			}
			
			
			//Count up correct responses and timing information
			int iqCount =0, aqCount =0, tqCount=0;
			double icCount =0, acCount =0, tcCount =0;
			
			int iqtCount = 0, aqtCount =0;
			double iqTime =0, aqTime = 0;
			
			for (String header : headerMap.keySet())
			{
				if (header.startsWith("Answer."))
				{
					String qName = header.substring(7);
					if (answerMap.containsKey(qName))
					{
						// {"question_id","user_id","trial_id","correct","treatment","question_type","elapsed_time"};

						HashMap<String,String> qRecord = new HashMap<String,String>();
						qRecord.put("question_id", qName);
						qRecord.put("user_id",workerID);
						qRecord.put("trial_id",trialID);
						qRecord.put("treatment",conditionNameMap.get(condition.getCondition()));
						
						String dateHeader = header.substring(0,header.length()-1)+"time";

						//If the question was actually answered, try to figure out how long it took to answer.
						if (!record.get(header).isEmpty() && !record.get(dateHeader).isEmpty() )
						{
							String dateString = record.get(dateHeader);
							Date date = jsDateStringToJavaDate(dateString);
	
							if (dateMap.lowerKey(date.getTime()) != null)
							{
								long ms = date.getTime() - dateMap.lowerKey(date.getTime());
								double seconds = ((double)ms)/(1000);
								qRecord.put("elapsed_time",Double.toString(seconds));
								
								if (qName.endsWith("i"))
								{
									iqtCount++;
									iqTime += seconds; //Get time in seconds 
								}
								else
								{
									aqtCount++;
									aqTime += seconds; //Get time in seconds 
	
								}
							}
						}
						
						tqCount++;
						if (answerMap.get(qName).equalsIgnoreCase(record.get(header)))
						{
							result.incrementCCount(qName, 1);
							tcCount++;
							oRecord.put(qName+"c","1");
							qRecord.put("correct","1");
						}
						else
						{
							result.incrementCCount(qName, 0);
							oRecord.put(qName+"c","0");
							qRecord.put("correct","0");

						}
						
						if (qName.endsWith("i"))
						{
							qRecord.put("question_type","individual");

							iqCount++;
							if (answerMap.get(qName).equalsIgnoreCase(record.get(header)))
								icCount++;
						}
						else if (qName.endsWith("a"))
						{
							qRecord.put("question_type","audience");
							aqCount++;
							if (answerMap.get(qName).equalsIgnoreCase(record.get(header)))
								acCount++;
						}
						//output qRecord to question output file
						String[] qRecordArr = new String[qHeaderArray.length];
						for (int i = 0; i < qRecordArr.length; i++)
						{
							qRecordArr[i] = qRecord.get(qHeaderArray[i]);
						}
						qPrinter.printRecord(qRecordArr);
					}
				}
			}
			
			acCount /= aqCount; //audience question accuracy
			result.acCountSum += acCount;
			icCount /= iqCount; //individual question accuracy
			result.icCountSum += icCount;
			tcCount /= tqCount; //total question accuracy
			result.tcCountSum += tcCount;
			
			int tqtCount = iqtCount + aqtCount; 
			double tqTime = iqTime + aqTime; //Total question answer time
			
			if (aqtCount > 0 && iqtCount > 0) //if there was some time data for this user's answers
			{
				iqTime /= iqtCount;
				result.iTimeSum += iqTime;
				aqTime /= aqtCount;
				result.aTimeSum += aqTime;
				tqTime /= tqtCount;
				result.tTimeSum += tqTime;
			}
			
			int time = Integer.parseInt(record.get("WorkTimeInSeconds"));
			result.timeSum += time;
			
			System.out.println("# questions w/answers: " + tqCount);
			System.out.println("Total accuracy: " + 100*tcCount + "%"); 
			System.out.println("Audience questions accuracy: " + 100*icCount + "%"); 
			System.out.println("Individual questions accuracy: " + 100*acCount + "%"); 
			System.out.println("Work time: " + time/60 + " minutes"); 
			
			
			/*
			 * 	private static String[] headerArray = {"worker_id","hit_id","condition_id","condition_name","trial_id"
				"i_score","i_num","i_avg_time","a_score","a_num","a_avg_time","t_score","t_num","t_avg_time","t_time",
				"v_res","h_res","window","browser",
				"q1ic","q2ic","q4ic","q5ic","q6ic","q7ic",
				"q8ac","q9ac","q10ac","q11ac","q12ac","q13ac","q14ac","q15ac","q16ac","q17ac","q18ac"};
			 */
			oRecord.put("sample_id",condition.getId().toString());
			oRecord.put("worker_id", workerID);
			oRecord.put("hit_id",condition.getHitID());
			oRecord.put("condition_id",condition.getCondition().toString());
			oRecord.put("condition_name", conditionNameMap.get(condition.getCondition()));
			oRecord.put("trial_id", condition.getExpID());
			oRecord.put("i_score", Double.toString(icCount));
			oRecord.put("i_num",Double.toString(iqCount));
			oRecord.put("i_avg_time",Double.toString(iqTime));
			oRecord.put("a_score", Double.toString(acCount));
			oRecord.put("a_num",Double.toString(aqCount));
			oRecord.put("a_avg_time",Double.toString(aqTime));
			oRecord.put("t_score", Double.toString(tcCount));
			oRecord.put("t_num",Double.toString(tqCount));
			oRecord.put("t_avg_time",Double.toString(tqTime));
			oRecord.put("t_time",Integer.toString(time));


			String[] oRecordArr = new String[oHeaderArray.length];
			for (int i = 0; i < oRecordArr.length; i++)
			{
				oRecordArr[i] = oRecord.get(oHeaderArray[i]);
			}
			oPrinter.printRecord(oRecordArr);
			
		}
		oPrinter.close();
		qPrinter.close();
		
		System.out.println("Summary by experimental condition: ");
		for (Integer conditionNum : resultMap.keySet())
		{
			System.out.println(resultMap.get(conditionNum).toString(questionMap));
		}
		
		//Write DB experiment table to a file for posterity, if requested
		if (outputDBTable)
		{
			System.out.println("Writing experimental conditions table to : " + outputDirectory + "/"+dbTableOutputName);
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputDirectory+"/"+dbTableOutputName));
			CSVPrinter printer = new CSVPrinter(writer,CSVFormat.DEFAULT.withHeader("hit_id","trial_id","condition_id","condition_name"));
			
			for (String trialID : trialIDMap.keySet())
			{
				ExperimentalCondition condition = trialIDMap.get(trialID);
				printer.print(condition.getHitID());
				printer.print(condition.getExpID());
				printer.print(condition.getCondition());
				printer.print(conditionNameMap.get(condition.getCondition()));

				printer.println();
			}
			printer.close();
			writer.close();
			
		}
		
		System.out.println("Done!");
	}

	
	private static LinkedHashMap<String, Integer> createOutputHeaderMap(String[] headers)
	{
		LinkedHashMap<String, Integer> rMap = new LinkedHashMap<String, Integer>();
		
		for (int i = 0; i < headers.length; i++)
		{
			rMap.put(headers[i], i);
		}
		
		return rMap;
	}


	private static HashMap<Integer, String> enumerateConditionNameMap()
	{
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(0, "Sankey");
		map.put(1, "Network");
		map.put(2, "List");

		return map;
	}


	protected static class ConditionResult
	{
		public Integer count;
		public String name;
		public Double acCountSum;
		public Double aTimeSum;
		public Double icCountSum;
		public Double iTimeSum;
		public Double tcCountSum;
		public Double tTimeSum;
		public Double timeSum;
		public LinkedHashMap<String,Double> cCountSums;

		
		public ConditionResult(int count, String name)
		{
			this.name = name;
			this.count = count;
			acCountSum = 0.0;
			aTimeSum = 0.0;
			icCountSum = 0.0;
			iTimeSum = 0.0;
			tcCountSum = 0.0;
			tTimeSum = 0.0;
			timeSum = 0.0;
			cCountSums = new LinkedHashMap<String, Double>();
		}
		
		public void incrementCount()
		{
			count++;
		}
		
		public String toString(HashMap<String,String> questionMap)
		{
			String rString = name + "\n\t"+count + " subjects.\n"
					+ "Average audience question accuracy: " + 100*(acCountSum/count)+"%\n"
					+ "Average audience question time: " + (aTimeSum/count)+" seconds\n"
					+ "Average individual question accuracy: " + 100*(icCountSum/count)+"%\n"
					+ "Average individual question time: " + (iTimeSum/count)+" seconds\n"
					+ "Average overall question accuracy: " + 100*(tcCountSum/count)+"%\n"
					+ "Average overall question time: " + (tTimeSum/count)+" seconds\n"

					+ "Average work time (in minutes): " + (timeSum/count/60)+"\n"
					+ "Average question performance for each question:\n";

			for (String qName: cCountSums.keySet())
			{
				rString += "\t"+qName +": "+(100*cCountSums.get(qName)/count) + "%";
				if (questionMap != null && questionMap.containsKey(qName))
				{
					rString += " | " + questionMap.get(qName);
				}
				rString+="\n";
			}
			
			return rString;
		}
		
		public String toString()
		{
			return this.toString(null);
		}
		
		public void incrementCCount(String qName, int num)
		{
			if (!cCountSums.containsKey(qName))
				cCountSums.put(qName, 0.0);
			
			cCountSums.put(qName,cCountSums.get(qName) + num);
		}
	}
	
	/**
	 * Taken from OP's solution at http://stackoverflow.com/questions/12908104/javascript-date-string-gmt-to-java-date
	 * @param jsDateString
	 * @return
	 * @throws ParseException
	 */
	public static Date jsDateStringToJavaDate(String jsDateString) throws ParseException
	{

	    String[] arrStrDateParts = jsDateString.split(" ");
	    SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss", Locale.ENGLISH);
	    sdf.setTimeZone(TimeZone.getTimeZone(arrStrDateParts[5].substring(0,6)+":"+arrStrDateParts[5].substring(6)));
	    return sdf.parse(arrStrDateParts[0]+" "+arrStrDateParts[1]+" "+arrStrDateParts[2]+" "+arrStrDateParts[3]+" "+arrStrDateParts[4]);       
	}
}
