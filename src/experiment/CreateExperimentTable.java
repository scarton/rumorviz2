package experiment;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

import db.Constants;
import db.DBManager;
import entity.ExperimentalCondition;

/**
 * Generates a CSV corresponding to an Amazon Mechanical Turk HIT template, 
 * that has HIT id strings. 
 * Creates a database table with a row for each HIT that has the HIT id, 
 * another alphanumerical code that is displayed on RumorLens for the subject
 * to write as an input, their IP address, and a boolean indicating whether that
 * row has been claimed
 * 
 * @author Sam
 *
 */
public class CreateExperimentTable
{
	
	public static int numSamples = 200; //This should be divisible by the number of conditions
	public static Constants.Server server = Constants.Server.google;
	
	public static boolean one_hit_mode = true;
	
	public static int[] conditions = {0,//Sankey diagram
		1,//Network diagram
		2//User list
		};//Bar chart
	public static void main(String[] args) throws Exception
	{
		System.out.println("Creating (if necessary) and populating experimental conditions table");
		String outputFilePath = args[0];
		ArrayList<ExperimentalCondition> samples = new ArrayList<ExperimentalCondition>();
		DBManager manager = new DBManager(server);
		manager.create.createExperimentTableIfItDoesNotExist();
		SecureRandom random = new SecureRandom();

		String onlyHIT = new BigInteger(64, random).toString(36);
		
		int each = numSamples/conditions.length;
		for (int i = 0; i < conditions.length; i++)
		{
			for (int j = 0; j < each;j++)
			{
				if (!one_hit_mode)
					samples.add(new ExperimentalCondition(conditions[i], new BigInteger(64, random).toString(36), new BigInteger(64, random).toString(36)));
				else
					samples.add(new ExperimentalCondition(conditions[i], onlyHIT, new BigInteger(64, random).toString(36)));

			}
		}
		
		Collections.shuffle(samples);
		
		samples.add(new ExperimentalCondition(0,"sankey_test","sankey_test"));
		samples.add(new ExperimentalCondition(1,"network_test","network_test"));
		samples.add(new ExperimentalCondition(2,"list_test","list_test"));

		
		BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath));
		writer.write("hit_id\n");
		if (!one_hit_mode)
		{
			for (int i =0; i < samples.size()-3; i++)
			{
				ExperimentalCondition condition = samples.get(i);
				System.out.println(condition.getCondition() + " | HID: " +condition.getHitID()+ " | ExpID: " +condition.getExpID());
				writer.write(condition.getHitID());
				writer.newLine();
			}
		}
		else
		{
			writer.write(onlyHIT);
			writer.newLine();
		}
		
		writer.close();
		System.out.println("HIT IDs written to " + outputFilePath);
		manager.insert.writeSampleBatchToExperimenalTable(samples);
		System.out.println("Done. " + samples.size() + " condition rows written to table: " + Constants.U_EXPERIMENT);
	}
}
