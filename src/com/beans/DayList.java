/**
 * 
 */
package com.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author spark
 *
 */


public class DayList  {
	/**
	 * 
	 */
//	private ArrayList<String> daystring;
	private ArrayList<DayTS> daystring;
	public DayList() {
		// TODO Auto-generated constructor stub
		super();
		daystring = new ArrayList<DayTS>();
		
		GregorianCalendar gcal = new GregorianCalendar();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
	    Date start = null, end = null;
		try {
			start = sdf.parse("2013.02.14");
		    end = sdf.parse("2013.02.25");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    gcal.setTime(start);
	    while (gcal.getTime().before(end)) {
	        gcal.add(Calendar.DAY_OF_YEAR, 1);
	        DayTS temp = new DayTS();
	        long millis = gcal.getTimeInMillis();
	        long epochm = millis / 1000;

	        temp.setDate(gcal.getTime().toString());
	        temp.setTimestamp(epochm);
	        daystring.add(temp);
	    }

	}
	
	public ArrayList<DayTS> getDaystring() {
		return daystring;
	}

	public void setDaystring(ArrayList<DayTS> daystring) {
		this.daystring = daystring;
	}

}
