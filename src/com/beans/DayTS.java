package com.beans;

import java.io.Serializable;

public class DayTS implements Serializable {
	private String date;
	private long timestamp;
	
	
	public DayTS() {
		// TODO Auto-generated constructor stub
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public long getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

}
