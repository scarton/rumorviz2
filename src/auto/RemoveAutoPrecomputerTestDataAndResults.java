package auto;

import java.util.ArrayList;

import db.DBManager;

public class RemoveAutoPrecomputerTestDataAndResults
{

	public static void main(String[] args) throws Exception
	{
		System.out.println("Deleting precomputer test data from DB");
		DBManager db = new DBManager();
		
		ArrayList<String> commands = new ArrayList<String>();
		
		//Delete appropriate data from rumorRetri
		commands.add("delete from rumor_retri.rumorRetri where rumorID = 10001");

		
		//Delete appropriate data from rumorRetriStatus
		commands.add("delete from rumor_retri.rumorRetriStatus where rumorID = 10001");
		
		//Delete appropriate data from author
		commands.add("delete from author where rumor_id = 10001");

		
		//Delete appropriate data from follower
		commands.add("delete from follower where rumor_id = 10001");

		
		//Delete appropriate data from followed
		commands.add("delete from followed where rumor_id = 10001");
	
		//Delete appropriate data from rumor
		commands.add("delete from rumor where rumor_id = 10001");
		
		//Delete appropriate data from author_activity
		commands.add("delete from author_activity where rumor_id = 10001");

		//Delete appropriate data from sankey_state_update
		commands.add("delete from sankey_state_update where rumor_id = 10001");

		//Delete appropriate data from tweet ? 
		commands.add("delete from tweet where rumor_id = 10001");

		//Delete appropriate data from neighborhood_tweets ? 
		commands.add("delete from neighborhood_tweets where rumor_id = 10001");

		db.executeQueries(commands, true);
		System.out.println("done");
		db.close();

	}

}
