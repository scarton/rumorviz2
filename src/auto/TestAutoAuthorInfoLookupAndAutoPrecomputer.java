package auto;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;
import db.AutoConstants;
import db.Constants;
import db.DBManager;


/**
 * Script to test the AutoAuthorInfoLookup class. As of 3-18-2016, this seems to indicate that all is working correctly.
 * @author Sam
 *
 */
public class TestAutoAuthorInfoLookupAndAutoPrecomputer
{
	
	

	//Just test preprocessor with existing author data
/*	public static boolean createAuthorTempStuff= false;
	public static boolean validateAuthorStuff = false;
	public static boolean validatePreprocessingStuff = true;
	public static boolean deletePreprocessingTempStuff = true;
	public static boolean deleteAuthorTempStuff = false;*/
	

	//Just clear preprocessor stuff
/*	public static boolean createAuthorTempStuff= false;
	public static boolean validateAuthorStuff = false;
	public static boolean validatePreprocessingStuff = false;
	public static boolean deletePreprocessingTempStuff = true;
	public static boolean deleteAuthorTempStuff = false;*/
	
	//Just run author lookup without deleting anything
	public static boolean createAuthorTempStuff= true;
	public static boolean validateAuthorStuff = true;
	public static boolean validatePreprocessingStuff = false;
	public static boolean deletePreprocessingTempStuff = false;
	public static boolean deleteAuthorTempStuff = false;
	
	//Just delete everything
/*	public static boolean createAuthorTempStuff= false;
	public static boolean validateAuthorStuff = false;
	public static boolean validatePreprocessingStuff = false;
	public static boolean deletePreprocessingTempStuff = true;
	public static boolean deleteAuthorTempStuff = true;*/
	
	//Run & test everything
/*	public static boolean createAuthorTempStuff= true;
	public static boolean validateAuthorStuff = true;
	public static boolean validatePreprocessingStuff = true;
	public static boolean deletePreprocessingTempStuff = true;
	public static boolean deleteAuthorTempStuff = true;*/
	
	//Create everything, delete nothing
/*	public static boolean createAuthorTempStuff= true;
	public static boolean validateAuthorStuff = true;
	public static boolean validatePreprocessingStuff = true;
	public static boolean deletePreprocessingTempStuff = false;
	public static boolean deleteAuthorTempStuff = false;*/
	
	
	public static Twitter mpok = AutoConstants.getTwitter(AutoConstants.Auth.MPOKResearch);
	public static Twitter aofp = AutoConstants.getTwitter(AutoConstants.Auth.AOFPResearch);
	public static Twitter fpfp = AutoConstants.getTwitter(AutoConstants.Auth.FPFPResearch);
	public static Twitter ewks = AutoConstants.getTwitter(AutoConstants.Auth.EWKSResearch);
	public static Twitter mnlp = AutoConstants.getTwitter(AutoConstants.Auth.MNLPResearch);
	public static Twitter axao = AutoConstants.getTwitter(AutoConstants.Auth.AXAOResearch);
	public static Twitter scsp = AutoConstants.getTwitter(AutoConstants.Auth.SCSPResearch);
	public static Twitter scsp2 = AutoConstants.getTwitter(AutoConstants.Auth.SCSPResearch2);
	
	public static Twitter[] twitters  = {mpok, aofp, fpfp, ewks, mnlp, axao, scsp, scsp2};
	
	public static HashMap<Long, User> userMap = new HashMap<Long, User>();
	
	public static void main(String[] args) throws Exception
	{
		//Create temporary social network
		System.out.println("Testing pieces of author lookup and precomputation");
		
		//Initialize statuses because Java has OCD
		Status fpfpStatus = null;
		Status aofpStatus = null;
		Status scspStatus = null;
		Status scsp2Status = null;
		Status mnlpStatus = null;
		List<User> activeUsers = null;
		

		
		if (createAuthorTempStuff)
		{
			System.out.println("Creating temporary author data");
			
	
			
			System.out.println("\tConstructing test social network on twitter");
			scsp2.createFriendship(aofp.getId());
			mpok.createFriendship(aofp.getId());
			mnlp.createFriendship(aofp.getId());
			ewks.createFriendship(aofp.getId());
			
			scsp.createFriendship(fpfp.getId());
			axao.createFriendship(fpfp.getId());
			mnlp.createFriendship(fpfp.getId());
			ewks.createFriendship(fpfp.getId());
			
			
			
			//Create temporary rumor on temporary social network
			SimpleDateFormat f = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
			System.out.println("\tHaving bot accounts make rumor or correction-related tweets");
			System.out.println("\t\tfpfp rumor tweet");
			fpfpStatus = fpfp.updateStatus("Rumor 1 "+f.format(new Date()));
			
			TimeUnit.SECONDS.sleep(5);
			System.out.println("\t\taofp correction tweet");
			aofpStatus = aofp.updateStatus("Correction 2 "+f.format(new Date()));
			
			TimeUnit.SECONDS.sleep(5);
			System.out.println("\t\tscsp rumor tweet");
			scspStatus = scsp.updateStatus("Rumor 3 "+f.format(new Date()));
			
			TimeUnit.SECONDS.sleep(5);
			System.out.println("\t\tscsp2 correction tweet");
			scsp2Status = scsp2.updateStatus("Correction 4 "+f.format(new Date()));
			
			TimeUnit.SECONDS.sleep(5);
			System.out.println("\t\tmnlp correction tweet");
			mnlpStatus = mnlp.updateStatus("Correction 5 "+f.format(new Date()));
			
			Status[] statuses = {fpfpStatus,aofpStatus,scspStatus,scsp2Status,mnlpStatus};
			int[] statusTypes = {1,2,1,2,2};
	
			
			
			//Add temporary rumor-related tweets to Cheng's DB
			addTempTweetsToRumorRetriDB(statuses,statusTypes);
			
			addTempRumorToRumorRetriDB(statuses);

		}	
		
		
		System.out.println("\tTemporary social network consists of following members: ");
		User[] users = new User[twitters.length];
		for (int i =0; i < twitters.length; i++)
		{
			users[i] = twitters[i].showUser(twitters[i].getId());
			userMap.put(users[i].getId(), users[i]); 
			System.out.println("\t\t"+users[i].getScreenName()+ ": " + users[i].getFriendsCount() + " followed and " + users[i].getFollowersCount() + " followers.");
		}
		activeUsers = Arrays.asList(users[2],users[1],users[6],users[7],users[4]);

	
		if (validateAuthorStuff)
		{
			//Create an AutoAuthorInfoLookup instance and let it run for a few iterations to get author information for the rumor related tweets
			System.out.println("Running and validating AutoAuthorInfoLookup");
			AutoAuthorInfoLookup apc = new AutoAuthorInfoLookup();
			int numruns = 5;
			for (int i = 0; i < numruns; i++)
			{
				System.out.println("\tRunning AutoAuthorInfoLookup.main(). Run #"+i);
				apc.main(args);
				System.out.println("*******");
			}
	
			
			
			
			//Check results of auto-looking-up to make sure it worked okay
			System.out.println("\tChecking results of auto author lookup");
			checkRumorVizDBForAuthors(activeUsers);
		}
		
		
		if (validatePreprocessingStuff)
		{
			System.out.println("Running precomputer and testing its results");

			//Run autoprecomputer, and hope it notices that there is a completed rumor and functions properly
			try
			{
				AutoPrecomputer precomputer = new AutoPrecomputer();
				
				precomputer.main(args);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				System.err.println("Continuing with rest of test suite");
			}
			
			//Check to see if autoprecomputer produced the right state transitions
			
			checkRumorVizDBForCorrectPrecomputation();
		}
		
		if (deletePreprocessingTempStuff)
		{
			System.out.println("Deleting temporary precomputation data");

			//Delete information about this rumor from the rumorviz tables used in precomputation
			deleteTempPrecomputationFromVizDB();
		
			
		}
		
		 
		if (deleteAuthorTempStuff)
		{
			
			System.out.println("Deleting temporary author lookup data");
			
			//Delete temporary rumor from Cheng's DB
			deleteTempRumorFromRumorRetriDB();
			
			//Delete tweets from both tables	
			deleteTempTweetsFromRumorRetriDb();
			
			deleteTempAuthorsFromRumorVizDB();
	
			//Delete tweets and social network using twitter API
			System.out.println("\tDeleting temporary social network and rumor-related tweets");
			scsp2.destroyFriendship(aofp.getId());
			mpok.destroyFriendship(aofp.getId());
			mnlp.destroyFriendship(aofp.getId());
			ewks.destroyFriendship(aofp.getId());
			
			scsp.destroyFriendship(fpfp.getId());
			axao.destroyFriendship(fpfp.getId());
			mnlp.destroyFriendship(fpfp.getId());
			ewks.destroyFriendship(fpfp.getId());
			
			try
			{
				fpfp.destroyStatus(fpfpStatus.getId());
				aofp.destroyStatus(aofpStatus.getId());
				scsp.destroyStatus(scspStatus.getId());
				scsp2.destroyStatus(scsp2Status.getId());
				mnlp.destroyStatus(mnlpStatus.getId());
			}
			catch (Exception ex)
			{
				System.err.println("Couldn't delete tweets, probably because they weren't tweeted this run of the test script. Doesn't really matter if they hang around, however.");
			}
		}
		
		
		System.out.println("Done!"); 


			
	}
	
	
	public static String[] s_cols = {"Start_RumorExp1", "Start_RumorTwt1", "Start_DebunkExp1", "Start_DebunkTwt1", "RumorExp1_RumorExp2", "RumorExp1_BothExp", "RumorExp1_RumorTwt1", "RumorExp1_DebunkTwt1", "RumorExp2_RumorExp3m", "RumorExp2_BothExp", "RumorExp2_RumorTwt1", "RumorExp2_DebunkTwt1", "RumorExp3m_BothExp", "RumorExp3m_RumorTwt1", "RumorExp3m_DebunkTwt1", "DebunkExp1_DebunkExp2", "DebunkExp1_BothExp", "DebunkExp1_RumorTwt1", "DebunkExp1_DebunkTwt1", "DebunkExp2_DebunkExp3m", "DebunkExp2_BothExp", "DebunkExp2_RumorTwt1", "DebunkExp2_DebunkTwt1", "DebunkExp3m_BothExp", "DebunkExp3m_RumorTwt1", "DebunkExp3m_DebunkTwt1", "BothExp_RumorTwt1", "BothExp_DebunkTwt1", "RumorTwt1_RumorTwt2", "RumorTwt1_BothTwt", "RumorTwt2_RumorTwt3m", "RumorTwt2_BothTwt", "RumorTwt3m_BothTwt", "DebunkTwt1_DebunkTwt2", "DebunkTwt1_BothTwt", "DebunkTwt2_DebunkTwt3m", "DebunkTwt2_BothTwt", "DebunkTwt3m_BothTwt"};
	private static void checkRumorVizDBForCorrectPrecomputation() throws Exception
	{
		System.out.println("\tChecking contents of precomputation tables to see if they captured rumor progress correctly");
		DBManager db = new DBManager(Constants.Server.google_rumorviz,false);

		/*
		 * Tables which need to be checked
		 * author_activity
		 * neighborhood_tweets
		 * tweet
		 * sankey_state_update
		 */
		
		//Check to make sure author activity is correct for each author
		
		checkAuthorActivityTable(db);
		
		
		//Check to make sure neighborhood tweets are correct (are there any?)
		System.out.println("\t\tChecking neighborhood tweets table");
		rPrint("\t\tWARNING: I am not as thorough here as I should be.");

		String query = "select count(*) from neighborhood_tweets where rumor_id = 0";
		ResultSet rs = db.executeQuery(query);
		rs.first();
		if (rs.getInt(1) == 3)
			System.out.println("\t\t\tCorrect. Found 3 neighborhood relationships in neighborhood_tweets table, as expected ");
		else
			rPrint("\t\t\tERROR: found " +rs.getInt(1) + " neighborhood relationships in neighborhood_tweets table instead of the expected 3");
		rs.close();
		
		//Check to make sure tweets are all present
		System.out.println("\t\tChecking tweet table");
		query = "select count(*) from tweet where rumor_id = 0";
		rs = db.executeQuery(query);
		rs.first();
		if (rs.getInt(1) == 5)
			System.out.println("\t\t\tCorrect. Found 5 tweets in tweet table, as expected ");
		else
			rPrint("\t\t\tERROR: found " +rs.getInt(1) + " in tweet table instead of the expected 5");
		rs.close();
		
		
		
		//Check to make sure sankey_state_update transitions are correct  
		checkSankeyStateUpdateTable(db);
		
		
		
		db.close();
	}

	
	public static String[] a_cols = {"rumor_tweets",
			"debunk_tweets",
			"rumor_exposed",
			"debunk_exposed"};
	
	private static void checkAuthorActivityTable(DBManager db) throws Exception
	{
		System.out.println("\t\tChecking author_activity table");
		HashMap<Long,HashMap<String,Integer>> esv = new HashMap<Long,HashMap<String,Integer>> ();
		
		esv.put(fpfp.getId(), new HashMap<String,Integer>());
		esv.get(fpfp.getId()).put("rumor_tweets", 1);
		
		esv.put(aofp.getId(), new HashMap<String,Integer>());
		esv.get(aofp.getId()).put("debunk_tweets", 1);
		
		esv.put(scsp.getId(), new HashMap<String,Integer>());
		esv.get(scsp.getId()).put("rumor_exposed", 1);
		esv.get(scsp.getId()).put("rumor_tweets", 1);
		
		esv.put(scsp2.getId(), new HashMap<String,Integer>());
		esv.get(scsp2.getId()).put("debunk_exposed", 1);
		esv.get(scsp2.getId()).put("debunk_tweets", 1);
		
		esv.put(mnlp.getId(), new HashMap<String,Integer>());
		esv.get(mnlp.getId()).put("rumor_exposed", 1);
		esv.get(mnlp.getId()).put("debunk_exposed", 1);
		esv.get(mnlp.getId()).put("debunk_tweets", 1);
		
		
		String sc_query = "select count(*) from author_activity where rumor_id = 0 order by id asc";
		ResultSet sc_rs = db.executeQuery(sc_query);
		sc_rs.first();
		int s_count;
		if ((s_count = sc_rs.getInt(1)) == 5)
		{
			System.out.println("\t\tCorrect: There are the appropriate number of active authors");
			
			String s_query = "select * from author_activity where rumor_id = 0 order by id asc";
			ResultSet sr = db.executeQuery(s_query);
			sr.beforeFirst();
			while (sr.next())
			{
				long id = sr.getLong("author_id");
				String sn = userMap.get(id).getScreenName();
				if (esv.containsKey(id))
				{
					HashMap<String, Integer> exp = esv.get(id);
					for (String a_col : a_cols)
					{
						int ecount;
						if (exp.containsKey(a_col))
						{
							ecount = exp.get(a_col);
						}
						else
						{
							ecount = 0;
						}
						if (sr.getInt(a_col) == ecount)
						{
							if (ecount > 0)
								System.out.println("\t\t\tCorrect: author " + sn + " column " + a_col + " has " + ecount + " as expected ");
						}
						else
						{
							rPrint("\t\t\tERROR: author " + sn + " column " + a_col + " has " + sr.getInt(a_col) +" instead of the expected " + ecount );
						}
					}
				}
				else
				{
					rPrint("\t\t\tERROR: author_activity table contained unknown author: " + sn);
				}
			}
			sr.close();
		}
		else
		{
			rPrint("\t\t\tERROR: found " + s_count + " active authors for the rumor in author_activities instead of the expected 5");
		}
		sc_rs.close();
		
	}

	private static void checkSankeyStateUpdateTable(DBManager db) throws Exception
	{
		System.out.println("\t\tChecking sankey_state_update table");
		ArrayList<HashMap<String,Integer>> esv = new ArrayList<HashMap<String,Integer>>();
		for (int i = 0; i < 5; i ++){esv.add(new HashMap<String,Integer>());};
		
		//First tweet should move one person from start state to "tweeted rumor" state and 4 people from start state to "exposed to rumor" state
		esv.get(0).put("Start_RumorTwt1", 1);
		esv.get(0).put("Start_RumorExp1", 4);
		
		//Second tweet should move one person from start to "tweeted correction", two from start to "exposed to correction", and two from "exposed to rumor" to "exposed to both"
		esv.get(1).put("Start_DebunkTwt1", 1);
		esv.get(1).put("Start_DebunkExp1", 2);
		esv.get(1).put("RumorExp1_BothExp", 2);
		
		//Third should move one person from "exposed to rumor" to "tweeted rumor"
		esv.get(2).put("RumorExp1_RumorTwt1", 1);
		
		//Fourth should move one person from "exposed to correction" to "tweeted correction"
		esv.get(3).put("DebunkExp1_DebunkTwt1", 1);
		
		//Fifth should move one person from "exposed to both" to "tweeted correction"
		esv.get(4).put("BothExp_DebunkTwt1", 1);
		
		String sc_query = "select count(*) from sankey_state_update where rumor_id = 0  and active_flag = 0 order by id asc";
		ResultSet sc_rs = db.executeQuery(sc_query);
		sc_rs.first();
		int s_count;
		if ((s_count = sc_rs.getInt(1)) == 5)
		{
			System.out.println("\t\tCorrect: There are the appropriate number of sankey state updates ");
			String s_query = "select * from sankey_state_update where rumor_id = 0 and active_flag = 0 order by id asc";
			ResultSet sr = db.executeQuery(s_query);
			sr.first();
			for (int i = 0; i < esv.size(); i++)
			{
				HashMap<String, Integer> exp = esv.get(i);
				for (String s_col : s_cols)
				{
					int ecount;
					if (exp.containsKey(s_col))
					{
						ecount = exp.get(s_col);
					}
					else
					{
						ecount = 0;
					}
					if (sr.getInt(s_col) == ecount)
					{
						if (ecount > 0)
							System.out.println("\t\t\tCorrect: tweet " + i + " column " + s_col + " has " + ecount + " as expected ");
					}
					else
					{
						rPrint("\t\t\tERROR: tweet " + i + " column " + s_col + " has " + sr.getInt(s_col) +" instead of the expected " + ecount );
					}
					
				}
				System.out.println("\t\t\t*******");
				sr.next();
			}
			sr.close();
		}
		else
		{
			rPrint("\t\t\tERROR: found " + s_count + " tweets for the rumor in sankey_state_updates instead of the expected 5");
		}
		sc_rs.close();
		
	}

	private static void deleteTempPrecomputationFromVizDB() throws Exception
	{
		/*
		 * Tables from which stuff needs to be deleted:
		 * author_activity
		 * neighborhood_tweets
		 * tweet
		 * sankey_state_update
		 */
		
		String[] queries = {"delete from author_activity where rumor_id = 0",
				"delete from neighborhood_tweets where rumor_id = 0",
				"delete from tweet where rumor_id = 0",
				"delete from sankey_state_update where rumor_id = 0",
				"update rumor set preprocessed = 0 where rumor_id = 0"};
		System.out.println("\tDeleting results of precomputation from  viz DB");
		DBManager db = new DBManager(Constants.Server.google_rumorviz,false);
		for (String query : queries)
		{
			System.out.println("\t\t"+query);
			db.execute(query);
		}
		db.close();
		
	}

	private static void addTempRumorToRumorRetriDB(Status[] statuses) throws Exception
	{
/*	Query used to check DB	
 * "select rumor.* from rumor join (rumor_retri.rumorRetriStatus as status) "
				+ "on (rumor.rumor_id = status.rumorID) "
				+ "where (status.progress >= "+progressThreshold+" "
				+ "and rumor.judged_count = (status.judgedSpread + status.judgedCorrection + status.predictedRel))";*/
		
		System.out.println("\tAdding rumor to rumorStatus table in Cheng's DB");
		Status s1 = statuses[0];
		Status s2 = statuses[statuses.length-1];
		String query = "replace into rumor_retri.rumorRetriStatus ("
					+ "rumorID, "
					+ "stage, "
					+ "numsearches, "
					+ "judgedSpread, "
					+ "judgedCorrection, "
					+ "predictedRel, "
					+ "progress,"
					+ "sinceTime,"
					+ "untilTime"
				+ ") values ("
					+ "0,"
					+ "1,"
					+ "1,"
					+ "2,"
					+ "3,"
					+ "0,"
					+ "100,"
					+ shortWeirdFormat.format(s1.getCreatedAt())+","
					+ shortWeirdFormat.format(s2.getCreatedAt())+")";
		
		System.out.println("\t\t"+query);
		DBManager db = new DBManager(Constants.Server.google_rumorviz,false);
		db.execute(query);
		db.close();
		
	}
	
	private static void deleteTempRumorFromRumorRetriDB() throws Exception
	{
/*	Query used to check DB	
 * "select rumor.* from rumor join (rumor_retri.rumorRetriStatus as status) "
				+ "on (rumor.rumor_id = status.rumorID) "
				+ "where (status.progress >= "+progressThreshold+" "
				+ "and rumor.judged_count = (status.judgedSpread + status.judgedCorrection + status.predictedRel))";*/
		
		
		String query = "delete from rumor_retri.rumorRetriStatus where rumorID = 0";
		
		DBManager db = new DBManager(Constants.Server.google_rumorviz,false);
		db.execute(query);
		db.close();
		
	}
	
	

/*	public static void addTempTweetsToRumorVizDB() throws Exception
	{
		System.out.println("Adding temporary rows to rumorviz table with looked up authors");
		
		//author #2 will be set as already known about. 
		String query = "replace into author (screen_name,author_id,followers,followed,completed,rumor_id)"
				+ " values ('test_author_2',2,50,100,1,0))";
		
		DBManager db = new DBManager(Constants.Server.google_rumorviz,false);
		
		db.execute(query);
		
		db.close();

	}*/
	
	private static void checkRumorVizDBForAuthors(List<User> activeUsers) throws Exception
	{
		DBManager db = new DBManager(Constants.Server.google_rumorviz,false);

		for (int i = 0; i < activeUsers.size(); i++)
		{
			User user = activeUsers.get(i);
			
			
			
			System.out.println("\t*********");
			System.out.println("\tVerifying DB correctness for user " + user.getScreenName());
			//Check that there is a listing for the author in the authors table
			String q1 = "select count(*) from author where author_id="+user.getId();
			ResultSet rs1 = db.executeQuery(q1);
			rs1.first();
			int cnt1  = rs1.getInt(1);
			if (cnt1 != 1)
			{
				rPrint("\t\tError: found "+cnt1+" rows in author table");
			}
			else
			{
				System.out.println("\t\tSuccess: found "+cnt1+" rows in author table");
			}
			rs1.close();
			
			
			//Check that there are appropriate numbers of links in the followers and followed tables for this author
			String q2 = "select count(*) from followed where author_id="+user.getId();
			ResultSet rs2 = db.executeQuery(q2);
			rs2.first();
			int cnt2  = rs2.getInt(1);
			
			if (cnt2 != user.getFriendsCount())
			{
				rPrint("\t\tError: found "+cnt2+" rows in followed table; " + user.getFriendsCount() + " users followed (friends)");
			}
			else
			{
				System.out.println("\t\tSuccess: found "+cnt2+" rows in followed table for test author; " + user.getFriendsCount()+ " users followed (friends)");
			}
			rs2.close();
			
			
			String q3 = "select count(*) from follower where author_id="+user.getId();
			ResultSet rs3 = db.executeQuery(q3);
			rs3.first();
			int cnt3  = rs3.getInt(1);
			
			if (cnt3 != user.getFollowersCount())
			{
				rPrint("\t\tError: found "+cnt3+" rows in follower table; " + user.getFollowersCount()+ " followers");
			}
			else
			{
				System.out.println("\t\tSuccess: found "+cnt3+" rows in follower table; " + user.getFollowersCount()+ " followers");
			}
			rs3.close();
		}
		
		db.close();
	}

	public static void deleteTempAuthorsFromRumorVizDB() throws Exception
	{
		System.out.println("\tDeleting temporary rows from rumorviz table");

		String q1 = "delete from author where rumor_id = 0";
		String q2 = "delete from followed where rumor_id = 0";
		String q3 = "delete from follower where rumor_id = 0";
		String q4 = "delete from rumor where rumor_id = 0";

		
		DBManager db = new DBManager(Constants.Server.google_rumorviz,false);
		
		db.execute(q1);
		db.execute(q2);
		db.execute(q3);
		db.execute(q4);
		
		db.close();
	}
	
	public static void addTempTweetsToRumorRetriDB(Status[] statuses, int[] statusTypes) throws Exception
	{
		System.out.println("\tAdding temporary tweets to Chengs DB");

		
		DBManager db = new DBManager(Constants.Server.google_rumor_retri,false);
		for (int i =0; i < statuses.length; i++)
		{
			Status status = statuses[i];
			int type = statusTypes[i];
			String query = "replace into rumorRetri (rumorID, tweetID,authorID,authorName,iterRetri,tweetType, tweetDate, text) "
					+ "values (0"
					+ ","+status.getId()+
					","+status.getUser().getId()+
					",'"+status.getUser().getScreenName()+"'"
					+ ",1"
					+ ","+type
					+ ","+date2WeirdDateLong(status.getCreatedAt())+","
					+ "'"+status.getText()+"')";
//			System.out.println("\t\t"+query);
			db.execute(query);
		}
		
		
//		db.executeQueries(Arrays.asList(queries), true);
		
		db.close();
		
	}
	
	public static void deleteTempTweetsFromRumorRetriDb() throws Exception
	{
		System.out.println("\tDeleting temporary rows from rumorRetri table");

		String query = "delete from rumorRetri where rumorID = 0";
		
		DBManager db = new DBManager(Constants.Server.google_rumor_retri,false);
		
		db.execute(query);
		
		db.close();
	}
	
	/**
	 * If date is 2016-03-04 15:11:27, this returns 20,160,304,151,127
	 * This is what Cheng uses in her database for tweet dates
	 * @return
	 */
	public static SimpleDateFormat weirdFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	public static SimpleDateFormat shortWeirdFormat = new SimpleDateFormat("yyyyMMddHH");


	public static Long date2WeirdDateLong(Date date) 
	{
		return new Long(weirdFormat.format(date));
	}

	public static void rPrint(String str)
	{
		System.out.println("\033[31m " + str + " \033[0m" );

	}
}
