package auto;

import java.util.ArrayList;

import db.Constants;
import db.DBManager;

/**
 * This class replaces some data into the rumor_retri and rumorviz databases to test the precomputer process on. 
 * 
 * It assumes that the auto-author-lookup process has been run, getting followers and followed for the two authors, Bob and Alice
 * 
 *  rumor id 10001
 * 
 * users:
 * bob: 30001
 * alice: 30002
 * sara: 30003
 * jeff: 30004
 * bill: 30005
 * 
 * tweets:
 * 20001 rumor tweet by bob
 * 20002 correction tweet by alice
 * 
 * edges:
 * bob and alice follow each other
 * sara follows bob
 * jeff follows both
 * bill follows alice
 * 
 * @author Sam
 *
 */
public class InsertAutoPrecomputerTestData
{
	public static void main(String[] args) throws Exception
	{
		System.out.println("replaceing test precomputer data");
		DBManager db = new DBManager();
		
		ArrayList<String> commands = new ArrayList<String>();
		
		//replace appropriate data into rumorRetri
		commands.add("replace into rumor_retri.rumorRetri (rumorID,tweetID,authorID,authorName,tweetDate,iterRetri,tweetType,text) "
				+ "values (10001,20001,30001,'Bob',20150521045700,1,1,'Our favorite actor is getting a divorce :-(')");
		commands.add("replace into rumor_retri.rumorRetri (rumorID,tweetID,authorID,authorName,tweetDate,iterRetri,tweetType,text) "
				+ "values (10001,20002,30002,'Alice',20150521045800,1,0,'@Bob No way! You are totally wrong!')");
		
		//replace appropriate data into rumorRetriStatus
		commands.add("replace into rumor_retri.rumorRetriStatus (rumorID,judgedCount,judgedSpread,JudgedCorrection,progress,searchQuery) "
				+ "values (10001,2,1,1,100,'Sam test rumor')");
		
		//replace appropriate data into author
		commands.add("replace into author (screen_name,author_id,description,followers,followed,rumor_id,lookup_date,completed,"
				+ "followers_cursor,followed_cursor,private,error,too_many_followers,too_many_followed,valid) "
				+ "values ('Bob',30001,'Test author named Bob',3,1,10001, '2015-05-21',true,0,0,false,false,false,false,true)");
		commands.add("replace into author (screen_name,author_id,description,followers,followed,rumor_id,lookup_date,completed,"
				+ "followers_cursor,followed_cursor,private,error,too_many_followers,too_many_followed,valid) "
				+ "values ('Alice',30002,'Test author named Alice',3,1,10001, '2015-05-21',true,0,0,false,false,false,false,true)");
		
		//replace appropriate data into follower
		//bob followers
		commands.add("replace into follower (rumor_id,author_id,follower_id) values (10001,30001,30002)");
		commands.add("replace into follower (rumor_id,author_id,follower_id) values (10001,30001,30003)");
		commands.add("replace into follower (rumor_id,author_id,follower_id) values (10001,30001,30004)");
		//alice followers
		commands.add("replace into follower (rumor_id,author_id,follower_id) values (10001,30002,30001)");
		commands.add("replace into follower (rumor_id,author_id,follower_id) values (10001,30002,30004)");
		commands.add("replace into follower (rumor_id,author_id,follower_id) values (10001,30002,30005)");
		
		//replace appropriate data into followed
		commands.add("replace into followed (rumor_id,author_id,followed_id) values (10001,30001,30002)");
		commands.add("replace into followed (rumor_id,author_id,followed_id) values (10001,30002,30001)");
	
		//replace appropriate data into rumor
		commands.add("replace into rumor (rumor_id,name,description,valid_count,error_count,private_count,"
				+ "too_many_followers_count,too_many_followed_count,uncompleted_count,first_lookup_date,completed_count,preprocessed)"
				+ " values (10001,'test_rumor','rumor sam made for testing preprocessing',2,0,0,0,0,0,'2015-05-21',2,0)");
		
		db.executeQueries(commands, true);
		System.out.println("done");
		db.close();
	}

}
