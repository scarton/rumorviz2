package auto;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import struct.LongInt;
import twitter4j.IDs;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;
import db.AutoConstants;
import db.Constants;
import db.DBManager;
import entity.Author;
import entity.AuthorInfo;
import entity.AuthorStatus;
import entity.Rumor;

/**
 * This class is a script which monitors a table of authors found by the Double
 * loop, and performs lookups on authors we don't know about. 
 * 
 * It is intended to be run as a cron job, so it doesn't have to worry about
 * timing on its own
 * @author Sam
 *
 */
public final class AutoAuthorInfoLookup
{
	private static long testAuthorID = 14197312;
	private static AutoConstants.Auth auth = AutoConstants.Auth.MPOKResearch;
	
	private static DBManager db;
	private static Twitter twitter;
	
	private static final int numAuthors = 12;
	
	private static final int pageSize = 5000;
	
	private static final boolean getOldest = false; //if true, get oldest author we don't know about. If false, get newest
	private static final String mode = (getOldest ? "oldest":"newest");
	
	private static Logger logger;
	
//	private static String logDir = "/home/scarton/rumors_project/rumorlens_deployment/rumorviz2/scripts/log/";
	private static String logDir = "./";

	private static String logName = "author_lookup.log";
	
	private static boolean debug = true; //It logs more output if this is true, and sends it to the console in addition to the log
	
	public static int maxLinksForLookup = 1000000; //Maximum number of followers or followed for us to try to look up
	
	public static void main(String[] args) throws Exception
	{
		
		logger = Logger.getLogger(AutoAuthorInfoLookup.class.getName());
		logger.setUseParentHandlers(false);
		

		ConsoleHandler cHandler = new ConsoleHandler();
		cHandler.setFormatter(new CustomFormatter(debug));
		logger.addHandler(cHandler);
		
/*		FileHandler newHandler = new FileHandler(logDir+logName,50000,20,true);
		newHandler.setFormatter(new CustomFormatter(debug));
		logger.addHandler(newHandler);*/	
		
		logger.info("Running auto author information lookup");
		db = new DBManager(Constants.Server.google_rumorviz,false);
		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setDebugEnabled(true);
		builder.setOAuthConsumerKey(auth.consumerKey);
		builder.setOAuthConsumerSecret(auth.consumerSecret);
		builder.setOAuthAccessToken(auth.accessToken);
		builder.setOAuthAccessTokenSecret(auth.accessTokenSecret);
		TwitterFactory factory = new TwitterFactory(builder.build());
		twitter = factory.getInstance();
		try
		{
			performMostUrgentLookups();
		}
		catch (Exception ex)
		{
			logger.log(Level.SEVERE,ex.getMessage());
			if (debug) ex.printStackTrace();
		}
		
		db.close();
		
		
		//It's stupid that I have to do this, but I do. 
		for (Handler handler : logger.getHandlers())
		{
			logger.removeHandler(handler);
		}

		
	}

	
	private static void debugLog(String str)
	{
		if (debug)
		{
			logger.log(Level.INFO,str);
		}
	}

	/**
	 * Figure out which author is next in line to be looked up, then look them
	 * up
	 */
	@SuppressWarnings("unused")
	private static void performMostUrgentLookups() throws Exception
	{
		
		debugLog("Looking for " + mode + " uncompleted author (if one exists)");
		
		//See if there are any authors we haven't finished looking up followers for
		Author unfinished = db.get.getUncompletedAuthor(getOldest);
		//Author unfinished = null;
		
		if (unfinished != null)
		{
			int rumorID = (Integer) unfinished.getOther().get("rumor_id");
			//If so, continue paging through followers and/or followed for that person
			debugLog("Found an uncompleted author, so continuing to page through results for them.");
			int followersFound =0;
			int followedFound =0;
			
			if (unfinished.getFollowersCursor() != 0)
			{
				IDs followerIDs = twitter.getFollowersIDs(unfinished.getID(),unfinished.getFollowersCursor());
				followersFound = followerIDs.getIDs().length;
				db.insert.writeFollowerList(unfinished.getID(), followerIDs.getIDs(),rumorID);
				debugLog("Found more followers.");
				unfinished.setFollowersCursor(followerIDs.getNextCursor());
			}
			
			if (unfinished.getFollowedCursor() != 0)
			{
				IDs followedIDs = twitter.getFriendsIDs(unfinished.getID(),unfinished.getFollowedCursor());
				followedFound = followedIDs.getIDs().length;
				db.insert.writeFollowedList(unfinished.getID(), followedIDs.getIDs(), rumorID);
				debugLog("Found more followed");
				unfinished.setFollowedCursor(followedIDs.getNextCursor());
			}
			
			logger.log(Level.INFO,"Uncompleted author found: " + unfinished.getID() + " from rumor #" + (Integer)unfinished.getOther().get("rumor_id") + 
					" | Screen name: " + unfinished.getScreenName() + " | new followers added: " + followersFound + " | New followed found: "  + followedFound);
			
			if (unfinished.getFollowersCursor() == 0 && unfinished.getFollowedCursor() ==0)
			{
				logger.log(Level.INFO, "Author " + unfinished.getID() + " completed. Updating rumor table."); 
				unfinished.setCompleted(true);
				unfinished.setValid(true);
				Rumor rumor = db.get.getRumor((Integer)unfinished.getOther().get("rumor_id"));
				rumor.setUncompleted_count(rumor.getUncompleted_count()-1);
				rumor.setValid_count(rumor.getValid_count()+1);
				rumor.setCompleted_count(rumor.getCompleted_count()+1);

				db.insert.replaceRumor(rumor);

			}
			
			db.insert.writeAuthorInfo(unfinished, (Integer)unfinished.getOther().get("rumor_id"));
			


		}
		else
		{
			//If not, look up the oldest (or newest, depending on the value of getOldest)
			//user from Cheng's rumorRetri table. Get the basic info for that user, and
			//their first 5000 followers and followed. Determine whether we have all their
			//followers and followed, and mark them accordingly. 
			debugLog("No uncompleted author found, so looking up "+mode+" totally unknown author");
			LongInt ids = db.get.getUnknownAuthorID(getOldest);
			
			
			if (ids != null)
			{
				Long userID = ids.getLong();
				Integer rumorID = ids.getInt();
				
				debugLog("Unknown author found: " + userID + " from rumor #" + rumorID);
				
				try
				{
					User user = twitter.showUser(userID);
					debugLog("Screen name: " + user.getScreenName());
					Author author = new Author(user.getId(),
					user.getScreenName(),
					user.getFollowersCount(),
					user.getFriendsCount(),
					user.getDescription(),
					true, //completed
					null,//followers cursor
					null, //followed cursor
					user.isProtected(),
					false, //error
					false, //too many followers
					false, //too many followed
					true); //valid author
					
					
					if (!user.isProtected()) //If user profile is not protected
					{
						logger.log(Level.INFO,"Unknown author found: " + userID + " from rumor #" + rumorID + 
								" | Screen name: " + user.getScreenName() + " | Followers : " + user.getFollowersCount() + " | Following: " + user.getFriendsCount() );
						
						if (user.getFollowersCount() > maxLinksForLookup)
						{
							author.setTooManyFollowers(true);
							author.setValid(false);
							logger.log(Level.INFO,"Author has too many followers; not extracting link information.");
						}
						
						if (user.getFriendsCount() > maxLinksForLookup)
						{
							author.setTooManyFollowed(true);
							author.setValid(false);
							logger.log(Level.INFO,"Author has too many friends (people they follow); not extracting link information.");

						}
						
						//If the user doesn't  have too many followers or too many followed to mine
						if (! author.getTooManyFollowers() && !author.getTooManyFollowed())
						{
							IDs followerIDs =  twitter.getFollowersIDs(userID,-1);
							IDs followedIDs =  twitter.getFriendsIDs(userID,-1);
										
							if (followerIDs.getNextCursor() != 0 || followedIDs.getNextCursor() != 0)
							{
								author.setCompleted(false);
								author.setValid(false);
							}
							else
							{
								author.setCompleted(true);
								author.setValid(true);
							}
							author.setFollowersCursor(followerIDs.getNextCursor());
							author.setFollowedCursor(followedIDs.getNextCursor());
							
							
							db.insert.writeFollowerList(userID, followerIDs.getIDs(), rumorID);
							db.insert.writeFollowedList(userID, followedIDs.getIDs(), rumorID);

							logger.log(Level.INFO,followerIDs.getIDs().length + " followers written to followers table | " + followedIDs.getIDs().length + " followed written to followed table");
						}

					}
					else //if the user is protected
					{
						logger.log(Level.INFO,"User " + userID + " was protected, so no information could be extracted from Twitter API");
						author.setValid(false);
					}
					
					
					debugLog("Writing new info to DB");
					db.insert.writeAuthorInfo(author, rumorID);
					
					debugLog("New author info written");
					
					//Once we've sorted out this author, update our rumor record table
					debugLog("Updating rumor data");
					Rumor rumor = db.get.getRumor(rumorID);
					if (rumor == null)
					{
						logger.log(Level.INFO,"Rumor #" + rumorID + " not previously known. Adding to rumor table");
						rumor = new Rumor(rumorID,"","",0,0,0,0,0,0,new Date(System.currentTimeMillis()),0);
					}
					else
						debugLog("Rumor #" + rumorID + " is known, so just updating counts to reflect newest author");
					if (author.getValid()) rumor.setValid_count(rumor.getValid_count()+1);
					if (!author.getCompleted()) rumor.setUncompleted_count(rumor.getUncompleted_count()+1);
					if (author.getCompleted()) rumor.setCompleted_count(rumor.getCompleted_count()+1);
					if (author.getIsPrivate()) rumor.setPrivate_count(rumor.getPrivate_count()+1);
					if (author.getTooManyFollowers()) rumor.setToo_many_followers_count(rumor.getToo_many_followers_count()+1);
					if (author.getTooManyFollowed()) rumor.setToo_many_followed_count(rumor.getToo_many_followed_count()+1);
					if (author.getError()) rumor.setError_count(rumor.getError_count()+1);
					db.insert.replaceRumor(rumor);
					debugLog("Rumor table updated");

					
					//The rate limit for profile lookups is much higher than the one for getting followers/followed (180/15min vs 15/15min) 
					//If we didn't get followers/followed for this person for whatever reason, we might as well start the process again and
					//not let this minute get
					if (user.isProtected() || author.getTooManyFollowers() || author.getTooManyFollowed())
					{
						
						logger.log(Level.INFO,"User was either protected or had too many followers or followed to look up; doing another user to use up API bandwidth");
						performMostUrgentLookups();
					}
				}
				catch (TwitterException ex)
				{
					Author author = new Author(userID,null,null,null,null,null,null,null,null,true,null,null,false);
					db.insert.writeAuthorInfo(author, rumorID);
					logger.log(Level.SEVERE,"Author: " + userID + " from rumor #" + rumorID + " | Threw an error, so logging it as an error user: "+ex.getMessage());
					
					
				}

				
			}
			else
			{
				logger.log(Level.INFO,"No unknown author found.");
			}
			
		}
		
		

		
		
		//Look up oldest unknown author and add basic info		
	}


	/**
	 * Make a call to the Twitter API to get information about the author
	 * @param authorID
	 * @return
	 */
	private static Author lookupAuthorInfo(long authorID) throws TwitterException
	{
		long[] ids = {authorID};
		ResponseList<User> infos = twitter.lookupUsers(ids);
		User userInfo = infos.get(0);
		Author author = new Author(userInfo.getId(),userInfo.getScreenName(),
				userInfo.getFollowersCount(),userInfo.getFriendsCount(),
				userInfo.getDescription());
		return author;
	}
	
	private static class CustomFormatter extends Formatter {
		private boolean debug;
		
		private CustomFormatter()
		{
			debug = false;
		}
		
		private CustomFormatter(boolean debug)
		{
			this.debug=debug;
		}
		private static SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy hh:mm:ss a");
		
		@Override
		public String format(LogRecord record) {
			
			StringBuffer sb = new StringBuffer();
			
			if (!debug)
			{
			sb.append("<< ");
			sb.append(dateFormat.format(new Date(record.getMillis())));
			sb.append(" ");
			sb.append(record.getSourceClassName());
			sb.append(" ");
			sb.append(record.getSourceMethodName());
			sb.append(" >>\n");
			sb.append(record.getLevel());
			sb.append(": ");
			sb.append(record.getMessage());
			sb.append("\n");
			}
			else
			{
				sb.append(record.getSourceClassName()+"."+record.getSourceMethodName()+": " + record.getMessage()+"\n");
			}
			return sb.toString();
		}
		
	}
}
