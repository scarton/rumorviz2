package auto;

import java.util.ArrayList;

import db.DBManager;
import entity.Rumor;
import entity.RumorStatus;
import precompute.RumorLensPrecomputer;

public class AutoPrecomputer
{
	private static boolean getOldest = false; //whether to get the oldest or the newest ready rumor
	private static int threshold = 95; //only get rumors for which the stability field in rumorRetriStatus is greater than or equal to this
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("Finding " + (getOldest ? "oldest" : "newest") + " rumor that has been labelled to a reasonable level of completion in Cheng's database");
		DBManager db = new DBManager();
		
		//get oldest or newest fully-labeled rumor for which we have all author data
		RumorStatus rumor = db.get.getRumorWhichIsReadyForPreprocessing(getOldest, threshold);
		
		if (rumor != null)
		{
			System.out.println("Retrieved following rumor: " + rumor);
			
			
			System.out.println("Creating and running precomputer");
			RumorLensPrecomputer precomputer = new RumorLensPrecomputer(rumor.rumorID, db,db);
			precomputer.precomputeStates();
			rumor.precomputed = true;
			System.out.println("Updating rumor status in DB");
			db.execute("update rumor set preprocessed = 1 where rumor_id = " + rumor.rumorID);
		}
		else
		{
			System.out.println("Could not find a rumor to preprocess");
		}
		
	}
	


}
