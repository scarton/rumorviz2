package db;

public class Constants {

	
	public static enum Server {
		resnick1("resnick1.si.umich.edu","BostonSet_Rumors","test","newscube"),
		resnick1_2("resnick1.si.umich.edu","preprocessed","test","newscube"),

		google("173.194.246.253", "rumorlens","root","alpharmingapproam"),
		google_readonly("173.194.246.253", "rumorlens","rumorlens","plouterramming"),
		
		
		//Three databases on the same GCloud SQL server that form the backend of the full website
		google_rumorviz("173.194.241.163", "rumorviz","root","rumorlens"),
		google_rumor_retri("173.194.241.163", "rumor_retri","root","rumorlens"),
		google_rumor_detection("173.194.241.163", "rumor_detection","root","rumorlens"),

		local("localhost","rumorlens","root","ostagarmory");
		
		public final String host;
		public final String db;
		public final String user;
		public final String pw;
		
		Server(String h, String d, String u, String p)
		{
			host = h;
			db = d;
			user = u;
			pw = p;
		}
	
	}
	
	public static Server defaultServer = Server.google_rumorviz;
	
	public static String Meteor_Tweets = "meteor_rumors"; 			//meteor_rumor_tweets_seeds
	public static String Meteor_Followers = "meteor_followers";
	public static String Meteor_Followees = "meteor_followee";
	public static String Meteor_IStates = "meteor_initial_state_of_tweets";
	public static String Meteor_IStatesME = "meteor_initial_state_of_tweets_2";
	
	public static String Meteor_UStates = "meteor_state_update_of_tweets";
	public static String Meteor_UStatesME = "meteor_state_updates_multi_exp_2";
	
	public static String Meteor_Tweeters = "meteor_tweeter";
	public static String Meteor_TweetNet = "tweets_of_network";

	public static String Meteor_Multi_IStates = "meteor_initial_states_multi_exp_2";
	public static String Meteor_Multi_UStates = "meteor_state_updates_multi_exp_2";

	public static String Meteor_Multi_IStates_2 = "meteor_initial_states_multi_exp_2";
	public static String Meteor_Multi_UStates_2 = "meteor_state_updates_multi_exp_2";
	
	public static String Meteor_Seeds = "temp_meteor_seeds";
	public static String Meteor_trackbacks = "temp_meteor_trackbacks";
	
	public static String AP_IStates = "AP_initial_state";
	public static String AP_UStates = "AP_state_update";
	public static String AP_UStatesME = "AP_state_updates_multi_exp_2";
	public static String AP_Tweets = "AP_tweets";
	public static String AP_Tweeters = "AP_tweeter";
	public static String AP_Followers = "AP_follower";
	public static String AP_TweetNet = "AP_tweets_of_network";
	public static String AP_Followees = "AP_followee";
	
	
	public static String AP_Multi_IStates_2 = "AP_initial_states_multi_exp_2";
	public static String AP_Multi_UStates_2 = "AP_state_updates_multi_exp_2";
	
	public static String Tweeter_Relation = "tweeter_relation";
	
	public static String BostonRumor_Tweeter = "TweeterInfo";
	public static String BostonRumor_Tweets = "Tweets";
	public static String BostonRumor_Follower = "Follower";
	public static String BostonRumor_Followee = "Followee";
	public static String BostonRumor_Sankey_State_Updates = "Sankey_State_Updates";
	public static String BostonRumor_Tweets_of_Network ="Tweets_of_Network";
	public static String BostonRumor_Tweeter_Network_Topology = "Tweeter_Network_Topology";
	
	
	//Constants for GCloud infrastructure
	
	public static final String U_UNKNOWN_AUTHORS = "rumor_detection.authors";
	public static final String U_NEIGHBORHOOD_TWEETS = "neighborhood_tweets";
	public static final String U_AUTHOR_ACTIVITY ="author_activity";
	public static final String U_FOLLOWER = "follower";
	public static final String U_FOLLOWED = "followed";
	public static final String U_RUMOR = "rumor";
	public static final String U_SANKEY_STATE_UPDATE = "sankey_state_update";
	public static final String U_TWEET = "tweet";
	public static final String U_AUTHOR = "author";
	public static final String U_RUMOR_STATUS = "rumor_retri.rumorRetriStatus";
	public static final String U_EXPERIMENT = "experiment";
	public static final String U_RUMOR_RETRI = "rumor_retri.rumorRetri";

	
	
	
}


/*	public static boolean ec2_deploy_version = false;
public static String host = (ec2_deploy_version == true) ? "127.0.0.1" : "resnick1.si.umich.edu";		
public static String db = (ec2_deploy_version == true) ? "rumors" : "Rumors";
public static String id = (ec2_deploy_version == true) ? "rumor" : "test";
public static String pw = (ec2_deploy_version == true) ? "rumorinfo" : "newscube";*/

/*	public static String host = "14.63.220.121";
public static String db = "test";
public static String id = "root"
public static String pw = "rosuatlals12#$^";*/

/*	public static String host = "resnick1.si.umich.edu";		//14.63.220.121
public static String db = "BostonSet_Rumors";						//test
public static String id = "test";					//root
public static String pw = "newscube";				// rosuatlals12#$^
*/	
/*	public static String host = "gauss.si.umich.edu";		
public static String db = "rumors_6_5";					
public static String id = "root";					
public static String pw = "newton";	
*/

/*	public static String host = "localhost";		
public static String db = "rumor_spoof";						
public static String id = "root";				
public static String pw = "ostagarmory";				
*/	