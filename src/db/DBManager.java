package db;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import precompute.InitStateME;
import precompute.StateManager;
import precompute.StateManager.State;
import precompute.StateManager.TweetType;
import precompute.StateManager.UserType;
import precompute.InitState;
import precompute.StateUpdate;
import precompute.StateUpdateME;
import precompute.TweeterLink;
import struct.LongInt;

import java.sql.Types;
import java.text.SimpleDateFormat;

import entity.Author;
import entity.AuthorActivitySummary;
import entity.AuthorInfo;
import entity.AuthorStatus;
import entity.CompleteTweet;
import entity.DoubleLoopTweet;
import entity.ExperimentalCondition;
import entity.ExperimentalConditionResponse;
import entity.NeighborhoodTweet;
import entity.Rumor;
import entity.TimeStamp;
import entity.TweetNeighborhood;
import entity.RumorStatus;
import entity.NeighborhoodTweet.Relationship;

/**
 * Wrapper around a MySQL database. Has some subclasses that handle specific 
 * kinds of functionality
 * @author Sam
 *
 */
public class DBManager {
	protected Connection con = null;
	protected PreparedStatement ps = null;
	protected Statement s = null;
	protected ResultSet rs = null;
	private Constants.Server server;
	
	public DBCreate create;
	public DBInsert insert;
	public DBUpdate update;
	public DBGet get;
	public DBDelete delete;
	
	public boolean debug = true;

	public DBManager(Constants.Server s) {
		server = s;
		initInnerClasses();
		OpenConnect(server.host, server.db, server.user, server.pw);
	}
	
	public DBManager(Constants.Server s, boolean debug) {
		server = s;
		this.debug = debug;
		initInnerClasses();
		OpenConnect(server.host, server.db, server.user, server.pw);
	}
	
	public DBManager() {
		server = Constants.defaultServer;
		initInnerClasses();
		OpenConnect(server.host, server.db, server.user, server.pw);
	}
	
	private void initInnerClasses()
	{
		this.create = new DBCreate(this);
		this.insert = new DBInsert(this);
		this.update = new DBUpdate(this);
		this.get = new DBGet(this);
		this.delete = new DBDelete(this);
		
	}
	
	public void executeQueries(Collection<String> commands,boolean verbose) throws Exception
	{
		Statement s = con.createStatement();
		for (String command : commands)
		{
			if (verbose) System.out.println(command);
			s.execute(command);
			
		}
		s.close();
	}
	

	
	
	public void dPrint(Object obj)
	{
		if (debug)
			System.out.println(obj);
		
	}
	
	public void setDebug(boolean bool)
	{
		this.debug = bool;
	}
	
	public void close() throws Exception
	{
		con.close();
	}
	
	public boolean sameDB(DBManager other)
	{
		return this.server.host.equals(other.getHost()) && this.server.db.equals(other.getDBName());
	}
	
	public void OpenConnect(String host, String db, String user, String pw) {
		String url = "jdbc:mysql://" + host + ":3306/" + db +"?rewriteBatchedStatements=true";
				/*+"?useCursorFetch=true&defaultFetchSize=1000";*/
		dPrint("Connecting to " + host + " database " + db + " user name " + user);
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(url, user, pw);
			ps = null;
			rs = null;
			s = null;
		} catch (ClassNotFoundException e) {
			dPrint("Do not find driver");
		} catch (SQLException ex) {
			ex.printStackTrace();
			dPrint("Uncomplete Connection");
		}
		if(con == null)
			dPrint("connection failed");
//		dPrint("logging anyway?");
		
		dPrint("Testing connection...");
		testConnection();
	} // getConnection
	
	private void testConnection() 
	{
		try
		{
			String queryString = "SHOW TABLES";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(queryString);
			
			rs.beforeFirst();
			dPrint("Tables:");
			while (rs.next())
			{
				dPrint("\t"+rs.getString(1));
			}
			s.close();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	public void initBuffer() {
		try
		{
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();				
				ps = null;
			}
			if (s != null)
			{
				s.close();
				s = null;
			}
		}
		
		catch (Exception e)
		{
			dPrint ( e);
		}
	}
	
	public HashMap<String,Long> TweeterNameToID(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String query, screen_name;
		long uid;
		
		HashMap<String,Long> Tweeters = new HashMap<String,Long>();
		
		try {
			statement = con.createStatement();

			query = "select AuthorName,Author from " + Constants.BostonRumor_Tweets + " where Rumor_ID="+rumor_type;
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				screen_name = rs.getString(1);
				uid = rs.getLong(2);
				
				Tweeters.put(screen_name, uid);
			}
			statement.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return Tweeters;
	}
	
	public HashMap<Long,String> TweeterIDToName(int rumor_type){
		Statement statement = null;
		ResultSet rs = null;
		String query, screen_name, TableName;
		long uid;
		
		HashMap<Long,String> Tweeters = new HashMap<Long, String>();
		
		if(rumor_type == 0)
			TableName = Constants.Meteor_Tweeters;
		else 
			TableName = Constants.AP_Tweeters;
		
		try {
			statement = con.createStatement();
			query = "select screen_name,uid from " + TableName;
			
			rs = statement.executeQuery(query);
			
			while(rs.next()){
				screen_name = rs.getString(1);
				uid = rs.getLong(2);
				Tweeters.put(uid, screen_name);
			}
			statement.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return Tweeters;
	}
	
	

	public void setValue(PreparedStatement s, int index, Object obj, int type) throws SQLException
	{
		
		if (obj == null)
		{	
			s.setNull(index, type);
		}
		else
		{
			if (obj instanceof String)
			{
				s.setString(index, (String) obj);
			}
			else if (obj instanceof Integer)
			{
				s.setInt(index, (Integer) obj);
			}
			else if (obj instanceof Long)
			{
				s.setLong(index, (Long) obj);
			}
			else if (obj instanceof Boolean)
			{
				s.setBoolean(index, (Boolean) obj);
			}
			else
			{
				throw new SQLException("Unsupported type");
			}
		}
	}

	/**
	 * Version of updateInitState that accounts for the additional rumor and debunk exposure fields
	 * @param rumor_type
	 * @param tweet_id
	 * @param date
	 * @param start
	 * @param b_exp
	 * @param r_twt
	 * @param d_twt
	 * @param b_twt
	 * @param r_exp_1
	 * @param r_exp_2
	 * @param r_exp_3
	 * @param d_exp_1
	 * @param d_exp_2
	 * @param d_exp_3
	 */
	public void multiUpdateInitState(int rumor_type, long tweet_id, long date, int start,
			int b_exp, int r_twt, int d_twt, int b_twt, 
			int r_exp_1, int r_exp_2, int r_exp_3, 
			int d_exp_1, int d_exp_2, int d_exp_3, int user_type) {
		PreparedStatement ps = null;
		String TableName;
		
		//We don't care about rumor type right now because we've only reconfigured the database to
		//handle the multiple exposure states variant for the meteor rumor
/*		if(rumor_type == 0)*/
			TableName = Constants.Meteor_Multi_IStates;
/*		else 
			TableName = Constants.AP_IStates;*/
		
		
		String queryString="INSERT INTO "+ TableName +"(Tweet_ID, Date, Start, BothExp, RumorTwt, DebunkTwt, BothTwt, ActiveFlag, " +
				"RumorExp1, RumorExp2, RumorExp3m, " +
				"DebunkExp1, DebunkExp2, DebunkExp3m) " +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
		int result;
		
		try {
			if(ps == null){
				ps = con.prepareStatement(queryString);
			}
			ps.setLong(1, tweet_id);
			ps.setLong(2, date);
			ps.setInt(3, start);
			ps.setInt(4, b_exp);
			ps.setInt(5, r_twt);
			ps.setInt(6, d_twt);
			ps.setInt(7, b_twt);
			ps.setInt(8, user_type);
			ps.setInt(9,  r_exp_1);
			ps.setInt(10,  r_exp_2);
			ps.setInt(11,  r_exp_3);
			ps.setInt(12, d_exp_1);
			ps.setInt(13, d_exp_2);
			ps.setInt(14, d_exp_3);
			result = ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
	
	public void execute(String command) throws Exception
	{
		Statement s = con.createStatement();
		s.execute(command);
		s.close();
	}
	
	public ResultSet executeQuery(String query) throws Exception
	{
		Statement s = con.createStatement();
		ResultSet result = s.executeQuery(query);
//		s.close();
		return result;
		
	}
	
private RumorStatus resultSetToRumorStatus(ResultSet rSet) throws Exception
	{
		Integer rumorID = rSet.getInt(1);
		Long date = rSet.getLong(2);
		Boolean labeled = rSet.getBoolean(3);
		Boolean authFound = rSet.getBoolean(4);
		Boolean precomputed = rSet.getBoolean(5);
		Integer numAuthors = rSet.getInt(6);
		Integer foundAuthors = rSet.getInt(7);
		RumorStatus status = new RumorStatus(rumorID, date,labeled,authFound,precomputed,numAuthors,foundAuthors);
		return status;
	}

	public String getHost()
{
	return server.host;
}

public String getDBName()
{
	return server.db;
}

	public class DBCreate
	{
		private DBManager outer;
		
		public DBCreate(DBManager outer)
		{
			this.outer = outer;
		}

		/*	protected ResultSet executeQuery(String command) throws Exception
		{
			Statement s = con.createStatement();
			ResultSet rset = s.executeQuery(command);
			s.close();
			return rset;
		}*/
		
		
		//Table creation methods
		/**
		 * Creates a table that hold references to rumors. All rumor tweets are going to be held together in
		 * a single table, so this table is a guide to the individual rumors represented in that table
		 */
		public void createRumorsTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS "+Constants.U_RUMOR+"("
					 +"rumor_id int(11),"
					 + "name text,"
					 + "description text,"
					 + "valid_count int(11) DEFAULT NULL,"
					 + "error_count int(11) DEFAULT NULL,"
					 + "private_count int(11) DEFAULT NULL,"
					 + "too_many_followers_count int(11) DEFAULT NULL,"
					 + "too_many_followed_count int(11) DEFAULT NULL,"
					 + "uncompleted_count int(11) DEFAULT NULL,"
					 + "first_lookup_date date,"
					 + "completed_count int,"
					 + "PRIMARY KEY (id))";

			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
			
		}
		

		public void createFollowersTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS "+Constants.U_FOLLOWER+" "
					+ "(id bigint unsigned auto_increment, "
					+ "rumor_id int unsigned,"
					+ "author_id bigint unsigned,"
					+ "follower_id bigint unsigned, "
					+ "primary key (id), "
					+ "key (rumor_id))";
			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
			
		}
		
		public void createFollowedTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS "+Constants.U_FOLLOWED+" (id bigint auto_increment,author_id bigint,followed_id bigint, PRIMARY KEY (id), key (author_id))";
			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
			
		}

		/**
		 * A table that holds an experimental condition in each row. The app can read
		 * off the top row and "check" it off each time it is loaded to generate a 
		 * different random visualization
		 */
		public  void createExperimentTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS " + Constants.U_EXPERIMENT 
					+ "(id INT UNSIGNED AUTO_INCREMENT,"
					+ "condition_id INT,"
					+ "hit_id VARCHAR(16),"
					+ "exp_id VARCHAR(16),"
					+ "ip_address VARCHAR(32),"
					+ "checked TINYINT,"
					+ "PRIMARY KEY(id))";
			dPrint(command);
			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
		}

		/**
		 * A table which holds records of what authors neighborhoods do. For a given
		 * author, we can use this table to look up tweets related to the rumor
		 * by their followers, people they follow, and themselves. 
		 * @throws Exception
		 */
		public void createNeighborhoodTweetsTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS "+Constants.U_NEIGHBORHOOD_TWEETS+"(id INT UNSIGNED AUTO_INCREMENT, "
					+ "rumor_id INT, "
					+ "original_tweet_id BIGINT(20),new_tweet_id BIGINT(20), date BIGINT(20), "
					+ "relationship INT, "
					+ "PRIMARY KEY (id), "
					+ "UNIQUE KEY (rumor_id,relationship,original_tweet_id,new_tweet_id))";
			
			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
		}

		/**
		 * Each row summarizes the relationship between one author and one rumor
		 * @throws Exception
		 */
		public void createAuthorActivityTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS "+Constants.U_AUTHOR_ACTIVITY+" (id INT UNSIGNED AUTO_INCREMENT, rumor_id INT,"
					+ "author_id BIGINT(30),"
					+ "rumor_tweets INT, debunk_tweets INT, rumor_exposed INT, debunk_exposed INT,"
					+ "PRIMARY KEY (id), "
					+ "UNIQUE KEY (rumor_id, author_id))";
			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
		}

		/**
		 * This table hold tweets. The format is copied from AP_tweets, with the addition of a rumor_id field.
		 * Creates the table with an index on Rumor_ID as well as id as the primary key
		 */
		public void createTweetsTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS "+Constants.U_TWEET+"("
					+ "id INT UNSIGNED AUTO_INCREMENT,"
					+ "author BIGINT(30)," 
					+ "text VARCHAR(300), "
					+ "tweet_id BIGINT(20), "
					+ "date BIGINT(20), "
					+ "type INT, "
					+ "retweet TINYINT, " 
					+ "author_name VARCHAR(40), "
					+ "embed VARCHAR(700), "
					+ "rumor_id INT, "
					+ "PRIMARY KEY (id), " +
					"INDEX (rumor_id,author))";
			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
			
		}

		/**
		 * In this table, each row represents the impact of one Tweet with respect to
		 * one rumor. Each column counts how many people were moved between one pair
		 * of states by the tweet.
		 * @throws Exception
		 */
		public void createSankeyStateUpdatesTableIfItDoesNotExist() throws Exception
		{
			StringBuilder sankeyColumns = new StringBuilder();
			for (String columnName : StateUpdateME.transitionNames) //The order here is important
			{
				sankeyColumns.append(columnName + " INT NOT NULL, ");
			}
			
			String command = "CREATE TABLE IF NOT EXISTS "+Constants.U_SANKEY_STATE_UPDATE 
					+ "(id INT UNSIGNED AUTO_INCREMENT, date BIGINT(20) NOT NULL, tweet_id BIGINT(20) NOT NULL, "
					+ sankeyColumns.toString() 
					+ "active_flag TINYINT NOT NULL, rumor_id INT NOT NULL, "
					+ "PRIMARY KEY (id), "
					+ "UNIQUE KEY (rumor_id, active_flag, tweet_id))";
			execute(command);
		}

		public void createAuthorTableIfItDoesNotExist() throws Exception
		{
			String command = "CREATE TABLE IF NOT EXISTS " +Constants.U_AUTHOR+ "("
					+ "id INT UNSIGNED AUTO_INCREMENT,"
					+ "screen_name VARCHAR(60),"
					+ "author_id BIGINT(20) UNSIGNED,"
					+ "description VARCHAR(200),"
					+ "followers INT,"
					+ "followed INT,"
					+ "rumor_id INT,"
					+ "completed BOOLEAN," //whether we've collected all follower and followed info for this person
					+ "followers_cursor INT,"
					+ "followed_cursor INT,"
					+ "private BOOLEAN,"
					+ "error BOOLEAN,"
					+ "too_many_followers BOOLEAN,"
					+ "too_many_followed BOOLEAN,"
					+ "PRIMARY KEY (id),"
					+ "UNIQUE KEY (rumor_id,author_id))";
					
			execute(command);
		}

		public void createPrecomputationTablesIfTheyDoNotExist() throws Exception
		{
			createSankeyStateUpdatesTableIfItDoesNotExist();
			createNeighborhoodTweetsTableIfItDoesNotExist();
			createAuthorActivityTableIfItDoesNotExist();
			createAuthorTableIfItDoesNotExist();
			createTweetsTableIfItDoesNotExist();
			
		}
	
	}

	public class DBInsert
	{
		private DBManager outer;
		
		public DBInsert(DBManager outer)
		{
			this.outer = outer;
		}


		public void insertRumor(Integer rumorID, String rumorName, String rumorDescription) throws Exception
		{
			String command = "INSERT INTO rumors VALUES ("+rumorID+",'"+rumorName+"','"+rumorDescription+"')";
			Statement statement = con.createStatement();
			statement.execute(command);
			statement.close();
			
		}

		/**
			 * We use REPLACE instead of INSERT so that we don't end up with duplicate tweets
			 * @param authorID
			 * @param text
			 * @param tweetID
			 * @param date
			 * @param type
			 * @param retweet
			 * @param authorName
			 * @param embed
			 * @param rumorID
			 * @throws Exception
			 */
			public void insertTweetIntoPS(PreparedStatement aps,Long authorID, String text, Long tweetID, Long date, Integer type, 
					Boolean retweet, String authorName, String embed , Integer rumorID) throws Exception
			{
		/*		aps.setLong(1, authorID);
				aps.setString(2, text);
				aps.setLong(3, tweetID);
				aps.setLong(4, date);
				aps.setInt(5, type);
				aps.setBoolean(6, retweet);
				aps.setString(7,authorName);
				aps.setString(8,embed);
				aps.setInt(9,rumorID);*/
				aps.setObject(1, authorID);
				aps.setObject(2, text);
				aps.setObject(3, tweetID);
				aps.setObject(4, date);
				aps.setObject(5, type);
				aps.setObject(6, retweet);
				aps.setObject(7,authorName);
				aps.setObject(8,embed);
				aps.setObject(9,rumorID);
				aps.addBatch();
				
			}

		public void insertTweet(Long authorID, String text, Long tweetID, Long date, Integer type, Boolean retweet, 
				String authorName, String embed , Integer rumorID) throws Exception
		{
			PreparedStatement aps = con.prepareStatement("REPLACE INTO "+Constants.U_TWEET+" (author,text,tweet_id,date,type,retweet,author_name,embed,rumor_id) VALUES (?,?,?,?,?,?,?,?,?)");
			insertTweetIntoPS(aps, authorID, text, tweetID, date, type, retweet, authorName, embed, rumorID);
			aps.executeBatch();
			aps.close();
		}

		/**
			 * Again, REPLACE instead of INSERT to avoid duplicates
			 * @param screenName
			 * @param authorID
			 * @param description
			 * @param followers
			 * @param followees
			 * @param rumorTweets
			 * @param debunkTweets
			 * @param rumorExposures
			 * @param debunkExposures
			 * @param rumorID
			 * @throws Exception
			 */
			public void insertTweeterIntoPS(PreparedStatement aps, String screenName, Long authorID, String description, Integer followers, 
					Integer followees, Integer rumorTweets, Integer debunkTweets, Integer rumorExposures, 
					Integer debunkExposures, Integer rumorID) throws Exception
			{
		/*		aps.setString(1, screenName);
				aps.setLong(2, authorID);
				aps.setString(3, description);
				aps.setInt(4, followers);
				aps.setInt(5, followees);
				aps.setInt(6, rumorTweets);
				aps.setInt(7,debunkTweets);
				aps.setInt(8,rumorExposures);
				aps.setInt(9,debunkExposures);
				aps.setInt(10,rumorID);*/
				aps.setObject(1, screenName);
				aps.setObject(2, authorID);
				aps.setObject(3, description);
				aps.setObject(4, followers);
				aps.setObject(5, followees);
				aps.setObject(6, rumorTweets);
				aps.setObject(7,debunkTweets);
				aps.setObject(8,rumorExposures);
				aps.setObject(9,debunkExposures);
				aps.setObject(10,rumorID);
				aps.addBatch();		
			}

		public void insertTweeter(String screenName, Long authorID, String description, Integer followers, 
				Integer followees, Integer rumorTweets, Integer debunkTweets, Integer rumorExposures, 
				Integer debunkExposures, Integer rumorID) throws Exception
		{
			PreparedStatement aps = con.prepareStatement("REPLACE INTO tweeter (screen_name,uid,description,followers,followees," +
					"RumorTweet,DebunkTweet,RumorExp,DebunkExp,Rumor_ID) VALUES (?,?,?,?,?,?,?,?,?,?)");
			insertTweeterIntoPS(aps, screenName, authorID, description, followers, followees, rumorTweets, debunkTweets, rumorExposures, debunkExposures, rumorID);
			aps.executeBatch();
			aps.close();
			
		}

		public void insertDoubleLoopTweetBatch(LinkedList<DoubleLoopTweet> tweets, Integer rumorID) throws Exception
		{
			PreparedStatement aps = con.prepareStatement("REPLACE INTO tweets (Author,Text,Tweet_ID,Date,Type,Retweet,AuthorName,Embed,Rumor_ID) VALUES (?,?,?,?,?,?,?,?,?)");
			for (DoubleLoopTweet tweet: tweets)
			{
				insertTweetIntoPS(aps,tweet.getAuthorID(),tweet.getText(),tweet.getID(), tweet.getTime(), tweet.getClassification(), null, null, 
					null, rumorID);
			}
			aps.executeBatch();
			aps.close();
		}

		public void insertDoubleLoopTweeterBatch(LinkedList<DoubleLoopTweet> tweets, Integer rumorID) throws Exception
		{
			PreparedStatement aps = con.prepareStatement("REPLACE INTO tweeter (screen_name,uid,description,followers,followees,RumorTweet," +
					"DebunkTweet,RumorExp,DebunkExp,Rumor_ID) VALUES (?,?,?,?,?,?,?,?,?,?)");
			for (DoubleLoopTweet tweet: tweets)
			{
				insertTweeterIntoPS(aps,null,tweet.getAuthorID(),null,null,null,null,null,null,null,rumorID);
			}
			aps.executeBatch();
			aps.close();
			
		}

		/**
			 * Flush the values of the StateManager's moveMap object into the DB. It 
			 * captures the state movement resulting from one Tweet
			 * @param tweet_id
			 * @param date
			 * @param moveMap: counts people moves from state to state by the tweet
			 * @param rumor_type: rumor ID
			 * @param type: whether we are recording movement for active or passive users
			 */
			public void writeSankeyMovementMap(long tweet_id, long date, LinkedHashMap<State, LinkedHashMap<State, Integer>> moveMap, int rumor_type, StateManager.UserType type)  {
				PreparedStatement ps = null;
				String TableName;
				
				TableName = Constants.U_SANKEY_STATE_UPDATE;
		
				
		/*		String queryString="INSERT INTO " + TableName +"(Tweet_ID, Date, " +
						"Start_RumorExp1,   " +
						"Start_DebunkExp1, " +
						"Start_RumorTwt, Start_DebunkTwt, " +
						"RumorExp1_BothExp, RumorExp2_BothExp, RumorExp3m_BothExp, " +
						"RumorExp1_RumorTwt, RumorExp2_RumorTwt, RumorExp3m_RumorTwt, " +
						"RumorExp1_DebunkTwt, RumorExp2_DebunkTwt, RumorExp3m_DebunkTwt," +
						"DebunkExp1_BothExp, DebunkExp2_BothExp, DebunkExp3m_BothExp, " +
						"DebunkExp1_RumorTwt, DebunkExp2_RumorTwt, DebunkExp3m_RumorTwt, " +
						"DebunkExp1_DebunkTwt, DebunkExp2_DebunkTwt, DebunkExp3m_DebunkTwt, " +
						"BothExp_RumorTwt, BothExp_DebunkTwt, RumorTwt_BothTwt, DebunkTwt_BothTwt, " +
						"RumorExp1_RumorExp2, RumorExp2_RumorExp3m, " +
						"DebunkExp1_DebunkExp2, DebunkExp2_DebunkExp3m, " +
						"ActiveFlag) " +
						"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";*/
				StringBuilder transitions = new StringBuilder("tweet_id, date, active_flag");
				StringBuilder qMarks = new StringBuilder("?,?,?");
				
				for (State source : moveMap.keySet())
				{
					for (State dest: moveMap.get(source).keySet())
					{
						transitions.append(", "+source.column+"_"+dest.column);
						qMarks.append(",?");
					}
				}
				
		
				transitions.append(", Rumor_ID");
				qMarks.append(","+rumor_type);
		
				
				String queryString = "INSERT INTO " + TableName + " ("+transitions.toString()+") VALUES (" + qMarks.toString() + ")";
				
				try {
					if(ps == null){
						ps = con.prepareStatement(queryString);
					}
					ps.setLong(1, tweet_id);
					ps.setLong(2, date);
					ps.setBoolean(3, type == UserType.active);
					
					int i = 4;
					for (State source : moveMap.keySet())
					{
						for (State dest: moveMap.get(source).keySet())
						{
							ps.setInt(i, moveMap.get(source).get(dest));
							i++;
						}
					}
		
					ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}

		public void writeBasicAuthorInformationBatch(HashMap<Long,Author> authorMap,int rumor_num) throws Exception
		{
			String command = "INSERT INTO " + Constants.U_AUTHOR + "("
					+ "screen_name,author_id,description,followers,followed,rumor_id)"
					+ "VALUES (?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(command);
			for (Author author:authorMap.values())
			{
				ps.setString(1, author.getScreenName());
				ps.setLong(2, author.getID());
				ps.setString(3, author.getDescription());
				ps.setInt(4, author.getNumFollowers());
				ps.setInt(5, author.getNumFollowed());
				ps.setInt(6,rumor_num);
				ps.addBatch();
			}
			ps.executeBatch();
			
			ps.close();
		}

		public void writeAuthorInfo(Author author, int rumorID) throws Exception
			{
				//Insert summarized information about the author
				String command = "REPLACE INTO " + Constants.U_AUTHOR + 
						" (author_id,screen_name,description,followers,followed,"
						+ "rumor_id,completed,followers_cursor,followed_cursor,"
						+ "private,error,too_many_followers,too_many_followed,valid) "
						+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement statement = con.prepareStatement(command);
				setValue(statement,1, author.getID(),Types.BIGINT);
				setValue(statement,2, author.getScreenName(),Types.VARCHAR);
				setValue(statement,3, author.getDescription(),Types.VARCHAR);
				setValue(statement,4, author.getNumFollowers(),Types.INTEGER);
				setValue(statement,5, author.getNumFollowed(),Types.INTEGER);
				setValue(statement,6, rumorID,Types.BIGINT);
				setValue(statement,7, author.getCompleted(),Types.BOOLEAN);
				setValue(statement,8, author.getFollowersCursor(),Types.BIGINT);
				setValue(statement,9, author.getFollowedCursor(),Types.BIGINT);
				setValue(statement,10, author.getIsPrivate(),Types.BOOLEAN);
				setValue(statement,11, author.getError(),Types.BOOLEAN);
				setValue(statement,12, author.getTooManyFollowers(),Types.BOOLEAN);
				setValue(statement,13, author.getTooManyFollowed(),Types.BOOLEAN);
				setValue(statement,14,author.getValid(),Types.BOOLEAN);

				statement.execute();
				
				statement.close();
				
		/*		//Write lists of followers and followed
				writeFollowerList(author.getID(), author.getFollowerIDs());
				writeFollowedList(author.getID(),author.getFollowedIDs());*/
			}

		public void writeFollowerList(Long id, long[] followerIDs, int rumorID) throws Exception
		{
			writeLinks(Constants.U_FOLLOWER,id,followerIDs, "follower_id", rumorID);
		
			
		}

		public void writeFollowedList(Long id, long[] followedIDs, int rumorID) throws Exception
		{
			writeLinks(Constants.U_FOLLOWED,id,followedIDs,"followed_id", rumorID);
			
		}

		private void writeLinks(String tableName, Long authorID, long[] ids, String colName) throws Exception
		{
			//dPrint("Writing " + ids.length + " IDs to " + tableName);
			String command = "INSERT INTO " + tableName + "(id, author_id, "+colName+") VALUES (DEFAULT,?,?)";
			PreparedStatement statement = con.prepareStatement(command);
			for (long id : ids)
			{
				statement.setLong(1,authorID);
				statement.setLong(2, id);
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			
		}
		
		private void writeLinks(String tableName, Long authorID, long[] ids, String colName, int rumorID) throws Exception
		{
			//dPrint("Writing " + ids.length + " IDs to " + tableName);
			String command = "INSERT INTO " + tableName + "(id, author_id, "+colName+", rumor_id) VALUES (DEFAULT,?,?,?)";
			PreparedStatement statement = con.prepareStatement(command);
			for (long id : ids)
			{
				statement.setLong(1,authorID);
				statement.setLong(2, id);
				statement.setInt(3, rumorID);
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
			
		}

		public void writeNeighborhoodTweetBatches(Iterable<NeighborhoodTweet> neighborhoodTweets, Integer rumorID) throws Exception
		{
			String command = "INSERT INTO "+Constants.U_NEIGHBORHOOD_TWEETS+" VALUES (DEFAULT,?,?,?,?,?)";
			PreparedStatement statement = con.prepareStatement(command);
			
			int count = 0;
			
			for (NeighborhoodTweet nTweet : neighborhoodTweets)
			{
				statement.setInt(1, rumorID);
				statement.setLong(2, nTweet.getOriginalTweetID());
				statement.setLong(3, nTweet.getNewTweetID());
				statement.setLong(4,nTweet.getDate());
				statement.setInt(5,NeighborhoodTweet.relationshipToInt(nTweet.getRelationship()));
				if (nTweet.getRelationship().equals(Relationship.author))
					statement.setInt(5,0);
				else if (nTweet.getRelationship().equals(Relationship.follower))
					statement.setInt(5,1);
				else
					statement.setInt(5,2);
		
				statement.addBatch();
				
				count++;
				if (count  % 1000 == 0)
				{
					
					statement.executeBatch();
					dPrint("\t"+count + " neighborhood tweets written to DB");
				}
			}
			
			if (count > 0)
				statement.executeBatch();
			
			
			statement.close();
		}

		public void writeAuthorActivityBatch(LinkedHashMap<Long, AuthorActivitySummary> authorActivities, int rumorID) throws Exception
		{
			String command = "INSERT INTO "+Constants.U_AUTHOR_ACTIVITY+" VALUES (DEFAULT,?,?,?,?,?,?)";
			PreparedStatement statement = con.prepareStatement(command);
			
			for (Long authorID : authorActivities.keySet())
			{
				AuthorActivitySummary activity = authorActivities.get(authorID);
				statement.setInt(1, rumorID);
				statement.setLong(2, authorID);
				statement.setInt(3, activity.getNumTweets().get(TweetType.rumor));
				statement.setInt(4, activity.getNumTweets().get(TweetType.debunk));
				statement.setInt(5, activity.getNumTimesExposed().get(TweetType.rumor));
				statement.setInt(6, activity.getNumTimesExposed().get(TweetType.debunk));
				statement.addBatch();
			}
			statement.executeBatch();
			statement.close();
		}

		public void writeCompleteTweetBatch(ArrayList<CompleteTweet> tweets) throws Exception
		{
			String command = "INSERT INTO " + Constants.U_TWEET 
					+ " (author,text,tweet_id,date,type,retweet,author_name,embed,rumor_id)"
					+ "VALUES (?,?,?,?,?,?,?,?,?)";
					
			PreparedStatement s = con.prepareStatement(command);
			for (CompleteTweet tweet : tweets)
			{
				s.setLong(1,tweet.getAuthorID());
				s.setString(2, tweet.getText());
				s.setLong(3, tweet.getTweetID());
				s.setLong(4, tweet.getDate());
				s.setInt(5, tweet.getType());
				s.setBoolean(6, tweet.isRetweet());
				s.setString(7, tweet.getAuthorName());
				s.setString(8, tweet.getEmbed());
				s.setInt(9, tweet.getRumorID());
				s.addBatch();
			}
			s.executeBatch();
			s.close();
		}

		public void writeSampleBatchToExperimenalTable(ArrayList<ExperimentalCondition> samples) throws Exception
		{
			String command = "INSERT INTO " + Constants.U_EXPERIMENT 
					+ " (condition_id,hit_id,exp_id,ip_address,checked) VALUES (?,?,?,?,?)";
			
			PreparedStatement s = con.prepareStatement(command);
			for (ExperimentalCondition condition : samples)
			{
				s.setInt(1, condition.getCondition());
				s.setString(2,condition.getHitID());
				s.setString(3,condition.getExpID());
				s.setNull(4, Types.VARCHAR);
				s.setBoolean(5,false); //Stuff is unchecked to begin with
				s.addBatch();
			}
			s.executeBatch();
			s.close();
			
		}


		public void replaceRumor(Rumor rumor) throws Exception
		{
			String command = "replace into " + Constants.U_RUMOR 
					+ " (rumor_id,name,description,valid_count,error_count,private_count,too_many_followers_count,"
					+ "too_many_followed_count,uncompleted_count,first_lookup_date,completed_count)"
					+ "values (?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement s = con.prepareStatement(command);
			
			s.setInt(1,rumor.getId());
			s.setString(2,rumor.getName());
			s.setString(3,rumor.getDescription());
			s.setInt(4,rumor.getValid_count());
			s.setInt(5,rumor.getError_count());
			s.setInt(6,rumor.getPrivate_count());
			s.setInt(7,rumor.getToo_many_followers_count());
			s.setInt(8,rumor.getToo_many_followed_count());
			s.setInt(9,rumor.getUncompleted_count());
			s.setDate(10,new java.sql.Date(rumor.getFirst_lookup_date().getTime()));
			s.setInt(11,rumor.getCompleted_count());

			
			s.execute();
			s.close();
		}
		
	}
	
	public class DBUpdate
	{
		private DBManager outer;
		
		public DBUpdate(DBManager outer)
		{
			this.outer = outer;
		}


		public void updateInitState(int rumor_type, long tweet_id, long date, int start, int r_exp, int d_exp,
				int b_exp, int r_twt, int d_twt, int b_twt) {
			PreparedStatement ps = null;
			String TableName;
			
			if(rumor_type == 0)
				TableName = Constants.Meteor_IStates;
			else 
				TableName = Constants.AP_IStates;
			
			String queryString="INSERT INTO "+ TableName +"(Tweet_ID, Date, Start, RumorExp, DebunkExp, BothExp, RumorTwt, DebunkTwt, BothTwt) VALUES ( ?,?,?,?,?,?,?,?,? )";
			int result;
			
			try {
				if(ps == null){
					ps = con.prepareStatement(queryString);
				}
				ps.setLong(1, tweet_id);
				ps.setLong(2, date);
				ps.setInt(3, start);
				ps.setInt(4,  r_exp);
				ps.setInt(5, d_exp);
				ps.setInt(6, b_exp);
				ps.setInt(7, r_twt);
				ps.setInt(8, d_twt);
				ps.setInt(9, b_twt);
				result = ps.executeUpdate();
				ps.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}

		public void updateTweeterInfo(int rumor_type, String name, int re_count, int de_count, int tr_count, int td_count){
				String TableName;
				if(rumor_type == 0)
					TableName = Constants.Meteor_Tweeters;
				else 
					TableName = Constants.AP_Tweeters;
				
				String queryString = "UPDATE " + TableName + " SET RumorExp=?, DebunkExp=?, RumorTweet=?, DebunkTweet=? WHERE screen_name=?";
				PreparedStatement ps = null;
				int result;
				try {
					if(ps == null){
						ps = con.prepareStatement(queryString);
					}
					if(re_count != -1)
						ps.setInt(1, re_count);
					else
						ps.setNull(1, Types.INTEGER);
					
					if(de_count != -1)
						ps.setInt(2, de_count);
					else
						ps.setNull(2, Types.INTEGER);
					
					if(tr_count != -1)
						ps.setInt(3, tr_count);
					else
						ps.setNull(3, Types.INTEGER);
					
					if(td_count != -1)
						ps.setInt(4, td_count);
					else 
						ps.setNull(4, Types.INTEGER);
					
					ps.setString(5, name);
					
		//			dPrint("RE = " + re_count + ", DE = " + de_count + ", TR = " + tr_count + ", TD = " + td_count);
		//			dPrint(ps);
					result = ps.executeUpdate();
					ps.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}

		public void updateFollowers(long tweet_id, String followers, int rumor_type) {
			PreparedStatement ps = null;
			String TableName = "";
			if(rumor_type == 0){
				TableName = Constants.Meteor_TweetNet;
			} else {
				TableName = Constants.AP_TweetNet;
			}
			String queryString="UPDATE " + TableName + " SET Tweets_of_Followers=? WHERE Tweet_ID=?";
			int result;
			try {
				if(ps == null){
					ps = con.prepareStatement(queryString);
				}
				ps.setString(1, followers);
				ps.setLong(2, tweet_id);
				
				result = ps.executeUpdate();
				ps.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}

		public void updateFollowees(long tweet_id, String followees, int rumor_type) {
			PreparedStatement ps = null;
			String TableName = "";
			if(rumor_type == 0){
				TableName = Constants.Meteor_TweetNet;
			} else {
				TableName = Constants.AP_TweetNet;
			}
			String queryString="UPDATE " + TableName + " SET Tweets_of_Followees=? WHERE Tweet_ID=?";
			int result;
			try {
				if(ps == null){
					ps = con.prepareStatement(queryString);
				}
				ps.setString(1, followees);
				ps.setLong(2, tweet_id);
				
				result = ps.executeUpdate();
				ps.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}

		public void updateAuthorTweets(long tweet_id, String at, int rumor_type) {
			PreparedStatement ps = null;
			String TableName;
			if(rumor_type == 0)
				TableName = Constants.Meteor_TweetNet;
			else 
				TableName = Constants.AP_TweetNet;
			
			String queryString="UPDATE " + TableName + " SET Tweets_of_Author=? WHERE Tweet_ID=?";
			int result;
			try {
				if(ps == null){
					ps = con.prepareStatement(queryString);
				}
				ps.setString(1, at);
				ps.setLong(2, tweet_id);
				
				result = ps.executeUpdate();
				ps.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}

		public void updateText(long tweet_id, String text) {
			PreparedStatement ps = null;
			String queryString = "UPDATE " + Constants.Meteor_Seeds + " SET Text=? WHERE Tweet_ID=?";
			String queryString2 = "UPDATE " + Constants.Meteor_trackbacks + " SET Text=? WHERE Tweet_ID=?";
			int result;
			try {
				if(ps == null){
					ps = con.prepareStatement(queryString);
				}
				ps.setString(1, text);
				ps.setLong(2, tweet_id);
				result = ps.executeUpdate();
				
				ps = con.prepareStatement(queryString2);
				ps.setString(1, text);
				ps.setLong(2, tweet_id);
				result = ps.executeUpdate();	
				ps.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}

		public void updateLinkTable(Integer source, Integer target, int rumor_type) {
			PreparedStatement ps = null;
			String TableName = Constants.Tweeter_Relation;
			
			String queryString="INSERT INTO "+ TableName +"(RumorID,Source,Target) VALUES ( ?,?,? )";
			int result;
			
			try {
				if(ps == null){
					ps = con.prepareStatement(queryString);
				}
				ps.setLong(1, rumor_type);
				ps.setLong(2, source);
				ps.setInt(3, target);
				result = ps.executeUpdate();
				ps.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}

		//Update our record of how completely we have scoped out this particular author
		public void updateAuthorStatus(AuthorStatus status) throws Exception
		{
			String command = "REPLACE INTO " + Constants.U_UNKNOWN_AUTHORS + 
					"(author_id,rumor_id,unknown,missing_followers,missing_followed,followers_found,followed_found) "
					+ "VALUES (?,?,?,?,?,?,?)";
			PreparedStatement statement = con.prepareStatement(command);
			statement.setLong(1, status.authorID);
			statement.setInt(2, status.rumorID);
			statement.setBoolean(3, status.unknown);
			statement.setBoolean(4, status.missingFollowers);
			statement.setBoolean(5,status.missingFollowed);
			statement.setInt(6, status.followersFound);
			statement.setInt(7,status.followedFound);
			
		}

		//If we have completely scoped out an author, let the DB know that we have
		//successfully found one of that rumor's authors
/*		public void updateRumorAuthorStatus(AuthorStatus authorStatus) throws Exception
		{
			if (!authorStatus.unknown && !authorStatus.missingFollowers && !authorStatus.missingFollowed)
			{
				RumorStatus rumorStatus = outer.get.getRumorStatus(authorStatus.rumorID);
				rumorStatus.foundAuthors++;
				if (rumorStatus.foundAuthors == rumorStatus.numAuthors) rumorStatus.allAuthorsFound = true;
				updateRumorStatus(rumorStatus);
			}
			
		}*/



		public void changeTypeOfAllThatMatchID(Integer id, Integer type, String tableName) throws Exception
		{
			
			String query = "UPDATE "+tableName+" t1 JOIN "+tableName+" t2 SET t1.Type = " + type + " WHERE t2.id = "
			+ id + " AND t1.Text = t2.Text";
			Statement statement = con.createStatement();
			statement.execute(query);
			statement.close();
			
		}
		
	}
	
	public class DBGet
	{
		private DBManager outer;
		
		public DBGet(DBManager outer)
		{
			this.outer = outer;
		}


		public ArrayList<Long> getTimeStampsInRange(long start, long end){
			if(start >= end) return null;
			
			String queryString="select distinct Date from " + Constants.Meteor_Tweets + " where Date > " + start + " and Date < " + end;
			ResultSet rs;
			ArrayList<Long> result = new ArrayList<Long>();
			
			try {
				Statement s = con.createStatement();
				
				rs = s.executeQuery(queryString);
				
				while(rs.next()){
					long timestamp = rs.getLong(1);
					result.add(timestamp);
				}
				s.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return result;
		}

		public ArrayList<Long> selectSpreader(int start, int end, int rumor_type) {
			Statement statement = null;
			ResultSet rs = null;
			String id, query, TableName;
			ArrayList<Long> SpreaderList = new ArrayList<Long>();
			Long a_id = (long) -1;
			
			if(rumor_type == 0)
				TableName = Constants.Meteor_Tweets;
			else 
				TableName = Constants.AP_Tweets;
			
			try {
				statement = con.createStatement();
				
				if(start == -1 && end == -1){
					query = "select Author from " + TableName + " where Type=1 group by Tweet_ID";
				} else {
					query = "select Author from " + TableName + " where Type=1 and Date >=" + start + " and Date <=" + end + " group by Tweet_ID";
				}
				rs = statement.executeQuery(query);
				
				while(rs.next()){
						a_id = rs.getLong(1);
						SpreaderList.add(a_id);
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return SpreaderList;
		}

		public ArrayList<Long> selectDebunker(int start, int end, int rumor_type) {
			Statement statement = null;
			ResultSet rs = null;
			String id, query, TableName;
			ArrayList<Long> DebunkerList = new ArrayList<Long>();
			Long a_id = (long) -1;
			
			if(rumor_type == 0)
				TableName = Constants.Meteor_Tweets;
			else 
				TableName = Constants.AP_Tweets;
			
			try {
				statement = con.createStatement();
				
				if(start == -1 && end == -1){
					query = "select Author from " + TableName + " where Type=2 group by Tweet_ID";
				} else {
					query = "select Author from " + TableName + " where Type=2 and Date >=" + start + " and Date <=" + end + " group by Tweet_ID";
				}
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					a_id = rs.getLong(1);
					DebunkerList.add(a_id);
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return DebunkerList;
		}

		public HashMap<String,ArrayList<Long>> selectFollowersOfTSet(String TSet){
			Statement statement = null;
			ResultSet rs = null;		
			Long follower = (long) -1;
			String query, author;
			HashMap<String,ArrayList<Long>> Followers = new HashMap<String,ArrayList<Long>>();
			
			try {
				statement = con.createStatement();
				if(TSet.length() > 1)
					query = "select Author,Follower from " + Constants.Meteor_Followers + " where Author in (" + TSet + ")";
				else
					return null;
				dPrint(query);
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					author = rs.getString(1);
					follower = rs.getLong(2);
					if(Followers.get(author) == null){
						Followers.put(author,new ArrayList<Long>());
					} 
					(Followers.get(author)).add(follower);				
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
			return Followers;
		}

		public ArrayList<TimeStamp> getTweetTimeline(int start, int end, int rumor_type) {
			Statement statement = null;
			ResultSet rs = null;
			String author_name = "", query, tweet, TableName;
			long date, tweet_id, author_id = -1;
			int type;
			ArrayList<TimeStamp> SpreaderList = new ArrayList<TimeStamp>();
			dPrint("Rumor ID: "+ rumor_type);
		
				TableName = Constants.U_TWEET;
			
			try {
				statement = con.createStatement();
				
				if(start == -1 && end == -1){
					query = "select author, author_name, date, type, tweet_id, text from " + TableName + " where rumor_id = "+rumor_type+" and type in (1,2) group by tweet_id order by date asc";
				} else {
					query = "select author, author_name, date, type, tweet_id, text from " + TableName + " where rumor_id = "+rumor_type+" and type in (1,2) date >=" + start + " and date <=" + end + " group by tweet_id order by date asc";
				}
				rs = statement.executeQuery(query);
				
				while(rs.next())
				{
					author_id = rs.getLong(1);	
					author_name = rs.getString(2);
					date = rs.getLong(3);
					type = rs.getInt(4);
					tweet_id = rs.getLong(5);
					tweet = rs.getString(6);
					SpreaderList.add(new TimeStamp(author_id,author_name, date, type, tweet_id, tweet));				
		
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return SpreaderList;
		}

		/**
		 * That is, get timeline of CompleteTweets, as opposed to the TimeStamps
		 * that are returned by getTweetTimeline()
		 * 
		 * For the rumors that I hand-annotated, the tweets are living in my database. For tweets coming in from the double loop, they
		 * are in Cheng's DB. Therefore this method first looks in Cheng's DB, then in mine. 
		 * @return
		 */
		public ArrayList<CompleteTweet> getCompleteTweetTimeline(int rumor_id) throws Exception
		{
			
			System.out.println("Grabbing all tweets for rumor " + rumor_id + " for to preprocess them");
			ArrayList<CompleteTweet> tweets = new ArrayList<CompleteTweet>();
			
			String q3 =  "SELECT author,text,tweet_id,date,type,retweet,author_name,embed,rumor_id FROM "
						+ Constants.U_TWEET + " WHERE rumor_id = " + rumor_id + " AND "
						+ "type IN (1,2) ORDER BY date ASC";
			
			
			
			ResultSet r = this.outer.executeQuery(q3);
			
			while (r.next())
			{
				Long dateLong = r.getLong(4); 
				tweets.add(new CompleteTweet(r.getLong(1),r.getString(2),r.getLong(3),dateLong,r.getInt(5),r.getBoolean(6),r.getString(7),r.getString(8),r.getInt(9)));
			}
			return tweets;
		}
		
		private SimpleDateFormat chengDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		private SimpleDateFormat shortChengDateFormat = new SimpleDateFormat("yyyyMMddHH");
		
		public Long correctChengDate(Long chengDate) throws Exception
		{
			return chengDateFormat.parse(chengDate.toString()).getTime();
		}

		public ArrayList<CompleteTweet> getPartialTweetTimeline(int rumor_id, int num,int offset) throws Exception
		{
			ArrayList<CompleteTweet> tweets = new ArrayList<CompleteTweet>();
			String query = "SELECT author,text,tweet_id,date,type,retweet,author_name,embed,rumor_id FROM "
					+ Constants.U_TWEET + " WHERE rumor_id = " + rumor_id + " AND "
					+ "type IN (1,2) ORDER BY date ASC LIMIT " + offset +","+num;
			Statement s = con.createStatement();
			ResultSet r = s.executeQuery(query);
			
			while (r.next())
			{
				tweets.add(new CompleteTweet(r.getLong(1),r.getString(2),r.getLong(3),r.getLong(4),r.getInt(5),r.getBoolean(6),r.getString(7),r.getString(8),r.getInt(9)));
			}
			s.close();
			return tweets;
		}

		public ArrayList<CompleteTweet> getTopTweetsFromTimeline(int rumor_id, int num) throws Exception
		{
			ArrayList<CompleteTweet> tweets = new ArrayList<CompleteTweet>();
			String query = "SELECT q.* FROM (SELECT tweet.author,tweet.text,tweet.tweet_id,tweet.date,tweet.type,tweet.retweet,tweet.author_name,tweet.embed,tweet.rumor_id FROM "
					+ Constants.U_TWEET+"," +Constants.U_AUTHOR+ " WHERE tweet.author = author.author_id AND tweet.rumor_id = author.rumor_id AND "
					+ "tweet.rumor_id = " + rumor_id + " AND tweet.type IN (1,2) ORDER BY author.followers DESC LIMIT "+num+") q ORDER BY q.date";
			Statement s = con.createStatement();
			ResultSet r = s.executeQuery(query);
			
			while (r.next())
			{
				tweets.add(new CompleteTweet(r.getLong(1),r.getString(2),r.getLong(3),r.getLong(4),r.getInt(5),r.getBoolean(6),r.getString(7),r.getString(8),r.getInt(9)));
			}
			s.close();
			return tweets;
		}

		/*	*//**
		 * Version of updateInitState that accounts for the additional rumor and debunk exposure fields
		 * @param rumor_type
		 * @param tweet_id
		 * @param date
		 * @param start
		 * @param b_exp
		 * @param r_twt
		 * @param d_twt
		 * @param b_twt
		 * @param r_exp_1
		 * @param r_exp_2
		 * @param r_exp_3
		 * @param d_exp_1
		 * @param d_exp_2
		 * @param d_exp_3
		 *//*
		public void multiUpdateInitState(int rumor_type, long tweet_id, long date, StateManager states, StateManager.UserType userType) 
		{
			PreparedStatement ps = null;
			String TableName;
			
			if (rumor_type == 0)
				TableName = Constants.Meteor_Multi_IStates_2;
			else
				TableName = Constants.AP_Multi_IStates_2;
			
			String queryString="INSERT INTO "+ TableName +"(Tweet_ID, Date, Start, BothExp, RumorTwt, DebunkTwt, BothTwt, ActiveFlag, " +
					"RumorExp1, RumorExp2, RumorExp3m, " +
					"DebunkExp1, DebunkExp2, DebunkExp3m) " +
					"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
			StringBuilder columns = new StringBuilder("Tweet_ID, Date, ActiveFlag");
			StringBuilder qMarks = new StringBuilder("?,?,?");
			
			for (StateManager.State state : StateManager.State.values())
			{
				columns.append(", "+state.column);
				qMarks.append(",?");
			}
			
			String queryString = "INSERT INTO "+ TableName +"(" + columns.toString() + ") VALUES (" + qMarks.toString() + ")";
			
			int result;
			
			try {
				if(ps == null){
					ps = con.prepareStatement(queryString);
				}
				ps.setLong(1, tweet_id);
				ps.setLong(2, date);
				ps.setBoolean(3, userType.flag == 1);
		
				for (int i = 0; i < StateManager.State.values().length; i++)
				{
					ps.setInt(i+4, states.getCount(StateManager.State.values()[i],userType));
				}
				
				result = ps.executeUpdate();
			} catch (SQLException e) {
				
				
				e.printStackTrace();
			}
		}*/
		
		public ArrayList<InitState> getInitStates(int rumor_type){
			Statement statement = null;
			ResultSet rs = null;
			String query, TableName;
			ArrayList<InitState> iStates = new ArrayList<InitState>();
			
			if(rumor_type == 0)
				TableName = Constants.Meteor_IStates;
			else 
				TableName = Constants.AP_IStates;
			
			try {
				statement = con.createStatement();
				query = "select Date, Start, RumorExp, DebunkExp, BothExp, RumorTwt, DebunkTwt, BothTwt from " + 
						TableName + " order by Date asc";
				
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					iStates.add(new InitState(rs.getLong(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getInt(8)));
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return iStates;
		}

		public ArrayList<InitStateME> getInitStatesME(int rumor_type, boolean active){
			Statement statement = null;
			ResultSet rs = null;
			String query, TableName;
			ArrayList<InitStateME> iStates = new ArrayList<InitStateME>();
			
			if(rumor_type == 0)
				TableName = Constants.Meteor_Multi_IStates;
			else 
				TableName = Constants.AP_IStates;
			
			try {
				statement = con.createStatement();
				query = "select * from " + 
						TableName + " where ActiveFlag=" + active + " order by Date asc";
				
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					InitStateME state_instance = new InitStateME();
					for(int i = 0; i < InitStateME.StateNames.length; i++){
						state_instance.setValue(i, rs.getInt(InitStateME.StateNames[i]));
					}
					iStates.add(state_instance);
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return iStates;
		}

		/**
		 * Returns a mapping from the ID of a tweet to the IDs of tweets by followers
		 * and followed users of the author of the tweet that were part of the rumor.
		 * 
		 * Note that tweets by followers from before the original tweet, and tweets
		 * by followed users from after the tweet, are discarded in the precomputation
		 * process. The tweets that are left are either those that could have influenced
		 * the original tweet, or those that the original tweet could have influenced. 
		 * 
		 * @param rumor_type
		 * @return
		 */
		public HashMap<Long,TweetNeighborhood> getTweetNeighborhoods(int rumor_type){
			Statement statement = null;
			ResultSet rs = null;
			String query, TableName;
			HashMap<Long,TweetNeighborhood> TN = new HashMap<Long,TweetNeighborhood>();
			
			TableName = Constants.U_NEIGHBORHOOD_TWEETS;
			
			try {
				statement = con.createStatement();
				query = "SELECT original_tweet_id, new_tweet_id, relationship FROM " + 
						TableName + " WHERE rumor_id = " + rumor_type;
				
				rs = statement.executeQuery(query);
				
				Long oTweetID;
				Relationship relationship;
				Long nTweetID;
				while(rs.next()){
					oTweetID = rs.getLong(1);
					nTweetID = rs.getLong(2);
					relationship = NeighborhoodTweet.intToRelationship(rs.getInt(3));
					if (!TN.containsKey(oTweetID))
						TN.put(oTweetID,new TweetNeighborhood(oTweetID));
		
					TN.get(oTweetID).get(relationship).add(nTweetID);
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return TN;
		}

		/*	public HashMap<Long,TweetsOfNetwork> getTweetNetworkMapWithJoin(int rumor_type){
			Statement statement = null;
			ResultSet rs = null;
			String query, TableName;
			HashMap<Long,TweetsOfNetwork> TN = new HashMap<Long,TweetsOfNetwork>();
			
			TableName = Constants.BostonRumor_Tweets_of_Network;
			
			try {
				statement = con.createStatement();
				query = "select Tweets_of_Followers, Tweets_of_Followees, Tweets_of_Author, Tweet_ID from " + 
						TableName + " WHERE Rumor_ID = " + rumor_type+ "  order by Date asc";
				
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					TN.put(rs.getLong(4),new TweetsOfNetwork(rs.getLong(4),rs.getString(1), rs.getString(2), rs.getString(3)));
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return TN;
		}*/
		
		public ArrayList<StateUpdate> getStateUpdate(int rumor_type){
			Statement statement = null;
			ResultSet rs = null;
			String query, TableName;
			ArrayList<StateUpdate> StateUpdates = new ArrayList<StateUpdate>();
			
			if(rumor_type == 0)
				TableName = Constants.Meteor_UStates;
			else 
				TableName = Constants.AP_UStates;
			
			try {
				statement = con.createStatement();
				query = "select Date, Start_RumorExp, Start_DebunkExp, Start_RumorTwt, Start_DebunkTwt, RumorExp_BothExp, " +
						"RumorExp_RumorTwt, RumorExp_DebunkTwt, DebunkExp_BothExp, DebunkExp_RumorTwt, DebunkExp_DebunkTwt, " +
						"BothExp_RumorTwt, BothExp_DebunkTwt, RumorTwt_BothTwt, DebunkTwt_BothTwt, RumorExp_RumorExp, " +
						"DebunkExp_DebunkExp, BothExp_BothExp, RumorTwt_RumorTwt, DebunkTwt_DebunkTwt, BothTwt_BothTwt from " + 
						TableName + " order by Date asc";
				
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					StateUpdates.add(new StateUpdate(rs.getLong(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), 
							rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getInt(11), rs.getInt(12), 
							rs.getInt(13), rs.getInt(14), rs.getInt(15), rs.getInt(16), rs.getInt(17), rs.getInt(18), rs.getInt(19),
							rs.getInt(20), rs.getInt(21)));
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return StateUpdates;
		}

		public ArrayList<StateUpdateME> getStateUpdateME(int rumor_type, boolean active){
			Statement statement = null;
			ResultSet rs = null;
			String query, TableName;
			ArrayList<StateUpdateME> StateUpdates = new ArrayList<StateUpdateME>();
			
			TableName = Constants.BostonRumor_Sankey_State_Updates;
			
			try {
				statement = con.createStatement();
				query = "select * from " + 
						TableName + " where ActiveFlag=" + active + " AND  Rumor_ID = " + rumor_type+" order by Date asc";
				
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					StateUpdateME su_instance = new StateUpdateME(rs.getLong("Tweet_ID"));
					for(int i = 0; i < su_instance.transitionNames.length; i++){
						su_instance.setTransition(i, rs.getInt(su_instance.transitionNames[i]));
					}
					StateUpdates.add(su_instance);
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return StateUpdates;
		}

		public HashMap<Long,StateUpdateME> getStateUpdateMEMap(int rumor_type, boolean active){
			Statement statement = null;
			ResultSet rs = null;
			String query, TableName;
			HashMap<Long,StateUpdateME> StateUpdates = new HashMap<Long,StateUpdateME>();
			
			TableName = Constants.U_SANKEY_STATE_UPDATE;
			
			try {
				statement = con.createStatement();
				query = "select * from " + 
						TableName + " where active_flag=" + active + " AND  rumor_id = " + rumor_type;
				dPrint(query);
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					StateUpdateME su_instance = new StateUpdateME(rs.getLong("Tweet_ID"));
					for(int i = 0; i < StateUpdateME.transitionNames.length; i++){
						su_instance.setTransition(i, rs.getInt(StateUpdateME.transitionNames[i]));
					}
					StateUpdates.put(su_instance.tweet_id,su_instance);
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return StateUpdates;
		}

		/**
		 * 
		 * @param rumor_id
		 * @return
		 */
		public HashMap<Long,HashSet<Long>> selectAllFollower(int rumor_id){
			
			String fTable,tTable, query;
		
			fTable = Constants.U_FOLLOWER;
			tTable = Constants.U_AUTHOR;
			
			query = "SELECT "+fTable+".author_id, "+fTable+".follower_id FROM " + fTable 
			+ " JOIN ("+tTable+") ON ("+tTable+".rumor_id = " + rumor_id+" AND "+tTable+".author_id = "+fTable+".author_id)";
			dPrint(query);
				
			return selectAllFollower(query,rumor_id);
		}

		public HashMap<Long,HashSet<Long>> selectAllFollower(HashMap<String, Long> idMap){
			
			String TableName;
			TableName = Constants.BostonRumor_Follower;
			StringBuilder queryBuilder = new StringBuilder("select Author, Follower from " + TableName + " where Author in ( ");
			boolean first = true;
			for (String name : idMap.keySet())
			{
				queryBuilder.append((first ? "":",")+idMap.get(name).toString());
				first = false;
			}
			queryBuilder.append(")");
			return selectAllFollower(queryBuilder.toString(),1);
		}

		private HashMap<Long, HashSet<Long>> selectAllFollower(String query, int rumor_type) 
		{
			Statement statement = null;
			ResultSet rs = null;
		
			Long id = (long) -1, author_id = (long) -1;
			HashMap<Long,HashSet<Long>> FollowerTable = new HashMap<Long,HashSet<Long>>();
			
			try 
			{
				statement = con.createStatement();
				rs = statement.executeQuery(query);
				
				while(rs.next())
				{
					author_id = rs.getLong(1);
					id = rs.getLong(2);
		
					if(FollowerTable.get(author_id) == null)
					{
						FollowerTable.put(author_id, new HashSet<Long>());
					} 
					FollowerTable.get(author_id).add(id);
				}
				statement.close();
			} 
			catch (SQLException e) 
			{
				
				e.printStackTrace();
			}
		
		return FollowerTable;
		}

		public HashMap<String,HashMap<Long,Object>> selectAllFollowing(boolean followers, int rumor_type){
			Statement statement = null;
			ResultSet rs = null;
			String author = null;
			Long id = (long) -1;
			String query, TableName = "";
			HashMap<String,HashMap<Long,Object>> FollowerTable = new HashMap<String,HashMap<Long,Object>>();
			
			if(rumor_type == 0 && followers == true){
				TableName = Constants.Meteor_Followers;
			} else if(rumor_type == 0 && followers == false){
				TableName = Constants.Meteor_Followees;
			} else if(rumor_type != 0 && followers == true){
				TableName = Constants.AP_Followers;
			} else if(rumor_type != 0 && followers == false){
				TableName = Constants.AP_Followees;
			}
			try {
				statement = con.createStatement();
				if(followers == true){
					query = "select Author,Follower from " + TableName;
				} else {
					query = "select Author,Followee from " + TableName;
				}
				rs = statement.executeQuery(query);
				Object dummy = new Object();
				
				while(rs.next()){
					if(rumor_type == 0){
						author = rs.getString(1);					
					} else {
						author = Long.toString(rs.getLong(1));
					}
					id = rs.getLong(2);
					if(FollowerTable.get(author) == null){
						FollowerTable.put(author,new HashMap<Long,Object>());
					}
					(FollowerTable.get(author)).put(id,dummy);
				}
				statement.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
			return FollowerTable;
		}

		public HashMap<Long,Author> getBasicAuthorInformation(int rumor_num) throws Exception
		{
			HashMap<Long,Author> authorMap = new HashMap<Long, Author>();
			String query = "SELECT screen_name, author_id, description, followers, followed "
					+ "FROM " + Constants.U_AUTHOR + " WHERE rumor_id = " + rumor_num;
			
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(query);
			while (rs.next())
			{
				authorMap.put(rs.getLong(2), new Author(rs.getLong(2),rs.getString(1),rs.getInt(4),rs.getInt(5),rs.getString(3)));
			}
			
			statement.close();
			return authorMap;
		}

		public HashMap<Long,AuthorInfo> getAllAuthorInfo(int rumor_type){
			HashMap<Long,AuthorInfo> AuthorInfoMap = new HashMap<Long,AuthorInfo>();
			String query;
			
			query = "select auth.author_id, auth.followers, auth.followed, "
					+ "act.rumor_tweets, act.debunk_tweets, "
					+ "act.rumor_exposed, act.debunk_exposed, "
					+ "auth.screen_name from " + Constants.U_AUTHOR+ " AS auth "
					+ "JOIN ("+Constants.U_AUTHOR_ACTIVITY+" AS act) "
					+ "ON (auth.rumor_id = " + rumor_type + 
					" AND act.rumor_id = auth.rumor_id AND act.author_id = auth.author_id)";
			
			dPrint(query);
			Statement statement = null;
			ResultSet rs = null;
			
			String name; 
			Long ID;
			int followers, followees, RumorTweet, DebunkTweet, RumorExp, DebunkExp;
			
			try {
				statement = con.createStatement();
				rs = statement.executeQuery(query);
				
				while(rs.next()){
		
					name = rs.getString(8);					
		
					followers = rs.getInt(2);
					followees = rs.getInt(3);
					RumorTweet = rs.getInt(4);
					DebunkTweet = rs.getInt(5);
					RumorExp = rs.getInt(6);
					DebunkExp = rs.getInt(7);
					
					ID = rs.getLong(1);
		
					AuthorInfoMap.put(ID, new AuthorInfo(ID, followers, followees, RumorTweet, DebunkTweet, RumorExp, DebunkExp, name));
				}
			statement.close();
		
			
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return AuthorInfoMap;
		}

		public HashMap<Long,String> getAllEmbed(int rumor_id){
			String TableName, query;
			HashMap<Long,String> EmbedHTML = new HashMap<Long,String>();
			
			TableName = Constants.U_TWEET;
			
			query = "SELECT tweet_id, embed FROM " + TableName + " WHERE rumor_id = " + rumor_id;
			Statement statement;
			try {
				statement = con.createStatement();
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					long id = rs.getLong(1);
					String HTML = rs.getString(2);
					
					if(HTML == null)
						continue;
					else
						EmbedHTML.put(id, HTML);
				}
				statement.close();
			}catch (SQLException e){
				e.printStackTrace();
			}
			
			return EmbedHTML;
		}

		public ArrayList<TweeterLink> selectTweetLinks(int rumor_type){
			String TableName = Constants.Tweeter_Relation;
			String query;
			
			ArrayList<TweeterLink> links = new ArrayList<TweeterLink>();
			query = "SELECT Source,Target FROM " + TableName + " WHERE RumorID=" + rumor_type;
			Statement statement;
			try {
				statement = con.createStatement();
				rs = statement.executeQuery(query);
				
				while(rs.next()){
					int s = rs.getInt(1);
					int t = rs.getInt(2);
					
					links.add(new TweeterLink(s, t));
				}
				statement.close();
			}catch (SQLException e){
				e.printStackTrace();
			}
			
			return links;
		}

		public int getMaxRumorID() throws Exception
		{
			String command = "SELECT max(id) from rumors";
			Statement statement = con.createStatement();
			ResultSet rSet = statement.executeQuery(command);
			rSet.first();
			Integer rVal = rSet.getInt(1);
			statement.close();
			return rVal;
		}

		//Select oldest author ID out of Authors table and remove it. Assume that
		//Records were only added to this table if
		public AuthorStatus getOldestUnknownAuthorStatus() throws Exception
		{
			return getOldestAuthorStatusByStatusValue("unknown", true);
		}

		public AuthorStatus getOldestAuthorNeedingFollowers() throws Exception
		{
			return getOldestAuthorStatusByStatusValue("missing_followers", true);
		}

		public AuthorStatus getOldestAuthorNeedingFollowed() throws Exception
		{
			return getOldestAuthorStatusByStatusValue("missing_followed", true);
		}

		public AuthorStatus getOldestAuthorStatusByStatusValue(String column, Boolean value) throws Exception
		{
			String query = "SELECT authorID, rumorID, unknown,missing_followers,"
					+ "missing_followed, followers_found,followed_found, "
					+ "num_followers, num_followed FROM " 
					+ Constants.U_UNKNOWN_AUTHORS 
					+ " WHERE "+column+" = " + value.toString().toUpperCase() 
					+ " ORDER BY createDate ASC, authorID ASC LIMIT 1";
			Statement s = con.createStatement();
			ResultSet result = s.executeQuery(query);
			result.first();
			long authorID = result.getLong(1);
			int rumorID = result.getInt(2);
			boolean unknown = result.getBoolean(3);
			boolean missingFollowers = result.getBoolean(4);
			boolean missingFollowed = result.getBoolean(5);
			int foundFollowers = result.getInt(6);
			int foundFollowed = result.getInt(7);
			int numFollowers = result.getInt(8);
			int numFollowed = result.getInt(9);
			s.close();
			return new AuthorStatus(authorID, rumorID, unknown,missingFollowers,
					missingFollowed, foundFollowers, foundFollowed, numFollowers, numFollowed);
		}

		/**
		 * 	query = "SELECT "+fTable+".Author, "+fTable+".Follower FROM " + fTable 
			+ " JOIN ("+tTable+") ON ("+tTable+".rumor_id = " + rumor_type+" AND "+tTable+".author_id = "+fTable+".Author)";
			dPrint(query);
		 * @param getOldest
		 * @return
		 */
		
		public LongInt getUnknownAuthorID (boolean getOldest) throws Exception
		{
			LongInt rIDs = null;
			
			String query = "select retri.authorID,retri.rumorID from " + Constants.U_RUMOR_RETRI + " as retri" +
					" left join " + Constants.U_AUTHOR + " as auth on  " +
					"(retri.authorID=auth.author_id and retri.rumorID = auth.rumor_id) " +
					"where (auth.author_id is null and retri.tweetType in (1,2)) "+ 
					" order by retri.tweetDate " +(getOldest ? "asc":"desc")+ " limit 1";
			
			//dPrint(query);
			
			Statement s = con.createStatement();
			ResultSet rSet = s.executeQuery(query);
			
		
			if (rSet.first())
			{
				rIDs =  new LongInt(rSet.getInt(2), rSet.getLong(1));
			}
			s.close();
			
			
			return rIDs;
		}

		private RumorStatus getRumorStatus(Integer rumorID) throws Exception
		{
			String command = " SELECT (rumor_id,date,fully_labeled,all_authors_found,precomputed,num_authors,found_authors)"
					+ " FROM " + Constants.U_RUMOR_STATUS + " WHERE rumor_id = " + rumorID;
			
			Statement s = con.createStatement();
			ResultSet rSet = s.executeQuery(command);
			rSet.first();
			RumorStatus status = resultSetToRumorStatus(rSet);
			s.close();
			return status;
		}

		/**
		 * Get newest or oldest rumor that is ready to be preprocessed. Criteria for establishing readiness:
		 * rumor is unprocessed as indicated by preprocessed flag in rumor table
		 * rumor progress is above threshold specified in arguments, as indicated by progress field in rumor_retri.rumoRetriStatus 
		 * all authors have been collected for that rumor, as indicated by completed_count equaling number of authors for that rumor in rumor_retri.rumorRetri
		 * @param getOldest
		 * @return
		 * @throws Exception
		 */
		public RumorStatus getRumorWhichIsReadyForPreprocessing(boolean getOldest, int progressThreshold) throws Exception
		{
			
			
			RumorStatus status = null;
/*			String command = " SELECT (rumor_id,date,fully_labeled,all_authors_found,"
					+ "precomputed,num_authors,found_authors)"
					+ " FROM " + Constants.U_RUMOR_STATUS + " WHERE fully_labeled = TRUE "
					+ " AND all_authors_found = TRUE AND precomputed = FALSE ORDER BY date";*/
			String command = "select rumor.* from rumor join (rumor_retri.rumorRetriStatus as status) "
					+ "on (rumor.rumor_id = status.rumorID) "
					+ "where "
					+ "status.stability >= "+progressThreshold
					+ " and rumor.completed_count >= (status.judgedSpread + status.judgedCorrection + status.predictedRel)"
					+ " and rumor.preprocessed = 0 "
					+ " order by status.untilTime " + (getOldest ? "asc":"desc") + " limit 1 ";
			
			System.out.println(command);
			Statement s = con.createStatement();
			ResultSet rSet = s.executeQuery(command);
			rSet.beforeFirst();
			if (rSet.next())
			{
				status = resultSetToRumorStatus(rSet);
			}

			s.close();
		
			return status;
		}

		public ArrayList<Author> getAuthorList(int rumorID) throws Exception
		{
			ArrayList<Author> authors = new ArrayList<Author>();
			String command = "SELECT author_id,screen_name,followers,followed FROM " + Constants.U_AUTHOR + " WHERE rumor_id ="+rumorID;
			dPrint(command);
			Statement s = con.createStatement();
			ResultSet rSet = s.executeQuery(command);
			
			while (rSet.next())
			{
				long authorID = rSet.getLong(1);
				String screenName = rSet.getString(2);
				int followers = rSet.getInt(3);
				int followed = rSet.getInt(4);
				authors.add(new Author(authorID, screenName, followers, followed, "Placeholder description"));
			}
			
			s.close();
			
			return authors;
		}

		/**
		 * Return the experimental condition corresponding to the given Mechanical 
		 * Turk HIT ID. Update that row in the DB with the IP address of the 
		 * requesting client, and with a boolean indicator that the row has been
		 * accessed. 
		 * @param hitID
		 * @param ipAddress
		 * @return
		 * @throws Exception
		 */
		public ExperimentalConditionResponse getExperimentalConditionByHitID(String hitID, String ipAddress) throws Exception
		{
			dPrint("hitID: " + hitID);
			dPrint("IP address: " + ipAddress);
		
			String selectCommand = "SELECT condition_id,exp_id FROM " + Constants.U_EXPERIMENT 
					+ " WHERE hit_id = '" + hitID + "' FOR UPDATE";
			Statement statement = con.createStatement();
			dPrint(selectCommand);
			ResultSet rSet = statement.executeQuery(selectCommand);
			
			//if resultset is empty, return error response
			if (!rSet.isBeforeFirst())
				return new ExperimentalConditionResponse(null,ExperimentalConditionResponse.Error.bad_hit_id);
			
			rSet.first();
			ExperimentalConditionResponse response = new ExperimentalConditionResponse(new ExperimentalCondition(rSet.getInt(1),hitID, rSet.getString(2)),null);
			String updateCommand = "UPDATE " + Constants.U_EXPERIMENT + " SET checked = TRUE, ip_address = '"+ipAddress+"' "
					+ "WHERE hit_id = '" + hitID+"'";
			dPrint(updateCommand);
			statement.execute(updateCommand);
			statement.close();
			
			return response;
			
		}

		/**
		 * This method checks the DB for the given IP. If it is in the DB, 
		 * it serves up the experimental condition associated with that IP address.
		 * 
		 * If not, it looks for an unchecked row, and returns the first one it finds. 
		 * It sets that row to checked when it does so.
		 * 
		 * If it cannot find an unchecked row, it returns an error
		 * 
		 * @param ipAddress
		 * @return
		 */
		public ExperimentalConditionResponse getNextExperimentalCondition(String hitID, String ipAddress) throws Exception
		{
			String ipSelect, nextSelect;
			
			if ("sankey_test".equals(hitID) || "list_test".equals(hitID) || "network_test".equals(hitID))
			{
				 ipSelect = "SELECT condition_ID, exp_id,id FROM " + Constants.U_EXPERIMENT + " "
						+ "WHERE hit_id = '"+hitID+"' AND ip_address = '"+ipAddress+"'";
				
				 nextSelect = "SELECT condition_ID, exp_id,id FROM " + Constants.U_EXPERIMENT + " "
						+ "WHERE hit_id = '"+hitID+"' LIMIT 1 FOR UPDATE";
			}
			else
			{
				ipSelect = "SELECT condition_ID, exp_id,id FROM " + Constants.U_EXPERIMENT + " "
					+ "WHERE hit_id = '"+hitID+"' AND ip_address = '"+ipAddress+"'";
			
			 	nextSelect = "SELECT condition_ID, exp_id,id FROM " + Constants.U_EXPERIMENT + " "
					+ "WHERE hit_id = '"+hitID+"' AND checked = FALSE LIMIT 1 FOR UPDATE";
			}
			
			Statement statement = con.createStatement();
			
			ResultSet rSet = statement.executeQuery(ipSelect);
			
			
			//If the resultSet is empty, then try to find the next unchecked row
			dPrint("Getting experimental condition...");
			if (!rSet.isBeforeFirst())
			{
				dPrint("IP address not found. Selecting new row");
				rSet = statement.executeQuery(nextSelect);
			}
			else
			{
				dPrint("IP address found. Returning corresponding row.");
			}
			
			if (!rSet.isBeforeFirst())
			{
				dPrint("No new rows available");
				return new ExperimentalConditionResponse(null,ExperimentalConditionResponse.Error.none_found);
			}
			
			rSet.first();
			ExperimentalConditionResponse response = new ExperimentalConditionResponse(new ExperimentalCondition(rSet.getInt(1), hitID, rSet.getString(2)),null);
			String updateCommand = "UPDATE " + Constants.U_EXPERIMENT + " SET checked = TRUE, ip_address = '"+ipAddress+"' "
					+ "WHERE id = '" + rSet.getInt(3)+"'";
			dPrint(updateCommand);
			statement.execute(updateCommand);
			
			statement.close();
			
			return response;
			
		}

		/**
		 * Return a map of trial IDs to experimental condition rows from the experiment table
		 * @return
		 * @throws Exception
		 */
		public HashMap<String, ExperimentalCondition> getTrialIDExperimentalConditionMap(String hitID) throws Exception
		{
			HashMap<String, ExperimentalCondition> map = new HashMap<String, ExperimentalCondition>();
			String command = "SELECT id, hit_id,exp_id,condition_id FROM " + Constants.U_EXPERIMENT + " WHERE hit_id = '"+hitID+"'";
			Statement statement = con.createStatement();
			dPrint(command);
			ResultSet rSet = statement.executeQuery(command);
			
			while (rSet.next())
			{
				ExperimentalCondition condition = new ExperimentalCondition(rSet.getInt("condition_id"),rSet.getString("hit_id"),rSet.getString("exp_id"),rSet.getInt("id"));
				map.put(condition.getExpID(), condition);
			}
			
			rSet.close();
			
			return map;
		}

		/**
		 * Retrieves either the newest or the oldest uncompleted author from the viz tool's author table. 
		 * @param getoldest
		 * @return
		 */
		
		public Author getUncompletedAuthor(boolean getOldest) throws Exception
		{
			Author author = null;
			String command = "select * from " + Constants.U_AUTHOR+ " where completed = false order by lookup_date " + (getOldest ? "desc":"asc"); 
			//dPrint(command);
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery(command);
			
			if (rs.first())
			{
				author = new Author(rs.getLong("author_id"),
						rs.getString("screen_name"),
						rs.getInt("followers"),
						rs.getInt("followed"),
						rs.getString("description"),
						rs.getBoolean("completed"),
						rs.getLong("followers_cursor"),
						rs.getLong("followed_cursor"),
						rs.getBoolean("private"),
						rs.getBoolean("error"),
						rs.getBoolean("too_many_followers"),
						rs.getBoolean("too_many_followed"),
						rs.getBoolean("valid"));
				
				author.getOther().put("rumor_id",rs.getInt("rumor_id"));
				
			}
		
			s.close();
		
			return author;
		
		}

		//Try to retrieve rumor from DB. Return null if it's not in there
		public Rumor getRumor(Integer rumorID) throws Exception
		{
			Rumor rumor = null;
			String command = "select * from rumor where rumor_id = " + rumorID;
			Statement s = con.createStatement();
			
			ResultSet rs = s.executeQuery(command);
			
			if (rs.first())
			{
				rumor = new Rumor(rs.getInt("rumor_id"),
						rs.getString("name"),
						rs.getString("description"),
						rs.getInt("valid_count"),
						rs.getInt("error_count"),
						rs.getInt("private_count"),
						rs.getInt("too_many_followers_count"),
						rs.getInt("too_many_followed_count"),
						rs.getInt("uncompleted_count"),
						rs.getDate("first_lookup_date"),
						rs.getInt("completed_count"));
				
			}
			s.close();
			return rumor;
		}
		
	}
	
	public class DBDelete
	{
		private DBManager outer;
		
		public DBDelete(DBManager outer)
		{
			this.outer = outer;
		}


		public void dropPrecomputationTables() throws Exception
		{
			String command1 = "DROP TABLE " + Constants.U_SANKEY_STATE_UPDATE;
			String command2 = "DROP TABLE " + Constants.U_AUTHOR_ACTIVITY;
			String command3 = "DROP TABLE " + Constants.U_NEIGHBORHOOD_TWEETS;
		
			execute(command1);
			execute(command2);
			execute(command3);
			
		}

		public void deleteSankeyStateUpdates(int rumorID) throws Exception
		{
			String command = "DELETE FROM " + Constants.U_SANKEY_STATE_UPDATE + " WHERE rumor_id = "+rumorID;
			Statement statement = con.createStatement();
			dPrint(command);
			statement.execute(command);
			statement.close();
			
		}

		public void deleteTweets(int rumorID) throws Exception
		{
			String command = "DELETE FROM " + Constants.U_TWEET + " WHERE rumor_id = "+rumorID;
			Statement statement = con.createStatement();
			dPrint(command);
			statement.execute(command);
			statement.close();
			
		}

		public void deleteNeighborhoodTweets(int rumorID) throws Exception
		{
			String command = "DELETE FROM " + Constants.U_NEIGHBORHOOD_TWEETS + " WHERE rumor_id = "+rumorID;
			Statement statement = con.createStatement();
			dPrint(command);
			statement.execute(command);
			statement.close();
			
		}

		public void deleteAuthors(int rumorID) throws Exception
		{
			String command = "DELETE FROM " + Constants.U_AUTHOR + " WHERE rumor_id = "+rumorID;
			Statement statement = con.createStatement();
			dPrint(command);
			statement.execute(command);
			statement.close();
			
		}

		public void deleteAuthorActivities(int rumorID) throws Exception
		{
			String command = "DELETE FROM " + Constants.U_AUTHOR_ACTIVITY + " WHERE rumor_id = "+rumorID;
			Statement statement = con.createStatement();
			dPrint(command);
			statement.execute(command);
			statement.close();
			
		}
		
	}

	
	public void copyTweetsBetweenDatabases(int rumor_id) throws Exception
	{
		//Funky timestamp thing is needed to correct from Cheng's date format to epoch seconds
		String query = "insert into "+Constants.U_TWEET + " (author,text,tweet_id,date,type,retweet,author_name,embed,rumor_id) "
				+ "select "
				+ "authorID as author, text, tweetID as tweet_id, unix_timestamp(str_to_date(cast(tweetDate as char), '%Y%m%d%H%i%s')) as date, tweetType as type, retweet, authorName as author_name, '' as embed, rumorID as rumor_id "
				+ "from " + Constants.U_RUMOR_RETRI + " where rumorID = " + rumor_id;
		System.out.println(query);
		this.execute(query);
	}

}
