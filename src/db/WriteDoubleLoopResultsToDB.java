package db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Locale;

import entity.DoubleLoopTweet;

/**
 * Writes data annotated by the tool to the DB
 * 
 * args[0]: path to raw tweets file. For the time being, this is /storage3/users/zhezhao/boston
 * args[1]: path to data input directory. This should be /storage4/foreseer/users/lichengz/TREC/data/train/ for the time being
 * args[2]: query id 1
 * args[3]: query id 2
 * ...
 * args[n]: query id n
 * 
 * @author Sam
 *
 */
public class WriteDoubleLoopResultsToDB
{
	public static void main(String[] args) throws Exception
	{
		String rawDataFile = args[0];
		String inputDirectory = args[1];
		
		HashMap<Integer,String> queryIDs = new HashMap<Integer,String>();
		HashMap<Long, DoubleLoopTweet> tweets = new HashMap<Long, DoubleLoopTweet>();
		HashMap<Integer,LinkedList<DoubleLoopTweet>> querySets = new LinkedHashMap<Integer, LinkedList<DoubleLoopTweet>>();
		
		DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy",Locale.ENGLISH);

		
		//Go through the Double Loop output file for each query ID and add all labeled examples to a couple of unified data structures
		System.out.println("Reading labeled data for various query IDs");
		for (int i = 2; i < args.length; i++)
		{
			String fileName = inputDirectory+"/"+args[i]+"_debunk_train.txt";
			queryIDs.put(i, args[i]);
			readDoubleLoopTweets(fileName, tweets,querySets,i);
			System.out.println("\t"+args[i]+": " + querySets.get(i).size() + " found.");
		}
			
		//Go through the entire ~5 gig raw tweets file and match discovered tweets up with the ones we already know about
		System.out.println("Matching labeled data against raw tweets");
		BufferedReader reader = new BufferedReader(new FileReader(new File(rawDataFile)));
		String line = null;
		while ((line = reader.readLine()) != null)
		{
			String[] parts = line.split("\t");
			
			if (parts.length == 4)
			{
				Long ID = Long.parseLong(parts[0]);
				Date date = dateFormat.parse(parts[3]);
			
				if (tweets.containsKey(ID))
				{
					Long authorID = Long.parseLong(parts[1]);
					tweets.get(ID).setAuthorID(authorID);
					tweets.get(ID).setTime(date.getTime());
				}
			}

			
		}
		reader.close();
		
		//Add the tweets for each query ID to the DB
		System.out.println("Adding stuff to DB...");
		DBManager database = new DBManager();
		database.create.createRumorsTableIfItDoesNotExist();
		database.create.createTweetsTableIfItDoesNotExist();
		database.create.createAuthorTableIfItDoesNotExist();
		for (Integer queryNum : queryIDs.keySet())
		{
			Integer rumorID = database.get.getMaxRumorID() + 1;
			database.insert.insertRumor(rumorID,queryIDs.get(queryNum),"");
			int nullCount = 0;
			for (DoubleLoopTweet tweet : querySets.get(queryNum))
			{
				//We leave out a lot of data because we just do not have access to it. 
/*				database.insertTweet(tweet.getAuthorID(),tweet.getText(),tweet.getID(), null, null, null, null, 
						null, rumorID);
				database.insertTweeter(null,tweet.getAuthorID(),null,null,null,null,null,null,null,rumorID);*/
				if (tweet.getAuthorID() == null) nullCount++;
			}
			database.insert.insertDoubleLoopTweetBatch(querySets.get(queryNum),rumorID);
			database.insert.insertDoubleLoopTweeterBatch(querySets.get(queryNum),rumorID);
			
			System.out.println("\t"+queryIDs.get(queryNum)+" rumor added. " + nullCount + " Tweets found that couldn't be matched to a user ID");
		}
		
		System.out.println("Done. ");
		
	}

	private static void readDoubleLoopTweets(String fileName, HashMap<Long, DoubleLoopTweet> tweets, HashMap<Integer, LinkedList<DoubleLoopTweet>> querySets, int queryNum) throws Exception
	{
		BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
		String line = null;
		while ((line = reader.readLine()) != null)
		{
			DoubleLoopTweet tweet = DoubleLoopTweet.parseDLT(line);
			tweets.put(tweet.getID(),tweet);
			if (!querySets.containsKey(queryNum)) querySets.put(queryNum, new LinkedList<DoubleLoopTweet>());
			querySets.get(queryNum).add(tweet);
		}
		
		reader.close();
	}

}
