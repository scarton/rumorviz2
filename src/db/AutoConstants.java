package db;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class AutoConstants
{
	/**

	 * @author Sam
	 *
	 */
	public enum Auth {
		SCSPResearch("scsp.research.1.@gmail.com",
			"2261871864-vP8cMMZ0w4itacrNLxGCffFZKzgqIY9qyRG7w2p",
			"7cTXauacj56FpEKuJdve69g0MbuJw0guJAz1tEl0yzZnE",
			"5n3VGqXkpfjWYEKn2UDA",
			"pxHlF4Nwqajavu5acW6RkhNqQoC5ltzSJAY5pst14",
			true,
			"2261871864"),
			
		SCSPResearch2("scsp.research.2.@gmail.com",
			"2261939359-4waeOaL0zGiMiEVDDJjDtV1632XxA9i9ojVUKwo",
			"hyJM5a2hLFz0vaZepnBOhUuLcZxkXFAAK2qVo0YFTlqLM",
			"tQ7NwhxOV5AkmdMJUPRMQ",
			"PAebgkLlWskpU4GAXAH9rv1wgWw1ERfrdTNAda985o",
			true,
			"2261939359"),
			
		AXAOResearch("axao.research@gmail.com",
			"2261947464-Ybeo3ghLIiGNZRffLBD1JfoK08Qw2mKeKfyLnIi",
			"JG6pszdqIGAZSoFHpuT3ZRhAHcyhLuoDhliN5JqgYxG1j",
			"QwR8OsXCdTvQREkcWQ8pLw",
			"vUgAUXtpJsiHoYtkReMv9wFYm7JIt8hcZTw0kXDdU",
			true,
			"2261947464"),
			
		MNLPResearch("mnlp.research@gmail.com",
			"2261953154-lazVrSBAHNTVFesgUidE7BPCtPcJq9ZcEsQBdw7",
			"LyGgq3UheFwJkA9NQfy09vKdykfkooYse7nJPY5Sv1vnH",
			"E0fbDM66zGrF5N1ogIpxzZFye",
			"5lqOcRLSgo0tpACo5HsT9JV94cqPE7eZdjfaL1hXOAAXjLaSHL",
			true,
			"2261953154"),
			
		EWKSResearch("ewks.research@gmail.com",
			"2261957124-YxWlnZH5L0Z63aDFJ1r0PqHiRMlKuJhp9njFTwr",
			"XTyzpx4zqZttFpiLIMbkoJLPOeGs9YrKIhhEyqrMsYgX1",
			"X8WDvUwt11a75Y6nhhFw",
			"9EbXxgbNEBqYNtlQo3M6n2nSSJ63xqx4MlZMA1GSQ",
			true,
			"2261957124"),
			
		FPFPResearch("fpfp.research@gmail.com",
			"2261969496-fPUWhHLpTZWhxV6Mb9IUBN0k3PDa3sSbdUuCxZt",
			"jFaP4yS5O8t8duKd20ay7Rmdzi3ijw15SiJaTs1TJGw1t",
			"9cTtQDPpFM3ousv7JdcSCw",
			"iEfa3VWZzHEtMcvMIAxZ7p1afeVA0eYmdO2JwbEiIA",
			true,
			"2261969496"),
			
		AOFPResearch("aofp.research@gmail.com",
			"2261978755-myW3qBwM27VfOQlcxFqF8iteCxkcpQ6wb5L5Yeo",
			"tbwc1kHtDKRV09BCHz1BitiRF7MsHqQpvHN89ZlavIa9L",
			"mXEMPOCUy9311TmsTSqrA",
			"2tVy5MFRygylxtOuP6FXwEFV8kAf4YdlCqKBtNDg",
			true,
			"2261978755"),
			
		MPOKResearch("mpok.research@gmail.com",
			"2261980194-zlhaRcV0mhdOut004YzTKKd4hHATBWRoiZFrveh",
			"PGlIVVsVKOBrknCYrmTy147LDTrRxNHQM1E6FOkuboJRD",
			"Ye6RAO571iSElVQtQW42w",
			"4TDG1Ut8AMJExWpZP7z18geIIzExT1UGTZEWFM8",
			true,
			"2261980194"),
			
		acct17426278493("i.ate.god@gmail.com",
			"516011372-C0qM9naZQtp2oHXkI6iKHkVYHTzjB3JotZnUNaM3",
			"UiwKaoEuYIKPO83kWz0jGfzCny6lZIKSs8DDbIh6X",
			"yRJehhPmgOmXRV6OFtLI5Q",
			"v9G9SoZNILMUgSQP0R1VNAPQ2b7LLVuhy3LSgZkuk",
			false,
			null);
		
		
		public final String email;
		public final String accessToken;
		public final String accessTokenSecret;
		public final String consumerKey;
		public final String consumerSecret;
		public final boolean readWrite;
		public final String userId;

		
		private Auth(String e,String at, String ats,String ck, String cs, boolean rw, String id)
		{
			email=e;
			accessToken = at;
			accessTokenSecret = ats;
			consumerKey = ck;
			consumerSecret = cs;
			readWrite = rw;
			userId = id;
		}
	}
	public static Twitter getTwitter(Auth account)
	{
		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setDebugEnabled(true);
		builder.setOAuthConsumerKey(account.consumerKey);
		builder.setOAuthConsumerSecret(account.consumerSecret);
		builder.setOAuthAccessToken(account.accessToken);
		builder.setOAuthAccessTokenSecret(account.accessTokenSecret);
		TwitterFactory factory = new TwitterFactory(builder.build());
		Twitter twitter = factory.getInstance();
		return twitter;
	}
}
