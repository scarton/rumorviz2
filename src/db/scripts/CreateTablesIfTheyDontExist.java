package db.scripts;

import db.Constants;
import db.DBManager;

public class CreateTablesIfTheyDontExist
{
	public static void main(String[] args) throws Exception
	{
		DBManager db = new DBManager(Constants.Server.google_rumorviz);
		
		db.create.createAuthorActivityTableIfItDoesNotExist();
		db.create.createAuthorTableIfItDoesNotExist();
		db.create.createSankeyStateUpdatesTableIfItDoesNotExist();
		db.create.createFollowedTableIfItDoesNotExist();
		db.create.createFollowersTableIfItDoesNotExist();
		db.create.createRumorsTableIfItDoesNotExist();
		db.create.createTweetsTableIfItDoesNotExist();
		db.create.createNeighborhoodTweetsTableIfItDoesNotExist();
		
		db.close();

	}

}
