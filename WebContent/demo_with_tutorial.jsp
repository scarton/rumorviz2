<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Rumor analysis</title>
    <link rel="stylesheet" href="style/base_html.css">
    <link rel="stylesheet" href="style/app.css">
    <link rel="stylesheet" href="style/timebar.css">
    <link rel="stylesheet" href="style/sankey.css">
    <link rel="stylesheet" href="style/barchart.css">
    <link rel="stylesheet" href="style/userlist.css"> 
    <link rel="stylesheet" href="style/network.css"> 
    <link rel="stylesheet" href="style/guiders.css"> 
    <link rel="stylesheet" href="style/jquery.dataTables.min.css">
    <link rel="stylesheet" href="style/dataTables.colVis.css">
    <link rel="stylesheet" href="style/dataTables.colReorder.css">
    <link rel="stylesheet" href="style/dataTables.tableTools.css">
    
    
    

</head>
<body>
	 <table id="main"> 
		 <tr>
			 <td>
			 	<table id = "main-top">
				 	<tr>
				 		<td id = "main-top-left">

						 	<div id = "central_diagram_div" >
						 	

								<!-- RumorLens central diagram gets created here by javascript -->
							</div>
							<div id="sankey_mode_div">
								    <form action="">
								    	<table>
				
										<tr id="sankey_button_box">
											<td><b>Mode:</b></td>
											<td><input type="radio" name="sm" onclick="changeAppMode(appMode.COMBINED)" checked>Sankey</td>
											<!-- <td><input type="radio" name="sm" onclick ="changeAppMode(appMode.BAR_CHART)">Bar graph</td> -->
											<td><input type="radio" name="sm" onclick = "changeAppMode(appMode.USER_LIST)">Table</td>
											<td><input type="radio" name="sm" onclick = "changeAppMode(appMode.NETWORK)">Network</td>
										</tr>
										</table>
									</form>	
							</div>
						</td>
						<td id = "main-top-right">
							<div id="informationPanel">
								<table class="informationBox" id ="tweetDisplayBox"> 
									<thead><tr><th>Selected Tweet</th></tr></thead>
									<tbody id = "tweet_display">
									<tr><td>
										<div id="tweet_display_info">  </div>
										
										<div id="tweet_display_content"> </div>
									</td></tr>
									</tbody>
								</table>
								<div id="selected_delete_button_div" style="visibility:hidden">
									<button id="selected_delete_button" type="button" onclick="displayAndHighlightTweets(-1);">X</button>		
								</div>
								 <table class="informationBox" id="instructionBox">
									<thead><tr><th scope="col">Help</th></tr></thead>
									<tbody><tr><td>
										<br>
										<div id = "instructions_button_div">
											<button id="show_instructions_button" type="button" onclick="showCheatSheet()">Show Cheatsheet</button>
										</div>
										<br>
										<div id = "instructions_button_div">
											<button id="replay_tutorial_button" type="button" onclick="showTutorial()">Show Tutorial</button>
										</div>
									</td></tr></tbody>
								</table> 
								
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id = "main-bottom">
			<td>
				<div id="timebar_div">
					<div id="timespan_delete_button_div" style="visibility:hidden">
						<button id="delete_button" type="button" onclick="clearBrushAndMagnifiedTimeBar()">X</button>		
					</div>
				</div>
			</td>
		</tr>
	</table>
	<!--scripty scripts-->
	  
	<script src="lib/jquery.min.js"></script>  
	<script src = "lib/d3_49.min.js"></script>
	<script src="lib/guiders.js"></script>
	<script src = "scripts/d3_force_network_2.js"></script>
	<script src = "lib/d3_tip.js"></script>
	<script src = "lib/jquery.dataTables.min.js"></script>
	<script src = "lib/dataTables.colVis.min.js"></script>
	<script src = "lib/dataTables.colReorder.min.js"></script>
	<script src = "lib/dataTables.tableTools.min.js"></script>
	
	<script src="scripts/dataModule.js"></script>
	<script src="scripts/rumorlens_sankey.js"></script>
	<script src="scripts/d3_sankey.js"></script>
    <script src="scripts/rumorlens_timebar.js"></script>
    <script src="scripts/rumorlens_tweetinfo.js"></script>
    <script src="scripts/rumorlens_barchart.js"></script>
   	<script src="scripts/rumorlens_list.js"></script>
   	<script src="scripts/rumorlens_networkdiagram.js"></script>
   	<script src="scripts/rumorlens_tweetdisplay.js"></script>
   	<script src="scripts/rumorlens_instructions.js"></script>
    <script src="scripts/rumorlens_app.js"></script>  <!-- This comes last --> 
    <script> 
    	var rumorNum = 5000000; 
    	//Whenever the window is resized, change the central diagram div to take up the appropriate amount of space
    	$(window).resize(function() {
    		resizeCentralDiagramDiv();
    		});
		requestDataFromServer(rumorNum);
    </script>
    
</body>
</html>
