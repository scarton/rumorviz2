/**
 * D3 Sankey Diagram plugin adapted from 
 * https://github.com/d3/d3-plugins/tree/master/sankey by Mike Bostock
 * 
 * The plugin has been modified to expect some manual settings from the caller
 * to define the layout of the diagram.
 * 
 * For nodes, passed by calling d3.sankey.nodes([nodes]), it expects the following
 * 	{
 		name:[val] name of the node
		position: [val] horizontal position of the node on an ordinal scale, from the left (e.g. 0 will be put in the leftmost column)
		upper:[val] boolean indicating whether this node is in the upper half of the diagram.
			There are only ever two nodes per column, so we don't need more gradiation than this
		middle:[val] boolean indicating whether this node will be alone and should be centered
		lower:[val] boolean indicating whether this node will be in the lower half of the diagram
		passive:[val] boolean indicating if this is a passive node
		active:[val] boolean indicating if this is an active node
	}
 * 
 * For links, passed by calling d3.sankey.links([links]), it expects the following:
 * 	{
		source:[val] index of the source node
		target:[val] index of the target node
		value:[val] value of the link
		source_position:[val] where the link should be positioned on the source node, from the top down (e.g. 0 will make it the topmost link leaving the node)
		target_position:[val] ditto for the target node
	}
	
	The plugin makes lots of decisions that are specific to our particular use case,
	where passive nodes are always on the left side of the diagram, there are only
	ever 1 or 2 nodes per column, and there is a particular manually-chosen scheme 
	for how to lay out the links that minimizes overlap. 
	
	Any major changes to these assumptions will warrant changes to the plugin.
 */

d3.sankey = function() {
	var sankey = {},
	nodeWidth = 24,
	nodePadding = 8,
	size = [1, 1],
	nodes = [],
	links = [],
	tx_start, //start and end x for scale transition region. Only defined in bimodal mode
	tx_end,
	tx_gap = 2, //start transitioning width 2 columns out from the target node
	kx = -1;//horizontal scaling factor
	
	sankey.nodeWidth = function(_) {
		if (!arguments.length) return nodeWidth;
		nodeWidth = +_;
		return sankey;
	};

	sankey.nodePadding = function(_) {
		if (!arguments.length) return nodePadding;
		nodePadding = +_;
		return sankey;
	};

	sankey.nodes = function(_) {
		if (!arguments.length) return nodes;
		nodes = _;
		return sankey;
	};

	sankey.links = function(_) {
		if (!arguments.length) return links;
		links = _;
		return sankey;
	};

	sankey.size = function(_) {
		if (!arguments.length) return size;
		size = _;
		return sankey;
	};
	
	sankey.tx_start = function()
	{
		return tx_start;
	}
	
	sankey.tx_end = function()
	{
		return tx_end;
	}

	/**
	 * The principle function in this plugin. Compute whole layout for
	 * sankey diagram based on values of nodes, links, nodeWidth, and nodePadding
	 * 
	 * outside: indicates whether nodes should gravitate toward (if false) or away
	 * (if true) from the horizontal center line of the diagram
	 * 
	 * bimodal: if true, put active nodes on their own vertical scale 
	 */
	sankey.layout = function(outside,bimodal, active_scale) {
		//console.log("d3.sankey.layout()");
		computeNodeLinks();
		computeNodeValues();
		computeNodeBreadths(bimodal);
		computeAutomaticNodeDepths(bimodal,active_scale);
		// computeNodeDepths(iterations);
		computeLinkDepths(outside,bimodal);
		return sankey;
	};

	sankey.relayout = function() {
		computeLinkDepths();
		return sankey;
	};

	sankey.link = function() {
		var curvature = .5;

		function link(d) {
			var x0 = d.source.x + d.source.dx,
			x1 = d.target.x,
			xi = d3.interpolateNumber(x0, x1),
			x2 = xi(curvature),
			x3 = xi(1 - curvature),
			y0 = d.source.y + d.sy + d.dy / 2,
			y1 = d.target.y + d.ty + d.dy / 2;

			return "M" + x0 + "," + y0
			+ "C" + x2 + "," + y0
			+ " " + x3 + "," + y1
			+ " " + x1 + "," + y1;
		}


		link.curvature = function(_) {
			if (!arguments.length) return curvature;
			curvature = +_;
			return link;
		};

		return link;
	};

	/**
	 * This is a variant of link() which produces an area bounded by two lines
	 * rather than a single line.
	 * 
	 * @param scale: scales the link width down
	 */
	sankey.linkArea = function(d, scale) {
		var curvature = .5;
		if (isNaN(scale))
			scale = 0;
		var x0 = d.source.x + d.source.dx,
		ssdy = (1-scale)*d.sdy/2;
		if (scale > 0 && d.sdy > 10) ssdy = Math.min(ssdy, d.sdy/2-1); 

		
		var x1 = d.target.x,
		xi = d3.interpolateNumber(x0, x1),
		x2 = xi(curvature),
		x3 = xi(1 - curvature),
		y00 = d.source.y + d.sy + ssdy,
		y10 = d.target.y + d.ty + ssdy,
		y01 = d.source.y + d.sy + d.sdy - ssdy,
		y11 = d.target.y + d.ty + d.sdy - ssdy;

/*		console.log("Evidently valid:");
		console.log(x0);
		console.log(x2);
		console.log(x3);
		console.log(x1);*/
		var str =  "M" + x0 + "," + y00
		+ "C" + x2 + "," + y00
		+ " " + x3 + "," + y10
		+ " " + x1 + "," + y10
		+ "L" + x1 + "," + y11
		+ "C" + x3 + "," + y11
		+ " " + x2 + "," + y01
		+ " " + x0 + "," + y01;
		
		if (str.indexOf("NaN") > -1)
		{
			console.log("NaN detected...");
			console.log("x0:" + x0);
			console.log("x2:" +x2);
			console.log("x3:" +x3);
			console.log("x1:" +x1);
			console.log("y00:" + y00);
			console.log("y10:" +y10);
			console.log("y01:" +y01);
			console.log("y11:" +y11);
			console.log("d.source.y: "+d.source.y);
			console.log("d.target.y: "+d.target.y);
			console.log("d.sy: "+d.sy);
			console.log("d.ty: "+d.ty);
			console.log("d.sdy: "+d.sdy);
			console.log("d.tdy: "+d.tdy);
			console.log("scale: " +scale);
		}
		
		return str;


	};
	
	/**
	 * This is another variant of link() which produces an area bounded by two lines
	 * rather than a single line, and which expands from one width at the beginning
	 * to another at the end
	 * 
	 * @param d: a link object
	 * @param scale: scales the link width down
	 */
	sankey.bimodalLinkArea = function(d, scale) {
		var curvature = .5;
		var widthChange = d.tdy-d.sdy;
		//If the link doesn't change width, just send back a normal link
		if (widthChange == 0)
			return sankey.linkArea(d,scale);
		
		var ssdy = ((1-scale)*d.sdy/2); //How much to subtract from sdy on both sides to fit the desired scale
		
		//Flows should be a minimum of one pixel wide so they can be seen
		//as long as this rescaling isn't unacceptably deceptive
		if (scale > 0 && d.sdy > 10) ssdy = Math.min(ssdy, d.sdy/2-1); 
/*		console.log("bimodalLinkArea()");
		console.log(d.source.name + " --> " + d.target.name + " scaled to " + (100*scale) + "%");
		console.log("sdy: " +d.sdy);
		console.log("ssdy: " + ssdy);
		console.log("Flow width: " + (d.sdy-2*ssdy));*/
		
		
		var stdy = ((1-scale)*d.tdy/2);
		if (scale > 0 && d.tdy > 10) stdy = Math.min(stdy, d.tdy/2-1);


		
/*		console.log(d.source.name + " --> " + d.target.name + ": " + d.sdy + " to " + d.tdy);
		console.log("Change in width: " + widthChange);*/
		var x0 = d.source.x + d.source.dx,
		x3 = d.target.x-tx_gap*kx, //The shape will begin widening two column's width out from the target
		i1 = d3.interpolateNumber(x0, x3),
		x1 = i1(curvature), //x1 and x2 are intermediate x values that control how sharply the link curves
		x2 = i1(1 - curvature),
		x6 = d.target.x,
		i2 = d3.interpolateNumber(x3, x6),
		x4 = i2(curvature),
		x5 = i2(1-curvature),
		y00 = d.source.y + d.sy + ssdy, //top starting y value
		y01 = d.source.y + d.sy + d.sdy - ssdy, //bottom starting y value
		
		y10 = d.target.y + d.ty + widthChange/2 + ssdy,//Intermediate y values, just before the link should widen
		y11 = d.target.y + d.ty + d.tdy - widthChange/2 - ssdy, 
		
		y20 = d.target.y + d.ty + stdy,	//final widened y values
		y21 = d.target.y + d.ty + d.tdy - stdy;
		
/*		console.log("y00: "+y00);
		console.log("y01: "+y01);
		console.log("y10: "+y10);
		console.log("y11: "+y11);
		console.log("y20: "+y20);
		console.log("y21: "+y21);*/
/*		console.log(x0);
		console.log(x1);
		console.log(x2);
		console.log(x3);
		console.log(x4);*/
		
/*		console.log("\n");
*/
		var str = 
		"M" + x0 + "," + y00
		+ "C" + x1 + "," + y00
		+ " " + x2 + "," + y10
		+ " " + x3 + "," + y10
		+ "C" + x4 + "," + y10
		+ " " + x5 + "," + y20
		+ " " + x6 + "," + y20
		+ "L" + x6 + "," + y21
		+ "C" + x5 + "," + y21
		+ " " + x4 + "," + y11
		+ " " + x3 + "," + y11
		+ "C" + x2 + "," + y11
		+ " " + x1 + "," + y01
		+ " " + x0 + "," + y01;
		//console.log(str);
		return str;

	};


	// Populate the sourceLinks and targetLinks for each node.
	// Also, if the source and target are not objects, assume they are indices.
	function computeNodeLinks() {
		nodes.forEach(function(node) {
			node.sourceLinks = [];
			node.targetLinks = [];
		});
		
		links.forEach(function(link) {
			var source = link.source,
			target = link.target;
			if (typeof source === "number") source = link.source = nodes[link.source];
			if (typeof target === "number") target = link.target = nodes[link.target];
			source.sourceLinks.push(link);
			target.targetLinks.push(link);
		});
		
		
		//Sort each node's links by the vertical order we want it to be in. We
		//lay out these orders in enumerateStateTransitions() in app.js
		nodes.forEach(function(node) {
			node.sourceLinks.sort(compareSourcePosition);
			node.targetLinks.sort(compareTargetPosition);
		});
		
		function compareSourcePosition(a,b){
			return a.source_position - b.source_position;
		}

		function compareTargetPosition(a,b){
			return a.target_position - b.target_position;
		}
	}

	// Compute the value (size) of each node by summing the associated links.
	function computeNodeValues() {
		nodes.forEach(function(node) {
			node.value = Math.max(
				d3.sum(node.sourceLinks, value),
				d3.sum(node.targetLinks, value)
			);
			node.true_value = Math.max(
				d3.sum(node.sourceLinks, true_value),
				d3.sum(node.targetLinks, true_value)
			);
		});
	}

	// Iteratively assign the breadth (x-position) for each node.
	// Nodes are assigned the maximum breadth of incoming neighbors plus one;
	// nodes with no incoming links are assigned breadth zero, while
	// nodes with no outgoing links are assigned the maximum breadth.
	function computeNodeBreadths(bimodal) {
		var remainingNodes = nodes,
		nextNodes,
		x = 0;

		nodes.forEach(function(node){
			node.x = node.position;
			node.dx = nodeWidth;
			x = Math.max(x,node.x);
			
		if(bimodal && node.active)
		{
			if(!tx_start || tx_start > (node.x-tx_gap))
			{
				tx_start = node.x-tx_gap;
			}
		}
	});
		/*    while (remainingNodes.length) {
      nextNodes = [];
      remainingNodes.forEach(function(node) {
        node.x = x;
        //console.log("Node: " + node.name + " x set to " + node.x + " in computeNodeBreadths()");
        node.dx = nodeWidth;
        node.sourceLinks.forEach(function(link) {
          nextNodes.push(link.target);
        });
      });
      remainingNodes = nextNodes;
      ++x;
    }

    moveSinksRight(x);*/
		// console.log("size[0]: " + size[0]);
		// console.log("nodeWidth: " + nodeWidth);

		kx = (size[0] - nodeWidth) / (x);
		
		if (bimodal)
		{
			tx_start = kx*tx_start;
			tx_end = tx_start  + kx*tx_gap;
		}
		
		scaleNodeBreadths();
	}

	function moveSourcesRight() {
		nodes.forEach(function(node) {
			if (!node.targetLinks.length) {
				node.x = d3.min(node.sourceLinks, function(d) { return d.target.x; }) - 1;
				//console.log("Node: " + node.name + " x set to " + node.x + " in moveSourcesRight()");

			}
		});
	}

	function moveSinksRight(x) {
		nodes.forEach(function(node) {
			if (!node.sourceLinks.length) {
				node.x = x - 1;
				//console.log("Node: " + node.name + " x set to " + node.x + " in moveSinksRight()");
			}
		});
	}

	function scaleNodeBreadths() {
		nodes.forEach(function(node) {
			node.x *= kx;
			// console.log("Node: " + node.name + " x set to " + node.x + " in scaleNodeBreadths()");
		});
	}

	/**
	 * Utility function I added to try to track appearance of NaN node y values
	 */
	function checkForNaNs() {
		console.log("Checking for NaNs...");
		nodes.forEach(function(node){
			if (isNaN(node.y)) console.log("\t"+node.name+".y is NaN");
		});
	}

	/**
	 * A layout algorithm that tries to make a very clean diagram for our particular
	 * use case.
	 */
	function computeAutomaticNodeDepths(bimodal, active_scale)
	{

		//Group nodes with the same horizontal position

		//Hacky solution: don't set splitGap = 2, so we avoid the issue
		var nodesByBreadth = d3.nest()
			.key(function(d) { return d.x; })
			.sortKeys(comparePosition)
			.entries(nodes)
			.map(function(d) { return d.values; });
		
		//Have to do this because d3.ascending was sorting alphabetically for some reason
		function comparePosition(a,b)
		{
			if (a.x < b.x ) return -1;
			else if (a.x > b.x) return 1;
			else return 0;
		}
		// console.log("nodePadding: " + nodePadding);
		
		//Compute scaling factor, ky, between node value and pixel height
		//Compute one scaling factor for active nodes and one for passive
		//If not in bimodal mode, make these identical, based on global maximum
		
		var pky = d3.min(nodesByBreadth, function(nodes) {
			//If the first node at this x position is active, we assume they all are
			return (bimodal && nodes[0].active) ? Number.POSITIVE_INFINITY : (size[1] - (nodes.length - 1) * nodePadding)/d3.sum(nodes, value);
		});
		
		var aky = d3.min(nodesByBreadth, function(nodes) {
			return (bimodal && nodes[0].passive) ? Number.POSITIVE_INFINITY : active_scale*(size[1] - (nodes.length - 1) * nodePadding)/d3.sum(nodes, value);
		});
		


		//Kind of a wonky layout algorithm designed for our particular use case where
		//we have rumor nodes on the top and debunk nodes on the bottom, and exposure
		//nodes on the left and activity nodes on the right. 
		//Basically try to center nodes on a center line, while keeping them out of
		//the way of links that are trying to go past them. 
		var first = true;
		var centerLine; //Nodes should be centered on this y value
		var upperGap=nodePadding/2; //Upper nodes should be this far above the centerline
		var lowerGap=nodePadding/2; //Lower nodes should be this far below the centerline
		var ky; //the scaling factor to use for each set of nodes
		
		nodesByBreadth.forEach(function(nodes){

			//Center first set of nodes evenly across the height
			if (nodes[0].active) ky = aky;
			else ky = pky;
			
			

			//If this is the first set of nodes in one section of the diagram
			//Currently, this is true for the start node, and later for the tuple of
			// the rumortwt1 and debunktwt1 nodes
			if (first)
			{
				var y = (size[1]-(ky*d3.sum(nodes, value)+((nodes.length - 1) * nodePadding)) )/2;
				nodes.forEach(function(node){
					node.y = y;
					node.dy = ky*node.value;
					node.ky = ky;
					y += node.dy+nodePadding;

				});
				first = false;

				//The way we define the center line is different if it is based on
				//one or two nodes
				if (nodes.length == 1)
				{
					centerLine = nodes[0].y;
					nodes[0].sourceLinks.forEach(function(link){

						if (link.target.upper)
						{
							centerLine += ky*link.value;
						}
					});
				}
				else
				{
					centerLine = nodes[0].y;
					nodes.forEach(function(node){
						if (node.upper)
							centerLine = centerLine +node.dy + nodePadding;
					});
					centerLine -= nodePadding/2;
					
				}
				console.log("Center line: " + centerLine);

			}
			//Compute position of subsequent nodes by the position of their 
			//source node and the relative position of the links connecting them
			else
			{
				/*	    		console.log("non first");
				 */	    		//For each node, let its position be the average of where its source nodes would like it to be
				nodes.forEach(function(node){
					node.dy = ky*node.value;
					node.ky = ky;
					if (node.upper)
					{
						node.dy = ky*node.value;
						node.y = (centerLine - node.dy)/2;
						//console.log(upperGap);
						//console.log(centerLine + " | " + upperGap + " | " + node.dy);
						//Make room for this node's links that are trying to run up the middle of the diagram
						var found = false;
						node.sourceLinks.forEach(function(link){
							if (found)
							{
								upperGap+=ky*link.value;
								//console.log("\tupperGap increased to upperGap to make way for : " + link.source.name + " --> " + link.target.name);
							}
							else
							{
								if (link.target.position == node.position+1)
									found = true;
							}
						});
						//upperGap += nodePadding;
					}
					else if (node.lower)
					{
						node.y = (centerLine + size[1]-node.dy)/2;
						var found = true;
						node.sourceLinks.forEach(function(link){

							if (link.target.position == node.position+1)
								found = false;
							else if (found)
							{
								lowerGap+=ky*link.value;
								//console.log("\tlower gap increased to " + lowerGap + " to make room for link to " + link.target.name);

							}

						});
					}
					//If we find a middle node, reset our gaps 
					else if (node.middle)
					{
						first = true;
						 upperGap=nodePadding/2; 
						 lowerGap=nodePadding/2;

						 node.y = centerLine;
						 node.targetLinks.forEach(function(link){
							 if (link.source.upper)
								 node.y-=ky*link.value;
						 });
					}
				});
			}
		});

/*		nodes.forEach(function(node){
			console.log("Node: " + node.name + " | y: " + node.y + " | dy: " +node.dy); 
		});*/

	}

	function computeNodeDepths(iterations) {
		var nodesByBreadth = d3.nest()
		.key(function(d) { return d.x; })
		.sortKeys(d3.ascending)
		.entries(nodes)
		.map(function(d) { return d.values; });

		console.log("Nodes by breadth");
		console.log(nodesByBreadth);
		//
		initializeNodeDepth();
		resolveCollisions();
		for (var alpha = 1; iterations > 0; --iterations) {
			relaxRightToLeft(alpha *= .99);
			resolveCollisions();
			relaxLeftToRight(alpha);
			resolveCollisions();
		}

		function initializeNodeDepth() {
			var ky = d3.min(nodesByBreadth, function(nodes) {
				return (size[1] - (nodes.length - 1) * nodePadding) / d3.sum(nodes, value);
			});

			nodesByBreadth.forEach(function(nodes) {
				nodes.forEach(function(node, i) {
					node.y = i;
					//console.log("Node: " + node.name + " y set to " + node.y + " in initializeNodeDepth()");
					node.dy = node.value * ky;
					// console.log("Node: " + node.name + " dy set to " + node.dy + " in initializeNodeDepth()");

				});
			});

			nodes.forEach(function(node){
				node.sourceLinks.forEach(function(link) {
					link.dy = link.value * ky;
				});
				node.targetLinks.forEach(function(link) {
					link.dy = link.value * ky;
				});
			});

		}

		function relaxLeftToRight(alpha) {
			nodesByBreadth.forEach(function(nodes, breadth) {
				nodes.forEach(function(node) {
					if (node.targetLinks.length) {
						var y = d3.sum(node.targetLinks, weightedSource) / d3.sum(node.targetLinks, value);
						node.y += (y - center(node)) * alpha;
						if (isNaN(node.y)) node.y =0;

						/*            if (isNaN(node.y))
            {
	            console.log("Node: " + node.name + " y set to " + node.y + " in relaxRightToLeft()");
	            console.log("\tcenter(node): " + center(node));
	            console.log("\talpha: " + alpha);
	            console.log("\ty: " + y);
            }*/
					}
				});
			});

			function weightedSource(link) {
				return center(link.source) * link.value;
			}
		}

		function relaxRightToLeft(alpha) {
			nodesByBreadth.slice().reverse().forEach(function(nodes) {
				nodes.forEach(function(node) {
					if (node.sourceLinks.length) {
						var y = d3.sum(node.sourceLinks, weightedTarget) / d3.sum(node.sourceLinks, value);
						node.y += (y - center(node)) * alpha;
						if (isNaN(node.y)) node.y =0;
						/*            if (isNaN(node.y))
            {
	            console.log("Node: " + node.name + " y set to " + node.y + " in relaxRightToLeft()");
	            console.log("\tcenter(node): " + center(node));
	            console.log("\talpha: " + alpha);
	            console.log("\ty: " + y);
            }*/

					}
				});
			});

			function weightedTarget(link) {
				return center(link.target) * link.value;
			}
		}

		function resolveCollisions() {
			nodesByBreadth.forEach(function(nodes) {
				var node,
				dy,
				y0 = 0,
				n = nodes.length,
				i;

				// Push any overlapping nodes down.
				nodes.sort(ascendingDepth);
				for (i = 0; i < n; ++i) {
					node = nodes[i];
					dy = y0 - node.y;
					if (dy > 0) 
					{
						node.y += dy;
						//console.log("Node: " + node.name + " y set to " + node.y + " in resolveCollisions() 1");

					}
					y0 = node.y + node.dy + nodePadding;
				}

				// If the bottommost node goes outside the bounds, push it back up.
				dy = y0 - nodePadding - size[1];
				if (dy > 0) {
					y0 = node.y -= dy;

					// Push any overlapping nodes back up.
					for (i = n - 2; i >= 0; --i) {
						node = nodes[i];
						dy = node.y + node.dy + nodePadding - y0;
						if (dy > 0) 
						{
							node.y -= dy;
							//console.log("Node: " + node.name + " y set to " + node.y + " in resolveCollisions() 2");

						}
						y0 = node.y;
					}
				}
			});
		}

		function ascendingDepth(a, b) {
			return a.y - b.y;
		}
	}

	/**
	 * Compute y values for the beginning and end of each link. 
	 * outside: if true, try to start at the outside of each node
	 * 	and work our way in toward the middle. Otherwise, start inside and work
	 * 	outward
	 * 
	 * Don't need to specify different behavior for bimodal case, because if 
	 * it isn't bimodal, pdy and ady should be equal
	 */
	function computeLinkDepths() {

		nodes.forEach(function(node) 
		{
			//Compute width of each link end that attaches to this node
			var totalsdy = 0;
			var totaltdy = 0;
			node.sourceLinks.forEach(function(link) {
				link.sdy = node.ky*link.value;
				totalsdy += link.sdy;
			});
			node.targetLinks.forEach(function(link) {
				link.tdy = node.ky*link.value;
				totaltdy += link.tdy;
			});
			
			//Compute how far down from the top of the node the links should begin
			var sy, ty;

			sy = node.dy/2-totalsdy/2;
			ty = node.dy/2-totaltdy/2;


			//Compute where each link should begin
			node.sourceLinks.forEach(function(link) {
				link.sy = sy;
				sy += link.sdy;
			});
			node.targetLinks.forEach(function(link) {
				link.ty = ty;
				ty += link.tdy;
			});
			
		});
	}
	
	function pickdy(link,node)
	{
		if (node.active)
			return link.ady;
		else
			return link.pdy;
	}

	function center(node) {
		/*	  if (isNaN(node.y) || isNaN(node.dy))
	  {
		 console.log("\t\tcenter(): node.y: " + node.y + " | node.dy: " + node.dy); 
	  }*/

		return node.y + node.dy / 2;
	}

	function value(link) {
		return link.value;
	}
	
	function true_value(link) {
		return link.true_value;
	}

	return sankey;
};