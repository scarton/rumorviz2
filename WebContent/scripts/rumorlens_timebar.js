/**
 * This file controls functionality of the timebar/timeline element.
 * 
 * Global variables
 */

	
/**
 * Constructor for an object that represents the paired time bar elements of the app
 */
function TimeBar()
{
	this.margin = { top : 20,
		right : 15,
		bottom : 118,
		left : 60 };
	this.width = 1120 - this.margin.left - this.margin.right;
	this.sel = null; // d3 selection that contains all pieces of this element
	this.rumor_density_css_class = "rarea";
	this.debunk_density_css_class = "carea";
	this.rumor_css_class = "rumor";
	this.rumor_highlight_css_class = "rumor_highlight";
	this.debunk_css_class = "debunk";
	this.debunk_highlight_css_class = "debunk_highlight";
	this.highlight_increase = 15; //When we highlight a tweet on a timebar, we increase its height by this much
	this.density = { // vals will be an array of {date:[val],ry:[val],cy:[val]} objects that represent the density of rumor and correction at a given date
		vals : null,
		min : null,
		max : null,
		y_scale : null };
	this.tweet_selection_px_threshold = 15; //When someone wants to select a tweet, they have to click within this distance of it
	this.css_class = "timebar";
	this.default_brush_fraction =0.125; //brush should span 1/8 the timespan when first spawned
	this.label = { //A text label that describes the time interval being highlighted by the brush
		length: 700,
		height:20,
		sel:null, //Corresponding d3 selection
		default_text:"Click timebar for magnified view",
		css_class: "brush_label"};
	this.delete_button_id = "timespan_delete_button_div"; //DOM id for the div that contains the button that is used to clear the magnified timebar. Defined directly in the html
	this.main_bar = { // Main timebar
		initialized: false,
		visible: false,
		height : 50,
		item_height: 25, //How tall displayed tweet bars ought to be
		sel : null, //d3 selection that contains all pieces of this component
		css_class: "main_bar",
		div_id:"main_bar_div",
		brush : null, //d3 brush function that gets called by this component
		brush_holder: null, //d3 g element that holds brush
		brush_css_class: "main_brush", //DOM/css class
		brush_visible: false,
		x_scale : null,
		y_scale : null,
		x_time_axis : null, 
		x_time_axis_css_class:"main_x_time_axis",
		x_cursor_axis : null,
		x_cursor_axis_css_class:"main_x_cursor_axis",
		item_css_class:"main_bar_item",
		item_holder: null, //A <g> element onto which the items will be drawn
		old_brush_extent:null //Stores brush extent so we can tell if brush moved
		}; 
	
	this.mag_bar = {  // magnified timebar. Structurally similar to main one, but interacts differently with the rest of the app
		initialized: false,
		visible:false,
		height : 50,
		item_height:35,
		sel : null,
		css_class: "mag_bar",
		brush : null, 
		brush_holder: null,
		brush_css_class: "mag_brush",
		brush_visible: false,
		x_scale : null,
		y_scale : null,
		x_time_axis: null,
		x_time_axis_css_class:"mag_x_time_axis",
		x_cursor_axis : null,
		x_cursor_axis_css_class:"mag_x_cursor_axis",
		item_css_class:"mag_bar_item",
		item_holder: null,
		old_brush_extent:null};
}

/**
 * Timebar-related functions
 */


function showTimeBar()
{
	timeBar.sel.attr("visibility","visible");
	
}

/**
 * Generate the d3 elements of the time bar and initialize those parts of
 * it that depend on the loaded data
 */
function initializeTimeBar()
{
	timeBar.sel =
		d3.select('#timebar_div')
			.append('svg')
			.attr('width',timeBar.width + timeBar.margin.right 
				+ timeBar.margin.left)
			.attr('height',timeBar.main_bar.height + timeBar.margin.bottom 
				+ timeBar.mag_bar.height + timeBar.margin.top)
			.attr('class', timeBar.css_class)
			.attr("name","timebar");

	// Main timebar d3 element
	timeBar.main_bar.sel =
		timeBar.sel
/*			.append("div")
			.attr("id",timeBar.main_bar.div_id)*/
			.append('g')
			.attr('transform', 'translate(' + timeBar.margin.left + ',' + 110 + ')')
			.attr('width', timeBar.width + timeBar.margin.right + timeBar.margin.left)
			.attr('height', timeBar.main_bar.height)
			.attr('class', timeBar.main_bar.css_class);
	
	timeBar.main_bar.sel.selectAll('rect.background').remove();

	// Magnified timebar d3 element
	timeBar.mag_bar.sel =
		timeBar.sel
			.append('g')
			.attr('transform','translate(' + timeBar.margin.left + ',' + 25 + ')')
			.attr('width',timeBar.width + timeBar.margin.right + timeBar.margin.left)
			.attr('height',timeBar.mag_bar.height + timeBar.margin.top)
			.attr('class', timeBar.mag_bar.css_class)

	timeBar.label.sel = timeBar.sel
		.append("text")
		.attr("class",timeBar.label.css_class)
		.attr('transform', 'translate(' + (timeBar.width-timeBar.label.length) + ',' + timeBar.label.height + ')')
		.text(timeBar.label.default_text);
	
	console.log("tweets");
	console.log(appData.tweets);
	
	console.log("items");
	console.log(timeBar.main_bar.items);

	initializeMainTimeBar();
	//Not bothering to initialize the magnified timebar till there is some data
	//to initialize it with
}

function initializeMainTimeBar()
{
	
	var startTime = appData.tweets[0].date;
	var endTime = appData.tweets[appData.tweets.length-1].date;

	timeBar.main_bar.x_scale =
		d3.time
		.scale()
		.domain([new Date(startTime - 0.01* (endTime-startTime)),
		         new Date(endTime + 0.01* (endTime-startTime))]) //add a buffer at beginning and end
		.range([ 0, timeBar.width ]);

	// Generate set of rumor and correction densities for times on the timeline
	timeBar.density = extractActivityDensities(appData.tweets, timeBar.main_bar.x_scale);

	timeBar.density.y_scale =
		d3.scale
			.linear()
			.domain([ timeBar.density.min, timeBar.density.max ])
			.range([ timeBar.main_bar.height, 0 ]);

	timeBar.main_bar.x_time_axis =
		d3.svg
			.axis()
			.scale(timeBar.main_bar.x_scale)
			.orient('bottom')
/*			.ticks(
				d3.time.days,
				(timeBar.main_bar.x_scale.domain()[1] - timeBar.main_bar.x_scale.domain()[0]) > 15552e6 ? 2
					: 1)
			.tickFormat(d3.time.format('%b %d, %H:%M'))*/
			.tickSize(6, 0, 0);

	timeBar.main_bar.x_cursor_axis =
		d3.svg
			.axis()
			.scale(timeBar.main_bar.x_scale)
			.orient('top')
			.tickValues([timeBar.main_bar.x_scale.domain()[0]])
			.tickFormat('cursor')
			.tickSize(15, 0, 0);

	timeBar.main_bar.sel
		.append('g')
		.attr('transform', 'translate(0,' + timeBar.main_bar.height + ')')
		.attr('class', timeBar.main_bar.x_time_axis_css_class)
		.call(timeBar.main_bar.x_time_axis);

	timeBar.main_bar.sel
		.append('g')
		.attr('transform', 'translate(0,0.5)')
		.attr('class', timeBar.main_bar.x_cursor_axis_css_class)
		.call(timeBar.main_bar.x_cursor_axis)
		.selectAll('text')
		.attr('dx', 22)
		.attr('dy', 12);

	//Generate time-frequency histograms of rumors and corrections on the main
	//time bar. 
	var rumorHist =
		d3.svg
			.area()
			.interpolate("cardinal")
			.tension(0.5)
			.x(function(d) //the d here is a val object generated by generateActivityDensities()
			{
				return timeBar.main_bar.x_scale(d.x);
			})
			.y0(timeBar.main_bar.height)
			.y1(function(d)
			{
				return timeBar.density.y_scale(d.ry);
			});

	var correctionHist =
		d3.svg
			.area()
			.interpolate("cardinal")
			.tension(0.5)
			.x(function(d)
			{
				return timeBar.main_bar.x_scale(d.x);
			})
			.y0(timeBar.main_bar.height)
			.y1(function(d)
			{
				return timeBar.density.y_scale(d.cy);
			});

	//Attach the histograms to the time bar
/*	timeBar.main_bar.sel
		.append("path")
		.datum(timeBar.density.vals)
		.attr("class", timeBar.rumor_density_css_class)
		.attr("d", rumorHist);

	timeBar.main_bar.sel
		.append("path")
		.datum(timeBar.density.vals)
		.attr("class", timeBar.debunk_density_css_class)
		.attr("d", correctionHist);*/

	timeBar.main_bar.item_holder = timeBar.main_bar.sel
	.append("g")
	.attr("id","main_item_holder");
	updateMainTimeBarItems();
	//bar_items.exit().remove();

	//Invisible hit area so the brush can be repositioned by clicking on the timebar
	timeBar.main_bar.sel
		.append("rect")
		.attr("pointer-events", "painted")
		.attr("width", timeBar.width)
		.attr("height", timeBar.main_bar.height)
		.attr("visibility", "hidden")
		.on("mousedown", function()
		{
			moveBrushToPointerLocation(this);
			updateAppData(timeBar.main_bar.brush.extent());
			showDeleteButton();
			updateTimeBarLabelBasedOnMainBrush();
			updateMagnifiedTimeBarBasedOnMainBrushAndAppData();
			showCentralElementOverlay();
			updateCentralElement(true,false,true);

		});
		//.on('mousemove', displayRelated(true, this)) //Don't think i want this...
		//.on('mouseout', cleanHovered());

	//Create empty brush
	timeBar.main_bar.brush =
		d3.svg
			.brush()
			.x(timeBar.main_bar.x_scale)
			.on("brush", function()
			{
				updateTimeBarLabelBasedOnMainBrush();
				updateAppData(timeBar.main_bar.brush.extent());
				updateMagnifiedTimeBarBasedOnMainBrushAndAppData();
				updateCentralElement(false,false,true);
			})
			.on("brushstart", function()
			{
				updateCentralElement(false,true,false);
				timeBar.main_bar.old_brush_extent = timeBar.main_bar.brush.extent();

			})
			.on("brushend", function()
			{
				updateCentralElement(true,false,false);
				
				if (areExtentsEqual(timeBar.main_bar.brush.extent(),timeBar.main_bar.old_brush_extent))
				{
					selectTweetFromTimeBar(this,timeBar.main_bar.x_scale);
				}
				else
				{
					console.log("main bar extent changed from \n[" + timeBar.main_bar.old_brush_extent + "] to \n[" + timeBar.main_bar.brush.extent() + "] so not selecting a tweet");
				}


				timeBar.main_bar.old_brush_extent = timeBar.main_bar.brush.extent();
				console.log(appData.subStart);
				console.log(appData.subEnd);
				
			});

	
	
	//Attach brush to timebar in brush holder
	timeBar.main_bar.brush_holder = 
		timeBar.main_bar.sel
			.append('g')
			.attr('class', timeBar.main_bar.brush_css_class)
			.attr('bar','bottom')
			.call(timeBar.main_bar.brush)
			.on("mousewheel", function()
			{
				zoomBrush(this,timeBar.main_bar.x_scale);
				updateTimeBarLabelBasedOnMainBrush();
				updateAppData(timeBar.main_bar.brush.extent());
				updateMagnifiedTimeBarBasedOnMainBrushAndAppData();
				updateCentralElement(false,false,true);
			});
			
		timeBar.main_bar.brush_holder	
			.selectAll('rect')
			.attr('y', 1)
			.attr('height', timeBar.main_bar.height - 1);

	timeBar.main_bar.sel.selectAll('rect.background').remove();
	timeBar.main_bar.initialized = true;
}

/**
 * Update the text label that says how much time is being covered by the brush
 */
function updateTimeBarLabelBasedOnMainBrush()
{
	if (timeBar.main_bar.brush.empty())
	{
		timeBar.label.sel.text(timeBar.label.default_text);
	}
	else
	{
		var minExtent = d3.time.second(timeBar.main_bar.brush.extent()[0]);
		var maxExtent = d3.time.second(timeBar.main_bar.brush.extent()[1]);
		var dSpan = (maxExtent.getTime() - minExtent.getTime())/(1000*60*60);
	
		timeBar.label.sel.text("Magnifying "+shortDateFormat(minExtent) + " to " 
			+ shortDateFormat(maxExtent) + " (" + shortFloatFormat(dSpan) 
			+ " hours)");
	}
}

function initializeMagnifiedTimeBar()
{
	console.log("initializing magnified timebar");
	timeBar.mag_bar.x_scale = 
		d3.time.scale().domain(timeBar.main_bar.brush.extent()).range([ 0, timeBar.width ]);
	
	var x_scale = timeBar.mag_bar.x_scale;

	timeBar.mag_bar.x_time_axis =
		d3.svg
			.axis()
			.scale(x_scale)
			.orient("bottom")
			//.ticks(d3.time.days,
			//	(x_scale.domain()[1] - x_scale.domain()[0]) > 15552e6 ? 2 : 1)
			//.tickFormat(d3.time.format('%b %d, %H:%M'))
			.tickSize(6, 0, 0);

	timeBar.mag_bar.x_cursor_axis =
		d3.svg
			.axis()
			.scale(timeBar.mag_bar.x_scale)
			.orient("top")
			.tickValues([timeBar.main_bar.x_scale.domain()[0]]) //Set cursor to earliest possible date
			.tickFormat(d3.time.format("cursor"))
			.tickSize(15, 0, 0);

	timeBar.mag_bar.sel
		.append("g")
		.attr("transform", "translate(0," + timeBar.mag_bar.height + ")")
		.attr("class", timeBar.mag_bar.x_time_axis_css_class)
		//.attr("visibility", "hidden")
		.call(timeBar.mag_bar.x_time_axis);

	timeBar.mag_bar.sel
		.append("g")
		.attr("transform", "translate(0,0.5)")
		.attr("class", timeBar.mag_bar.x_cursor_axis_css_class)
		//.attr("visibility", "hidden")
		.call(timeBar.mag_bar.x_cursor_axis)
		.selectAll("text")
		.attr("dx", 0)
		.attr("dy", 0);

	//We display a subset of main_bar"s items delineated by appData.subStart and
	//appData.subEnd
	timeBar.mag_bar.item_holder =
		timeBar.mag_bar.sel
		.append("g")
		.attr("id","magnified_item_holder");
		
	updateMagnifiedTimeBarItems();

	//mag_bar_items.exit().remove();

/*	timeBar.mag_bar.sel
		.append('rect')
		.attr('pointer-events', 'painted')
		.attr('width', timeBar.width)
		.attr('height', timeBar.mag_bar.height)
		.attr('visibility', 'hidden')
		.on('mousedown', displayTweet)
		.on('mousemove', function()
		{
			displayRelated(false, this);
		})
		.on('mouseout', cleanHovered);*/

	timeBar.mag_bar.brush =
		d3.svg
		.brush()
		.x(x_scale)
		.extent(timeBar.main_bar.brush.extent())
		.on("brush",function()
		{
			//Update everything that needs to change smoothly
			//as the brush brushes
			updateMainBrushBasedOnMagnifiedBrush();
			updateAppData(timeBar.mag_bar.brush.extent());
			updateTimeBarLabelBasedOnMainBrush();
			updateCentralElement(false,false,true);
		})
		.on("brushstart",function(){
			updateCentralElement(false,true,false);
			//Save old extent so we can check if it has changed at the end of the brushing
			timeBar.mag_bar.old_brush_extent = timeBar.mag_bar.brush.extent();})
		.on("brushend",function()
		{
			//If the extent changed, update the magnified timebar. If not,
			//assume user was trying to select a tweet and do that
			updateCentralElement(true,false,false);
			if (! areExtentsEqual(timeBar.mag_bar.brush.extent(),timeBar.mag_bar.old_brush_extent))
			{
				console.log("Extent changed. Update mag bar");
				updateMagnifiedTimeBarBasedOnMainBrushAndAppData();
			}
			else
			{
				console.log("Extent did not change. Select tweet.");
				selectTweetFromTimeBar(this,timeBar.mag_bar.x_scale);
			}
			timeBar.mag_bar.old_brush_extent = timeBar.mag_bar.brush.extent();
		}); 

	timeBar.mag_bar.brush_holder = timeBar.mag_bar.sel
		.append('g')
		.attr('class', timeBar.mag_bar.brush_css_class)
		.attr('bar', 'top')
		.call(timeBar.mag_bar.brush)
/*		.on('mousedown', displayTweet)
		.on('mousemove', function()
		{
			displayRelated(false, this);
		})
		.on('mouseout', cleanHovered)*/
/*		.on("mouseup",function() //When the mouse is released, have the magnified timebar contract 
		{
			updateMagnifiedTimeBarBasedOnMainBrushAndAppData();
		})*/
		//When we zoom on the magnified timebar, treat it as though we were zooming in on the main bar
		.on('mousewheel', function()
		{
			//See comment in zoomBrush() for why we pass the main brush here
			zoomBrush(this,timeBar.mag_bar.x_scale);
			updateTimeBarLabelBasedOnMainBrush();
			updateAppData(timeBar.main_bar.brush.extent());
			updateMagnifiedTimeBarBasedOnMainBrushAndAppData();
			updateCentralElement(false,false,true);
		});
		
	timeBar.mag_bar.brush_holder	
		.selectAll('rect')
		.attr('y', 1)
		.attr('height', timeBar.mag_bar.height - 1);

	timeBar.mag_bar.sel.selectAll('rect.background').remove();
	timeBar.mag_bar.initialized = true;
}

/**
 * Update extent of the main brush to match that of the magnified brush
 */
function updateMainBrushBasedOnMagnifiedBrush()
{
	//console.log("updating main brush");
	timeBar.main_bar.brush.extent(timeBar.mag_bar.brush.extent());
	timeBar.main_bar.brush_holder.call(timeBar.main_bar.brush);
	
}

function updateMainTimeBarItems()
{
	console.log("Updating main bar items");
	// Generate vertical colored tweet ticks and attach them to the timeline
	timeBar.main_bar.item_holder
		.selectAll("."+timeBar.main_bar.item_css_class)
		.remove();
		
	timeBar.main_bar.item_holder
		.selectAll()	
		.data(generateTimeBarItems(timeBar.main_bar))
		.enter()
		.append("path")
		.attr("class", function(d){
			return d.css_class;})
		.attr("d", function(d){
			return d.path;});
	
}

/**
 * Update the magnified timebar. Scale and axes get updated based on the extent
 * of the main brush, and items get updated based on changes to appData that
 * should have been invoked prior to this function being called.
 * 
 * @param minimize
 */
function updateMagnifiedTimeBarBasedOnMainBrushAndAppData()
{

	if (!timeBar.mag_bar.initialized)
	{
		initializeMagnifiedTimeBar();
	}

	//console.log("Updating magnified timebar...");

/*		console.log("Old mag timebar domain: " + timeBar.mag_bar.x_scale.domain());
		console.log("Old main brush extent: " + timeBar.main_bar.brush.extent());
		console.log("Old mag brush extent: " + timeBar.mag_bar.brush.extent());
		console.log("Old mag brush x_scale domain: " + timeBar.mag_bar.brush.x().domain());
		console.log("Old mag brush x_scale range: " + timeBar.mag_bar.brush.x().range());*/

	timeBar.mag_bar.x_scale.domain( timeBar.main_bar.brush.extent());
	
	updateMagnifiedTimeBarItems();
	
	//The two axes both depend internally on timeBar.mag_bar.x_scale, so they
	//Should change on their own, we just need to invoke them again.
	timeBar.mag_bar.sel
		.select("."+timeBar.mag_bar.x_cursor_axis_css_class)
		.call(timeBar.mag_bar.x_cursor_axis);

	timeBar.mag_bar.sel
		.select("." +timeBar.mag_bar.x_time_axis_css_class)
		.call(timeBar.mag_bar.x_time_axis);

	timeBar.mag_bar.brush.extent(timeBar.main_bar.brush.extent());
	timeBar.mag_bar.brush_holder.call(timeBar.mag_bar.brush);
		

	if (!timeBar.mag_bar.visible)
	{
		showMagnifiedTimeBar();
	}
}

/**
 * Update items displayed on the magnified timebar based on appData.subStart and
 * appData.subEnd, as changed by updateAppData
 */
function updateMagnifiedTimeBarItems()
{
	if (timeBar.mag_bar.initialized)
	{
		timeBar.mag_bar.item_holder
			.selectAll("."+timeBar.mag_bar.item_css_class)
			.remove();
		
		timeBar.mag_bar.item_holder
			.selectAll()
			.data(generateTimeBarItems(timeBar.mag_bar))
			.enter()
			.append("path")
			.attr("class", function(d){
				return d.css_class;})
			.attr("d", function(d){
				return d.path;});
	}
}

/**
 * Show the button that clears the magnified timebar and brush, so these actions
 * become available
 */
function showDeleteButton()
{
	d3.select("#"+timeBar.delete_button_id).style("visibility","visible");
}

/**
 * Show the button that clears the magnified timebar and brush, so these actions
 * become available
 */
function hideDeleteButton()
{
	d3.select("#"+timeBar.delete_button_id).style("visibility","hidden");
}


/**
 * Hide the magnified timebar, set the main brush to empty, and hide the delete 
 * button
 */
function clearBrushAndMagnifiedTimeBar()
{
	hideDeleteButton();
	hideMagnifiedTimeBar();
	clearMainBrush();
	clearSubValues();
	updateTimeBarLabelBasedOnMainBrush();
	//hideCentralElementOverlay();
	//console.log("Substart: " + appData.subStart);
	updateCentralElement(true,false,false);
}

function clearMainBrush()
{
	timeBar.main_bar.brush.clear();
	timeBar.main_bar.brush_holder.call(timeBar.main_bar.brush);
	timeBar.main_bar.brush_visible = false;
}


function hideMagnifiedTimeBar()
{
	timeBar.mag_bar.sel.style("visibility","hidden");
	timeBar.mag_bar.visible = false;
}

function showMagnifiedTimeBar()
{
	console.log("Showing magnified timebar...");
	timeBar.mag_bar.sel.style("visibility","visible");
	timeBar.mag_bar.visible = true;
}


/**
 * Take a set of tweets and generate a set of activity density levels for rumors and corrections at regular intervals along the span of the timeline
 * 
 * x_scale is a d3 time scale between the beginning and end of the timeline
 * 
 * Operates on a vector of items that look like this (from collapseLanes() in dataModule.js): { id: item.id, lane: laneId, start: item.start, end: item.end,
 * class: classes[item.Type-1], desc: item.desc, Author: item.Author }
 * 
 * Returns a struct that looks like this: { max min densities }
 * 
 * Where max and min are max and min density found, and densities is a vector of objects that look like this: { x ry cy }
 * 
 * @param tweets: should be appData.tweets 
 */
function extractActivityDensities(tweets, x_scale)
{
	var densities = [];

	var numSamples = 100; // Will sample at evenly spaced points along the timeline
	console.log("Start: " + x_scale.domain()[0] + " | End: "
		+ x_scale.domain()[1]);
	var span = x_scale.domain()[1] - x_scale.domain()[0];
	//console.log("Span (days): " + span / 1000 / 24 / 60 / 60);
	// alert('Span: ' + span);
	var interval = span / numSamples;
	//console.log("Sample interval (hours): " + interval / 1000 / 60 / 60);
	var hourlength = 60 * 60 * 1000;
	var densityinterval = hourlength; // We report tweets-per-hour
	var window = interval; //3.0 * hourlength; // We examine one hour-long window at a time
	//console.log("Sample window (hours)" + window / 1000 / 60 / 60);
	var halfwindow = window / 2;
	var rend = 0, cend = 0;
	var rstart = 0, cstart = 0;
	var rdensity = 0, cdensity = 0;
	var rcount = 0, ccount = 0;
	var max = 0.0001;
	var min = 0;
	for (var time = x_scale.domain()[0].getTime(); time <= x_scale.domain()[1]
		.getTime(); time += interval)
	{
		// Move the rend cursor to the first rumor tweet that is outside the rightmost edge of the window
		while (rend < tweets.length
			&& (tweets[rend].date < (time + halfwindow) || tweets[rend].type != 1))
		{
			// console.log("Class: " + items[rend].class + " class == 'rumor'?" + (items[rend].class == "rumor"));
			// console.log("\tType: "+ items[rend].type);
			if (tweets[rend].type == 1)
			{
				rcount++;
			}
			rend++;
		}
		// Move rstart cursor to first rumor tweet that is inside the leftmost edge of the window
		while (rstart < tweets.length
			&& (tweets[rstart].date < (time - halfwindow) || tweets[rstart].type != 1))
		{
			if (tweets[rstart].type == 1)
			{
				rcount--;
			}
			rstart++;
		}
		// console.log("rstart: " + rstart + " | rend: " + rend);

		rdensity = rcount;
		// console.log("rcount:" + rcount);

		rdensity =(rcount)/ (Math.min(halfwindow, time - 
			x_scale.domain()[0].getTime()) + 
			Math.min(halfwindow, x_scale.domain()[1].getTime() - time))
			* densityinterval;
		
		rdensity = Math.log(rdensity + 1);
		
		if (rdensity > max)
			max = rdensity;
		else if (rdensity < min)
			min = rdensity;

		// Move the cend cursor to the first debunk tweet that is outside the rightmost edge of the window
		while (cend < tweets.length
			&& (tweets[cend].date < (time + halfwindow) || tweets[cend].type != 2))
		{
			if (tweets[cend].type == 2)
			{
				ccount++;
			}
			cend++;
		}
		// Move cstart cursor to first debunk tweet that is inside the leftmost edge of the window
		while (cstart < tweets.length
			&& (tweets[cstart].date < (time - halfwindow) || tweets[cstart].type != 2))
		{
			if (tweets[cstart].type == 2)
			{
				ccount--;
			}
			cstart++;
		}

		cdensity = ccount;
		// console.log("ccount:" + ccount);
		cdensity =
			(ccount)
				/ (Math.min(halfwindow, time - x_scale.domain()[0].getTime()) + Math
					.min(halfwindow, x_scale.domain()[1].getTime() - time))
				* densityinterval;
		cdensity = Math.log(cdensity + 1);

		if (cdensity > max)
			max = cdensity;
		else if (cdensity < min)
			min = cdensity;

		densities.push({ x : new Date(time),
			ry : rdensity,
			cy : cdensity });

	}
	return { max : max,
		min : min,
		vals : densities,
		y_scale : null };
}

/**
 * Change the extent of the brush in response to the user zooming with the scroll
 * wheel
 * @param drawnBrush: brush object created by call(brush)
 * @param x_scale: x scale of calling object
 */

function zoomBrush(drawnBrush, x_scale)
{
	
	var brush = timeBar.main_bar.brush;
	var baseDelta = 15000; // Base zoom speed (ms)
	var minDist = 1000; // How close the zoom can come to the location of the mouse
	// pointer.

	d3.event.preventDefault();

	//console.log(drawnBrush);
	var origin = d3.mouse(drawnBrush);
	console.log("old extent: " + brush.extent()[0] + " to " + brush.extent()[1]);

	var point= (x_scale.invert(origin[0])).getTime();
	var leftExtent= (brush.extent()[0]).getTime();
	var rightExtent= (brush.extent()[1]).getTime();
	var leftLimit= timeBar.main_bar.x_scale.domain()[0].getTime();
	var rightLimit= timeBar.main_bar.x_scale.domain()[1].getTime();

	var span = rightExtent - leftExtent;

	// zoom the brush such that the point the mouse is hovering over stays in
	// the middle
	var newLeftExtent;
	var newRightExtent;

	newLeftExtent =
		leftExtent + d3.event.wheelDelta * baseDelta
			* ((point - leftExtent) / span);
	newRightExtent =
		rightExtent - d3.event.wheelDelta * baseDelta
			* ((rightExtent - point) / span);

	
	// Make sure the brush isn't going outside the bounds of the time-bar, or
	// collapsing to a point
	if (leftLimit <= (newLeftExtent - minDist)
		&& (point >= (newLeftExtent + minDist))
		&& (rightLimit >= (newRightExtent + 1000))
		&& (point <= (newRightExtent - 1000)))
	{
		console.log("new extent: " + newLeftExtent + " to " + newRightExtent);
		brush.extent([ new Date(newLeftExtent), new Date(newRightExtent) ]);
		
		//If the call came in from the magnified brush, we actually change the
		//extent of the main brush and then update the magnified brush in a different
		//function. In this case, we don't want to have brush_holder call brush,
		//lest we attach the main brush to the magnified bar. 
		
		timeBar.main_bar.brush_holder.call(brush);
	}

}

/**
 * Move the brush on the main timebar so that it is centered wherever the mouse
 * clicked. 
 */
function moveBrushToPointerLocation(container)
{
	console.log("moveBrushToPointerLocation()");
	var origin = d3.mouse(container);
	var point = timeBar.main_bar.x_scale.invert(origin[0]);

	var halfExtent;
	//Default to a 12-hour span. Otherwise, keep current width
	if (timeBar.main_bar.brush.empty())
	{
		halfExtent = timeBar.default_brush_fraction * (timeBar.main_bar.x_scale.domain()[1] - 
			timeBar.main_bar.x_scale.domain()[0])/2;
	}
	else
	{
		halfExtent = (timeBar.main_bar.brush.extent()[1] - 
			timeBar.main_bar.brush.extent()[0])/2;
	}
	var start = new Date(point.getTime() - halfExtent);
	var end = new Date(point.getTime() + halfExtent);


	if (start < timeBar.main_bar.x_scale.domain()[0])
	{
		start = timeBar.main_bar.x_scale.domain()[0];
	}

	if (end > timeBar.main_bar.x_scale.domain()[1])
	{
		end = timeBar.main_bar.x_scale.domain()[1];
	}

	timeBar.main_bar.brush_holder.call(timeBar.main_bar.brush.extent([ start, end ]));
}

function showMainBrush()
{
	timeBar.main_bar.brush_holder.style("visibility","visible");
	timeBar.main_bar.brush_visible = true;
}




/**
 * Generates items that will be drawn onto a timebar. Generates these items
 * by looking at appData.tweets and appData.highlight_tweets
 * @param bar: either timeBar.main_bar or timeBar.mag_bar
 */
function generateTimeBarItems(bar)
{
	//Figure out subset of tweets we will generate items for
	var startIndex, endIndex;
	if (bar == timeBar.mag_bar)
	{
		startIndex = appData.subStart;
		endIndex = appData.subEnd;
		console.log("Generating magnified bar items: " + startIndex + " to " + endIndex);
		
	}
	else
	{
		startIndex =0;
		endIndex = appData.tweets.length-1;
		console.log("Generating main bar items: " + startIndex + " to " + endIndex);
	}
	
	if (startIndex < 0 || endIndex < 0) return [];

	var result = new Array(endIndex-startIndex+1);
	console.log("\tAllocated " + result.length + " elements to result");
	var tweet;
	var x_scale;
	var date;
	var offset = (bar.height-bar.item_height)/2; //Item offset from top of bar
	var increase = timeBar.highlight_increase/2;
	
	for (var i = startIndex; i <= endIndex; i++)
	{
		tweet = appData.tweets[i];
		x_scale = bar.x_scale;
		date = new Date(tweet.date);
		
		var pathString; //SVG path data string we'll associate with this item
		var css_class = bar.item_css_class + " " + ((tweet.type == 1) ? timeBar.rumor_css_class:timeBar.debunk_css_class);
		
		//If we're supposed to highlight this tweet, 
		if (appData.highlight_tweets[i])
		{
			pathString = "M"+x_scale(date)+","+(offset -increase)+" "
				+"L"+x_scale(date)+","+(offset+bar.item_height+increase);
			css_class = chooseItemCSSClass(bar,tweet,true);
			console.log("Found a tweet to highlight!");
		}
		else
		{
			pathString = "M"+x_scale(date)+","+offset +" "
				+"L"+x_scale(date)+","+(offset+bar.item_height);
			css_class = chooseItemCSSClass(bar,tweet,false);
		}
		result[i-startIndex] = {path:pathString,
			css_class:css_class, 
			index:i};
		
	}
	
	console.log("\t"+result.length+" items generated");
	return result;
}

function chooseItemCSSClass(bar,tweet,highlighted)
{
	var secondClass;
	if (tweet.type == 1)
	{
		if (highlighted)
			secondClass = timeBar.rumor_highlight_css_class;
		else
			secondClass = timeBar.rumor_css_class;
	}
	else
	{
		if (highlighted)
			secondClass = timeBar.debunk_highlight_css_class;
		else
			secondClass = timeBar.debunk_css_class;
	}
	
	return bar.item_css_class+ " " + secondClass;
}

function endsWith(str, suffix)
{
	return str.indexOf(suffix, str.length - suffix.length) !== -1;
}


function areExtentsEqual(arr1,arr2)
{
	if (arr1.length != arr2.length) return false;
	for (var i = 0; i < arr1.length; i++)
	{
		if (arr1[i].getTime() !== arr2[i].getTime()) return false;
	}
	return true;
}

/**
 * This function uses the given x scale to convert the mouse click on the container
 * to a date, and then finds the closest tweet to that date, which it then
 * displays more information about. 
 * 
 * If the selected date is too far from the closest tweet, it displays nothing
 * @param container
 * @param x_scale
 */
function selectTweetFromTimeBar(container, x_scale)
{
	console.log("selectTweet()");
	var origin = d3.mouse(container);
	console.log("Click detected at location: " + origin);
	var date = x_scale.invert(origin[0]);
	console.log("Selecting tweet closest to " + date);
	
	var tweetIndex = findTweetClosestToDate(date);
	
	console.log("Closest tweet is at index: " + tweetIndex);
	var tweetDate = new Date(appData.tweets[tweetIndex].date);
	
	//If closest tweet is not within given pixel distance of click,
	//act as though user selected nothing
	if (Math.abs(x_scale(tweetDate)-origin[0]) > timeBar.tweet_selection_px_threshold)
		tweetIndex = -1;
	
	displayAndHighlightTweets(tweetIndex);

	
}

/**
 * This function moves the each timebar's cursor to the given date. By the time
 * this function gets called, updateMagnifiedTimebarBasedOnMainTimeBar() should
 * already have been called, so the date should be available on both bars.
 * 
 * @param date
 */
function moveTimeBarCursorsToTweetIndex(index)
{
	
	var date;
	if (index == -1) 
		date = timeBar.main_bar.x_scale.domain()[0];
	else
		date = new Date(appData.tweets[index].date);

	timeBar.main_bar.x_cursor_axis.tickValues([date]);
	timeBar.main_bar.sel
		.select("." +timeBar.main_bar.x_cursor_axis_css_class)
		.call(timeBar.main_bar.x_cursor_axis);
	
	if (timeBar.mag_bar.initialized)
	{
		timeBar.mag_bar.x_cursor_axis.tickValues([date]);
		timeBar.mag_bar.sel
			.select("." +timeBar.mag_bar.x_cursor_axis_css_class)
			.call(timeBar.mag_bar.x_cursor_axis);
	}
	
}


/**
 * Updates both timebars to reflect changes to appData. In practice, the only
 * change to appData that warrants an update to both bars is a change to
 * appData.highlight_tweets, in which case the items on both bars need to be
 * redrawn
 */
function updateBothTimeBarsItems()
{
	updateMagnifiedTimeBarItems();
	updateMainTimeBarItems();
}
