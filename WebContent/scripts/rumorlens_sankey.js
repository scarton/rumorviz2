/**
 * This script holds variables and functions that relate to our particular 
 * sankey diagram, as opposed to d3_sankey.js which contains code governing
 * the basic structure of sankey diagrams generally
 */

/**
 * Global variables
 */


/**
 * Constructor for an object that represents a Sankey diagram. mode should
 * either be "active" or "passive" to represent the two current kinds of 
 * diagram.
 */
function SankeyDiagram(mode, totalWidth, totalHeight, nodeWidth, nodePadding)
{
	this.sankey = null;
	this.initialized = false;
	this.node_width = nodeWidth;
	this.node_padding = nodePadding;
	
	this.nodes = states;
	this.links = [];
	this.strokeWidth = {
		dy : [],
		value : [] };
	
	this.sel = null;
	this.outer_sel = null;
	
	this.margin = {top: 70,left:80,right:60};
	this.total_width = totalWidth;
	this.total_height = totalHeight;
	this.width = totalWidth-this.margin.left -this.margin.right;
	this.height = totalHeight-this.margin.top;
	this.link_holder = null;
	this.overlay_link_holder = null;
	this.selected_link_holder = null;
	this.link_css_class = "link"; //DOM class for grey total cumulative state-to-state links
	this.node_holder = null;
	this.node_css_class = "node"; //DOM class for colored rectangular nodes
	this.empty_css_class = "empty"; //DOM class for empty links, so we can visually distinguish them 
	this.overlay_link_css_class = "overlay_link";
	this.selected_link_css_class = "selected_link";
	this.active_scale = 0.8,
	this.css_class = "sankey";
	this.title_css_class = "title";
	this.title = "Sankey Diagram";
	if (mode == appMode.PASSIVE)
	{
		this.mode_css_class = "passive_sankey";
	}
	else if (mode == appMode.ACTIVE)
	{
		this.mode_css_class = "active_sankey";
	}
	else if (mode == appMode.COMBINED)
	{
		this.mode_css_class = "combined_sankey";
		this.region_css_class = "region";
		this.axis_css_class = "axis";
		this.divider_css_class = "divider";
	}
	this.tooltip_css_class = "tooltip";
	
	//Each node gets assigned two of these to determine its appearance
	this.rumor_css_class = "rumor";
	this.debunk_css_class = "debunk";
	this.both_css_class = "both";
	this.passive_css_class = "passive";
	this.active_css_class = "active";
	this.start_css_class = "start";
}

/**
 * Initialize the passive sankey diagram with the total cumulative impact of the
 * tweets
 */
function initializePassiveSankeyDiagram()
{
	initializeSankeyDiagram(appMode.PASSIVE);	
}

function initializeActiveSankeyDiagram()
{
	initializeSankeyDiagram(appMode.ACTIVE);
}

function initializeCombinedSankeyDiagram()
{
	initializeSankeyDiagram(appMode.COMBINED);
}

/**
 * Lay out the basic elements of the sankey diagram. These elements include:
 * 
 * a colored rectangle for each distinct state that flows can flow into or out of
 * a flow made of two svg cubic splines for each state-to-state transition, 
   	representing cumulative overall movement between states
 * a subflow for each transition representing cumulative movement during the
   	selected timespan, if any
 * a subflow for each transition representing movement caused by a single 
   	selected tweet, if any 
 * 
 * @param mode
 */
function initializeSankeyDiagram(mode)
{
	var sankeyDiagram;
	if (mode == appMode.PASSIVE)
		sankeyDiagram = passiveSankey;
	else if (mode == appMode.ACTIVE)
		sankeyDiagram = activeSankey;
	else if (mode == appMode.COMBINED)
		sankeyDiagram = combinedSankey;
	
	
	console.log("Initializing "+mode+" sankey diagram");
	sankeyDiagram.sankey = d3.sankey()
		.nodeWidth(sankeyDiagram.node_width)
		.nodePadding(sankeyDiagram.node_padding)
		.size([sankeyDiagram.width, sankeyDiagram.height]);
	
	//Convert set of states and state transitions (and counts) to a form recognized by the 
	//D3 Sankey Diagram plugin
	var nodes;
	var links;
	//Cut down the set of nodes and links for the passive diagram
	if (mode == appMode.COMBINED)
	{
		//The combined diagram is too busy if we try to include everything, so use a reduced set of nodes and links
		//that drops all nodes for multiple exposure or tweeting
		nodes = statesToReducedSankeyNodes(states);
		links = transitionsToReducedSankeyLinks(transitions,stateNameIndexMap,appData.cumulativeCombinedChanges);
		
		console.log("nodes");
		console.log(nodes);
		console.log("links");
		console.log(links);
	}
	else if (mode == appMode.ACTIVE)
	{
		nodes = statesToSankeyNodes(states);
		links = transitionsToSankeyLinks(transitions,stateNameIndexMap,appData.cumulativeActiveChanges);
	}
	else if (mode == appMode.PASSIVE)
	{
		//We only want to see passive nodes that have to do with exposure, which is the first 8
		nodes = statesToSankeyNodes(states);
		nodes = nodes.slice(0,8);
		links = transitionsToSankeyLinks(transitions,stateNameIndexMap,appData.cumulativePassiveChanges);
		links = links.filter(function(link){
			return (link.source < 8 && link.target < 8);
		});
	}
	
	//call d3_sankey code that determines the layout of the whole diagram
	sankeyDiagram.sankey
		.nodes(nodes)
		.links(links)
		.layout(true, (mode == appMode.COMBINED), sankeyDiagram.active_scale); //Lay the links out differently if it is a passive diagram


	/*console.log("nodes");
	console.log(nodes);
	console.log("links");
	console.log(links);*/

	sankeyDiagram.sel = centralDiv.append("svg")
	.attr("id","sankey_outer_svg")
	.style("position", "absolute") // This is kinda hacky, but whatever
	.style("left", "0px")
	.style("top", "0px")
	.attr("height",sankeyDiagram.total_height)
	.attr("width",sankeyDiagram.total_width);
	
	sankeyDiagram.inner_sel = sankeyDiagram.sel.append("g")
		.attr("name","#sankey_inner_g")
		.attr("height",sankeyDiagram.height)
		.attr("width",sankeyDiagram.width)
		.attr("transform","translate("+sankeyDiagram.margin.left+","+sankeyDiagram.margin.top+")");

	//Create large title text element
	sankeyDiagram.inner_sel
		.append("text")
		.attr("class", sankeyDiagram.css_class + " " + sankeyDiagram.title_css_class)
		//.style("position", "absolute")
/*		.attr("x", 10)
		.attr("y", 10)*/
		.attr("dy",0)
		.text(sankeyDiagram.title)
		.call(wrapTextElement,500)
		.attr("transform","translate(20,-35)");
		/*.style("position", "absolute")
		.style("width", "500px")
		.style("top", "35px")
		.style("left", "15px");*/
	
	//If we're doing a combined Sankey diagram, append a colored background area
	//and a gradient area that together indicate the change of scale from
	//the passive node region to the active node region
	
	if (mode == appMode.COMBINED)
	{

		sankeyDiagram.inner_sel.append("line")
			.attr("x1",sankeyDiagram.sankey.tx_start())
			.attr("y1",0)
			.attr("x2",sankeyDiagram.sankey.tx_start())
			.attr("y2",sankeyDiagram.height)
			.attr("class",sankeyDiagram.css_class+" "+sankeyDiagram.divider_css_class);
		
		var pyScale = d3.scale.linear()
			.domain([0,nodes[0].true_value])
			.range([sankeyDiagram.height-sankeyDiagram.node_padding/2,sankeyDiagram.node_padding/2]);
		console.log(pyScale.domain())
		console.log(pyScale.range());
		var pyAxis = d3.svg.axis().scale(pyScale).orient("right");

		
		sankeyDiagram.inner_sel.append("rect")
			.attr("x",0)
			.attr("y",0)
			.attr("height",sankeyDiagram.height)
			.attr("width",(sankeyDiagram.sankey.tx_start()))
			.attr("class",sankeyDiagram.css_class + " " +sankeyDiagram.passive_css_class +" "+ sankeyDiagram.region_css_class)
		
		var axis_sel =sankeyDiagram.inner_sel
			.append("g")
			.attr("class", sankeyDiagram.css_class+ " " + sankeyDiagram.axis_css_class)
			.attr("transform","translate(-75,0)");
		
		axis_sel.call(pyAxis);

/*		axis_sel.selectAll("g")
			.enter()
			.append("rect")
			.attr("x",0)
			.attr("y",0)
			.attr("height",10)
			.attr("width",10)
			.exit();*/

		axis_sel.append("text")
			.text("Number of people exposed")
			.attr("dy","0")
			.attr("transform","translate(0,-15)")
			.call(wrapTextElement, 100);
		
		var aOffset = (1-sankeyDiagram.active_scale)*sankeyDiagram.height/2-sankeyDiagram.node_padding/2;
		

		var ayScale = d3.scale.linear()
			.domain([0,nodes[4].value + nodes[5].value])
			.range([sankeyDiagram.height-aOffset,aOffset]);

		var ayAxis = d3.svg.axis().scale(ayScale).orient("left");
		
		sankeyDiagram.inner_sel.append("rect")
			.attr("x",sankeyDiagram.sankey.tx_start())
			.attr("y",0)
			.attr("height",sankeyDiagram.height)
			.attr("width",(sankeyDiagram.width - sankeyDiagram.sankey.tx_start() + sankeyDiagram.margin.right))
			.attr("class",sankeyDiagram.css_class + " " +sankeyDiagram.active_css_class +" "+ sankeyDiagram.region_css_class)

		sankeyDiagram.inner_sel
			.append("g")
			.attr("class", sankeyDiagram.css_class+ " " + sankeyDiagram.axis_css_class)
			.attr("transform","translate("+(sankeyDiagram.width+55)+",0)")
			.call(ayAxis)
			.append("text")
			.attr("text-anchor","end")
			.text("Number of people who tweeted")
			.attr("transform","translate(0,30)")
			.attr("dy","0")
			.call(wrapTextElement, 100);


	}
	
	
	
	
	//Construct tooltip for all kinds of sankey links
/*	var link_tip = generateLinkToolTip(sankeyDiagram,"cumulative");
*/	
	//Construct d3 elements that hold grey flows
	sankeyDiagram.link_holder = sankeyDiagram.inner_sel
		.append("g");
		
	sankeyDiagram.link_holder
		.selectAll()
		.data(links)
		.enter()
		.append("path")
		.attr("class", function(d) {
			if (d.value > 0)
				return sankeyDiagram.css_class + " " +sankeyDiagram.link_css_class; 
			else
				return sankeyDiagram.css_class + " " +sankeyDiagram.link_css_class + " " + sankeyDiagram.empty_css_class;
			})
		.attr("d", function(d,i){return sankeyDiagram.sankey.bimodalLinkArea(d,1);})
/*		.on("mouseover", link_tip.show)
		.on("mouseout", link_tip.hide)
		.call(link_tip);*/
		.append("title")
		.html(function(d) { 
			//return d.source.name + " --> " + d.target.name + "\nOverall: " + format(d.true_value); 
			return generateSankeyLinkTitle(d,false,false);
		});
	
	//Create tooltip for state rectangles
/*	var node_tip = d3.tip()
	.attr("class", sankeyDiagram.css_class + " " + sankeyDiagram.tooltip_css_class)
	.offset([ -10, 0 ])
	.html(function(d){
		//d will be an element of subGraph.baseVals
		return generateSankeyNodeTitle(d);});*/
	
	//Construct D3 elements for colored state rectangles
	sankeyDiagram.node_holder = sankeyDiagram.inner_sel
		.append("g");
		
	var node_elements = sankeyDiagram.node_holder
		.selectAll("."+sankeyDiagram.css_class+"." + sankeyDiagram.node_css_class)
		.data(sankeyDiagram.sankey.nodes())
		.enter()
		.append("g")
		.attr("class", function(d){return chooseSankeyNodeClass(sankeyDiagram,d);} )
		.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
	
	node_elements
		.append("rect")
		.attr("height", function(d) { return d.dy; })
		.attr("width", sankeyDiagram.sankey.nodeWidth())
/*		.on("mouseover", node_tip.show)
		.on("mouseout", node_tip.hide)
		.call(node_tip);*/
		.append("title")
		.html(function(d) { 
			//return d.name + "\n" + format(d.true_value); 
			return generateSankeyNodeTitle(d);
			});
		
	node_elements
		.append("text")
		.attr("x", function(d) { return -d.dy / 2 + 2.5*d.desc.length; })//assume 5 pixels per character
		.attr("y", function(d,i){
			if (i== 0)
				return -10;
			else
				return sankeyDiagram.node_width+10;
		})
		.attr("transform","rotate(-90)")
		.attr("dy", ".35em")
		.attr("text-anchor", "end")
		.text(function(d) { return d.desc; })
		//.filter(function(d) { return d.x < sankeyDiagram.width / 2; })
		//.attr("x", 6 + sankeyDiagram.sankey.nodeWidth())
		//.attr("text-anchor", "start");

	
	
	//Construct d3 elements for black overlays
/*	var overlay_link_tip = generateLinkToolTip(sankeyDiagram,"in timespan");
*/
	sankeyDiagram.overlay_link_holder = sankeyDiagram.inner_sel.append("g")
		.style("visibility", "hidden");
		
	sankeyDiagram.overlay_link_holder
		.selectAll()
		.data(links)
		.enter()
		.append("path")
		.attr("class", sankeyDiagram.css_class + " " + sankeyDiagram.overlay_link_css_class)
		.attr("d",function(d,i){return sankeyDiagram.sankey.bimodalLinkArea(d,0);})
/*		.on("mouseover", overlay_link_tip.show)
		.on("mouseout", overlay_link_tip.hide)
		.call(overlay_link_tip);*/
		.append("title")
		.html(function(d) { 
			return generateSankeyLinkTitle(d,true,false);
			});
	
	//construct d3 elements for colored single-element overlays
/*	var highlight_link_tip = generateLinkToolTip(sankeyDiagram,"highlighted");
*/
	sankeyDiagram.selected_link_holder = sankeyDiagram.inner_sel.append("g")
		.style("visibility", "hidden");
	
	sankeyDiagram.selected_link_holder
		.selectAll()
		.data(links)
		.enter()
		.append("path")
		.attr("class", function (d){
			if (d.rumor)
				return sankeyDiagram.css_class + " " + sankeyDiagram.selected_link_css_class+ " " + sankeyDiagram.rumor_css_class;
			else
				return sankeyDiagram.css_class + " " + sankeyDiagram.selected_link_css_class + " " + sankeyDiagram.debunk_css_class;
			})
		.attr("d",function(d,i){return sankeyDiagram.sankey.bimodalLinkArea(d,0);})
/*		.on("mouseover", highlight_link_tip.show)
		.on("mouseout", highlight_link_tip.hide)
		.call(highlight_link_tip);*/
		.append("title")
		.html(function(d) { 
			return generateSankeyLinkTitle(d,false,true);
			});

	sankeyDiagram.initialized = true;
}

/**
 * Select every overlay link and set its width equal to the base width times
 * some fraction based on the current value of appData.subCumulative[type]Changes, 
 * as updated by updateAppaData
 */
function updatePassiveSankeyDiagram()
{
	updateSankeyDiagram(appMode.PASSIVE);
}

function updateActiveSankeyDiagram()
{
	updateSankeyDiagram(appMode.ACTIVE);
}

function updateCombinedSankeyDiagram()
{
	updateSankeyDiagram(appMode.COMBINED);
}


function updateSankeyDiagram(mode)
{
	console.log("Updating " + mode + " Sankey diagram");

	var sankeyDiagram;

	if (mode == appMode.PASSIVE)
		sankeyDiagram = passiveSankey;
	else if (mode == appMode.ACTIVE)
		sankeyDiagram = activeSankey;
	else if (mode == appMode.COMBINED)
		sankeyDiagram = combinedSankey;
	else
		sankeyDiagram = null; //this should not ever happen
		
	if (!sankeyDiagram.initialized)
	{
		initializeSankeyDiagram(mode);
	}
	
	console.log(sankeyDiagram);
	//Redraw the black overlay elements to match the current value of the cumulative
	//state change subvalues calculated in updateAppData()
	sankeyDiagram.inner_sel.selectAll("."+sankeyDiagram.css_class+"."+sankeyDiagram.overlay_link_css_class)
		.attr("d",function(d){//Note that this selection does not occur in any particular order
			//return "M 100 100 L 300 "+(100+10*i)+" L 200 300 z";
			var change = d.value;
			var subChange = pickSubchange(mode,d);
			d.sub_value = subChange;
			
			var scale;
			if (change == 0)
				scale = 0;
			else
				scale = subChange/change;
			d.scale = scale;
			//console.log(d);
/*			console.log("\t"+d.source.name + " --> " + d.target.name + " scaled to " + (100*scale) + "%");
			console.log("\t\t"+transitions[d.index].name);
			console.log("\t\tChange: " + change + " | sub change: " + subChange);
			console.log("\t\tPassive change: " + appData.cumulativePassiveChanges[d.index] + " | passive sub change: " + appData.subCumulativePassiveChanges[d.index]);
			console.log("\t\tActive change: " + appData.cumulativeActiveChanges[d.index] + " | active sub change: " + appData.subCumulativeActiveChanges[d.index]);
			console.log("\t\tCombined change: " + appData.cumulativeCombinedChanges[d.index] + " | combined sub change: " + appData.subCumulativeCombinedChanges[d.index]);
*/
			return sankeyDiagram.sankey.bimodalLinkArea(d,scale);
		})
		.select("title")
		.html(function(d) { 
			//return d.source.name + " --> " + d.target.name + "\nHighlighted: "+format(d.scale*d.true_value);
			return generateSankeyLinkTitle(d,true,false);
			});;
		  //.attr("d", function(d,i){return sankeyDiagram.sankey.linkArea(d3.sankey.links()[i],subCumulativeChanges[i]/cumulativeChanges[i]);})
	
	console.log ("\nDrawing selection overlay\n");
		//Redraw the black overlay elements to match the current value of the cumulative
		//state change subvalues calculated in updateAppData()
	sankeyDiagram.inner_sel.selectAll("."+sankeyDiagram.css_class+"."+sankeyDiagram.selected_link_css_class)
		.attr("d",function(d){//Note that this selection does not occur in any particular order
			//return "M 100 100 L 300 "+(100+10*i)+" L 200 300 z";
			var change = d.value;
			var selectedChange = pickSelectedChange(mode,d);
			d.selected_value = selectedChange;
			var scale;
			if (change == 0)
				scale = 0;
			else
				scale = selectedChange/change;
			d.scale = scale;
			//console.log(d);
			/*console.log("\t"+d.source.name + " --> " + d.target.name + " scaled to " + (100*scale) + "%");
			console.log("\t\t"+transitions[d.index].name);
			console.log("\t\tChange: " + change + " | sub change: " + selectedChange);
			console.log("\t\tPassive change: " + appData.cumulativePassiveChanges[d.index] + " | passive sub change: " + appData.subCumulativePassiveChanges[d.index]);
			console.log("\t\tActive change: " + appData.cumulativeActiveChanges[d.index] + " | active sub change: " + appData.subCumulativeActiveChanges[d.index]);
			console.log("\t\tCombined change: " + appData.cumulativeCombinedChanges[d.index] + " | combined sub change: " + appData.subCumulativeCombinedChanges[d.index]);
*/
			return sankeyDiagram.sankey.bimodalLinkArea(d,scale);
		})
		.select("title")
		.html(function(d) { 
			//return d.source.name + " --> " + d.target.name + "\nSelected: "+format(d.scale*d.true_value);
			return generateSankeyLinkTitle(d,false,true);
			});

	
}

/**
 * Pick correct cumulative state change for the highlighted timespan
 * @param i
 * @param mode
 * @param link
 * @returns
 */
function pickSubchange(mode,link)
{
	if (mode == appMode.ACTIVE)
	{
		return appData.subCumulativeActiveChanges[link.index];
	}
	else if (mode == appMode.PASSIVE)
	{
		return appData.subCumulativePassiveChanges[link.index];
	}
	else if (mode == appMode.COMBINED)
	{
		//We show a reduced diagram for the combined case
		var reduction = sankeyLinkReductions[link.index];
		var value = appData.subCumulativeCombinedChanges[reduction.base_link];
		for (var i = 0; i < reduction.sub_links.length; i++)
		{
			value += appData.subCumulativeCombinedChanges[reduction.sub_links[i]];
		}
		return value;
	}
}

/**
 * Pick correct state change for the selected tweet
 * @param i
 * @param mode
 * @param link
 * @returns
 */
function pickSelectedChange(mode,link)
{
	//if no tweet is selected, return 0 by default
	if (appData.display_tweet < 0 || appData.display_tweet > appData.tweets.length-1)
		return 0;
	
	if (mode == appMode.ACTIVE)
	{
		return appData.activeStateChanges[appData.display_tweet][link.index];
	}
	else if (mode == appMode.PASSIVE)
	{
		return appData.passiveStateChanges[appData.display_tweet][link.index];
	}
	else if (mode == appMode.COMBINED)
	{
		
		//We show a reduced diagram for the combined case
		var reduction = sankeyLinkReductions[link.index];
		var value = appData.combinedStateChanges[appData.display_tweet][reduction.base_link];
		for (var i = 0; i < reduction.sub_links.length; i++)
		{
			value += appData.combinedStateChanges[appData.display_tweet][reduction.sub_links[i]];
		}
		return value;
	}
}

/**
 * Pick correct cumulative state change for the overall timespan
 * @param i
 * @param mode
 * @param link
 * @returns
 */
function pickChange(mode,link)
{
	if (mode == appMode.ACTIVE)
	{
		return appData.cumulativeActiveChanges[link.index];
	}
	else if (mode == appMode.PASSIVE)
	{
		return appData.cumulativePassiveChanges[link.index];
	}
	else if (mode == appMode.COMBINED)
	{
		//We show a reduced diagram for the combined case
		var reduction = sankeyLinkReductions[link.index];
		var value = appData.cumulativeCombinedChanges[reduction.base_link];
		for (var i = 0; i < reduction.sub_links.length; i++)
		{
			value += appData.cumulativeCombinedChanges[reduction.sub_links[i]];
		}
		return value;
	}
}

//Assigns a css class to a node based on whether it is upper, middle or lower,
//And whether it is active or passive
function chooseSankeyNodeClass(sankeyDiagram,node)
{
	var classStr = sankeyDiagram.css_class + " " + sankeyDiagram.node_css_class;
	
	if (node.name == "Start")
	{
		classStr = classStr + " "+ sankeyDiagram.start_css_class;
	}
	else
	{
		if (node.upper)
		{
			classStr = classStr + " " + sankeyDiagram.rumor_css_class;
		}
		else if (node.lower)
		{
			classStr = classStr + " " + sankeyDiagram.debunk_css_class;
		}
		else
		{
			classStr = classStr + " " + sankeyDiagram.both_css_class;
		}
		
		if (node.passive)
		{
			classStr = classStr + " " + sankeyDiagram.passive_css_class;
		}
		else
		{
			classStr = classStr + " " + sankeyDiagram.active_css_class;
		}
	}
	
	return classStr;
}


/**
 * Hide overlay links associated with the given sankey diagram
 */
function hideSankeyOverlay(sankeyDiagram)
{
/*	centralDiagram.selectAll("."+sankeyDiagram.css_class+"." + sankeyDiagram.overlay_link_css_class)
	.style("visibility","hidden");*/
	sankeyDiagram.overlay_link_holder.style("visibility","hidden");
}

function hidePassiveSankeyOverlay()
{
	hideSankeyOverlay(passiveSankey);
}

function hideActiveSankeyOverlay()
{
	hideSankeyOverlay(activeSankey);
}

function hideCombinedSankeyOverlay()
{
	hideSankeyOverlay(combinedSankey);
}

/**
 * Show overlay links associated with the given sankey diagram
 * @param sankeyDiagram
 */
function showSankeyOverlay(sankeyDiagram)
{
/*	centralDiagram.selectAll("."+sankeyDiagram.css_class+"." + sankeyDiagram.overlay_link_css_class)
	.style("visibility","visible");*/
	console.log("Showing sankey overlay");
	sankeyDiagram.overlay_link_holder.style("visibility","visible");
}

function showPassiveSankeyOverlay()
{
	showSankeyOverlay(passiveSankey);
}

function showActiveSankeyOverlay()
{
	showSankeyOverlay(activeSankey);
}

function showCombinedSankeyOverlay()
{
	showSankeyOverlay(combinedSankey);
}

/**
 * Show all elements of specified sankey diagram.
 * 
 * Only show overlay if overlays are enabled
 * @param sankeyDiagram
 */
function showSankeyDiagram(sankeyDiagram)
{
/*	if (appData.overlay_visible)
	{*/
		console.log("Showing all elements of " +sankeyDiagram.css_class); 

		sankeyDiagram.sel.style("visibility","visible");
		sankeyDiagram.overlay_link_holder.style("visibility","visible");
		sankeyDiagram.selected_link_holder.style("visibility","visible");
/*	}
	else
	{
		console.log("Showing only non-overlay elements of " + sankeyDiagram.css_class);
		//console.log("."+sankeyDiagram.css_class+":not(."+sankeyDiagram.overlay_link_css_class+")");
		centralDiagram.selectAll("."+sankeyDiagram.css_class+":not(."+sankeyDiagram.overlay_link_css_class+")")
			.style("visibility","visible");
		
		sankeyDiagram.inner_sel.style("visibility","visible");
		sankeyDiagram.overlay_link_holder.style("visibility","hidden");
		sankeyDiagram.selected_link_holder.style("visibility","hidden");
		
	}*/
}

function showPassiveSankeyDiagram()
{
	showSankeyDiagram(passiveSankey);
}

function showActiveSankeyDiagram()
{
	showSankeyDiagram(activeSankey);
}

function showCombinedSankeyDiagram()
{
	showSankeyDiagram(combinedSankey);
}

/**
 * Hide all elements of the given sankey diagram
 * @param sankeyDiagram
 */
function hideSankeyDiagram(sankeyDiagram)
{
	console.log("Hiding all elements of sankey diagram");
	sankeyDiagram.sel.style("visibility","hidden");
	sankeyDiagram.overlay_link_holder.style("visibility","hidden");
	sankeyDiagram.selected_link_holder.style("visibility","hidden");

}

function hidePassiveSankeyDiagram()
{
	hideSankeyDiagram(passiveSankey);
}

function hideActiveSankeyDiagram()
{
	hideSankeyDiagram(activeSankey);
}

function hideCombinedSankeyDiagram()
{
	hideSankeyDiagram(combinedSankey);
}

/**
 * Turn transition array into form recognized by d3.sankey():
 * example: 
 * {"source":0,"target":1,"value":124.729},
 * {"source":1,"target":2,"value":0.597},
 * {"source":1,"target":3,"value":26.862},
 * @param transitions
 * @param stateNameIndexMap
 */
function transitionsToSankeyLinks(transitions, stateNameIndexMap, values)
{
	var links = [];
	for (var i = 0; i < transitions.length; i++)
	{
		var trans = transitions[i];
	
		links.push({
			source:stateNameIndexMap[trans.start],
			target:stateNameIndexMap[trans.end],
			value:values[i],
			true_value:values[i],
			sub_value:0,
			selected_value:0,
			source_position:trans.start_position,
			target_position:trans.end_position,
			index:i,
			rumor: trans.rumor,
			debunk: trans.debunk
			});
	}
	return links;
}

/**
 * Generates a set of links that can be passed to the Sankey layout calculator
 * code. This set of links is a reduction from the full set based on the 
 * reduction rules enumerated at the beginning of app.js. It does the reduction
 * by picking one link from the original set and adding to it the values of
 * other links from the set, based on the scheme set out in 
 * enumerateSankeyLinkReductions() 
 * @param transitions
 * @param stateNameIndexMap
 * @param values
 * @returns
 */
function transitionsToReducedSankeyLinks(transitions, stateNameIndexMap, values)
{

	var allLinks = transitionsToSankeyLinks(transitions, stateNameIndexMap,values);
	var links = [];
	var link;
	for (var i = 0; i < sankeyLinkReductions.length; i++)
	{

		link = allLinks[sankeyLinkReductions[i].base_link];
		
		link.source = sankeyNodeReductions[link.source].new_index;
		link.target = sankeyNodeReductions[link.target].new_index;
	
		link.index = i;
		for (var j = 0; j < sankeyLinkReductions[i].sub_links.length; j++)
		{
			link.value +=allLinks[sankeyLinkReductions[i].sub_links[j]].value;
		}
		links.push(link);
	}
	
	return links;

}

function statesToSankeyNodes(states)
{
	var nodes = [];
	for (var i = 0; i < states.length; i++)
	{
		nodes.push({name:states[i].name, 
			desc:states[i].desc,
			position: states[i].position,
			upper:states[i].upper,
			middle:states[i].middle,
			lower:states[i].lower,
			passive:states[i].passive,
			active:states[i].active});
	}
	return nodes;
}


function statesToReducedSankeyNodes(states)
{
	var nodes = [];
	for (var i = 0; i < states.length; i++)
	{
		//Only include a node if it is in the reducer, and if it is "reduced" to itself
		if (sankeyNodeReductions[i] && sankeyNodeReductions[i].base == i)
		{
			nodes.push({name:states[i].name, 
				desc:states[i].desc,
				position: sankeyNodeReductions[i].new_position,
				upper:states[i].upper,
				middle:states[i].middle,
				lower:states[i].lower,
				passive:states[i].passive,
				active:states[i].active});
		}
	}
	return nodes;
}

function generateSankeyNodeTitle(node)
{
	console.log(node);
	var value = 0;
	var link;
	if (node.name != "Start")
	{
		for (var i = 0; i < node.targetLinks.length; i++)
		{
			link = node.targetLinks[i];
			value += link.value;
		}
		return /*"Any prior state &rarr; "+*/node.desc +"<br>All time: " + formatNumber(value);
	}
	else //special titling convention for start node
	{
		for (var i = 0; i < node.sourceLinks.length; i++)
		{
			link = node.sourceLinks[i];
			value += link.value;
		}
		return "No interaction: " + formatNumber(value);

	}
}


/**
 * Generate a title for a sankey link. Use different language and value if it 
 * is an overlay or selected link
 * @param link
 * @param isOverlay
 * @param isSelected
 * @returns {String}
 */
function generateSankeyLinkTitle(link, isOverlay, isSelected)
{
	//return "Sankey " + type + " link";
	var value,type;
	if (isOverlay)
	{
		value = link.sub_value;
		type = "Highlighted timespan";
	}
	else if (isSelected)
	{
		value = link.selected_value;
		type = "Selected tweet";
	}
	else
	{
		value = link.value;
		type = "All time";
	}
	
	return link.source.desc + " &rarr; " + link.target.desc + 
	"<br>"+type+": " +format(value);

}

function generateLinkToolTip(sankeyDiagram,linkType)
{
	return d3.tip()
		.attr("class", sankeyDiagram.css_class + " " + sankeyDiagram.tooltip_css_class)
		.offset(function(d){
			return[ sankeyLinkTitleVerticalOffset(d), 0 ];
			})
		.html(function(d){
			//d will be ban element of subGraph.baseVals
			//console.log(c);
			return generateSankeyLinkTitle(d,linkType);});
}

function sankeyLinkTitleVerticalOffset(link)
{
	return link.t_off-5;
}

