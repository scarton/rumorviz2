/**
 * Functions pertaining to user list element of visualization
 */


function UserList()
{
	this.initialized = false;
	this.sel = null;
	this.width = 860;
	this.table_width = 860*3;
	this.subtable_width = this.table_width - 25;
	this.height = 540;
	this.body_div_height = this.height - 145; //This depends on how tall the header columns are likely to be
	this.css_class = "userlist";
	this.title = "List of tweets";
	this.title_css_class = "userlist_title";
	this.outer_div_class = "userlist_outer_div";
	this.outer_table_css_class = "userlist_outer_table";
	this.outer_table_css_id = "userlist_outer_table";
	this.header_table_css_class = "userlist_header_table";
	this.footer_table_css_class = "userlist_footer_table";
	this.footer_table_sel = null;
	this.vertical_scroll_div_sel = null;
//	this.body_table_sel = null; //D3 element that 
	this.body_table_css_class = "userlist_table";
	this.column_class = "userlist_column";
	this.scroll_css_class = "userlist_scroll";
	this.rumor_css_class = "rumor"; //So we can color-code tweets in the table
	this.debunk_css_class = "debunk";
	this.row_css_class = "userlist_row";
	this.selected_css_class = "selected";
	this.sort_column = 0; //sort by tweet number by default
	this.sort_ascending = true;
	this.column_sums = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	this.columns = [
		{
			i:0, //Column number
			name : "Tweet #", //Displayed column name
			numeric: true, //Determines whether we sort alphabetically or numerically
			sum: true, //Whether it makes sense to sum this column in the footer
			columnName : "tweet", //columnName also used for column classes
			accessor: function(index) //How to access the value of this column given a tweet index
				{
					return index+1;
				},
			custom_sum: true,
			sum_accessor: function(index) //For the purpose of summing over this column, add one for each row
				{
					return 1;
				}
		},
		{
			i:1,
			name : "Author",
			numeric: false,
			sum: false,
			columnName : "author",
			accessor: function(index)
				{
					return appData.tweets[index].author_name;
				}
		},
		{
			i:2,
			name : "Date",
			numeric: true,
			sum:false,
			columnName : "tweet_date",
			accessor: function(index)
				{
					return shortDateFormat(new Date(appData.tweets[index].date));
				}
			
		},
		{
			i:3,
			name : "Tweet type",
			numeric: false,
			sum: false,
			columnName : "tweet_type",
			accessor: function(index)
				{
					if (appData.tweets[index].type == 1)
						return "Rumor";
					else
						return "Correction";
				}
		},
		{
			i:4,
			name : "Selected",
			numeric: true,
			sum: true,
			columnName : "select",
			accessor: function(index)
				{
					return index == appData.display_tweet;
				},
		},
		{
			i:5,
			name : "# of followers of tweet author",
			numeric: true,
			sum: true,
			columnName : "num_followers",
			accessor: function(index)
				{
					return appData.authorInfo[index].followerCount;

				}
		},
		{
			i:6,
			name : "# followed by tweet author",
			numeric: true,
			sum: true,
			columnName : "num_followed",
			accessor: function(index)
				{
					return appData.authorInfo[index].followedCount;

				}
		},
		{
			i:7,
			name: states[stateNameIndexMap[transitions[0].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[0].end]].desc,
			//name : "# Exposed to rumor with no previous exposure to anything",
			numeric: true,
			sum: true,
			columnName : "exposed_rumor",
			accessor: function(index)
				{
					return appData.passiveStateChanges[index][0];

				}
		},
		{
			i:8,
			name: states[stateNameIndexMap[transitions[2].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[2].end]].desc,
			//name : "# Exposed to correction with no previous exposure to anything",
			numeric: true,
			sum: true,
			columnName : "exposed_debunk",
			accessor: function(index)
				{
						return appData.passiveStateChanges[index][2];
				}
		},
		
		{
			i:9,
			name: states[stateNameIndexMap[transitions[16].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[16].end]].desc,
			//name : "# Exposed to rumor after previous exposure to correction",
			numeric: true,
			sum: true,
			columnName : "exposed_rumor_after_debunk",
			accessor: function(index)
				{
					return appData.passiveStateChanges[index][16]
						+ appData.passiveStateChanges[index][20]
						+ appData.passiveStateChanges[index][23];
				}
		},
		{
			i:10,
			name: states[stateNameIndexMap[transitions[5].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[5].end]].desc,
			//name : "# Exposed to correction after previous exposure to rumor",
			numeric: true,
			sum: true,
			columnName : "exposed_debunk_after_rumor",
			accessor: function(index)
				{

					return appData.passiveStateChanges[index][5]
						+ appData.passiveStateChanges[index][9]
						+ appData.passiveStateChanges[index][12];
				}
		},
		{
			i:11,
			name: states[stateNameIndexMap[transitions[6].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[6].end]].desc,
			//name : "Tweeted rumor with only previous exposure to rumor",
			numeric: true,
			sum: true,
			columnName : "rumor_twt_after_rumor_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][6]
						+ appData.passiveStateChanges[index][10]
						+ appData.passiveStateChanges[index][13]));
				}
		},
		{
			i:12,
			name: states[stateNameIndexMap[transitions[17].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[17].end]].desc,
			//name : "Tweeted rumor with only previous exposure to correction",
			numeric: true,
			sum: true,
			columnName : "rumor_twt_after_debunk_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][17]
						+ appData.passiveStateChanges[index][21]
						+ appData.passiveStateChanges[index][24]));
				}
		},
		{
			i:13,
			name: states[stateNameIndexMap[transitions[26].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[26].end]].desc,
			//name : "Tweeted rumor with previous exposure to both",
			numeric: true,
			sum: true,
			columnName : "rumor_twt_after_debunk_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][26]));
				}
		},
		{
			i:14,
			name: states[stateNameIndexMap[transitions[1].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[1].end]].desc,
			//name : "Tweeted rumor with previous exposure to nothing",
			numeric: true,
			sum: true,
			columnName : "rumor_twt_after_debunk_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][1]));
				}
		},
		{
			i:15,
			name: states[stateNameIndexMap[transitions[34].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[34].end]].desc,
			//name : "Tweeted rumor after previously tweeting correction",
			numeric: true,
			sum: true,
			columnName : "rumor_twt_after_debunk_twt",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][34]
					+ appData.passiveStateChanges[index][36]
					+ appData.passiveStateChanges[index][37]));
				}
		},
		{
			i:16,
			name: states[stateNameIndexMap[transitions[18].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[18].end]].desc,
			//name : "Tweeted correction with only previous exposure to correction",
			numeric: true,
			sum: true,
			columnName : "debunk_twt_after_debunk_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][18]
						+ appData.passiveStateChanges[index][22]
						+ appData.passiveStateChanges[index][25]));
				}
		},
		{
			i:17,
			name: states[stateNameIndexMap[transitions[7].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[7].end]].desc,
			//name : "Tweeted correction with only previous exposure to rumor",
			numeric: true,
			sum: true,
			columnName : "debunk_twt_after_rumor_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][7]
						+ appData.passiveStateChanges[index][11]
						+ appData.passiveStateChanges[index][14]));
				}
		},

		{
			i:18,
			name: states[stateNameIndexMap[transitions[27].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[27].end]].desc,
			//name : "Tweeted correction with previous exposure to both",
			numeric: true,
			sum: true,
			columnName : "debunk_twt_after_debunk_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][27]));
				}
		},
		{
			i:19,
			name: states[stateNameIndexMap[transitions[3].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[3].end]].desc,
			//name : "Tweeted correction with previous exposure to nothing",
			numeric: true,
			sum: true,
			columnName : "debunk_twt_after_debunk_exp",
			accessor: function(index)
				{

					return ((appData.passiveStateChanges[index][3]));
				}
		},
		{
			i:20,
			name: states[stateNameIndexMap[transitions[29].start]].desc + " &rarr; " + states[stateNameIndexMap[transitions[29].end]].desc,
			//name : "Tweeted correction after previously tweeting rumor",
			numeric: true,
			sum: true,
			columnName : "debunk_twt_after_rumor_twt",
			accessor: function(index)
				{
					return ((appData.passiveStateChanges[index][29]
					+ appData.passiveStateChanges[index][31]
					+ appData.passiveStateChanges[index][32]));
				}
		},
		
		{
			i:21,
			name : "Inspired by this",
			numeric: false,
			sum: true,
			columnName : "follower_tweets",
			accessor: function(index)
				{
				return listFollowerAfterTweets(index,false);
				},
			custom_sum:true,
			sum_accessor:function(index)
				{
					return listFollowerAfterTweets(index,true);
				},
			custom_sort:true,
			sort_accessor:function(index)
			{
				return listFollowerAfterTweets(index,true);
			},
		},
		{
			i:22,
			name : "Influenced this",
			numeric: false,
			sum: true,
			columnName : "followed_tweets",
			accessor: function(index)
				{
					return listFollowedBeforeTweets(index,false);
				},
			custom_sum: true,
			sum_accessor: function(index)
				{
					return listFollowedBeforeTweets(index,true);
	
				},
			custom_sort:true,
			sort_accessor:function(index)
			{
				return listFollowedBeforeTweets(index,true);

			},
		},
		];

}

/**
 * Initialize the tweet list table. The overall structure of the component is one
 * outer table with three rows, each of which has a different table. The top row
 * has a table consisting of column names which, when clicked, sort the table in 
 * the middle row, which displays the actual data. This middle table is contained
 * in a div element that scrolls vertically when it is too tall. 
 * The bottom row contains a table of sums of the columns of the middle table. 
 * 
 */
function initializeUserList()
{
	userList.sel = d3.select("#central_diagram_div")
		.append("div")
		.attr("class", userList.css_class)
		.style("position", "absolute") // This is kinda hacky, but whatever
		.style("left", "0px")
		.style("top", "0px")
		.style("width", "inherit")
		.style("min-width", "860px")
		.style("height", "inherit");
//		.style("overflow", "scroll");
	
	var data;
	var width = userList.width + "px";
	var table_width = userList.table_width + "px";
	var height = userList.height + "px";
	var subtable_width = userList.subtable_width + "px";
	var divHeight = userList.body_div_height + "px";

	// Add title
	userList.sel
		.append("text")
		.attr("class", userList.title_css_class)
		.text(userList.title)
		.style("position", "absolute")
		.style("width", "500px")
		.style("top", "5px")
		.style("left", "15px");
	
	var outerDiv = userList.sel
		.append("div")
		//.style("height",height)
		.attr("class",userList.outer_div_class);

	// Set up whole table
	var outerTable = outerDiv
		.append("table")
		//.attr("width", table_width)
		.attr("id",userList.outer_table_css_id)
		.attr("class", userList.outer_table_css_class);

	
	//Generate an object consisting of the rows and column headers of the table
	var tableData = generateAllTableData();
	
	
	/*//Add header  column titles
	outerTable
		.append("thead")
		.append("tr")
		.selectAll("th")
		.data(userList.columns)
		.enter()
		.append("th")
		.attr("class", function(d)
		{
			return userList.column_class+" "+d.columnName;
		})
		.html(function(d)
		{
			return d.name;
		});


		
	userList.body_table_sel = outerTable.append("tbody")
		.attr("class", userList.body_table_css_class)
		.attr("width", subtable_width);
	


	//Set up footer tables that display column sums
	userList.footer_table_sel = outerTable.append("tfoot")
		.attr("class", userList.footer_table_css_class)
		.attr("width", subtable_width);
	
	userList.footer_table_sel
		.append("tr")
		.selectAll("td")
		.data(userList.columns)
		.enter()
		.append("td")
		.text(function(d,i){
			if (d.i == 0)
				return "Sum:";
			else if (d.sum)
				return "0";
			else
				return "N/A";})
		.attr("class",function(d){return userList.column_class+ " " +d.columnName;});
		*/
	
	//Convert the table into a DataTable (http://www.datatables.net/)
	$(document).ready( function () {
	    $('#'+userList.outer_table_css_id).DataTable({
	    	data:tableData.rows,
	    	columns:tableData.headers,
	    	dom: 'TRC<"clear">lfrtip',
	        columnDefs: [
	            { visible: false, targets: 1 }],
            "tableTools": {
                "sSwfPath": "swf/copy_csv_xls_pdf.swf"
            }
	    });
	} );
	
	userList.initialized = true;
}

/**
 * Generates the data that should be in the table, in the form of an object containing two lists: headers, a list of column names, and
 * rows, a list of lists of 
 * 
 * Generates data from startIndex to endIndex inclusive. 
 * @returns {___anonymous13917_13948}
 */
function generateTableData(startIndex,endIndex)
{
	var headers = [];
	for (var i =0; i < userList.columns.length; i++)
	{
		headers.push({title:userList.columns[i].name});
	}
	
	var rows = [];
	for (var i = startIndex; i <= endIndex; i++)
	{
		var row = [];
		for (var j =0; j < userList.columns.length; j++)
		{ 
			row.push(userList.columns[j].accessor(i));
		}
		rows.push(row);
	}
	
	return {headers:headers,
			rows:rows};
}

function generateAllTableData()
{
	return generateTableData(0,appData.tweets.length-1);
}

/**
 * Draw a table with information about tweets and authors between appData.subStart
 * and appData.subEnd
 * 
 * Adapted from example at http://devforrest.com/examples/table/table.php
 */
function drawTable()
{
	var tbody = userList.sel.select("tbody");
	tbody.selectAll("tr").remove();
	
	
	for (var i = 0; i < userList.column_sums.length; i++)
	{
		userList.column_sums[i] = 0;
	}
	console.log("Substart: " + appData.subStart);
	var indices;
	if (appData.subStart < 0)
		indices = appData.indices;
	else
		indices = appData.subIndices;
	console.log("displaying " + indices.length + " rows");
	// Create a row for each object in the data. No initial sort should be necessary as tweets should be time-ordered to begin
	var rows = tbody.selectAll("tr")
	.data(indices)
	.enter()
	.append("tr")
	.attr("class", function(d){
		if (d == appData.display_tweet)
		{
			if (appData.tweets[d].type == 1)		
				return userList.row_css_class + " " + userList.selected_css_class + " " + userList.rumor_css_class;
			else
				return userList.row_css_class + " " + userList.selected_css_class + " " + userList.debunk_css_class;
		}
		else
			return userList.row_css_class;
		})
	.on("mousedown", function(d)
	{
		console.log("Clicked on user list user row");
		displayAndHighlightTweets(d);
	});

	var cells = rows.selectAll("td")
	.data(function(d)
	{
		return userList.columns.map(function(column)
		{
			return {
				i : column.i,
				value : column.accessor(d),
				sum_value: calculateSumValue(column,d),
				columnName: column.columnName,
				sum: column.sum,
				numeric: column.numeric,
				rumor:(appData.tweets[d].type == 1)
			};
		});
	})
	.enter()
	.append("td")
	.each(function(d){
		//if this is a sum column, add its value to the count for this column
		if(d.sum)
		{
			userList.column_sums[d.i]+=d.sum_value;
		}
	})
	.html(function(d)
	{
		if (d.value instanceof Date)
			return shortDateFormat(d.value);
		else if (typeof d.value === "boolean")
			return d.value;
		else if (d.numeric)
			return formatNumber(d.value);
		else
			return d.value;
	})
	.attr("class", function(d)
	{
		//For the tweet type column return the specific value so we can color rumors differently from corrections
		if ( d.i == 3)
			return userList.column_class + " " + d.value;
		//For the selected and highlighted column, color them bright blue or red if the tweet is selected or highlighted respectively
		/*else if ((d.i ==8 || d.i ==9) && d.value == true)
		{
			if (d.rumor)
				return userList.column_class + " " + d.columnName + " Rumor";
			else
				return userList.column_class + " " + d.columnName + " Correction";		
		}*/
		else
			return userList.column_class + " " + d.columnName;
	});
	// container.select(".outerTable").style("border-style","solid");
	
	userList.footer_table_sel.selectAll("td")
		.text(function(d) {
			if (d.i ==0)
				return formatNumber(userList.column_sums[d.i]) + " tweets";
			else if (d.sum || d.custom_sum)
				return formatNumber(userList.column_sums[d.i]);
			else
				return "N/A";
		});
	
	//Sort the displayed rows based on the currently selected sorting method
	console.log("Sorting on " + userList.columns[userList.sort_column].columnName + " ascending: " + userList.sort_ascending);
	var sort = createSort(userList.columns[userList.sort_column]);
	userList.sel.select("tbody")
		.selectAll("tr")
		.sort(sort);
}

function calculateSumValue(column,d)
{
	var rVal;
	if (column.custom_sum)
	{
		rVal = column.sum_accessor(d);
	}
	else
	{
		rVal = column.accessor(d);
	}
	
	if (column.name == "followed_tweets")
		console.log(d+ ": " +rVal);
	
/*	if (isNaN(rVal))
	{
		console.log("NaN found when trying to calculate sum value");
		console.log(column);
		console.log(d);
	}*/
	return rVal;
}

//Different comparators
var sortValueAscending = function(a, b)
{
	return a - b;
};
var sortValueDescending = function(a, b)
{
	return b - a;
};
var sortStringAscending = function(a, b)
{
	return a.localeCompare(b);
};
var sortStringDescending = function(a, b)
{
	return b.localeCompare(a);
};

/**
 * Show user list, a sortable table of the users in the current sel window. Hide Sankey diagram and/or bar chart
 */
function showUserList()
{
	console.log("Showing user list");
	userList.sel.style("visibility", "visible");
	//showUserListOverlay();
}

function showUserListOverlay()
{
//	userList.body_table_sel.style("visibility","visible");
//	userList.footer_table_sel.style("visibility","visible");

}

function hideUserList()
{
	userList.sel.style("visibility","hidden");
//	userList.body_table_sel.style("visibility","hidden");
//	userList.footer_table_sel.style("visibility","hidden");

}

function hideUserListOverlay()
{
//	userList.body_table_sel.style("visibility","hidden");
//	userList.footer_table_sel.style("visibility","hidden");
}

function clearUserList()
{
	var tbody = userList.sel.select("tbody");
	tbody.selectAll("tr").remove();
}

function updateUserList()
{
	if (!userList.initialized) initializeUserList();
	
	//drawTable();
}

/**
 * Creates a sorting function based on a column passed to it
 * @param column
 * @returns {Function}
 */
function createSort(column)
{
	var sort;
	var accessor;
	
	if (column.custom_sort)
		accessor = column.sort_accessor;
	else
		accessor = column.accessor;

	// Choose appropriate sorting function.
	if (column.numeric || column.custom_sort)
	{
		if (userList.sort_ascending)
			sort = function(a, b)
			{
				return sortValueAscending(accessor(a), 
					accessor(b));
			};
		else
			sort = function(a, b)
			{
				return sortValueDescending(accessor(a), 
					accessor(b));
			};
	}
	else
	{
		if (userList.sort_ascending)
			sort = function(a, b)
			{
				return sortStringAscending(accessor(a), 
					accessor(b));
			};
		else
			sort = function(a, b)
			{
				return sortStringDescending(accessor(a), 
					accessor(b));
			};
	}
	

	return sort;
}

/**
 * Return tweets by followers of the author of the given tweet, that
 * occurred after it in time
 * @param index
 * @param onlyReturnCount: if true, only return the number of tweets found
 */
function listFollowerAfterTweets(index, onlyReturnCount)
{
	var list = "";
	var fIndex;
	var first = true;

	var count = 0;
	for (var i = 0; i < appData.neighborhoodTweets[index].tweetsOfFollowers.length; i++)
	{
		fIndex = appData.tweetIDIndexMap[appData.neighborhoodTweets[index].tweetsOfFollowers[i]];
		
		if (fIndex > index)
		{
			if (!onlyReturnCount)
			{
				if (!first) list = list.concat(", ");
				list = list.concat("#"+fIndex);
				first = false;
			}
			count++;
		}
	}

	if (!onlyReturnCount)
	{
		if (list == "") 
		{
			list = "none";
		}
		else
		{
			list = "<b>"+count +"</b> (" + list + ")";
		}
		return list;
	}
	else
	{
		return count;
	}
}

/**
 * Return tweets by users followed by the author of the given tweet, that
 * occured before it in time
 * @param index: index of the tweet
 * @param onlyReturnCount: if true, only return the number of tweets found
 */
function listFollowedBeforeTweets(index, onlyReturnCount)
{
	var list = "";
	var fIndex;
	var first = true;
	var count = 0;
	for (var i = 0; i < appData.neighborhoodTweets[index].tweetsOfFollowed.length; i++)
	{
		fIndex = appData.tweetIDIndexMap[appData.neighborhoodTweets[index].tweetsOfFollowed[i]];
		if (fIndex < index)
		{
			if (!onlyReturnCount)
			{
				if (!first) list = list.concat(", ");
				list = list.concat("#"+fIndex);
				first = false;
			}
			count++;
		}
	}
	
	if (!onlyReturnCount)
	{
		if (list == "") 
		{
			list = "none";
		}
		else
		{
			list = "<b>"+count +"</b> (" + list + ")";
		}
		return list;
	}
	else
	{
		return count;
	}
	
}

