/**
 * This is the core javascript file for the project. It's primarily responsible 
 * for getting everything set up and making calls to the other scripts.
 * 
 * The entry point for the whole application is 
 */

/**
 * Global variables and initialization logic
 */


var formatNumber = d3.format(",f");
var shortDateFormat = d3.time.format('%b %d, %H:%M');
var shortFloatFormat = d3.format('0.1f');

/*var centralDiagram = d3.select("#central_diagram_div") //Main svg "canvas" we will draw various things on
	.append("svg")
	.attr("width", "860px")
	.attr("height", "600px")
	.attr("class", "diagram");*/

var centralDiv = d3.select("#central_diagram_div");


	//.append("g");

/*this.loadingMessage = d3.select("#main").append("text")
	.text("Default loading message");*/

var states = enumerateStates(); //List of possible states someone can be in. 
var stateNameIndexMap = buildIndexMapOnField(states,"name");
var transitions = enumerateStateTransitions(); //List of ways people can transition between states
var transitionNameIndexMap = buildIndexMapOnField(transitions,"name");
var sankeyNodeReductions = enumerateSankeyNodeReductions(); //Map of how to fold nodes together when we want to display a cut-back Sankey diagram
var sankeyLinkReductions = enumerateSankeyLinkReductions();

//The various modes the app can be in. This "enum" is referred to throughout the code
var appMode = {
	ACTIVE : "active",
	PASSIVE : "passive",
	COMBINED: "combined",
	BAR_CHART : "bar chart",
	USER_LIST : "user list",
	NETWORK: "network"};

//When a tweet is selected, the app will highlight other tweets that are related to it
//These are the other sets of tweets that can get highlighted
var highlightMode = {
	NONE:"none", 
	AUTHOR:"author", //Tweets by the same author as the selected one
	FOLLOWER:"follower", //Tweets by followers of the selected author
	FOLLOWED:"followed" //Tweets by people the selected author follows
};

var conditions = {
	0:appMode.COMBINED,
	1:appMode.NETWORK,
	2:appMode.USER_LIST,
	3:appMode.BAR_CHART
};

//Create the various elements of the application
var activeSankey = new SankeyDiagram(appMode.ACTIVE, 860,600,15,10); //displays state changes for users who tweeted the rumor or debunk
var passiveSankey = new SankeyDiagram(appMode.PASSIVE, 860,600,15,10); //displays state changes for users who were epxosed to the rumor or debunk
var combinedSankey = new SankeyDiagram(appMode.COMBINED, 860, 600,15,10);
var barGraph = new BarGraph(); //Summarizes state changes in a bar graph
var userList = new UserList(); //Displays individual users and their marginal impacts
var networkDiagram = new NetworkDiagram(860,600); //Displays users as circular nodes, relationships between users as links 
var instructions = new Instructions();


var timeBar = new TimeBar(); //Timeline of tweets
var tweetInfo = new TweetInfo(); //Information about individual tweet and its author

var currentMode = null; //Which visualization is currently being displayed
var currentHighlightMode = highlightMode.NONE;

//Backbone data structures: The app displays pieces of these data structures
//Look at loadAppDataAndInitialize() in dataModule.js to see how these are structured
var appData = {
	condition_num:-1, //Which visualization to show, if the app is being loaded in the context of an experiment
	trial_id:null, //Unique alphanumeric code associated with this session
	passiveStateChanges: [], //chronological list of tweets' passive impacts
	activeStateChanges: [], //chronological list of tweets' active impacts
	combinedStateChanges:[], //chronological list of tweets' combined changes
	cumulativePassiveChanges: [], //Cumulative passive impacts
	cumulativeActiveChanges: [], //Cumulative active impacts
	cumulativeCombinedChanges:[], //combination of passive and active impacts
	neighborhoodTweets: [], //chronological list of tweets' sets of related tweets
	tweets: [], //chronological list of tweets
	indices:[], //array of numerical indices of tweets
	authorInfo: [], //chronological list of author information about tweets
	tweetDateIndexMap:{}, //map from tweet date to chronological index
	tweetIDIndexMap:{}, //map from tweet ID to chronological index
	subCumulativePassiveChanges: [], //The summed passive impacts of all tweets in the subinterval
	subCumulativeActiveChanges: [], //Summed active impacts of tweets in the subinterval
	subCumulativeCombinedChanges:[], //Summed combined impact from tweets in subinterval
	subStart:-1, 	//subStart and subEnd delimit the subset of tweets being
	subEnd:-2, 		//		highlighted on the timebar and displayed by the app
	subIndices:[], //list of indices that are currently being highlighted
	overlay_visible: false, //indicates whether the diagram is displaying sub-data associated with subStart and subEnd 
	max_followers:0, //max number of followers any author in the set has
	max_followed:0, //ditto for followed
	display_tweet:-1, //index of tweet that has been selected for display on the sidebar
	highlight_tweets:{} //A "hashset" of tweet indexes that are to be highlighted by whatever visual element does tweet highlighting, e.g. the timebars
};




/**
 * Initialize the application based on data loaded from the back end. Called from
 * loadAppDataAndInitialize() in dataModule.js
 */
function initializeApplication()
{
	console.log("appData:");
	console.log(appData);

	initializeTimeBar();
	showTimeBar();
	
	initializeTweetInfo();
	//showTweetInfo();

	console.log("Condition number: " + conditions[appData.condition_num]);
	//Either show the selected visualization and the trial ID, or show some default visualization and no trial ID
	if (appData.condition_num > -1)
	{
		showTrialID();
		currentMode = conditions[appData.condition_num];
		//changeAppMode(conditions[appData.condition_num]);
		initializeGuiders();
		showGuiders();

	
	}
	else
	{
		initializeGuiders();
		//currentMode = appMode.COMBINED; //default to combined sankey
		changeAppMode(appMode.COMBINED);
	}
	
	//initializeCentralElement();
	updateCentralElement(true,false,false);
	resizeCentralDiagramDiv();
	showCentralElement();
	
}


/**
 * Resize central element div to take up appropriate amount of space
 */
function resizeCentralDiagramDiv()
{
	
	var bwidth = $(window).width();
	var bheight = $(window).height();
	//console.log("Body width: " + bwidth);

	var nwidth = bwidth-300;
	var nheight = bheight-250;
	console.log("resizing central element container to " + nwidth + " x " + nheight);
	
	$("#central_diagram_div").width(nwidth);
	//$("#central_diagram_div").height(nheight);
/*	d3.select("#central_diagram_div")
		.style("border","2px solid red")
		.attr("width",(bwidth-300))
		.attr("height",(bheight-200));*/
	 	
}

/**
 * Switch to a different mode by updating and showing the desired element, and
 * hiding the current element.
 * @param newMode
 */
function changeAppMode(newMode)
{
	console.log("Switching from " + currentMode + " to " + newMode);
	if(newMode == currentMode) return; //if the requested mode is the current one, do nothing
	
	
	//Hide current element
	switch (currentMode)
	{
	case appMode.PASSIVE:
		hidePassiveSankeyDiagram();
		break;
	case appMode.ACTIVE:
		hideActiveSankeyDiagram();
		break;
	case appMode.COMBINED:
		hideCombinedSankeyDiagram();
		break;
	case appMode.BAR_CHART:
		hideBarGraph();
		break;
	case appMode.USER_LIST:
		hideUserList();
		break;
	case appMode.NETWORK:
		hideNetworkDiagram();
		break;
	}
	
	currentMode = newMode;
/*	if (appData.condition_num > -1)
	{
		showInstructions();
	}
	*/
	//Show desired element
	switch (currentMode)
	{
	case appMode.PASSIVE:
		updatePassiveSankeyDiagram();
		showPassiveSankeyDiagram();
		break;
	case appMode.ACTIVE:
		updateActiveSankeyDiagram();
		showActiveSankeyDiagram();
		break;
	case appMode.COMBINED:
		updateCombinedSankeyDiagram();
		showCombinedSankeyDiagram();
		break;
	case appMode.BAR_CHART:
		updateBarGraph();
		showBarGraph();
		break;
	case appMode.USER_LIST:
		updateUserList();
		showUserList();
		break;
	case appMode.NETWORK:
		//showLoadingMessage("Calculating network diagram layout...");
		updateNetworkDiagram();
		//hideLoadingMessage();
		showNetworkDiagram();
		break;
	}
	
	
}

function showCentralElement()
{
	switch (currentMode)
	{
	case appMode.PASSIVE:
		showPassiveSankeyDiagram();
		break;
	case appMode.ACTIVE:
		showActiveSankeyDiagram();
		break;
	case appMode.COMBINED:
		showCombinedSankeyDiagram();
		break;
	case appMode.BAR_CHART:
		showBarGraph();
		break;
	case appMode.USER_LIST:
		showUserList();
		break;
	case appMode.NETWORK:
		showNetworkDiagram();
		break;
	}
}
	
function initializeCentralElement()
{
	switch (currentMode)
	{
	case appMode.PASSIVE:
		initializePassiveSankeyDiagram();
		break;
	case appMode.ACTIVE:
		initializeActiveSankeyDiagram();
		break;
	case appMode.COMBINED:
		initializeCombinedSankeyDiagram();
		break;
	case appMode.BAR_CHART:
		initializeBarGraph();
		break;
	case appMode.USER_LIST:
		initializeUserList();
		break;
	case appMode.NETWORK:
		initializeNetworkDiagram();
		break;
	}
}

/**
 * Update the central visual element in response to some manipulation of the
 * timebar. Different elements behave differently. 
 * @param mouseUp: mouse clicked down
 * @param mouseDown: mouse clicked up
 * @param extentChange: indicates that the size of the brush changed, as a 
 * result of a brush or zoom action
 */
function updateCentralElement(mouseUp, mouseDown, extentChange)
{
	switch (currentMode)
	{
	case appMode.PASSIVE:
		updatePassiveSankeyDiagram();
		break;
	case appMode.ACTIVE:
		updateActiveSankeyDiagram();
		break;
	case appMode.BAR_CHART:
		updateBarGraph();
		break;
	case appMode.USER_LIST:
		//Don't really want the user list to update smoothly the way other
		//components do, because it isn't graphical. So update it only when the
		//user stops brushing.
		if (mouseUp)
		{
			updateUserList();
		}
		else if (mouseDown)
		{
			clearUserList();
		}
		break;
	case appMode.COMBINED:
		updateCombinedSankeyDiagram();
		break;
	case appMode.NETWORK:
		updateNetworkDiagram();
		break;
	}
}


function hideCentralElementOverlay()
{
	appData.overlay_visible = false;
	switch (currentMode)
	{
	case appMode.PASSIVE:
		hidePassiveSankeyOverlay();
		break;
	case appMode.ACTIVE:
		hideActiveSankeyOverlay();
		break;
	case appMode.COMBINED:
		hideCombinedSankeyOverlay();
		break;
	case appMode.BAR_CHART:
		hideBarGraphOverlay();
		break;
	case appMode.USER_LIST:
		hideUserListOverlay();
		break;
	case appMode.NETWORK:
		hideNetworkDiagramOverlay();
		break;
	}
}

function showCentralElementOverlay()
{
	appData.overlay_visible = true;
	switch (currentMode)
	{
	case appMode.PASSIVE:
		showPassiveSankeyOverlay();
		break;
	case appMode.ACTIVE:
		showActiveSankeyOverlay();
		break;
	case appMode.COMBINED:
		showCombinedSankeyOverlay();
		break;
	case appMode.BAR_CHART:
		showBarGraphOverlay();
		break;
	case appMode.USER_LIST:
		showUserListOverlay();
		break;
	case appMode.NETWORK:
		showNetworkDiagram();
		break;
	}
}



function changeHighlightMode(newMode)
{
	console.log("Changing highlight mode from " + currentHighlightMode + " to " + newMode);
	currentHighlightMode = newMode;
	displayAndHigh3lightTweets(appData.display_tweet);
}


function maybeChangeHighlightModeAndTweet(newMode, new_index)
{
	console.log("Changing highlight mode and selected tweet from " + currentHighlightMode + " to " + newMode);
	if (currentHighlightMode == highlightMode.NONE && newMode == highlightMode.AUTHOR)
	{
		checkAuthorHighlightButton();
		currentHighlightMode = newMode;
	}
	displayAndHighlightTweets(new_index);
}

function checkAuthorHighlightButton()
{
	//d3.select("#author_highlight_button").attr("checked");
	var button = document.getElementById("author_highlight_button");
    button.checked = true;
}

/**
 * This function sets the currently-displayed tweet to the selected index,
 * rebuilds the set of tweets that are supposed to be highlighted, and tells
 * the various visual elements to display/highlight stuff as appropriate
 * 
 * @param tweetIndex
 */
function displayAndHighlightTweets(tweetIndex)
{
	appData.display_tweet = tweetIndex;
	
	//Figure out what tweets ought to be highlighted by finding the appropriate
	//list of neighborhood tweets and adding them to appData.highlight_tweets
	var idList = [];
	var idSet = {};
	
	if ( tweetIndex > -1)
	{
		console.log("Current highlight mode: " + currentHighlightMode);
		idSet[tweetIndex] = true;
		if (currentHighlightMode == highlightMode.AUTHOR)
		{
			idList = appData.neighborhoodTweets[tweetIndex].tweetsOfAuthor;
			console.log("Looking for other tweets by this author");
		}
		else if ( currentHighlightMode == highlightMode.FOLLOWER)
		{
			idList = appData.neighborhoodTweets[tweetIndex].tweetsOfFollowers;
			console.log("Looking for tweets by followers of this author");

		}
		else if ( currentHighlightMode == highlightMode.FOLLOWED)
		{
			idList = appData.neighborhoodTweets[tweetIndex].tweetsOfFollowed;
			console.log("Looking for tweets by people this author follows");

		}
		console.log("Neighborhood tweets");
		console.log(appData.neighborhoodTweets[tweetIndex]);
	}

	console.log("Highlighting tweets:");
	console.log(idList);

	for (var i = 0; i < idList.length; i++)
	{
		idSet[appData.tweetIDIndexMap[idList[i]]] = true;
	}
	appData.highlight_tweets = idSet;
	
	console.log("idSet");
	console.log(idSet);
	//Make changes to visual elements to reflect changes to app data
	displayTweetInfo(tweetIndex);
	moveTimeBarCursorsToTweetIndex(tweetIndex);
	updateBothTimeBarsItems();
	updateCentralElement(true,false,true);
}

/**
 * Display the trial ID in the div that would normally hold the application
 * mode-switching buttons. If this function is being called, that div should
 * be empty in the jsp file
 */
function showTrialID()
{
	d3.select("#sankey_mode_div")
		.text("Trial ID: " + appData.trial_id);
}




/**
 * Wrap text element if it is too wide. Copied from Mike Bostock's original code at http://bl.ocks.org/mbostock/7555321
 * 
 * @param text
 * @param width
 */
function wrapTextElement(text, width)
{
	text
		.each(function()
		{
			var text = d3.select(this), 
			words = text.text().split(/\s+/).reverse(), 
			word, 
			line = [], 
			lineNumber = 0, 
			lineHeight = 1.1; // ems
			
			var y = text.attr("y");
			var dy = parseFloat(text.attr("dy"));
			console.log(dy);
			var tspan = text.text(null)
				.append("tspan")
				.attr("x", 0)
				.attr("y", y)
				.attr("dy", dy + "em");
			console.log(tspan.node());
			while (word = words.pop())
			{
				line.push(word);
				tspan.text(line.join(" "));
				if (tspan.node().getComputedTextLength() > width)
				{
					line.pop();
					tspan.text(line.join(" "));
					line = [ word ];
					tspan = text.append("tspan")
						.attr("x", 0)
						.attr("y", y)
						.attr("dy", ++lineNumber * lineHeight + dy + "em")
						.text(word);
				}
			}
		});
}



function format(d)
{
	return formatNumber(d);
}

/**
 * Enumerate the possible states a Twitter use can be in with respect to a rumor
 * and return them as a list of objects
 * fields:
 *   name: name of the node. Must match "start" and "end" fields in transitions, defined below
 *   desc: description of the node
 *   position: indicates how far that node should be to the right in the sankey
 *     diagram, in even intervals
 *   upper,middle,lower: indicate the vertical orientation of the node
 *   passive,active: indicate if it is a passive or active node. 
 * 
 * @returns {Array}
 */
function enumerateStates()
{
	var s = [
 	{name:"Start",desc:"No interaction",position:0,upper:true,middle:false,lower:false,passive:true,active:false},
	{name:"RumorExp1",desc:"Exposed to rumor",position:1,upper:true,middle:false,lower:false,passive:true,active:false},
	{name:"RumorExp2",desc:"Rumor Exposure 2",position:2,upper:true,middle:false,lower:false,passive:true,active:false},
	{name:"RumorExp3m",desc:"Rumor Exposure 3+",position:3,upper:true,middle:false,lower:false,passive:true,active:false},
	/*4*/{name:"DebunkExp1",desc:"Exposed to correction",position:1,upper:false,middle:false,lower:true,passive:true,active:false},
	{name:"DebunkExp2",desc:"Correction Exposure 2",position:2,upper:false,middle:false,lower:true,passive:true,active:false},
	{name:"DebunkExp3m",desc:"Correction Exposure 3+",position:3,upper:false,middle:false,lower:true,passive:true,active:false},
	/*7*/{name:"BothExp",desc:"Exposed to both",position:4,upper:false,middle:true,lower:false,passive:true,active:false},
	{name:"RumorTwt1",desc:"Tweeted rumor",position:5,upper:true,middle:false,lower:false,passive:false,active:true},
	{name:"RumorTwt2",desc:"Tweet Rumor 2",position:6,upper:true,middle:false,lower:false,passive:false,active:true},
	{name:"RumorTwt3m",desc:"Tweet Rumor 3+",position:7,upper:true,middle:false,lower:false,passive:false,active:true},
	/*11*/{name:"DebunkTwt1",desc:"Tweeted correction",position:5,upper:false,middle:false,lower:true,passive:false,active:true},
	{name:"DebunkTwt2",desc:"Tweet Correction 2",position:6,upper:false,middle:false,lower:true,passive:false,active:true},
	{name:"DebunkTwt3m",desc:"Tweet Correction 3+",position:7,upper:false,middle:false,lower:true,passive:false,active:true},
	/*14*/{name:"BothTwt",desc:"Tweeted both",position:8,upper:false,middle:true,lower:false,passive:false,active:true} ];
	return s;
}

/**
 * Enumerate the transitions between states and return them as a list of objects
 * 
 * Fields:
 *   name: name of link
 *   start: name of start state node. Must match "name" field in states, defined above
 *   end: name of end node
 *   start_position: indicates the order, from the top down, that the link should be
 *   displayed as it is leaving its start state node
 *   end_position: the order the link should enter its end state node
 *   active: true if at least one of the ends of the link is an "active" state
 *   passive: opposite of active. Redundant, but included for readability later on
 *   rumor: whether this link is activated by a tweet of the rumor
 *   debunk: whether this link is activated by a tweet of the debunk
 * 
 * It's hard to see here, but manually picking these positions results in a much 
 * cleaner looking diagram down the line
 * @returns {Array}
 */
function enumerateStateTransitions()
{
	var t = [
	          	{i:0,name:"Start_RumorExp1",start:"Start",end:"RumorExp1",rumor:true,debunk:false,start_position:1,end_position:0,active:false,passive:true},
	          	{i:1,name:"Start_RumorTwt1",start:"Start",end:"RumorTwt1",rumor:true,debunk:false,start_position:0,end_position:0,active:true,passive:false},
	          	{i:2,name:"Start_DebunkExp1",start:"Start",end:"DebunkExp1",rumor:false,debunk:true,start_position:2,end_position:0,active:false,passive:true},
	          	{i:3,name:"Start_DebunkTwt1",start:"Start",end:"DebunkTwt1",rumor:false,debunk:true,start_position:3,end_position:7,active:true,passive:false},
	          	{i:4,name:"RumorExp1_RumorExp2",start:"RumorExp1",end:"RumorExp2",rumor:true,debunk:false,start_position:1,end_position:0,active:false,passive:true},
	          	{i:5,name:"RumorExp1_BothExp",start:"RumorExp1",end:"BothExp",rumor:false,debunk:true,start_position:3,end_position:2,active:false,passive:true},
	          	{i:6,name:"RumorExp1_RumorTwt1",start:"RumorExp1",end:"RumorTwt1",rumor:true,debunk:false,start_position:0,end_position:1,active:true,passive:false},
	          	{i:7,name:"RumorExp1_DebunkTwt1",start:"RumorExp1",end:"DebunkTwt1",rumor:false,debunk:true,start_position:2,end_position:0,active:true,passive:false},
	          	{i:8,name:"RumorExp2_RumorExp3m",start:"RumorExp2",end:"RumorExp3m",rumor:true,debunk:false,start_position:1,end_position:0,active:false,passive:true},
	          	{i:9,name:"RumorExp2_BothExp",start:"RumorExp2",end:"BothExp",rumor:false,debunk:true,start_position:3,end_position:1,active:false,passive:true},
	          	{i:10,name:"RumorExp2_RumorTwt1",start:"RumorExp2",end:"RumorTwt1",rumor:true,debunk:false,start_position:0,end_position:2,active:true,passive:false},
	          	{i:11,name:"RumorExp2_DebunkTwt1",start:"RumorExp2",end:"DebunkTwt1",rumor:false,debunk:true,start_position:3,end_position:2,active:true,passive:false},
	          	{i:12,name:"RumorExp3m_BothExp",start:"RumorExp3m",end:"BothExp",rumor:false,debunk:true,start_position:1,end_position:0,active:false,passive:true},
	          	{i:13,name:"RumorExp3m_RumorTwt1",start:"RumorExp3m",end:"RumorTwt1",rumor:true,debunk:false,start_position:0,end_position:3,active:true,passive:false},
	          	{i:14,name:"RumorExp3m_DebunkTwt1",start:"RumorExp3m",end:"DebunkTwt1",rumor:false,debunk:true,start_position:2,end_position:1,active:true,passive:false},
	          	{i:15,name:"DebunkExp1_DebunkExp2",start:"DebunkExp1",end:"DebunkExp2",rumor:false,debunk:true,start_position:2,end_position:0,active:false,passive:true},
	          	{i:16,name:"DebunkExp1_BothExp",start:"DebunkExp1",end:"BothExp",rumor:true,debunk:false,start_position:0,end_position:3,active:false,passive:true},
	          	{i:17,name:"DebunkExp1_RumorTwt1",start:"DebunkExp1",end:"RumorTwt1",rumor:true,debunk:false,start_position:1,end_position:7,active:true,passive:false},
	          	{i:18,name:"DebunkExp1_DebunkTwt1",start:"DebunkExp1",end:"DebunkTwt1",rumor:false,debunk:true,start_position:3,end_position:6,active:true,passive:false},
	          	{i:19,name:"DebunkExp2_DebunkExp3m",start:"DebunkExp2",end:"DebunkExp3m",rumor:false,debunk:true,start_position:2,end_position:0,active:false,passive:true},
	          	{i:20,name:"DebunkExp2_BothExp",start:"DebunkExp2",end:"BothExp",rumor:true,debunk:false,start_position:1,end_position:4,active:false,passive:true},
	          	{i:21,name:"DebunkExp2_RumorTwt1",start:"DebunkExp2",end:"RumorTwt1",rumor:true,debunk:false,start_position:0,end_position:5,active:true,passive:false},
	          	{i:22,name:"DebunkExp2_DebunkTwt1",start:"DebunkExp2",end:"DebunkTwt1",rumor:false,debunk:true,start_position:3,end_position:5,active:true,passive:false},
	          	{i:23,name:"DebunkExp3m_BothExp",start:"DebunkExp3m",end:"BothExp",rumor:true,debunk:false,start_position:1,end_position:5,active:false,passive:true},
	          	{i:24,name:"DebunkExp3m_RumorTwt1",start:"DebunkExp3m",end:"RumorTwt1",rumor:true,debunk:false,start_position:0,end_position:6,active:true,passive:false},
	          	{i:25,name:"DebunkExp3m_DebunkTwt1",start:"DebunkExp3m",end:"DebunkTwt1",rumor:false,debunk:true,start_position:2,end_position:4,active:true,passive:false},
	          	{i:26,name:"BothExp_RumorTwt1",start:"BothExp",end:"RumorTwt1",rumor:true,debunk:false,start_position:0,end_position:4,active:true,passive:false},
	          	{i:27,name:"BothExp_DebunkTwt1",start:"BothExp",end:"DebunkTwt1",rumor:false,debunk:true,start_position:1,end_position:3,active:true,passive:false},
	          	{i:28,name:"RumorTwt1_RumorTwt2",start:"RumorTwt1",end:"RumorTwt2",rumor:true,debunk:false,start_position:0,end_position:0,active:true,passive:false},
	          	{i:29,name:"RumorTwt1_BothTwt",start:"RumorTwt1",end:"BothTwt",rumor:false,debunk:true,start_position:1,end_position:2,active:true,passive:false},
	          	{i:30,name:"RumorTwt2_RumorTwt3m",start:"RumorTwt2",end:"RumorTwt3m",rumor:true,debunk:false,start_position:0,end_position:0,active:true,passive:false},
	          	{i:31,name:"RumorTwt2_BothTwt",start:"RumorTwt2",end:"BothTwt",rumor:false,debunk:true,start_position:1,end_position:1,active:true,passive:false},
	          	{i:32,name:"RumorTwt3m_BothTwt",start:"RumorTwt3m",end:"BothTwt",rumor:false,debunk:true,start_position:0,end_position:0,active:true,passive:false},
	          	{i:33,name:"DebunkTwt1_DebunkTwt2",start:"DebunkTwt1",end:"DebunkTwt2",rumor:false,debunk:true,start_position:1,end_position:0,active:true,passive:false},
	          	{i:34,name:"DebunkTwt1_BothTwt",start:"DebunkTwt1",end:"BothTwt",rumor:true,debunk:false,start_position:0,end_position:3,active:true,passive:false},
	          	{i:35,name:"DebunkTwt2_DebunkTwt3m",start:"DebunkTwt2",end:"DebunkTwt3m",rumor:false,debunk:true,start_position:1,end_position:0,active:true,passive:false},
	          	{i:36,name:"DebunkTwt2_BothTwt",start:"DebunkTwt2",end:"BothTwt",rumor:true,debunk:false,start_position:0,end_position:4,active:true,passive:false},
	          	{i:37,name:"DebunkTwt3m_BothTwt",start:"DebunkTwt3m",end:"BothTwt",rumor:false,debunk:true,start_position:0,end_position:5,active:true,passive:false} ];
	
	return t;
}

/**
 * Generate HTML for title of a transition based on its start and end state
 */
function generateTransitionTitleHTML(transition)
{
	var start = states[stateNameIndexMap[transition.start]];
	var end = states[stateNameIndexMap[transition.end]];
	return start.desc + " &rarr; " + end.desc;
}

/**
 * We want to do a reduced Sankey diagram with none of the multiple-exposure or
 * multiple-tweeting states. To do this, we need to fold counts to and from these
 * states into the corresponding first-exposure or first-tweet state. This
 * function returns the mapping that allows use to do that
 *	
 * It also assigns new horizontal positions to the nodes, so the reduced set
 * of nodes is spread evenly across the space
 */
function enumerateSankeyNodeReductions()
{
	return {
		0:{base:0,new_position:0,new_index:0},
		1:{base:1,new_position:1,new_index:1},
		2:{base:1,new_position:1,new_index:1},
		3:{base:1,new_position:1,new_index:1},
		4:{base:4,new_position:1,new_index:2},
		5:{base:4,new_position:1,new_index:2},
		6:{base:4,new_position:1,new_index:2},
		7:{base:7,new_position:2,new_index:3},
		8:{base:8,new_position:5,new_index:4},
		9:{base:8,new_position:5,new_index:4},
		10:{base:8,new_position:5,new_index:4},
		11:{base:11,new_position:5,new_index:5},
		12:{base:11,new_position:5,new_index:5},
		13:{base:11,new_position:5,new_index:5},
		14:{base:14,new_position:6,new_index:6},
	};
}

/**
 * This function enumerates the links for the reduced sankey diagram described
 * by enumerateSankeyNodeReductions. It drops any link to one of the nodes 
 * that has been dropped in the reduced diagram.
 * 
 * This set of values could be calculated automatically from sankeyNodeReductions,
 * but it's easier to just write it out than it would be to design an algorithm
 * to do so
 * 
 * For instance, RumorExp1-->RumorTwt1 from the original diagram remains,
 * but it needs to have its value augmented with RumorExp2-->RumorTwt1 and
 * RumorExp3-->RumorTwt1 in order to catch all the folks flowing into RumorTwt1.
 * 
 * The only original links that are dropped entirely are the ones that are 
 * between different states of exposure to the same thing, like 
 * RumorExp1-->RumorExp2. We don't want to include this information into the
 * diagram at all.
 * Result should be an array of things that look like this:
 * {
 *  	base_link:12,
 *   	sub_links:[13,14,15]
 * }
 * 
 */
function enumerateSankeyLinkReductions()
{
	var result = [
	    {base_link:0,sub_links:[]}, //start --> rumor exp
		{base_link:1,sub_links:[]}, //start --> rumor twt
		{base_link:2,sub_links:[]}, //start --> debunk exp
		{base_link:3,sub_links:[]}, //start --> debunk twt
		{base_link:5,sub_links:[9,12]}, //rumorexp --> bothexp
		{base_link:6,sub_links:[10,13]}, //rumorexp --> rumortwt
		{base_link:7,sub_links:[11,14]}, //rumorexp --> debunktwt
		{base_link:16,sub_links:[20,23]}, //debunkexp --> bothexp
		{base_link:17,sub_links:[21,24]}, //debunkexp --> rumortwt
		{base_link:18,sub_links:[22,25]}, //debunkexp --> debunktwt
		{base_link:26,sub_links:[]}, //bothexp --> rumortwt
		{base_link:27,sub_links:[]},//bothexp --> debunktwt
		{base_link:29,sub_links:[31,32]}, //rumortwt --> bothtwt
		{base_link:34,sub_links:[36,37]} //debunktwt --> bothtwt
	];
	
	return result;
}

/**
 * Builds a mapping of field values to array indexes
 * @param arrayOfNamedObjects
 */
function buildIndexMapOnField(objectArray, field)
{
	var index = {};
	for (var i = 0; i < objectArray.length; i++)
	{
		index[objectArray[i][field]] = i;
	}
	return index;
}

function showLoadingMessage(text)
{
	loadingMessage.text(text)
		.style("visibility","visible");
}

function hideLoadingMessage()
{
	loadingMessage.style("visibility","hidden");
}

function displayError(message)
{
	d3.select("#main")
		.html(message);
}
