/**
 * This file has a constructor and methods for manipulating the tweet/author information area
 * located at the right of the RumorLens app. I separated this stuff out from TimeControl_bar.js
 * (now timebar.js) for the sake of modularity. 
 */

function TweetInfo()
{
	this.default_info_text = "No tweet currently selected.";
	
	//These four have to match up with rumorLens_demo.jsp
	this.information_panel_id = "informationPanel";
	this.tweet_display_id = "tweet_display";
	this.tweet_display_content_id = "tweet_display_content";
	this.tweet_display_info_id = "tweet_display_info";
	this.tweet_author_info_id = "tweetAuthorInfo";
	this.highlight_options_id = "highlightOptions";
	this.delete_button_div_id = "selected_delete_button_div";
	
	//D3 selections corresponding to the different sections of this component
	this.tweet_display_content_sel = null; 
	this.tweet_display_info_sel = null; 
	this.tweet_display_info_sel = null;
	this.tweet_author_info_sel = null;
	this.highlight_options_sel = null;
	this.delete_button_sel = null;
	
	this.idPattern = /\/statuses\/[0-9]+/; //part of a hacky way of determining whether a tweet is a retweet
	
	//A floating div with instructions for using the application that can be dismissed and recalled
	this.instruct = 
	{
		sel:null,
		css_class:"instructions",
		header_css_class:"instruction_header",
		footer_css_class:"instruction_footer",
		pic_css_class:"instruction_pic",
		initialized:false
		
	};

}

function initializeTweetInfo()
{
	tweetInfo.tweet_display_content_sel = d3.select("#"+tweetInfo.tweet_display_content_id);
	tweetInfo.tweet_display_info_sel = d3.select("#"+tweetInfo.tweet_display_info_id);
	tweetInfo.tweet_display_sel = d3.select("#"+tweetInfo.tweet_display_id);
	tweetInfo.delete_button_sel = d3.select("#"+tweetInfo.delete_button_div_id);
	
	tweetInfo.tweet_display_content_sel.html("No tweet selected");
	
	
/*	tweetInfo.tweet_author_info_sel = d3.select("#"+tweetInfo.tweet_author_info_id);
	tweetInfo.tweet_author_info_sel.html("No tweet selected");
	
	tweetInfo.highlight_options_sel = d3.select("#"+tweetInfo.highlight_options_id);*/
	
}

function displayTweetInfo(tweetIndex)
{
	if (tweetIndex >= 0)
	{
		//var infoHTML = constructTweetAuthorInfoHTML(tweetIndex);
		//tweetInfo.tweet_author_info_sel.html(infoHTML);
		
		//If we were able to load tweet display HTML from DB, then display that.
		//Otherwise, say the tweet was deleted.
		if (appData.tweets[tweetIndex].tweet)
		{
			console.log(appData.tweets[tweetIndex].tweet);
			tweetInfo.tweet_display_info_sel.html(generateTweetInfoHTML(tweetIndex));
			tweetInfo.tweet_display_content_sel.html(appData.tweets[tweetIndex].tweet);
/**/			//tweetInfo.tweet_display_sel.text("");
			//This calls Twitter-provided code in tweetdisplay.js which renders
			//the html we just called up correctly.
			twttr.widgets.load();
			tweetInfo.delete_button_sel
				.style("visibility","visible");
			
		}
		else
		{
			//tweetInfo.tweet_display_content_sel.html("");
			tweetInfo.tweet_display_info_sel.text("");
			tweetInfo.tweet_display_content_sel.html("Tweet deleted");
		}
	}
	else
	{
		//tweetInfo.tweet_display_content_sel.html("");
		tweetInfo.tweet_display_content_sel.text("No tweet selected");
		tweetInfo.delete_button_sel.style("visibility","hidden");
		tweetInfo.tweet_display_info_sel.html("");

	}
}

function generateTweetInfoHTML(index)
{
	var isRetweet = determineIfTweetIsRetweet(index);
	
	return "Author: " + appData.tweets[index].author_name + (isRetweet ? "<br><br>This tweet is a retweet":"")+"<br><br>Original tweet: ";
}

/**
 * Compile information about the given tweet into an HTML string to be displayed
 * @param tweetIndex
 */
function constructTweetAuthorInfoHTML(index)
{
	var html = "Tweet #" + index
	+ "<br>Date: " + shortDateFormat(new Date(appData.tweets[index].date))
	+ "<br>Author: " + appData.tweets[index].author_name
	+ "<br>Tweet ID: " + appData.tweets[index].tweetID
	+ "<br>Number of times author..."
	+ "<br>...was exposed to rumor: " + appData.authorInfo[index].rumorExp
	+ "<br>...was exposed to correction: " + appData.authorInfo[index].debunkExp
	+ "<br>...tweeted the rumor: " + appData.authorInfo[index].rumorTweet
	+ "<br>...tweeted the correction: " + appData.authorInfo[index].debunkTweet;
//	+ "<br>Number of people tweet exposed..."
//	+ "<br>...to rumor, for the first time: " + appData.passiveStateChanges[index][0]
//	+ "<br>...to correction, for the first time: " + appData.passiveStateChanges[index][2]
//	+ "<br>...to both, for the first time: <placeholder>";

	return html;
}

function showTweetInfoArea_old()
{
	TweetInfoArea.append("text").attr({
		'class' : 'feedback_msg'
	}).style("font-weight", "bold").text("");
}

function determineIfTweetIsRetweet(index)
{
	var tweet = appData.tweets[index];
	if (!tweet.tweet) return false;
	
	var embedID = tweetInfo.idPattern.exec(tweet.tweet)[0].substring(10);
	console.log(embedID);

	
	if (embedID == tweet.tweetID)
	{
		return false;
	}
	else
	{
		return true;
	}
}


