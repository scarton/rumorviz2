/**
 * Functions related to the barchart element of the visualization
 */

/**
 * Constructor for a bar graph object. The bar graph shows an identical array
 * of information to the sankey diagram, namely the number of people who
 * move into each state in the higlighted time interval. For each state, 
 * it visualizes how many people come from each source state via some colored
 * subintervals which hopefully aren't too hard to understand.
 * 
 * It differs from the Sankey diagram in that it doesn't lay out the information
 * spatially. 
 */
function BarGraph()
{

	// Prototype for bargraph object. Should contain all the values and functions needed
	// to control the behavior of the bar graph

	this.initialized = false;
	this.sel = null; // d3 selection that contains the whole element
	this.padding = 0.3;
	this.sourceWidth = 0.15; //Horizontal fraction of column that source indicators should take up
	this.width = 860;
	this.height = 600;
	this.margin = { top : 100,
		right : 20,
		bottom : 60,
		left : 80,
		middle : 80 };
	
	this.subgraph_height = this.height-this.margin.top-this.margin.bottom;
	this.subgraph_width = (this.width-this.margin.right-this.margin.middle-this.margin.left)/2;
	this.indices = [ 0, 1, 2];
	this.yLabel = "Number of twitter users";
	this.tooltip_css_class = "barchart_tooltip";
	this.css_class = "barchart";
	this.title_css_class = "barchart_title";
	this.x_axis_css_class = "barchart_x_axis";
	this.y_axis_css_class = "barchart_y_axis";
	this.bar_base_css_class = "barchart_base_bar"; //For grey bars that display the base cumulative total value
	this.bar_source_base_css_class = "barchart_base_source_bar";
	this.bar_sub_css_class = "barchart_sub_bar"; //For colored bars that display subvalues calculated in updateAppData()
	this.bar_source_sub_css_class = "barchart_sub_source_bar";
	
	// Bar charts of passive tweet exposure (e.g. number of first exposures to the rumor)
	this.p =
	{
		sel : null, // d3 sel that contains active graph
		x : null,
		y : null,
		baseVals : [{val:0},{val:0},{val:0}],
		
		//Each bar is accompanied by some sub-bars that show its sources
		//This array holds the values for those sub-bars. First element is
		//the starting value, which determines where the sub-bar starts,
		//second is its own value, which determines how tall it is
		baseSourceVals : [[{val:[0,0]}],
		                  [{val:[0,0]}],
		                  [{val:[0,0]},{val:[0,0]}]],
		                  
		subVals : [ {val:0}, {val:0}, {val:0} ],
		
		subBars: [null,null,null],
		
		subSourceVals : [[{val:[0,0]}],
		                 [{val:[0,0]}],
		                 [{val:[0,0]},{val:[0,0]}]],
		                 
/*		subSourceBars: [[null],[null],[null,null]],*/

		title : "Number of distinct users passively exposed",
		columns : [
			"Exposed to rumor only",
			"Exposed to correction only",
			"Exposed to both" ],
		columnClass : [
			"prumorbar",
			"pdebunkbar",
			"pbothbar"],
		
		columnSourceClass: [ //from top down
			["startbar"],
			["startbar"],
			["prumorbar","pdebunkbar"]],
			
		//We use this array as a map of how to generate barchart values.
		//The numbers correspond to indices of state-to-state transitions defined
		//by enumerateStateTransitions().
		//For example, the total number of people exposed to both is the sum over
		//all the different transitions into the "exposed to both" state, of which
		//there are six, that start with the following:
		//RumorExp1, RumorExp2, RumorExp3+, DebunkExp1, DebunkExp2, DebunkExp3+
		transitionIndices : [ [ 0 ],  [ 2 ], [ 5, 9, 12, 16, 20, 23 ] ],
		
		sourceIndices: [[[0]],[[2]],[[5,9,12],[16,20,23]]] 
	};

	// Bar charts of active tweeting activity (e.g. number of tweets of the rumor)
	this.a =
	{
		sel : null, // d3 selection that contains active graph
		x : null,
		y : null,
		
		baseVals : [{val:0},{val:0},{val:0}],
		baseSourceVals : [[{val:[0,0]},{val:[0,0]},{val:[0,0]},{val:[0,0]}],
		                  [{val:[0,0]},{val:[0,0]},{val:[0,0]},{val:[0,0]}],
		                  [{val:[0,0]},{val:[0,0]}]],
		                  
		subVals : [{val:0},{val:0},{val:0}],
		subBars: [null,null,null],
		subSourceVals : [[{val:[0,0]},{val:[0,0]},{val:[0,0]},{val:[0,0]}],
		                 [{val:[0,0]},{val:[0,0]},{val:[0,0]},{val:[0,0]}],
		                 [{val:[0,0]},{val:[0,0]}]],

		
		title : "Number of distinct users that have actively propagated",
		columns : [
			"Propagated rumor only",
			"Propagated correction only",
			"Propagated both" ],
		columnClass : [
			"arumorbar",
			"adebunkbar",
			"abothbar" ],
		columnSourceClass: [ //from top down
		         			["pbothbar","prumorbar","pdebunkbar","startbar"],
		         			["pbothbar","prumorbar","pdebunkbar","startbar"],
		         			["arumorbar","adebunkbar"]],
		transitionIndices : [
			[ 1, 6, 10, 13, 17, 21, 24, 26 ],
			[ 3, 7, 11, 14, 18, 22, 25, 27 ],
			[ 29, 31, 32, 34, 36, 37 ] ],
			
		sourceIndices: [[[26],[6,10,13],[17,21,24],[1]],
		                      [[27],[7,11,14],[18,22,25],[3]],
		                      [[29,31,32],[34,36,37]]]
	};
}

/**
 * Initialize the d3 elements of a BarChart object, as defined in the BarChart() function above
 * 
 * @param obj
 */
function initializeBarGraph()
{
	barGraph.sel = centralDiagram
		.append("g")
		.attr("class", barGraph.css_class)
		.style("visibility", "hidden");

	for (var modeNum = 0; modeNum < 2; modeNum++)
	{
		//The initialization procedure for the active and passive subgraphs is
		//almost identical, so use a loop instead of cookie cutter code
		var subGraph;
		var offsetFromLeft;
		var cumulativeStateChanges;
		if (modeNum == 0)
		{
			console.log("initializing passive bar graph");
			subGraph = barGraph.p;
			offsetFromLeft = barGraph.margin.left;
			cumulativeStateChanges = appData.cumulativePassiveChanges;
		}
		else
		{
			console.log("initializing active bar graph");
			subGraph = barGraph.a;
			offsetFromLeft = barGraph.margin.left + barGraph.subgraph_width
				+ barGraph.margin.middle;
			cumulativeStateChanges = appData.cumulativeActiveChanges;
		}

		//Populate bar graph by summing one or more cumulative transition counts
		//per bar
		for (var i = 0; i < subGraph.baseVals.length; i++)
		{
			subGraph.baseVals[i].val = 0;
			for (var j = 0; j < subGraph.transitionIndices[i].length; j++)
			{
				subGraph.baseVals[i].val += cumulativeStateChanges[subGraph.transitionIndices[i][j]];
			}
			
			for (var k =0; k < subGraph.baseSourceVals[i].length; k++)
			{
				subGraph.baseSourceVals[i][k].val[0] = 0;
				subGraph.baseSourceVals[i][k].val[1] = 0;
				for (var m =0; m < subGraph.sourceIndices[i][k].length; m++)
				{
					subGraph.baseSourceVals[i][k].val[1] += cumulativeStateChanges[subGraph.sourceIndices[i][k][m]];
					
				}
				for (var n = 0; n < k; n++)
				{
					subGraph.baseSourceVals[i][k].val[0] += subGraph.baseSourceVals[i][n].val[1];
				}
			}
		}
		console.log(subGraph.baseVals);
		console.log(subGraph.baseSourceVals);
		

		//Create x and y scales for this subgraph
		subGraph.x = d3.scale.ordinal()
			.rangeRoundBands([ 0, barGraph.subgraph_width ], barGraph.padding)
			.domain(subGraph.columns);

		subGraph.y = d3.scale.linear()
			.range([ barGraph.subgraph_height,0])
			.domain([0,d3.max(subGraph.baseVals,function(d){return d.val;}) ]);

		// Set up axes for passive and active bar charts
		var xAxis = d3.svg.axis().scale(subGraph.x).orient("bottom");

		var yAxis = d3.svg.axis().scale(subGraph.y).orient("left");


		// tooltips for base values of passive bar chart
		var bar_tip = d3.tip()
			.attr("class", barGraph.tooltip_css_class)
			.offset([ -10, 0 ])
			.html(function(d){
				//d will be an element of subGraph.baseVals
				return "<span>" + d.val +"</span>";});
		
		var source_tip = d3.tip()
		.attr("class", barGraph.tooltip_css_class)
		.offset([ -10, 0 ])
		.html(function(d){
			//d will be an element of subGraph.baseVals
			return "<span>" + d.val[1]+"</span>";});
	

		//Containing g element for this whole subgraph
		subGraph.sel = barGraph.sel
			.append("g")
			.attr("class", barGraph.css_class)
			.attr("width", barGraph.subgraph_width)
			.attr("height", barGraph.subgraph_height)
			.attr("transform", "translate(" + offsetFromLeft + ","
				+ barGraph.margin.top + ")");
		

		//title
		subGraph.sel
			.append("text")
			.attr("class", barGraph.title_css_class)
			.attr("x", subGraph.x(subGraph.columns[0]))
			.attr("y", -30)
			.attr("dy",0) //must specify this for wrapTextElement() to function correctly
			.text(subGraph.title)
			.call(wrapTextElement, barGraph.subgraph_width);

		//x axis with column titles
		subGraph.sel
			.append("g")
			.attr("class", barGraph.x_axis_css_class)
			.attr("transform", "translate(0," + barGraph.subgraph_height + ")")
			.call(xAxis)
			.selectAll(".tick text")
			.call(wrapTextElement, subGraph.x.rangeBand());

		//y axis and label
		subGraph.sel
			.append("g")
			.attr("class", barGraph.y_axis_css_class)
			.call(yAxis)
			.append("text")
			.attr("transform", "translate(-70,"
				+ (barGraph.subgraph_height / 3) + "), rotate(-90)")
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")
			.text(barGraph.yLabel);

		//Add one rectangle per base value
		var barHolder = subGraph.sel.append("g");
		var subHolder = subGraph.sel.append("g");
		
		for (var k = 0; k < subGraph.baseVals.length; k++)
		{
			var bar = barHolder.append("g");
			
			
			//Base source bar(s)
			bar.selectAll("."+barGraph.bar_source_base_css_class)
				.data(subGraph.baseSourceVals[k])
				.enter()
				.append("rect")
				.each(function(d,i){
					//console.log("Column " + k + " subcolumn " + i);
					//console.log(i);
					//console.log(d);
					//console.log(barGraph.subgraph_height);
					//console.log("Start value: " +d[0] + " to " +subGraph.y(d[0]));
					//console.log("Value: " + d[1] + " to "  +subGraph.y(d[1]));
//					console.log("Calculated y: " + (barGraph.subgraph_height-subGraph.y(d[0])-subGraph.y(d[1])));
				})
				.attr("y", function (d,i){
					console.log("y set to " + d.val[0]);
					return subGraph.y(d.val[0])-(barGraph.subgraph_height-subGraph.y(d.val[1]));
					})
				.attr("height", function (d,i){
					return barGraph.subgraph_height-subGraph.y(d.val[1]);
					//return 2;
					})
				.attr("x", function(){
					return subGraph.x(subGraph.columns[k]);})
				.attr("width", barGraph.sourceWidth* subGraph.x.rangeBand())
				.attr("class",function(d,i){
					//return barGraph.bar_source_base_css_class+ " " + subGraph.columnSourceClass[k][i];
					return barGraph.bar_source_base_css_class;
					})
				.on("mouseover", source_tip.show)
				.on("mouseout", source_tip.hide)
				.call(source_tip,"blurp");
		
			//Base main bar
			bar
				.append("rect")
				.datum(subGraph.baseVals[k])
				.attr("class", barGraph.bar_base_css_class)
				.attr("x", function(){
					return subGraph.x(subGraph.columns[k])+barGraph.sourceWidth* subGraph.x.rangeBand();})
				.attr("width", (1-barGraph.sourceWidth)* subGraph.x.rangeBand())
				.attr("y", function(d){
					return subGraph.y(d.val);})
				.attr("height", function(d){
					return barGraph.subgraph_height-subGraph.y(d.val);})
				.on("mouseover", bar_tip.show)
				.on("mouseout", bar_tip.hide)
				.call(bar_tip);
			
		
			
			//Sub main bar
			subGraph.subBars[k] = subHolder.append("g");
			subGraph.subBars[k]
				.append("rect")
				.datum(subGraph.subVals[k])
				.attr("class", function(){return barGraph.bar_sub_css_class + " "+subGraph.columnClass[k];})
				.attr("x", function(){
					return subGraph.x(subGraph.columns[k])+barGraph.sourceWidth* subGraph.x.rangeBand();})
				.attr("width", (1-barGraph.sourceWidth)* subGraph.x.rangeBand())
				.attr("y", function(d){
					//return barGraph.subgraph_height-subGraph.y(subGraph.subVals[k]);
					return subGraph.y(d.val);
					})
				.attr("height", function(d){
					
					return barGraph.subgraph_height-subGraph.y(d.val);
					})
				.on("mouseover", bar_tip.show)
				.on("mouseout", bar_tip.hide)
				.call(bar_tip);
			
			//Sub source bar(s)
			subGraph.subBars[k]
				.selectAll("."+barGraph.bar_source_sub_css_class)
				.data(subGraph.subSourceVals[k])
				.enter()
				.append("rect")
				.attr("y", function (d,i){

					return subGraph.y(d.val[0])-(barGraph.subgraph_height-subGraph.y(d.val[1]));
					})
				.attr("height", function (d,i){
					return barGraph.subgraph_height-subGraph.y(d.val[1]);
					})
				.attr("x", function(){
					return subGraph.x(subGraph.columns[k]);})
				.attr("width", barGraph.sourceWidth* subGraph.x.rangeBand())
				.attr("class",function(d,i){
					return barGraph.bar_source_sub_css_class+ " " + subGraph.columnSourceClass[k][i];
					//return barGraph.bar_source_base_css_class;
					})
				.on("mouseover", source_tip.show)
				.on("mouseout", source_tip.hide)
				.call(source_tip,"test");	
		}
	}
	barGraph.initialized = true;
}

/**
 * Updates the barchart object based on 
 * 
 */
function updateBarGraph()
{

	if (!barGraph.initialized) initializeBarGraph();
	
	//Avoid cookie cutter code by updating each subgraph in a loop
	for (var modeNum = 0; modeNum <2; modeNum ++)
	{
		var subGraph;
		var subCumulativeStateChanges;
		if (modeNum == 0)
		{
			//console.log("Updating passive bar graph");
			subGraph = barGraph.p;
			subCumulativeStateChanges = appData.subCumulativePassiveChanges;
		}
		else
		{
			//console.log("Updating active bar graph");

			subGraph = barGraph.a;
			subCumulativeStateChanges = appData.subCumulativeActiveChanges;
		}
		
		//Update subgraph subvalues
		for (var i = 0; i < subGraph.transitionIndices.length; i++)
		{
			subGraph.subVals[i].val = 0;
			for (var j = 0; j < subGraph.transitionIndices[i].length; j++)
			{
				subGraph.subVals[i].val += subCumulativeStateChanges[subGraph.transitionIndices[i][j]];
			}
			
			for (var k =0; k < subGraph.subSourceVals[i].length; k++)
			{
/*				subGraph.subSourceVals[i][k] = 0;
	
				for (var m =0; m < subGraph.sourceIndices[i][k].length; m++)
				{
					subGraph.subSourceVals[i][k] += subCumulativeStateChanges[subGraph.sourceIndices[i][k][m]];
				}*/
				subGraph.subSourceVals[i][k].val[0] = 0;
				subGraph.subSourceVals[i][k].val[1] = 0;
				for (var m =0; m < subGraph.sourceIndices[i][k].length; m++)
				{
					subGraph.subSourceVals[i][k].val[1] += subCumulativeStateChanges[subGraph.sourceIndices[i][k][m]];
					
				}
				for (var n = 0; n < k; n++)
				{
					subGraph.subSourceVals[i][k].val[0] += subGraph.subSourceVals[i][n].val[1];
				}
			}
		}
		console.log("subSourceVals:");
		console.log(subGraph.subSourceVals);
	
		for (var k = 0; k < subGraph.subVals.length; k++)
		{
			//console.log(subGraph.subBars[k]);
			subGraph.subBars[k].select("."+barGraph.bar_sub_css_class)
			.datum(subGraph.subVals[k])
			.attr("y", function(d){
				//return barGraph.subgraph_height-subGraph.y(subGraph.subVals[k]);
				return subGraph.y(d.val);

				})
			.attr("height", function(d){
				//console.log(subGraph.y(subGraph.subVals[k]));
				return barGraph.subgraph_height-subGraph.y(d.val);
				});
			

			subGraph.subBars[k]
				.selectAll("."+barGraph.bar_source_sub_css_class)
				.data(subGraph.subSourceVals[k])
				.attr("y", function (d,i){
					//console.log("New y for column " + k + " source " + i + ": " + (subGraph.y(subGraph.baseSourceVals[k][i][0])-(barGraph.subgraph_height-subGraph.y(d))));

					return subGraph.y(d.val[0])-(barGraph.subgraph_height-subGraph.y(d.val[1]));
					})
				.attr("height", function (d,i){
					//console.log("New height for column " + k + " source " + i + ": " +(barGraph.subgraph_height-subGraph.y(d)));
					return barGraph.subgraph_height-subGraph.y(d.val[1]);

					});
					
		}
	}
}

/**
 * Show the bar chart
 */
function showBarGraph()
{
	barGraph.sel.style("visibility", "visible");
	if (appData.overlay_visible)
		showBarGraphOverlay();
}

function hideBarGraph()
{
	console.log("Hiding all elements of bar graph");
	barGraph.sel.style("visibility", "hidden");
	//The overlay elements are the only ones for which I ever set their own distinct
	//visibility values, so everything else can just inherit from the top-level
	//bar graph element
	hideBarGraphOverlay();
	
}

function showBarGraphOverlay()
{
	barGraph.sel.selectAll("."+barGraph.bar_sub_css_class)
		.style("visibility","visible");
	
	barGraph.sel.selectAll("."+barGraph.bar_source_sub_css_class)
	.style("visibility","visible");
}

function hideBarGraphOverlay()
{
	barGraph.sel.selectAll("."+barGraph.bar_sub_css_class)
		.style("visibility","hidden");
	
	barGraph.sel.selectAll("."+barGraph.bar_source_sub_css_class)
	.style("visibility","hidden");
}
