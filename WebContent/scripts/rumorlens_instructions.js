/**
 * This script contains functions for generate instructions on how to use RumorLens.
 * 
 * It also contains code that implements a guided tour of the application
 * 
 * 
 */

function Instructions()
{

	this.sel=null;
	this.css_class="instructions";
	this.header_css_class="instruction_header";
	this.footer_css_class="instruction_footer";
	this.pic_css_class="instruction_pic";
	this.initialized=false;
	this.sankey_guider_ids = ["sankey_1","sankey_2","sankey_3"];
	this.list_guider_ids = ["list_1","list_2","list_3"];
	this.network_guider_ids = ["network_1","network_2","network_3"];
	this.default_guider_ids = ["timeline","timeline_2","info_panel"];
	this.tutorialShown = false;


}


function initializeGuiders()
{
	initializeGuiderSequence(appMode.COMBINED,instructions.sankey_guider_ids);
	initializeGuiderSequence(appMode.NETWORK,instructions.network_guider_ids);
	initializeGuiderSequence(appMode.USER_LIST,instructions.list_guider_ids);

}


/**
 * This function creates the whole tutorial sequence for one application mode, including
 * both mode-specific slides and non-mode-specific slides. There doesn't seem to be a good
 * way of editing guiders you have already made, so we just let each mode have its own
 * set of non-mode-specific slides for the sake of simplicity
 */
function initializeGuiderSequence(mode, mode_ids)
{
	var guider_ids = chooseGuiderIDs(mode);
	
	//Welcome
	guiders.createGuider({
		//attachTo:"#TimelineZone",
		buttons: [{name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: "Click the <b>\"Next\"</b> button to continue with this brief animated tutorial.",
		id: mode+"_"+"welcome",
		next:mode+"_"+"intro_1",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		//position:12,
		title:"Welcome!",
		width:400
		
	})
	
	
	//Intro 
	guiders.createGuider({
		//attachTo:"#TimelineZone",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: generateIntroHTML1(),
		id: mode+"_"+"intro_1",
		next:mode+"_"+"intro_2",
		prev:mode+"_"+"welcome",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		//position:12,
		title:"Introduction 1",
		width:1000
		
	})
	
	guiders.createGuider({
		//attachTo:"#TimelineZone",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: generateIntroHTML2(),
		id: mode+"_"+"intro_2",
		next:mode+"_"+"intro_3",
		prev:mode+"_"+"intro_1",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		//position:12,
		title:"Introduction 2",
		width:1000
		
	})
	
	guiders.createGuider({
		//attachTo:"#TimelineZone",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: generateIntroHTML3(),
		id: mode+"_"+"intro_3",
		next:mode+"_"+"intro_4",
		prev:mode+"_"+"intro_2",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		//position:12,
		title:"Introduction 3",
		width:1000
		
	})
	
	guiders.createGuider({
		//attachTo:"#TimelineZone",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: generateIntroHTML4(),
		id: mode+"_"+"intro_4",
		next:mode+"_"+"intro_5",
		prev:mode+"_"+"intro_3",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		//position:12,
		title:"Introduction 4",
		width:1000
		
	})
	guiders.createGuider({
		//attachTo:"#TimelineZone",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: generateIntroHTML5(),
		id: mode+"_"+"intro_5",
		next:guider_ids[0],
		prev:mode+"_"+"intro_4",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		//position:12,
		title:"Introduction 5",
		width:1000
		
	})
	
	
	//Timebar
	guiders.createGuider({
		attachTo:"#timebar_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: "A <b>timeline</b> at the bottom shows all the tweets as <b>colored vertical lines</b>." +
		"<br><br>" +
		"<b>Blue</b> for <b>rumor</b> tweets; <b>red</b> for <b>correction</b> tweets.",
		id: mode+"_"+"timeline",
		next:mode+"_"+"timeline_1_2",
		prev:guider_ids[0],
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:12,
		title:"Timeline",
		highlight:"#timebar_div",
		width:400
		
	});
	
	//Timebar
	guiders.createGuider({
		attachTo:"#timebar_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: function(){
		        	  if (timeBar.main_bar.brush.empty())
		        		  alert("Please highlight a timespan");
		        	  else
		        		  guiders.next();
		        	  }}],
		//description: "This is a guider...",s
		description: "<b>Clicking</b> on the timeline <b>highlights a timespan</b> in light blue, which appears <b>magnified</b> as a <b>second timeline</b> above the first one." +
		"<br><br>" +
		"You can <b>change</b> the highlighted area by:<ul>" +
		"<li>Dragging it</li>" +
		"<li>Dragging its left and right edges</li>" +
		"<li>Zooming in or out on it with a scroll wheel</li>" +
		"</ul> Changes made to the main timeline will <b>change the magnified timeline</b>, and <b>vice versa.</b>" +
		"<br><br>"+
		"Please <b>click on the timeline</b> to create a highlighted timespan. Try changing the size of the highlighted area in the ways described above.",
		id: mode+"_"+"timeline_1_2",
		next:guider_ids[1],
		prev:mode+"_"+"timeline",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:12,
		title:"Timeline: Highlighting a Timespan",
		highlight:"#timebar_div",
		width:400
		
	});
	
	
	guiders.createGuider({
		attachTo:"#timebar_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: function(){
		        	  if (appData.display_tweet == -1)
		        		  alert("Please select a tweet");
		        	  else
		        		  guiders.next();
		        	  
		        	  }}],
		//description: "This is a guider...",s
		description: "<b>Clicking</b> on a tweet in the highlighted region of <b>either</b> timeline <b>selects</b> that tweet. " +
				"<br><br>" +
				"That tweet is marked on both timelines as a <b>taller, thicker</b> line with a <b>brighter color.</b>"+
				"<br><br>" +
				"Please <b>click one of the tweets inside the highlighted timespan </b> to select a tweet from the timeline.",
		id: mode+"_"+"timeline_2",
		next:mode+"_"+"infopanel",
		prev:guider_ids[1],
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:12,
		title:"Timeline: Selecting a Tweet",
		highlight:"#timebar_div",
		width:400
		
	});
	
	
	guiders.createGuider({
		attachTo:"#tweetDisplayBox",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next}],
		//description: "This is a guider...",s
		description: "The <b>content</b> of the selected tweet is shown in the <b>\"Selected Tweet\"</b> panel"+
		"<br><br>" +
		"If the tweet is a <b>retweet</b>, both the original and retweeting author are displayed.",
		id: mode+"_"+"infopanel",
		next:guider_ids[2],
		prev:mode+"_"+"timeline_2",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:9,
		title:"Tweet Information Panel",
		highlight:"#tweetDisplayBox",
		width:250
		
	});
	
	guiders.createGuider({
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Finish", onclick: guiders.hideAll}],
		//description: "This is a guider...",s
  		onHide:function(){
			console.log("Calling onHide callback function...");
			displayAndHighlightTweets(-1);
			clearBrushAndMagnifiedTimeBar();
			},
		description: "This is the end of the interactive tutorial. " +
				"<br><br>" +
				"Click the <b>\"Show Cheatsheet\"</b> button in the <b>\"Help\"</b>panel to see a reference guide to the tool ",
/*				"<br><br>" +
				"Click the <b>\"Replay Tutorial\"</b> button to replay this tutorial.",*/
		prev:guider_ids[2],
		id: mode+"_"+"conclusion",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		title:"Conclusion",
		highlight: "#instructionBox",
		width:400
		
	});
	
	//Cheat sheet guider that shows everything
	guiders.createGuider({
		//attachTo:"#TimelineZone",
		buttons: [{name:"Close", onclick:guiders.hideAll}],

		//description: "This is a guider...",s
		description: generateCorrectCheatSheetHTML(mode),
		id: mode+"_"+"cheat_sheet",
		overlay:true,
		
		//position:12,
		title:"Cheat Sheet",
		width:1000,
		closeOnEscape:true,
		xButton:true
		
	});
	
	
/*	guiders.createGuider({
		attachTo:"#main_bar_div",
		buttons: [{name:"Next", onclick: guiders.hideAll}],
		//description: "This is a guider...",s
		description: "This is the main timebar",
		id: "timeline_2",
		//next:"sankey_2",
		overlay:true,
		position:3,
		title:"Main timeline",
		highlight:"#main_bar_div",
		width:250
		
	});*/
	
	switch (mode)
	{
	case appMode.COMBINED:
		initializeSankeyGuiders(mode);
		break;
	case appMode.USER_LIST:
		initializeListGuiders(mode);
		break;
	case appMode.NETWORK:
		initializeNetworkGuiders(mode);
		break;
	}
	

}

function initializeSankeyGuiders(mode)
{
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "This version of the tool uses a <b>Sankey diagram</b> to visually show the combined impact of <b>all</b> the tweets that made up a rumor."+
			"<br><br>" +
			"<b>Colored rectangles</b> represent <b>states</b> that people moved into. We track the same states on this diagram as in the introduction." +
			"<br><br>" +
			"<b>Hover</b> your mouse over a state rectangle to see <b>how many people</b> entered that state during the <b>whole timespan</b>."+
			"<br><br>" +
			"From the diagram, see if you can identify how many distinct people <b>tweeted the rumor</b>.",
		id: "sankey_1",
		next:"sankey_1_2",
		prev:mode+"_"+"intro_5",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Sankey Diagram: States",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "A <b>grey flow</b> between a pair of rectangles represents <b>the people who moved between those two states</b>." +
			"<br><br>"+
			"<b>Hover</b> your mouse cursor over a grey flow to see <b>how many people</b> made that state-to-state transition over the <b>whole timespan</b>."+
			"<br><br>" +
			"From the diagram, see if you can identify how many distinct people were <b>exposed</b> to <b>first the rumor, then the correction</b>.",
		id: "sankey_1_2",
		next:mode+"_"+"timeline",
		prev:"sankey_1",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Sankey Diagram: Transitions",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		id:"sankey_2",
		description: "Notice that there are now <b>black flows</b> on the Sankey diagram, overlaying the grey flows." +
			"<br><br>" +
			"<b>Black flows</b> on the Sankey diagram represent the impact of tweets in the <b>highlighted timespan</b>"+
			"<br><br>"+
			"From the diagram, see if you can identify how many distinct people <b>tweeted the rumor</b> for the first time during the time period you have highlighted.",
		next:mode+"_"+"timeline_2",
		prev:mode+"_"+"timeline",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Sankey Diagram: Impact of a Highlighted Timespan",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "The <b>impact</b> of the selected tweet is shown as <b>brightly colored flows</b> on the Sankey diagram." +
		"<br><br>" +
		"<b>Bright blue</b> if the tweet propagated the rumor; <b>bright red</b> if the tweet propagated the correction. "+
		"<br><br>"+
		"From the diagram, see if you can identify <b>what type of tweet</b> you have selected (rumor or correction) and <b> how many people </b> that tweet exposed to the information for the first time. ",

		id: "sankey_3",
		next:mode+"_"+"conclusion",
		prev:mode+"_"+"infopanel",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Sankey Diagram: Impact of a Selected Tweet",
		highlight:"#central_diagram_div",
		width:250
		
	});
}


function initializeListGuiders(mode)
{
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "This version of the tool uses a <b>sortable list</b> to display the the tweets that made up a rumor."+
		"<br><br>" +
		"Each row represents a <b>single tweet</b>. The row displays information about the impact of the tweet. " ,
		id: "list_1",
		next:"list_1_2",
		prev:mode+"_"+"intro_5",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Tweet List",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description:"Scroll <b>up and down</b> to browse through the tweets. Scroll <b>sideways</b> to see all the information." +
		"<br><br>" +
		"Column <b>footers</b> show the <b>sums of their columns</b>." +
		"<br><br>" +
		//"Scroll sideways to the column titled <b>\"Exposed to rumor &rarr; Exposed to both\"</b> to see that 22,716 people were first exposed to the <b>rumor</b>, then later exposed to the <b>correction.</b>",
		"From the diagram, see if you can identify how many people were first exposed to the rumor, then subsequently exposed to the correction.",

		id: "list_1_2",
		next:"list_1_3",
		prev:"list_1",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Tweet List",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "Click on a <b>column heading</b> to <b>sort</b> the list by that column. Click again to <b>reverse</b> the sort order." +
		"<br><br>" +
		"Sorting the list <b>resets</b> it to the top."+
		"<br><br>" +
		//"Click on the title of the  <b>\"# of followers of tweet author\"</b> column to see that user <b>spinmagazine</b> was the most followed account involved in this rumor.",
		"From the diagram, see if you can identify which user had the greatest number of followers.",

		id: "list_1_3",
		next:mode+"_"+"timeline",
		prev:"list_1_2",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Tweet List",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "The tweet list <b>only</b> shows tweets in the <b>highlighted timespan.</b> " +
				"<br><br>" +
				"Changing the highlighted timespan on the timeline causes the tweet list to <b>shrink<b> or <b>expand</b>."+
				"<br><br>" +
				"From the diagram, see if you can identify how many tweets were made during the selected timespan, and how many people they exposed to the rumor. ",
		id: "list_2",
		next:mode+"_"+"timeline_2",
		prev:mode+"_"+"timeline",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Tweet List: Impact of a Highlighted Timespan",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "The selected tweet is highlighted in a <b>bright color</b> on the list, blue for tweets that spread the rumor, red for tweets that spread the correction." +
		"<br><br>" +
		"The <b>\"selected\"</b> column of that row is also set to <b>\"true\"</b>." +
		"<br><br>" +
		"A tweet can <b>also</b> be selected by clicking on the corresponding row in the tweet list."+
		"<br><br>" +
		"Try clicking the \"Selected\" column header to find your selected tweet. Identify what type of tweet it was and how many people it exposed to its type of information.",
		
		id: "list_3",
		next:mode+"_"+"conclusion",
		prev:mode+"_"+"infopanel",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Tweet List: Impact of a Selected Tweet",
		highlight:"#central_diagram_div",
		width:250
		
	});
}


function initializeNetworkGuiders(mode)
{
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "This version of the tool uses a <b>network diagram</b> to visually show the the tweets that made up a rumor."+
		"<br><br>" +
		"Each circular colored <b>node</b> represents a <b>tweet</b>. " +
		"<br><br>" +
		"<b>Blue</b> tweets spread the <b>rumor</b>, <b>red</b> tweets spread the <b>correction</b>. " +
		"<br><br>" +
		"Node <b>size</b> is proportional to the <b>number of people exposed to new information</b> by the tweet." +
		"<br><br>" +
		"Using the diagram, see if you can find the single most impactful rumor tweet. Who was the author of this tweet?",

		id: "network_1",
		next:"network_1_2",
		prev:mode+"_"+"intro",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Network Diagram",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "<b>Arrows</b> between tweet nodes indicate <b>relationships</b> between those tweets"+
		"<br><br>" +
		"A <b>solid arrow</b> goes between two tweet nodes if the author of the target tweet <b>follows</b> the author of source tweet, and if the target tweet came <b>after</b> the source tweet. It indicates that the source tweet may have <b>inspired</b> the target tweet." +
		"<br><br>" +
		"A <b>dashed arrow</b> goes between two tweet nodes they are <b>consecutive</b> tweets by the <b>same author</b>." +
		"<br><br>" +
		"Using the diagram, see if you can identify an instance in which an author changed their mind.",
		
		id: "network_1_2",
		next:mode+"_"+"timeline",
		prev:"network_1",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Network Diagram",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "Tweet nodes are <b>faded to grey</b> if they were not tweeted during the highlighted timespan."+
		"<br><br>" +
		"Using the diagram, see if you can identify an instance in which an author changed their mind.",
		id: "network_2",
		next:mode+"_"+"timeline_2",
		prev:mode+"_"+"timeline",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Network Diagram: Impact of a Highlighted Timespan",
		highlight:"#central_diagram_div",
		width:250
		
	});
	
	guiders.createGuider({
		attachTo:"#central_diagram_div",
		buttons: [{name:"Back", onclick: guiders.prev},
		          {name:"Next", onclick: guiders.next()}],
		//description: "This is a guider...",s
		description: "The selected tweet is highlighted in a <b>bright color</b> on the network, blue for tweets that spread the rumor, red for tweets that spread the correction." +
		"<br><br>" +
		"A tweet can be <b>selected</b> from the diagram by <b>clicking</b> on it.",
		id: "network_3",
		next:mode+"_"+"conclusion",
		prev:mode+"_"+"infopanel",
		overlay:true,
		closeOnEscape:true,
		xButton:true,
		position:3,
		title:"Network Diagram: Impact of a Selected Tweet",
		highlight:"#central_diagram_div",
		width:250
		
	});
}

/**
 * Instigates a guided tour of the application using Optimizely's guiders 
 * js library, available at:
 * https://github.com/jeff-optimizely/Guiders-JS
 */
function showTutorial()
{
	if (instructions.tutorialShown)
		guiders.show(guiders.get(currentMode+"_"+"intro_5").next);
	else
		guiders.show(currentMode+"_"+"welcome");
	
	instructions.tutorialShown = true;

}


function chooseGuiderIDs(mode)
{
	switch (mode)
	{
	case appMode.COMBINED:
		return instructions.sankey_guider_ids;
	case appMode.USER_LIST:
		return instructions.list_guider_ids;
	case appMode.NETWORK:
		return instructions.network_guider_ids;
	default:
		return instructions.default_guider_ids;	
	}
}

/*function initializeInstructions()
{
	console.log("Instructions?!?!?!?");
	instructions.sel = d3.select("body")
		.append("div")
		.attr("class", instructions.css_class)
		.style("visibility", "hidden")
		.style("position", "absolute") // This is kinda hacky, but whatever
		.style("left", "25px")
		.style("top", "25px")
		.html(generateInstructionHTML());
	instructions.initialized = true;
}*/

/*function showInstructions()
{
	if (!instructions.initialized)
		initializeInstructions();
	instructions.sel.html(generateInstructionHTML());
	instructions.sel.style("visibility","visible");
}*/

/*function hideInstructions()
{
	instructions.sel.style("visibility","hidden");
}
*/
function showCheatSheet()
{
	guiders.show(currentMode+"_"+"cheat_sheet");
}

/**
 * This is a pain in the butt to maintain, but it's the simplest way of doing it
 * right now
 * @returns {String}
 *//*
function generateInstructionHTML()
{
	console.log("Regenerating instruction HTML");
	return 	generateIntroductionHTML()+
			"<br><br>" +
			"<h2>How to use the tool</h2>"+
			"<br><br>" +
			generateCorrectInstructionHTML() +
			"<br><br>" +
			 "<div class = \""+instructions.footer_css_class+"\">"+
			"<button id=\"show_instructions_button\" type=\"button\" onclick=\"hideInstructions()\">Hide Instructions</button>"+
			"</div>";

}*/

/*function generateIntroductionHTML()
{
	"<div class = \""+instructions.header_css_class+"\">"+
	"<h1>Instructions</h1>" +
	"Please read instructions"+
			"<button id=\"show_instructions_button\" type=\"button\" onclick=\"hideInstructions()\">Hide Instructions</button>" +
			"</div>" +
	"<h2>Introduction</h2>"+
	"<br>" +
	return "A <b>rumor</b> is a <b>disputed factual claim</b>. Rumors spread from person to person. " +
	"<br><br>"+
	"Social media platforms, such as <b>Twitter</b>, are one important medium in which rumors spread. " +
	"<br><br>"+
	"As you can see below, some tweets <b>propagate</b> rumors while others <b>correct</b> rumors." +
	"<br><br>"+
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/img/rumor.png\"></img>"+
	"</div>"+
	"<br><br>"+
	"One way of looking at rumors is as <b> the movement of people between states of interaction</b> with a rumor. " +
	"<br><br>"+
	"The image below shows some <b>states</b> that a person can be in with respect to a rumor, and ways that a person can <b>move between those states.</b>"+
	"<br><br>"+
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/transition_diagram_small.png\"></img>"+
	"</div>"+
	"<br><br>" +
	"As an <b>example</b>, consider the very simple social network pictured below:" +
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/social_network.png\"></img>"+
	"</div>"+
	"<br><br>" +
	"Like Twitter, people in this social network can follow each other. In this example, <b>arrows point from the followed person to the follower, in the direction of the information flow.</b>" +
	"<br><br>"+
	"Therefore, Bob and Alice follow each other, Jeff follows both of them, Sara follows Bob only, and Bill follows Alice only."+
	"<br><br>" +
	"Before anyone makes a tweet, all five people start in a state of <b>no interaction</b> with the rumor:"+
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/example_start.png\"></img>"+
	"</div>"+
	"<br><br>" +
	"When Bob tweets the rumor, <b>everyone who follows him</b> is moved to a state of having been <b>exposed to the rumor</b>. " +
	"<br><br>" +
	"Bob himself is moved straight to a state of having <b>tweeted the rumor.</b>"+
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/example_1.png\"></img>"+
	"</div>"+
	"<br><br>" +
	"Then Alice <b>tweets the correction</b>, so she is moved to the <b>\"Tweeted correction\"</b> state." +
	"<br><br>" +
	"Jeff saw the rumor before Alice exposed him to the correction, so he is moved to the <b>\"Exposed to both\"</b> state" +
	"<br><br>" +
	"Bill never saw Bob's rumor tweet, so he is moved to the state of having been <b> exposed to the correction </b>" +
	"<br><br>" +
	"Bob has already tweeted something, so we <b>do not record the change</b> in his exposure. We assume that he still stands by his opinion, so we leave him in his current state unless he tweets otherwise."+
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/example_2.png\"></img>"+
	"</div>"+
	"<br><br>" +
	"Notice that each of these tweets <b>moved some number of people between states.</b>" +
	"<br><br>" +
	"We consider the <b> impact of a tweet </b> to be the number of people it moved between each pair of states." +
	"<br><br>"+
	"If we consider the combined impact of <b>every tweet</b> that made up the rumor, we can understand the <b>impact of the rumor</b>."+
	"<br><br>" +
	"But how can we <b>visually represent</b> the impact of <b>hundreds</b> or <b>thousands</b> of tweets at once? " ;
	"<br><br>" +
	"That question is what our tool tries to answer."
}*/

function generateIntroHTML1()
{
	return "A <b>rumor</b> is a <b>disputed factual claim</b>. Rumors spread from person to person. " +
	"<br><br>"+
	"Social media platforms, such as <b>Twitter</b>, are one important medium in which rumors spread. " +
	"<br><br>"+
	"As you can see below, some tweets <b>propagate</b> rumors while others <b>correct</b> rumors." +
	"<br><br>"+
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/rumor.png\"></img>"+
	"</div>";
}
function generateIntroHTML2()
{
	return "One way of looking at rumors is as <b> the movement of people between states of interaction</b> with a rumor. " +
	"<br><br>"+
	"The image below shows some <b>states</b> that a person can be in with respect to a rumor, and ways that a person can <b>move between those states.</b>"+
	"<br><br>"+
	"This diagram may seem too complicated, but this tutorial will illustrate how it works with an example" +
	"<br><br>"+ 
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/transition_diagram_small.png\"></img>"+
	"</div>";
}
function generateIntroHTML3()
{
	return "Consider the very simple social network pictured below:" +
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/social_network.png\"></img>"+
	"</div>"+
	"<br><br>" +
	"Like Twitter, people in this social network can follow each other. In this example, <b>arrows point from the followed person to the follower</b>" +
	"<br><br>"+
	"Therefore, Bob and Alice follow each other, Jeff follows both of them, Sara follows Bob only, and Bill follows Alice only."+
	"<br><br>" +
	"Before anyone makes a tweet, all five people start in a state of <b>no interaction</b> with the rumor:"+
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/example_start.png\"></img>"+
	"</div>";
}
function generateIntroHTML4()
{
	return "When Bob tweets the rumor, <b>everyone who follows him</b> is moved to a state of having been <b>exposed to the rumor</b>. " +
	"<br><br>" +
	"Bob himself is moved straight to a state of having <b>tweeted the rumor.</b>"+
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/example_1.png\"></img>"+
	"</div>";
}
function generateIntroHTML5()
{
	return 	"Then Alice <b>tweets the correction</b>, so she is moved from where she was to the <b>\"Tweeted correction\"</b> state." +
	"<br><br>" +
	"Jeff saw the rumor before Alice exposed him to the correction, so he is moved to the <b>\"Exposed to both\"</b> state" +
	"<br><br>" +
	"Bill never saw Bob's rumor tweet, so he is moved to the state of having been <b> exposed to the correction </b>" +
	"<br><br>" +
	"Bob has already tweeted something, so we <b>do not record the change</b> in his exposure. We assume that he still stands by his opinion, so we leave him in his current state unless he tweets otherwise."+
	"<br><br>" +
	"<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/example_2.png\"></img>"+
	"</div>"+
	"<br><br>" +
	"Notice that each of these tweets <b>moved some number of people between states.</b>" +
	"<br><br>" +
	"We consider the <b> impact of a tweet </b> to be the number of people it moved between each pair of states." +
	"<br><br>"+
	"So for example, a <b>very impactful</b> rumor tweet would be one that moved a lot of people from \"No interaction\" to \"Exposed to rumor\" and, perhaps, from \"Exposed to correction\" to \"Exposed to both\"."+
	"<br><br>"+
	"If we consider the combined impact of <b>every tweet</b> that made up the rumor, we can understand the <b>impact of the rumor</b>."+
	"<br><br>" +
	"But how can we <b>visually represent</b> the impact of <b>hundreds</b> or <b>thousands</b> of tweets at once? "
}

/*function generateCorrectInstructionHTML()
{
	if (currentMode == appMode.COMBINED)
	{
		return generateSankeyInstructionHTML();
	}
	else if (currentMode == appMode.NETWORK)
	{
		return generateNetworkInstructionHTML();
	}
	else if (currentMode == appMode.USER_LIST)
	{
		return generateListInstructionHTML();
	}
	else 
		return "Invalid application mode...";
}*/

function generateCorrectCheatSheetHTML(mode)
{
	if (mode == appMode.COMBINED)
	{
		return generateSankeyPicHTML();
	}
	else if (mode == appMode.NETWORK)
	{
		return generateNetworkPicHTML();
	}
	else if (mode == appMode.USER_LIST)
	{
		return generateListPicHTML();
	}
	else 
		return "Invalid application mode...";
}

/*function generateSankeyInstructionHTML()
{
	return "This version of the tool uses a <b>Sankey diagram</b> to visually display the <b>combined impact of all the tweets</b> that made up a rumor."+
	"<br><br>" +
	"<b>Colored rectangles</b> represent <b>states</b>. <b>Flows</b> between rectangles represent <b>movement between states</b>" +
	"<br><br>" +
	"<b>Grey flows</b> between each pair of rectangles represent <b>the number of people who moved between each pair of states</b> as a result of all the tweets."+
	"<br><br>" +
	"A <b>timeline</b> at the bottom shows all the tweets as <b>colored vertical lines, blue for rumor, red for correction.</b>"+
	"<br><br>" +
	"<b>Clicking on the timeline highlights a timespan</b> in light blue, which appears <b>magnified</b> as a <b>second timeline</b> above the first one."+
	"<br><br>" +
	"The <b>impact of tweets in the highlighted timespan</b> is shown as <b>black flows</b> on the Sankey diagram."+
	"<br><br>" +
	"<b>Clicking on a tweet</b> in the timeline <b>selects that tweet</b>. The <b>content</b> of the tweet is shown in the <b>upper right-hand corner.</b>"+
	"<br><br>" +
	"The <b>impact of the selected tweet</b> is shown as <b>colored (blue or red) flows</b> on the Sankey diagram."+
	"<br><br>" +
	generateSankeyPicHTML();
	
}*/

function generateSankeyPicHTML()
{
	return "<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/instruct_sankey_notes.png\"></img>"+
	"</div>";
}

/*function generateNetworkInstructionHTML()
{
	return 	"This version of the tool uses a <b>network diagram</b> to visually show the the tweets that made up a rumor."+
	"<br><br>" +
	"Each circular colored <b>node</b> represents a <b>tweet</b>. Blue tweets spread the <b>rumor</b>, red tweets spread the <b>correction</b>. " +
	"<br><br>" +
	"Node <b>size</b> is proportional to the <b>number of people exposed to new information</b> by the tweet" +
	"<br><br>" +
	"A <b>solid arrow</b> goes between two tweet nodes if the author of the target tweet <b>follows the author of source tweet</b>, and the target tweet <b>came after the source tweet</b>. It indicates that <b>the source tweet may have inspired the target tweet</b>." +
	"<br><br>" +
	"A <b>dashed arrow</b> goes between two tweet nodes they are <b>consecutive tweets by the same author.</b>" +
	"<br><br>" +
	"A <b>timeline</b> at the bottom shows all the tweets as <b>colored vertical lines, blue for rumor, red for correction.</b>"+
	"<br><br>" +
	"<b>Clicking on the timeline highlights a timespan</b> in light blue, which appears <b>magnified</b> as a <b>second timeline</b> above the first one."+
	"<br><br>" +
	"Tweet nodes are <b>faded to grey</b> if they are <b>outside the highlighted timespan.</b>" +
	"<br><br>" +
	"<b>Clicking on a tweet</b> in the timeline <b>selects that tweet</b>. The <b>content</b> of the tweet is shown in the <b>upper right-hand corner.</b>"+
	"<br><br>" +
	"Tweets can be <b>selected</b> from the <b>diagram</b> as well as the timeline. The selected tweet shows up in a <b>brighter color</b> on the diagram." +
	"<br><br>" + generateNetworkPicHTML();
	
}
*/
function generateNetworkPicHTML()
{
	return "<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/instruct_network_notes.png\"></img>"+
	"</div>";
}

/*function generateListInstructionHTML()
{
	return 	"This version of the tool uses a <b>sortable list</b> to display the the tweets that made up a rumor."+
	"<br><br>" +
	"Each row represents a single tweet. The row displays information about the impact of the tweet. " +
	"<br><br>" +
	"Click on a column heading to sort the list by that column. Click again to reverse the sort order." +
	"<br><br>" +
	"Column footers show the sums of their columns" +
	"<br><br>" +
	"Scroll up and down to browse through the tweets. Scroll sideways to see all the information." +
	"<br><br>" +
	"A <b>timeline</b> at the bottom shows all the tweets as <b>colored vertical lines, blue for rumor, red for correction.</b>"+
	"<br><br>" +
	"<b>Clicking on the timeline highlights a timespan</b> in light blue, which appears <b>magnified</b> as a <b>second timeline</b> above the first one."+
	"<br><br>" +
	"Only tweets within the highlighted timespan appear on the list." +
	"<br><br>" +
	"<b>Clicking on a tweet</b> in the timeline <b>selects that tweet</b>. The <b>content</b> of the tweet is shown in the <b>upper right-hand corner.</b>"+
	"<br><br>" +
	"Tweets can be <b>selected</b> from the <b>list</b> as well as the timeline by clicking on a row. The selected tweet is highlighted in a bright color on the list." +
	"<br><br>" + generateListInstructionbHTML();

}*/

function generateListPicHTML()
{
	return "<div class = \""+instructions.pic_css_class+"\">"+
	"<img src = \"img/instruct_list_notes.png\"></img>"+
	"</div>";
}