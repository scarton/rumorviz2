/**
 * This script controls the behavior of a network diagram, one option for the 
 * central diagram. For a given time interval, nodes represent users who
 * tweeted during that interval, their size representing the number of followers
 * they have, their color representing what kind of tweet they tweeted.
 * 
 * Links between nodes represent follower relationships, with followers pointing
 * toward their followed nodes. 
 * 
 * It uses the d3 force layout, described at:
 * https://github.com/mbostock/d3/wiki/Force-Layout
 */

function NetworkDiagram(width, height)
{
	this.sel = null;
	this.marker = null;
	this.x_scale = null; //scales between node index and x position
	this.marker_css_class = "network_marker";
	this.max_radius = 25; //Maximum node circle radius, in pixels
	this.min_radius = 2;
	this.max_node_value = null; //maximum node value, calculated in generate____NetworkNodes
	this.css_class = "network_diagram";
	this.faded_css_class = "faded";
	this.node_css_class = "network_node";
	this.link_css_class = "network_link";
	this.rumor_css_class = "rumor";
	this.debunk_css_class = "debunk";
	this.both_css_class = "both";
	this.self_css_class = "self";
	this.tooltip_css_class = "network_tooltip";
	this.highlighted_css_class = "highlighted";
	this.network = null; //d3 force diagram object that does all the calculation of node positions and stuff
	this.width = width;
	this.height = height;
	this.ticks = 1000;
	this.top_margin = 15;
	this.bottom_margin = 15;
	this.rumor_marker_id = "network_marker_rumor";
	this.debunk_marker_id = "network_marker_debunk";
	this.both_marker_id = "network_marker_both";
	this.faded_marker_id = "network_marker_faded";
	this.highlighted_rumor_marker_id = "network_marker_rumor_highlighted";
	this.highlighted_debunk_marker_id = "network_marker_debunk_highlighted";
	this.highlighted_both_marker_id = "network_marker_both_highlighted";
	this.self_marker_id = "network_marker_self";
	this.nodes = null;
	this.nodeMap = null;
	this.links = null;
	this.title = "Network diagram";
	this.title_css_class = "network_title";
	this.loading_message_css_class = "network_loading_message";
	this.loading_width = 300;
	this.loading_height = 200;
	this.loading_message = "Calculating network layout...";
	this.loading_message_sel = null;
	this.x_offset =0;
	this.y_offset=0;
	this.prev_scale = 1;
	this.targetNodeArea = 0.2;

}

/**
 * initialize the network diagram
 */
function initializeNetworkDiagram()
{
	console.log("Initializing network diagram...");
	
	//Define canvas and title element
	networkDiagram.sel = centralDiv.append("svg")
		.attr("class",networkDiagram.css_class)
		.attr("height",networkDiagram.height)
		.attr("width",networkDiagram.width);
	
	networkDiagram.sel.call( d3.behavior.drag()
	      .on("drag",dragNetwork) );
	networkDiagram.sel.call( d3.behavior.zoom()
	      .on("zoom", function(){zoomNetwork(this)}) );

	networkDiagram.sel
		.append("text")
		.attr("class",networkDiagram.title_css_class)
		.text(networkDiagram.title)
		.attr("x",15)
		.attr("y",60);
	
	
	//Define one arrow for rumor links, one for debunk links, etc
	//Will be attaching these things to the actual links later.
	//Kind of confusing way of doing things. 
	networkDiagram.marker = networkDiagram.sel.append("svg:defs")
		.selectAll("."+networkDiagram.marker_css_class)
		.data([networkDiagram.rumor_marker_id,
		       networkDiagram.debunk_marker_id, 
		       networkDiagram.both_marker_id,
		       networkDiagram.faded_marker_id,
		       networkDiagram.highlighted_rumor_marker_id,
		       networkDiagram.highlighted_debunk_marker_id,
		       networkDiagram.highlighted_both_marker_id,
		       networkDiagram.self_marker_id])
		.enter()
		.append("svg:marker")
		.attr("class",networkDiagram.marker_css_class)
		.attr("id",function(d){return d;})
		.attr("viewBox","0 -5 10 10")
		.attr("refX",10)
		.attr("refY",0)
		.attr("markerWidth", 5)
		.attr("markerHeight", 5)
		.attr("orient", "auto")
		.append("svg:path")
		.attr("d", "M0,-5L10,0L0,5");
	
	//define loading message to display while we wait for network diagram to generate
	networkDiagram.loading_message_sel = networkDiagram.sel.append("g")
		.attr("transform","translate("+(networkDiagram.width/2-networkDiagram.loading_width/2)+
			","+(networkDiagram.height/2-networkDiagram.loading_height/2)+")")
		.attr("class",networkDiagram.loading_message_css_class);
		
	networkDiagram.loading_message_sel.append("rect")
		.attr("height",networkDiagram.loading_height)
		.attr("width",networkDiagram.loading_width);

	networkDiagram.loading_message_sel.append("text")
		.attr("class",networkDiagram.title_css_class)
		.attr("text-anchor", "middle")
		.attr("x",networkDiagram.loading_width/2)
		.attr("y",networkDiagram.loading_height/2)
		.text(networkDiagram.loading_message);
	
	
	
	setTimeout(finishInitializingNetworkDiagram,200);
	
}


/**
 * This should be called from initializeNetworkDiagram after long enough to 
 * render a loading message.
 */
function finishInitializingNetworkDiagram()
{
	//var k = Math.sqrt(appData.tweets.length / (networkDiagram.width * networkDiagram.height));

	
/*	networkDiagram.network = d3.layout.force()
		.size([networkDiagram.width,networkDiagram.height-networkDiagram.top_margin-networkDiagram.bottom_margin])
		.charge(-1200)
		.chargeDistance(50)
		.gravity(1.0)
		.linkDistance(50)
		.linkStrength(1);*/
	
	networkDiagram.x_scale = d3.scale.linear()
		.domain([0,appData.tweets.length-1])
		.range([15,networkDiagram.width-15]);

	//One node per tweet
	var nodesAndNodeMap = generateTweetNetworkNodes(appData.tweets,networkDiagram.x_scale, networkDiagram.height);
	var nodes = nodesAndNodeMap.nodes;
	networkDiagram.nodes = nodes;
	var nodeMap = nodesAndNodeMap.nodeMap;
	networkDiagram.nodeMap = nodeMap;
	//Link from A to B says author of B follows author of A and B came after A
	//Authors considered to follow themselves, and we only link consecutive author nodes
	var links = generateTweetNetworkLinks(appData.tweets,appData.neighborhoodTweets,appData.tweetIDIndexMap, nodes, nodeMap);
	networkDiagram.links = links;
	console.log("Starting network force simulation...");
	console.log("Nodes before layout: " + nodes.length + " found");
	console.log(nodes);
	console.log("Links before layout: " + links.length + " found");
	console.log(links);
	var start = new Date();
	
	var str = 1600/links.length-0.22;
	console.log("Link strength: " + str);
	networkDiagram.network = d3.layout.custom_force_2()
	.size([networkDiagram.width,networkDiagram.height-networkDiagram.top_margin-networkDiagram.bottom_margin])
	//.charge(-300)
	.charge(function(d){return -300*d.radius + -100*d.weight;})
	//.charge(function(d){return 1000*d.weight;})
	.chargeDistance(50)
	.gravity(0.2)
	.linkDistance(30)
	.linkStrength(str);

	
	//Calling this plugin changes the values of nodes and links, so when we
	//reference them hereafter they will have the calculated x and y values
	networkDiagram.network
		.nodes(nodes)
		.links(links)
		.start();
	
	nodes.forEach(updateNodeAttributes);
	
	//Complete a predetermined number of steps in the simulation
	for (var i = 0; i < networkDiagram.ticks; ++i) 
		networkDiagram.network.tick();
	networkDiagram.network.stop();
	
	var end = new Date();
	console.log((end.getTime()-start.getTime()) + " ms elapsed"); 
	
	console.log("Nodes after layout");
	console.log(nodes);
	console.log("Links after layout");
	console.log(links);
	
	
	networkDiagram.loading_message_sel.style("visibility","hidden");
	
	
	networkDiagram.elementHolder = 	networkDiagram.sel.append("g")
		.attr("transform","translate(0,"+networkDiagram.top_margin+")");
	//For each link, create a straight svg path between its nodes
	networkDiagram.elementHolder
		.selectAll("."+networkDiagram.link_css_class)
		.data(links)
		.enter()
		.append("path")
		.attr("class", function(d){return chooseLinkClass(d);})
		.attr("d", function(d){return calculateLinkDimensions(d);})
		.on("click",function(d){
			displayAndHighlightTweets(d.source.tweet_index);
		})
		.attr("marker-end",function(d) { return "url(#"+chooseMarkerID(d)+")";});

	//Define a tooltip for nodes
	var node_tip = d3.tip()
		.attr("class", networkDiagram.tooltip_css_class)
		.offset([ -10, 0 ])
		.html(function(d){
			//d will be an element of subGraph.baseVals
			return generateTweetNodeTitle(d);});
	
	//For each node, append a circle
	networkDiagram.elementHolder
		.selectAll("."+networkDiagram.node_css_class)
		.data(nodes)
		.enter()
		.append("svg:circle")
		.attr("class",function(d){return chooseNodeClass(d);})
		.attr("cx", function(d) { return d.x; })
		.attr("cy", function(d) { return d.y; })
        .attr("r", function(d) { return d.radius;})
        .on("click",function(d){
        	displayAndHighlightTweets(d.tweet_index);
        	//maybeChangeHighlightModeAndTweet(highlightMode.AUTHOR, d.tweet_index);
        	})
		.append("title")
		.html(function (d) {return generateTweetNodeTitle(d);});
	
	
	networkDiagram.initialized = true;
}


function moveToFront(element)
{
	element.parentNode.appendChild(element);
}
/**
 * Calculate the radius of the circle that represents a tweet author.
 * 
 * Currently basing radius on log(# of followers). 
 * @param node
 * @returns
 */
/*function userNodeRadius(node)
{
	return valueToRadius(tweetIndexValue(node.tweet_indices[0]));
	
}

function tweetNodeRadius(node)
{
	return valueToRadius(tweetIndexValue(node.tweet_index));
	
}*/

function adjustRadius(radius, scaleFactor)
{
	return Math.max(networkDiagram.min_radius,Math.round(scaleFactor*radius));
}
function valueToRadius(value)
{
	return Math.pow(value,1/2);
}

function tweetIndexValue(index)
{
	//var val = appData.authorInfo[index].followerCount+1;
	return tweetFirstExposure(index)+tweetBothExposure(index);
}

function tweetFirstExposure(index)
{
	var val;
	if (appData.tweets[index].type == 1)
		val = appData.passiveStateChanges[index][0];
	else
		val = appData.passiveStateChanges[index][2];

	return val;
}

function tweetBothExposure(index)
{
	var val;
	if (appData.tweets[index].type == 1)
		val = appData.passiveStateChanges[index][16] +
		appData.passiveStateChanges[index][20] +
		appData.passiveStateChanges[index][23];
	else
		val = appData.passiveStateChanges[index][5] +
		appData.passiveStateChanges[index][9] +
		appData.passiveStateChanges[index][12];

	return val;
}



function generateUserNodeTitle(node)
{
	return "Author "+node.author_id +"\n"+formatNumber(appData.authorInfo[node.tweet_indices[0]].followerCount) + " followers";
}

function generateTweetNodeTitle(node)
{
	var index = node.index;
	var title;
	var type;
	var exp, alt_exp;
	var bexp1,bexp2,bexp3;
	var a1;
	var a2;
	var a3;
	var a4;
	var rumor;
	if (appData.tweets[node.index].type == 1)
	{
		type = "Rumor";
		exp = 0;
		alt_exp = 2;
		bexp1 = 16;
		bexp2 = 20;
		bexp3 = 23;
		rumor = true;
	}
	else
	{
		type = "Correction";
		exp = 2;
		bexp1 = 5;
		bexp2 = 9;
		bexp3 = 12;
		rumor = false;
	}
	
	title = "Tweet #"+node.index + ": " + type +
	"<br>"+generateTransitionTitleHTML(transitions[exp])+": " + appData.passiveStateChanges[index][exp]+
	"<br>"+generateTransitionTitleHTML(transitions[bexp1])+": " + tweetBothExposure(index);
	
	for (var i = 0; i < sankeyLinkReductions.length; i ++)
	{
		var reduced = sankeyLinkReductions[i];
		if (transitions[reduced.base_link].active && transitions[reduced.base_link].rumor == rumor)
		{
			var sum = appData.combinedStateChanges[index][reduced.base_link];
			for (var j =0; j < reduced.sub_links.length; j++)
			{
				sum += appData.combinedStateChanges[index][reduced.sub_links[j]];
			}
			if (sum > 0)
			{
				title += 	"<br>"+generateTransitionTitleHTML(transitions[reduced.base_link])+": " + sum;

			}
		}
	}
	
	return title;
		
}

/**
 * This function calculates the x and y values for the two endpoints of a link
 * based on its two end nodes. 
 * 
 * It cuts the link short to just touch the edge of the target node.
 * 
 * It also checks whether it is reflexive, and if so scoots itself a bit up or
 * down so that both links can be seen
 * 
 * It also takes into account any global x and/or y offsets that have been set
 * 
 * @param link
 * @returns {String}
 */
function calculateLinkDimensions(link)
{
	//End the path at the edge of the node it is connecting to, so the arrow looks right
	var slope = (link.target.y-link.source.y)/(link.target.x-link.source.x);
	var theta = Math.atan(slope);
	if (link.target.x > link.source.x) theta += Math.PI;
	
	var r1 = link.source.radius;
	var r2 = link.target.radius+2;

	var xOff = Math.cos(theta);
	var yOff = Math.sin(theta);
//	var tanSign = (link.target.x > link.source.x) ? -1:1;
	
	var x1 = (link.source.x-r1*xOff);
	var y1 = (link.source.y-r1*yOff);
	var x2 = (link.target.x+r2*xOff);
	var y2 = (link.target.y+r2*yOff);

	
	if (link.reflexive)
	{
		
		x1 += 2*yOff;
		y1 -= 2*xOff;
		
		x2 += 2*yOff;
		y2 -= 2*xOff;
	}
	
	return "M"+x1+","+y1+"L"+x2+","+y2;
}


/**
 * Create a node for each distinct author. Attach to each one a list
 * of the tweets that author made.
 */
function generateUserNetworkNodes(tweets,scale)
{
	var nodes = []; //Chronological list of nodes we'll be able to pass to the layout calculator
	var node;
	var nodeMap = {}; //mapping from author ID to node 
	var max_weight = 0;
	for (var i = 0; i < tweets.length; i++)
	{
		if (nodeMap[tweets[i].author_id])
		{
			//console.log("Author already recorded: " + tweets[i].author_id);
			nodeMap[tweets[i].author_id].tweet_indices.push(i);
		}
		else
		{
			//console.log("New author found: "+ tweets[i].author_id);
			var weight = tweetIndexValue(i);
			if (weight > max_weight) max_weight = weight;
			node = {
				x:scale(i),
				y:0,
				tweet_indices:[i],
				//weight: weight,
				author_id: tweets[i].author_id,
				
				//Set of booleans that will determine how to color the node.
				//Reassigned in chooseNodeClass(). Useful to store here because
				//attached links will refer to them as well
				rumor_span:false,
				debunk_span:false,
				rumor_highlight:false,
				debunk_highlight:false
			};
			nodes.push(node);
			nodeMap[tweets[i].author_id] = node;
		}
	}
	
	//Scale node weights so that they top out at chosen maximum
	//console.log(max_weight);
	 var weightscale = networkDiagram.max_radius/max_weight;
	 

	networkDiagram.value_radius_scale = weightscale;
	return {nodes:nodes,nodeMap:nodeMap};
}

/**
 * Creates a node for each distinct tweet. Generates a map of author ID to list
 * of nodes associated with that author, a bit different from that generated by 
 * generateUserNetworkNodes
 * 
 */
function generateTweetNetworkNodes(tweets)
{
	
	var nodes = []; //Chronological list of nodes we'll be able to pass to the layout calculator
	var node;
	var nodeMap = {}; //mapping from author ID to node 
	var max_val = 0;
	var total_val = 0;
	for (var i = 0; i < tweets.length; i++)
	{
		

			//console.log("New author found: "+ tweets[i].author_id);
		var val = tweetIndexValue(i);
		if (val > max_val) max_val = val;
		total_val += val;
		node = {
			tweet_index:i,
			author_id: tweets[i].author_id,
			value:val,
			radius:valueToRadius(val),
			//Set of booleans that will determine how to color the node.
			//Reassigned in chooseNodeClass(). Useful to store here because
			//attached links will refer to them as well
			rumor_span:false,
			debunk_span:false,
			rumor_highlight:false,
			debunk_highlight:false
		};
		nodes.push(node);
		
		//Collect all tweets by the same author into a map listing so we can
		//link them easily when we generate links
		if (nodeMap[tweets[i].author_id])
		{
			nodeMap[tweets[i].author_id].push(node);
		}
		else
		{
			nodeMap[tweets[i].author_id] = [node];
		}
			
	}
	
	//scale node radii so that no node has a radius that is too big or too small
	var sample_max_radius = valueToRadius(max_val);
	var scale_factor = networkDiagram.max_radius/sample_max_radius;
	
	//Scale node radii so that nodes take up some pre-selected percentage of the viewing window
	var nodeArea = Math.PI*total_val/(networkDiagram.height*networkDiagram.width);
	console.log("Nodes taking up " + nodeArea + " of window before adjustment. Aiming for " + networkDiagram.targetNodeArea);
	var scale_factor = Math.pow(networkDiagram.targetNodeArea/nodeArea,1/2);
	console.log("Scale factor: " + scale_factor);

	//if the maximum node radius would still be too large, smallen it
	var sample_max_radius = scale_factor*valueToRadius(max_val);
	if ( sample_max_radius > networkDiagram.max_radius)
	{
		scale_factor = scale_factor * (networkDiagram.max_radius/sample_max_radius);
	}
	
	for (var i =0; i < nodes.length; i++)
	{
		nodes[i].radius = adjustRadius(nodes[i].radius,scale_factor);
	}
	
	
	
	return {nodes:nodes,nodeMap:nodeMap};
}


/**
 * Generates set of links for a network where each node is a distinct user and
 * links are between users who follow each other
 * @param tweets
 * @param neighborhoodTweets
 * @param idIndexMap
 * @param nodes
 * @param nodeMap: maps one author ID to one node
 * @returns {Array}
 */
function generateUserNetworkLinks(tweets,neighborhoodTweets, idIndexMap, nodes, nodeMap)
{
	var links = [];
	var followerTweets;
	var followers;
	var followerID;
	var followerFollowerTweets;
	var reflexive;
	for (var i = 0; i < nodes.length; i++)
	{
		//console.log("Tweet index: " +  nodes[i].tweet_indices[0]);
		followerTweets = neighborhoodTweets[nodes[i].tweet_indices[0]].tweetsOfFollowers; //All tweets by the same author have the same neighborhood tweets, so the set for the first one can stand in for all of them
		//console.log("Follower tweets: " + neighborhoodTweets[nodes[i].tweet_indices[0]].tweetsOfFollowers);
		followers = {};
		
		for (var j = 0; j < followerTweets.length; j++)
		{
			console.log("Creating link");
			followerID = tweets[idIndexMap[followerTweets[j]]].author_id;
			
			//Check to see if this link is reflexive
			reflexive = false;
			followerFollowerTweets = neighborhoodTweets[nodeMap[followerID].tweet_indices[0]].tweetsOfFollowers;
	/*		console.log(nodeMap[followerID].tweet_indices[0]);
			console.log(followerFollowerTweets);*/
			for (var k = 0; k < followerFollowerTweets.length; k++)
			{
				//if the first tweet by the follower has, as a tagged follower tweet, the first tweet by
				//the node author, then the link we are about to make has a counterpart that has been
				//or will be created somewhere else in this loop. 
				if (idIndexMap[followerFollowerTweets[k]] == nodes[i].tweet_indices[0])
					reflexive = true;
			}
			
			if (! followers[followerID])
			{
				links.push({source:nodes[i],
					target:nodeMap[followerID],
					reflexive:reflexive});
				
				followers[followerID] = true;
			}
		}
	}
	
	return links;
}

/**
 * Generates set of links for network where each tweet is a node
 * @param tweets
 * @param neighborhoodTweets
 * @param idIndexMap
 * @param nodes
 * @param nodeMap: maps each user ID to a list of one or more nodes
 * @returns {Array}
 */
function generateTweetNetworkLinks(tweets,neighborhoodTweets, idIndexMap, nodes, nodeMap)
{
	var links = [];
	var followerTweets;
	var authorNodes = [];
	var followerTweetIndex;
	//For each tweet node, add a link to tweets made by the followers of the author of that tweet,
	//as enumerated by the neighborhoodTweets array
	for (var i = 0; i < nodes.length; i++)
	{
		//console.log("Tweet index: " +  nodes[i].tweet_indices[0]);
		followerTweets = neighborhoodTweets[i].tweetsOfFollowers; //All tweets by the same author have the same neighborhood tweets, so the set for the first one can stand in for all of them
		//console.log("Follower tweets: " + neighborhoodTweets[nodes[i].tweet_indices[0]].tweetsOfFollowers);
		
		for (var j = 0; j < followerTweets.length; j++)
		{
			followerTweetIndex = idIndexMap[followerTweets[j]];
		
			//Only draw links that go forward in time
			if (followerTweetIndex > i)
			{
				links.push({source:nodes[i],
					target:nodes[followerTweetIndex],
					reflexive:false});
			}
		}
	}
	
	//Add a link between each pair of consecutive tweets by the same author
	for (authorID in nodeMap)
	{
		authorNodes = nodeMap[authorID];
		for (var i = 0; i < authorNodes.length-1; i++)
		{
			links.push({source:authorNodes[i],
				target:authorNodes[i+1],
				reflexive:false,
				self:true});

		}
	}
	return links;
}

function showNetworkDiagram()
{
	networkDiagram.sel.style("visibility","visible");
}

/**
 * This function reassigns css classes to nodes and links to change their
 * appearance. Nodes and links that do not have a tweet in the current timespan
 * are greyed out. If they do have at least one tweet in the timespan, they
 * are colored based on whether they have a rumor tweet, a debunk tweet, or both.
 * 
 * If at least one of their tweets is highlighted, they are assigned a brighter color
 */
function updateNetworkDiagram()
{
	if (! networkDiagram.initialized)
	{
		initializeNetworkDiagram();
	}
	else
	{
		networkDiagram.nodes.forEach(updateNodeAttributes);
	}
	
//	console.log(networkDiagram.nodes);
	console.log("Updating network diagram...");
	
	networkDiagram.sel.selectAll("."+networkDiagram.node_css_class)
		.attr("class",function(d){
			//console.log(chooseNodeClass(d));
			return chooseNodeClass(d);})
		.each(function(d){if (d.rumor_highlight || d.debunk_highlight)
			{
				console.log("Attempting to move node " + d + " to front ");
				moveToFront(this);
			}
		});
			
		

	networkDiagram.sel.selectAll("."+networkDiagram.link_css_class)
		.attr("class",function(d){
			//console.log(chooseLinkClass(d));
			return chooseLinkClass(d);})
		.attr("marker-end",function(d) { return "url(#" +chooseMarkerID(d) +")";})
		.each(function(d){if (d.source.rumor_highlight || d.source.debunk_highlight)
			{
			console.log("Attempting to move link " + d + " to front ");

				moveToFront(this);
			}
		});
}

/**
 * Return all nodes and links to their default appearance
 */
function hideNetworkDiagramOverlay()
{
	networkDiagram.sel.selectAll("."+networkDiagram.node_css_class)
		.attr("class", function(d){return chooseNodeClass(d);});
				
	networkDiagram.sel.selectAll("."+networkDiagram.link_css_class)
		.attr("class", function(d){return chooseLinkClass(d);});
}

/**
 * Assign a CSS class to a link based on the type of its source
 * @param link
 * @returns {String}
 */
function chooseLinkClass(link)
{
	var node = link.source;
	var classStr;

	if (node.rumor_highlight || node.debunk_highlight)
	{
		if (node.rumor_highlight && node.debunk_highlight)
		{
			classStr =  networkDiagram.link_css_class + " " + networkDiagram.both_css_class + " " + networkDiagram.highlighted_css_class;
		}
		else if (node.rumor_highlight)
		{
			classStr =  networkDiagram.link_css_class + " " + networkDiagram.rumor_css_class + " " + networkDiagram.highlighted_css_class;
		}
		else
		{
			classStr =  networkDiagram.link_css_class + " " + networkDiagram.debunk_css_class + " " + networkDiagram.highlighted_css_class;
		}
	}
	else if (node.rumor_span || node.debunk_span)
	{
		if (node.rumor_span && node.debunk_span)
		{
			classStr =  networkDiagram.link_css_class + " " + networkDiagram.both_css_class;
		}
		else if (node.rumor_span)
		{
			classStr =  networkDiagram.link_css_class + " " + networkDiagram.rumor_css_class;
		}
		else
		{
			classStr =  networkDiagram.link_css_class + " " + networkDiagram.debunk_css_class;
		}
	}
	else
	{
		classStr =  networkDiagram.link_css_class + " " + networkDiagram.faded_css_class;
	}
	
	if (link.self)
	{
		classStr =  classStr+ " " + networkDiagram.self_css_class;
	}
	
	return classStr;
}

/**
 * Reassign node coloring based what timespan has been highlighted and what
 * set of tweets has been highlighted
 * @param node
 */
function calculateUserNodeColoring(node)
{
	node.rumor_span = false;
	node.debunk_span = false;
	node.rumor_highlight = false;
	node.debunk_highlight = false;
	var index;
	
	for (var i = 0; i < node.tweet_indices.length; i++)
	{
		index = node.tweet_indices[i];
		//Check if the tweet is highlighted, and is a rumor or a debunk
		if (appData.highlight_tweets[index])
		{
			if (appData.tweets[index].type == 1)
				node.rumor_highlight = true;
			else
				node.debunk_highlight = true;
		}
		
		//Check if tweet is in timespan, and is a rumor or debunk
		//If no timespan is highlighted, treat every tweet as being highlighted, so that the diagram defaults to having all nodes colored
		if ((appData.subEnd < 0) || (index >= appData.subStart && index <= appData.subEnd))
		{
			if (appData.tweets[index].type == 1)
				node.rumor_span = true;
			else
				node.debunk_span = true;
		}
	}
}

function updateNodeAttributes(node)
{
	node.rumor_span = false;
	node.debunk_span = false;
	node.rumor_highlight = false;
	node.debunk_highlight = false;
	var  index = node.tweet_index;
	//Check if the tweet is highlighted, and is a rumor or a debunk
	if (appData.highlight_tweets[index])
	{
		if (appData.tweets[index].type == 1)
			node.rumor_highlight = true;
		else
			node.debunk_highlight = true;
	}
	
	//Check if tweet is in timespan, and is a rumor or debunk
	//If no timespan is highlighted, treat every tweet as being highlighted, so that the diagram defaults to having all nodes colored
	if ((appData.subStart < 0) || (index >= appData.subStart && index <= appData.subEnd))
	{
		if (appData.tweets[index].type == 1)
			node.rumor_span = true;
		else
			node.debunk_span = true;
	}
}

/**
 * Assign CSS class to a node based on its color-indicator booleans
 * @param node
 * @returns {String}
 */
function chooseNodeClass(node)
{
	//Highlighted overrides spanned. So for instance, if a node contains both a rumor
	//and a debunk tweet in the current timespan, but only the rumor tweet is highlighted,
	//display the node as blue. 
	
	if (node.rumor_highlight || node.debunk_highlight)
	{
		if (node.rumor_highlight && node.debunk_highlight)
		{
			return networkDiagram.node_css_class + " " + networkDiagram.both_css_class + " " + networkDiagram.highlighted_css_class;
		}
		else if (node.rumor_highlight)
		{
			return networkDiagram.node_css_class + " " + networkDiagram.rumor_css_class + " " + networkDiagram.highlighted_css_class;
		}
		else
		{
			return networkDiagram.node_css_class + " " + networkDiagram.debunk_css_class + " " + networkDiagram.highlighted_css_class;
		}
	}
	else if (node.rumor_span || node.debunk_span)
	{
		if (node.rumor_span && node.debunk_span)
		{
			return networkDiagram.node_css_class + " " + networkDiagram.both_css_class;
		}
		else if (node.rumor_span)
		{
			return networkDiagram.node_css_class + " " + networkDiagram.rumor_css_class;
		}
		else
		{
			return networkDiagram.node_css_class + " " + networkDiagram.debunk_css_class;
		}
	}
	else
	{
		return networkDiagram.node_css_class + " " + networkDiagram.faded_css_class;
	}
	
}

/**
 * Choose a marker to append to the end of a path, based on the type of its source,
 * and on whether it ought to be highlighted or faded
 * @param link
 * @returns {String}
 */
function chooseMarkerID(link)
{
	var node = link.source;
	
/*	if (link.self)
	{
		return networkDiagram.self_marker_id;
	}
	else*/ if (node.rumor_highlight || node.debunk_highlight)
	{
		if (node.rumor_highlight && node.debunk_highlight)
		{
			return networkDiagram.highlighted_both_marker_id;
		}
		else if (node.rumor_highlight)
		{
			return networkDiagram.highlighted_rumor_marker_id;
		}
		else
		{
			return networkDiagram.highlighted_debunk_marker_id;
		}
	}
	else if (node.rumor_span || node.debunk_span)
	{
		if (node.rumor_span && node.debunk_span)
		{
			return networkDiagram.both_marker_id;
		}
		else if (node.rumor_span)
		{
			return networkDiagram.rumor_marker_id;
		}
		else
		{
			return networkDiagram.debunk_marker_id;
		}
	}
	else
	{
		return networkDiagram.faded_marker_id;
	}
	
}

function hideNetworkDiagram()
{
	networkDiagram.sel.style("visibility","hidden");
}


function dragNetwork()
{
	console.log("Dragging network. dx: " + d3.event.dx + " | dy: " + d3.event.dy);
	networkDiagram.x_offset = networkDiagram.x_offset + d3.event.dx;
	networkDiagram.y_offset = networkDiagram.y_offset + d3.event.dy;
	
	networkDiagram.sel.selectAll("."+networkDiagram.node_css_class)
	.attr("cx",function(d){
		d.x = d.x+d3.event.dx;
		return d.x;})
	.attr("cy",function(d){
		d.y = d.y+d3.event.dy;
		return d.y;});

	repositionEdges();

}

function zoomNetwork(container)
{
	var newScale = d3.event.scale;
	var incrementalScale = newScale/networkDiagram.prev_scale-1;
	networkDiagram.prev_scale = newScale;
	
	console.log(container);
	var mousePos = d3.mouse(container);
	
	networkDiagram.sel.selectAll("."+networkDiagram.node_css_class)
	.attr("cx",function(d){
		d.x = d.x+incrementalScale*(d.x-mousePos[0]);
		return d.x;})
	.attr("cy",function(d){
		d.y = d.y+incrementalScale*(d.y-mousePos[1]);
		return d.y;});
	
	repositionEdges();
	console.log("Zooming network. Cumulative zoom:" + d3.event.scale + " | incremental zoom: " + incrementalScale + " | mouse position: "  + mousePos);
	console.log(d3.event);
}

function repositionEdges()
{
	networkDiagram.sel.selectAll("."+networkDiagram.link_css_class)
	.attr("d",function(d){return calculateLinkDimensions(d);});

}