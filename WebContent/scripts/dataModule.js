/**
 * This is a script and set of functions that requests data from the server and manipulates it in various ways
 */

/**
 * Global variables
 */

var EdgeToTweetIndex = new Array(); // Index of Tweets related to a particular Edge.
var ETTI_Ubound = new Array(); // keep the allocated array size
var AJAX = createXMLHttpRequest();
var LinkAJAX = createXMLHttpRequest();

var start, end; //Just used to see how long it takes to get data from backend

/**
 * Function definitions
 */

function createXMLHttpRequest()
{
	// See http://en.wikipedia.org/wiki/XMLHttpRequest
	// Provide the XMLHttpRequest class for IE 5.x-6.x:
	if (typeof XMLHttpRequest == "undefined")
		XMLHttpRequest = function()
		{
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP.6.0");
			}
			catch (e)
			{
			}
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP.3.0");
			}
			catch (e)
			{
			}
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
			}
			try
			{
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
			}
			throw new Error("This browser does not support XMLHttpRequest.");
		};
	return new XMLHttpRequest();
}

function requestDataFromServer(rumorID)
{
	start = new Date().getTime();
	AJAX.onreadystatechange = loadAppDataAndInitialize;
	AJAX.open("GET", "fetch_data_2.jsp?RID=" + rumorID);
	AJAX.send("");
}

function requestExperimentDataFromServer(rumorID,hitID)
{
	start = new Date().getTime();
	console.log("HIT ID: " + hitID);
	AJAX.onreadystatechange = loadAppDataAndInitialize;
	AJAX.open("GET", "fetch_data_2.jsp?RID=" + rumorID+"&EXP=true&HID="+hitID);
	AJAX.send("");
}

/**
 * Retrieve JSON response from the AJAX call and load it into the 'appData'
 * global variable defined in app.js. Then initialize the app by calling
 * initializeApplication()
 */
function loadAppDataAndInitialize()
{
	if (AJAX.readyState == 4 && AJAX.status == 200)
	{
		console.log("Loading data from server...");
		var overallJson = eval('(' + AJAX.responseText + ')');
		appData.condition_num = overallJson.condition_num;
		appData.trial_id = overallJson.trial_id;
		var error = overallJson.error;
		var errorMessage = overallJson.error_message;
		
		if (error)
		{
			displayError(errorMessage);
		}
		else
		{
			console.log("Condition: " + appData.condition_num);
			console.log("Trial ID: " + appData.trial_id);
			var json = overallJson.tweets;
			//FIXME: Cutting off first few tweets because they are annoying in this particular rumor
//			json = json.slice(3);
			for (var i = 0; i < json.length; i++)
			{
				appData.indices.push(i);
				//Load tweet marginal impacts
				var passiveChanges = new Array();
				var activeChanges = new Array();	
				var combinedChanges = new Array();
				for (var j = 0; j < transitions.length; j++)
				{
					passiveChanges.push(json[i]['p_' + transitions[j].name]);
					activeChanges.push(json[i]['a_' + transitions[j].name]);
					
					
					//Maintain a listing of combined passive and active changes for when we want to display both at once
					//(which is always, now)
					if (transitions[j].active)
						combinedChanges.push(json[i]['a_' + transitions[j].name]);
					else
						combinedChanges.push(json[i]['p_' + transitions[j].name]);
				}
				appData.passiveStateChanges.push(passiveChanges);
				appData.activeStateChanges.push(activeChanges);
				appData.combinedStateChanges.push(combinedChanges);
				
				//Load basic tweet information
				appData.tweets.push({
					type : json[i].type, //1: rumor, 2:debunk
					tweet : json[i].tweet,
					author_name : json[i].author_name,
					author_id : json[i].author_id,
					tweetID : json[i].tweet_id,
					date : (json[i].date*1000) //Dates come in as seconds from epoch, but we need them in milliseconds. 
				});
				
				//Load information about what tweets are related to each other
				appData.neighborhoodTweets.push({
					tweetsOfFollowers : commaStringToArray(json[i].follower_tweets),
					tweetsOfFollowed : commaStringToArray(json[i].followed_tweets),
					tweetsOfAuthor : commaStringToArray(json[i].author_tweets)
				});
				
				
				//Load information about authors
				appData.authorInfo.push({
					followerCount : json[i].author_followers,
					followedCount : json[i].author_followed,
					rumorExp : json[i].author_rumorexp,
					debunkExp : json[i].author_debunkexp,
					rumorTweet : json[i].author_rumortweets,
					debunkTweet : json[i].author_debunktweets
				});
				appData.max_followers = Math.max(json[i].author_followers,appData.max_followers);
				appData.max_followed = Math.max(json[i].author_followed,appData.max_followed);
	
				appData.tweetDateIndexMap[json[i].date] = i;
				appData.tweetIDIndexMap[json[i].tweet_id] = i;
			}
	
			
			var end = new Date().getTime();
			console.log(i+ " tweets loaded from server. " +((end-start)/1000) + " seconds elapsed." );
			console.log(appData.tweets);
			
			//Initialize cumulative impact calculations
			for (var i =0; i < transitions.length; i++)
			{
				appData.subCumulativePassiveChanges.push(0);
				appData.subCumulativeActiveChanges.push(0);
				appData.subCumulativeCombinedChanges.push(0);
				
				appData.cumulativePassiveChanges.push(0);
				appData.cumulativeActiveChanges.push(0);
				appData.cumulativeCombinedChanges.push(0);
				
				for (var j = 0; j < appData.tweets.length; j++)
				{
					appData.cumulativePassiveChanges[i]+=appData.passiveStateChanges[j][i];
					appData.cumulativeActiveChanges[i]+=appData.activeStateChanges[j][i];
					appData.cumulativeCombinedChanges[i]+=appData.combinedStateChanges[j][i];
				}
				
			}
			
	/*		for (var i = 0; i < transitions.length; i++)
			{
				EdgeToTweetIndex[transitions[i].name] = new Array();
			}*/
			
			//Prompt the application to initialize based on the loaded data
			initializeApplication();
		}
	}
	
	
}

/**
 * Takes in a comma-delimited string and returns an array of strings
 * 
 * I tried to do this as numbers first, but Javascript started rounding them when
 * I didn't want it to
 * e.g. "2,4,5,6" --> ["2","4","8","6"]
 * @param commaString
 */
function commaStringToArray(commaString)
{
	if (commaString)
	{
		return commaString.split(",");
	}
	else
		return [];
}

/**
 * Update appData.subStart and appData.subEnd to be the indices of the first and
 * last tweets inside the current highlighted area. Find each index by means of
 * a binary search. 
 * @param brushExtent: two date objects in a 2-element array delineating the highlighted time window
 */
function updateAppData(brushExtent)
{
	var newStartDate = brushExtent[0].getTime();
	var newEndDate = brushExtent[1].getTime();

	//Find indexes of tweets associated with beginning and end of time window
	var newSubStart = binarySearchForIndex(newStartDate, appData.tweets, "date", false); 
	var newSubEnd = binarySearchForIndex(newEndDate,appData.tweets,"date",true);  

	appData.subStart = newSubStart;
	appData.subEnd = newSubEnd;
	

	appData.subIndices = new Array(Math.max(0,appData.subEnd-appData.subStart));
	for (var i = appData.subStart; i <= appData.subEnd; i++)
	{
		appData.subIndices[i-appData.subStart] = i;
	}
	
	//Calculate cumulative impact of tweets in the newly calculated subset
	for (var i = 0; i < transitions.length; i++)
	{
		appData.subCumulativePassiveChanges[i] = 0;
		appData.subCumulativeActiveChanges[i] = 0;
		appData.subCumulativeCombinedChanges[i] = 0;
		
		for (var j = appData.subStart; j <= appData.subEnd; j++)
		{
			appData.subCumulativePassiveChanges[i] += appData.passiveStateChanges[j][i];
			appData.subCumulativeActiveChanges[i] += appData.activeStateChanges[j][i];
			appData.subCumulativeCombinedChanges[i] += appData.combinedStateChanges[j][i];
		}
		/*console.log(transitions[i].name+" | Passive: " + (100*appData.subCumulativePassiveChanges[i]/appData.cumulativePassiveChanges[i]) 
			+ "% | Active: " + (100*appData.subCumulativeActiveChanges[i]/appData.cumulativeActiveChanges[i]) + "%");*/
	}
}

function findTweetClosestToDate(date)
{
	//Find the two tweets on either side of the desired date
	var closestOnLeft = binarySearchForIndex(date,appData.tweets,"date",true);
	var closestOnRight = closestOnLeft+1;
/*	console.log("\tClosest on left: "  + closestOnLeft + " | " + appData.tweets[closestOnLeft].date);
	console.log("\tClosest on right: " + closestOnRight+ " | " + appData.tweets[closestOnRight].date);
	console.log("\tLooking for date: " + date.getTime());*/
	//Return either the closest of the two, or the only valid one. It shouldn't
	//be possible for both of them to be invalid. 
	if (closestOnLeft < 0)
	{
		return closestOnRight;
	}
	else if (closestOnRight > (appData.tweets.length-1))
	{
		return closestOnLeft;
	}
	else if ((date.getTime() - appData.tweets[closestOnLeft].date) < (appData.tweets[closestOnRight].date - date.getTime()))
	{
		return closestOnLeft;
	}
	else
	{
		return closestOnRight;
	}
}

/**
 * A generic function for doing a binary search of an array of objects to find
 * the index of the object with the field value closest to 'newFieldVal', given 
 * that its old index and field value were 'index' and 'fieldVal' respectively.
 * 
 * Assumes that fieldVal is sorted, so objArray[i][fieldName] is always <= than
 * objArray[i+1][fieldName]
 * 
 * @param newVal: sought value
 * @param objArray: object array
 * @param fieldName: field we're looking at
 * @param searchLow: if true, find closest value that is lower than newFieldVal
 * 			otherwise, find closest value that is higher
 */
function binarySearchForIndex(newVal,objArray,fieldName,searchLow)
{
	//FIXME: This whole system could be made faster by splitting the timeline into buckets
	var left = -1; //We extend one beyond the set of valid indices so that we can express the situation in which no tweets are selected
	var right = objArray.length;
	var mid;
	var midVal;
	
	while (left < (right-1))
	{
		mid = Math.floor((left+right)/2);

		midVal = getFieldValue(objArray,mid,fieldName);
		
		if ( midVal > newVal)
		{
			right = mid;
		}
		else if (midVal < newVal)
		{
			left = mid;
		}
		else //very unlikely
		{
			return mid;
		}
	}
	
	
	if (searchLow)
	{
		return left;
	}
	else
	{
		return right;
	}
}

/**
 * Access objArray[index][fieldName], returning -infinity if index is too low, 
 * +infinity if index is too high
 * @param objArray
 * @param index
 * @param fieldName
 */
function getFieldValue(objArray,index,fieldName)
{
	if (index < 0)
		return Number.NEGATIVE_INFINITY;
	else if (index >= objArray.length)
		return Number.POSITIVE_INFINITY;
	else
		return objArray[index][fieldName];
}





function randomNumber(min, max)
{
	return Math.floor(Math.random(0, 1) * (max - min)) + min;
}

//Clear all values associated with the sub-time-interval
function clearSubValues()
{
	
	appData.subStart = -1;
	appData.subEnd = -2;
	appData.subIndices = [];
	for (var i = 0; i < transitions.length; i ++)
	{
		appData.subCumulativePassiveChanges[i]=0;
		appData.subCumulativeActiveChanges[i]=0;
		appData.subCumulativeCombinedChanges[i]=0;
	}
	
}


