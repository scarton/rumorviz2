<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="db.DBManager" %>
<%@page import="db.Constants" %>
<%@page import="entity.TimeStamp" %>
<%@page import="java.util.*" %>
<%@page import="precompute.InitState" %>
<%@page import="precompute.InitStateME" %>
<%@page import="precompute.StateUpdate" %>
<%@page import="precompute.StateUpdateME" %>
<%@page import="entity.NeighborhoodTweet" %>
<%@page import="entity.TweetNeighborhood" %>
<%@page import="entity.AuthorInfo" %>
<%@page import="entity.ExperimentalConditionResponse" %>
<%@page import="entity.ExperimentalCondition" %>

<%
	DBManager dbm = new DBManager(Constants.Server.google_rumorviz);
	Integer rumor_type = Integer.parseInt(request.getParameter("RID"));	
	
	//Whether or not we should load an experimental condition from the DB and base our display on that	
	Boolean use_experiment = Boolean.parseBoolean(request.getParameter("EXP")); 

	System.out.println("Rumor ID: " + rumor_type);
	System.out.println("Experiment? " + use_experiment);
	HashMap<Long,StateUpdateME> PassiveMEStateUpdate = dbm.get.getStateUpdateMEMap(rumor_type, false);
	HashMap<Long,StateUpdateME> ActiveMEStateUpdate = dbm.get.getStateUpdateMEMap(rumor_type, true);
	HashMap<Long,TweetNeighborhood> tweetNeighborhoods = dbm.get.getTweetNeighborhoods(rumor_type);
	ArrayList<TimeStamp> ts = dbm.get.getTweetTimeline(-1, -1, rumor_type);
	HashMap<Long, AuthorInfo> AuthorInfoMap = dbm.get.getAllAuthorInfo(rumor_type);
	HashMap<Long,String> EmbedHtml = dbm.get.getAllEmbed(rumor_type);
	
	int condition_num = -1;
	String trial_id = null;
	boolean error = false;
	String error_message = null;
	
	if (use_experiment)
	{
		String remoteAddress = request.getRemoteAddr();
		String hitID = request.getParameter("HID");
		//ExperimentalConditionResponse conditionResponse = dbm.getExperimentalConditionByHitID(hitID, remoteAddress);
		ExperimentalConditionResponse conditionResponse = dbm.get.getNextExperimentalCondition(hitID,remoteAddress);
		ExperimentalCondition condition = conditionResponse.getCondition();
		if (condition != null)
		{
	condition_num = condition.getCondition();
	trial_id = condition.getExpID();
		}
		else
		{
	error = true;
	error_message = "???";//conditionResponse.getError().message;
		}
	}
	
	System.out.println(condition_num);
	System.out.println(trial_id);
	
	
	System.out.println("fetch_data_2.jsp");
	System.out.println("Passive "+PassiveMEStateUpdate.size());
	System.out.println("Active "+ActiveMEStateUpdate.size());
	System.out.println("Tweet neighborhoods "+tweetNeighborhoods.size());
	System.out.println("Tweets "+ts.size());
	System.out.println("Author "+AuthorInfoMap.size());
	System.out.println("Embed "+EmbedHtml.size());
	int successCount = 0;

	long begin = ts.get(0).date;
	long testSpan = 1*24*60*60; 
	System.out.println("Timespan: " + (ts.get(ts.size()-1).date - ts.get(0).date));
	
	
	//For each tweet of the rumor, assemble a JSON object of info about that tweet
	//and add it to a list of such objects to be outputted
	JSONObject totalResponse = new JSONObject();
	totalResponse.put("condition_num", condition_num);
	totalResponse.put("trial_id",trial_id);
	totalResponse.put("error",error);
	totalResponse.put("error_message",error_message);
	
	if (error)
	{
	    out.print(totalResponse);
	    out.flush();
	    return;
	}
	
	JSONArray tweetJSONs=new JSONArray();
	int aFound = 0, aNotFound = 0;
	for(int i = 0; i < ts.size(); i++)
	{
		//if (!(i == 0 || (i > 8500 && i < 8550) || i == ts.size() - 1)) continue;
		
		Long tweetID = ts.get(i).tweet_id;
		Long authorID = ts.get(i).Author_ID;

		JSONObject tweetJson = new JSONObject();

		if (PassiveMEStateUpdate.containsKey(tweetID) &&
		ActiveMEStateUpdate.containsKey(tweetID))
		{
	tweetJson.put("date", new Long(ts.get(i).date));
	
	if (tweetNeighborhoods.containsKey(tweetID))
	{
		//System.out.println("Found neighborhood");
		tweetJson.put("follower_tweets", tweetNeighborhoods.get(tweetID).getString(NeighborhoodTweet.Relationship.follower));
		tweetJson.put("followed_tweets", tweetNeighborhoods.get(tweetID).getString(NeighborhoodTweet.Relationship.followed));
		tweetJson.put("author_tweets", tweetNeighborhoods.get(tweetID).getString(NeighborhoodTweet.Relationship.author));
	}
	else
	{
		//System.out.println("Could not find neighborhood");
		tweetJson.put("follower_tweets", "");
		tweetJson.put("followed_tweets", "");
		tweetJson.put("author_tweets", "");
	}
	tweetJson.put("type", new Integer(ts.get(i).type));
	
	if(EmbedHtml.get(ts.get(i).tweet_id) != null)
	{
		tweetJson.put("tweet", EmbedHtml.get(ts.get(i).tweet_id));
	} 
	else 
	{
		tweetJson.put("tweet", "");
	} 
	
	tweetJson.put("author_name", ts.get(i).Author);
	tweetJson.put("author_id",ts.get(i).Author_ID);
	
	tweetJson.put("tweet_id", new Long(ts.get(i).tweet_id).toString());
	
	if(AuthorInfoMap.containsKey(ts.get(i).Author_ID))
	{
		
		aFound++;
		Long key;
	
		key = ts.get(i).Author_ID;
	
		tweetJson.put("author_followers", new Integer(AuthorInfoMap.get(key).followers));
		tweetJson.put("author_followed", new Integer(AuthorInfoMap.get(key).followees));
		tweetJson.put("author_rumorexp", new Integer(AuthorInfoMap.get(key).RECount));
		tweetJson.put("author_debunkexp", new Integer(AuthorInfoMap.get(key).DECount));
		tweetJson.put("author_rumortweets", new Integer(AuthorInfoMap.get(key).TRCount));
		tweetJson.put("author_debunktweets", new Integer(AuthorInfoMap.get(key).TDCount));
	} 
	else 
	{
		aNotFound++;
		tweetJson.put("author_followers", new Integer(0));
		tweetJson.put("author_followed", new Integer(0));
		tweetJson.put("author_rumorexp", new Integer(0));
		tweetJson.put("author_debunkexp", new Integer(0));
		tweetJson.put("author_rumortweets", new Integer(0));
		tweetJson.put("author_debunktweets", new Integer(0));
	}
	
	if (i < 5)
		System.out.println(PassiveMEStateUpdate.get(tweetID));
	
	for(int j = 0; j < StateUpdateME.transitionNames.length; j++)
	{				// Put InitStates info
		
		tweetJson.put("p_" + StateUpdateME.transitionNames[j], new Integer(PassiveMEStateUpdate.get(tweetID).transitions[j]));
		tweetJson.put("a_" + StateUpdateME.transitionNames[j], new Integer(ActiveMEStateUpdate.get(tweetID).transitions[j]));
	//			System.out.print(ActiveMEStateUpdate.get(i).transitions[j] + ",");
	}
	//		System.out.println();
	tweetJSONs.add(tweetJson);
		}
	}
	System.out.println(aFound + " author informations found. " + aNotFound + " informations not found.");
	System.out.println(tweetJSONs.size() + " out of " + ts.size() + " tweets matched up to state updates");
//	System.exit(0);
	totalResponse.put("tweets",tweetJSONs);
    out.print(totalResponse);
    out.flush();
%>