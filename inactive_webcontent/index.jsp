<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>State Diagram </title>
    <link rel="stylesheet" href="base.css">
    <link rel="stylesheet" href="app.css">
	<script src="http://d3js.org/d3.v3.min.js"></script>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	
	<script type="text/javascript" src="randomData.js"></script>
	<script src="dataModule.js"></script>
</head>
<body onload="dataRequest(0)" onmouseup="triggerMagUpdate()" onkeydown="movePointer()">
	<div id="main">
	    <div id="highlight_choice">
	    <form action="">
			<input type="radio" name="hl" onclick="changeHighlight('author')">Author's
			<input type="radio" name="hl" onclick="changeHighlight('follower')">Followers'
			<input type="radio" name="hl" onclick="changeHighlight('followee')">Followees'
			<input type="radio" name="hl" onclick="changeHighlight('none')" checked>None
		</form>
    	</div>
    	<div id="pointer_control" style="visibility:hidden">
			<button id="leftButton" type="button" onclick="movePointerToLeft()"><</button>
			<button id="rightButton" type="button" onclick="movePointerToRight()">></button>
		</div>
	</div>
	<div id="barzone"></div>
    <script src="app.js"></script>
    <script src="TimeControl.js"></script>
    <script src="BarChart.js"></script>
</body>
</html>