var bar_margin = {top: 50, right: 0, bottom: 30, left: 60},
bar_width = 300 - bar_margin.left - bar_margin.right,
bar_height = 300 - bar_margin.top - bar_margin.bottom;

var color = d3.scale.ordinal()
.range(path_colors);
//.range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

var bar_svg = d3.select("#barzone").append("svg")
.attr("width", bar_width + bar_margin.left + bar_margin.right)
.attr("height", bar_height + bar_margin.top + bar_margin.bottom)
.append("g")
.attr("transform", "translate(" + bar_margin.left + "," + bar_margin.top + ")");

var rbar_svg = d3.select("#barzone").append("svg")
.attr("width", bar_width + bar_margin.left + bar_margin.right)
.attr("height", bar_height + bar_margin.top + bar_margin.bottom)
.append("g")
.attr("transform", "translate(" + bar_margin.left + "," + bar_margin.top + ")");
	
var format = d3.format("d");

var rformat = d3.format(".0%");

var bar_x = d3.scale.ordinal()
	.rangeRoundBands([0, bar_width], .1), 
	rbar_x = d3.scale.ordinal()
	.rangeRoundBands([0, bar_width], .1);

var bar_y = d3.scale.linear()
	.range([bar_height, 0]),
	rbar_y = d3.scale.linear()
	.range([bar_height,0]);

var xAxis = d3.svg.axis()
	.scale(bar_x)
	.orient("bottom"),
	r_xAxis = d3.svg.axis()
	.scale(rbar_x)
	.orient("bottom");

var yAxis = d3.svg.axis()
	.scale(bar_y)
	.orient("left")
	.tickFormat(format),
	r_yAxis = d3.svg.axis()
	.scale(rbar_y)
	.orient("left")
	.tickFormat(rformat);

var first_bar_plot = true;

function drawChart(bars){
	bar_x.domain(bars.map(function(d) { return d.name; }));
	bar_y.domain([0, d3.max(bars, function(d) { return d.frequency; })]);

	if(first_bar_plot){	
		bar_svg.append("g")
		.attr("class", "x baxis")
		.attr("transform", "translate(0," + bar_height + ")")
		.call(xAxis);
		
		bar_svg.append("g")
		.attr("class", "y baxis")
		.call(yAxis)
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", 6)
		.attr("dy", ".71em")
		.style("text-anchor", "end")
		.text("");
	} else {
		bar_svg.transition().duration(0).select(".y.baxis").call(yAxis);
		bar_svg.transition().duration(0).select(".x.baxis").call(xAxis);
//		svg.select(".y baxis").call(yAxis);
	}
	
	var bar_area = bar_svg.selectAll(".bar").data(bars);
	
	bar_area.enter().append("rect")
	.attr("class", "bar")
	.attr("x", function(d) { return bar_x(d.name); })
	.attr("width", bar_x.rangeBand())
	.attr("y", function(d) { return bar_y(d.frequency); })
	.attr("height", function(d) { return bar_height - bar_y(d.frequency); } )
	.style("fill", function(d) { return path_colors[d.id]; });
	
	bar_area.exit().remove();
	
	bar_svg.selectAll(".bar").data(bars).transition().duration(0)
	.attr("x", function(d) { return bar_x(d.name); })
	.attr("width", bar_x.rangeBand())
	.attr("y", function(d) { return bar_y(d.frequency); })
	.attr("height", function(d) { return bar_height - bar_y(d.frequency); } )
	.style("fill", function(d) { return path_colors[d.id]; });
	
	var bar_labels = bar_svg.selectAll(".bar_label").data(bars);
	bar_labels.enter().append("text")
		.attr("class", "bar_label")
		.text(function(d) { return d.frequency;});
	bar_labels.exit().remove();
	bar_svg.selectAll(".bar_label").data(bars).transition().duration(0)
		.attr("x", function (d){ return bar_x(d.name) + bar_x.rangeBand()/2;})
		.attr("y", function (d){ return bar_y(d.frequency);})
		.text(function(d) { return d.frequency;})
		.attr("text-anchor", "middle");
	
	rbar_x.domain(bars.map(function(d) { return d.r_name; }));
	rbar_y.domain([0, d3.max(bars, function(d) { return d.r_frequency; })]);

	if(first_bar_plot){	
		rbar_svg.append("g")
		.attr("class", "rx baxis")
		.attr("transform", "translate(0," + bar_height + ")")
		.call(r_xAxis);
		
		rbar_svg.append("g")
		.attr("class", "ry baxis")
		.call(r_yAxis)
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", 6)
		.attr("dy", ".71em")
		.style("text-anchor", "end")
		.text("");
	} else {
		rbar_svg.transition().duration(0).select(".ry.baxis").call(r_yAxis);
		rbar_svg.transition().duration(0).select(".rx.baxis").call(r_xAxis);
//		svg.select(".y baxis").call(yAxis);
	}
	
	bar_area = rbar_svg.selectAll(".rbar").data(bars);
	
	bar_area.enter().append("rect")
	.attr("class", "rbar")
	.attr("x", function(d) { return rbar_x(d.r_name); })
	.attr("width", rbar_x.rangeBand())
	.attr("y", function(d) { return rbar_y(d.r_frequency); })
	.attr("height", function(d) { return bar_height - rbar_y(d.r_frequency); } )
	.style("fill", function(d) { return path_colors[d.id]; });
	
	bar_area.exit().remove();
	
	rbar_svg.selectAll(".rbar").data(bars).transition().duration(0)
	.attr("x", function(d) { return rbar_x(d.r_name); })
	.attr("width", rbar_x.rangeBand())
	.attr("y", function(d) { return rbar_y(d.r_frequency); })
	.attr("height", function(d) { return bar_height - rbar_y(d.r_frequency); } )
	.style("fill", function(d) { return path_colors[d.id]; });
	
	var rbar_labels = rbar_svg.selectAll(".rbar_label").data(bars);
	rbar_labels.enter().append("text")
		.attr("class", "rbar_label")
		.text(function(d) { return d.r_frequency;});
	rbar_labels.exit().remove();
	rbar_svg.selectAll(".rbar_label").data(bars).transition().duration(0)
		.attr("x", function (d){ return rbar_x(d.name) + rbar_x.rangeBand()/2;})
		.attr("y", function (d){ return rbar_y(d.r_frequency);})
		.text(function(d) { return ((d.r_frequency * 100).toFixed(3) + '%'); })
		.attr("text-anchor", "middle");
	first_bar_plot = false;
}
