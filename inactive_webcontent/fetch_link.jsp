<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="db.DBManager" %>
<%@page import="entity.TimeStamp" %>
<%@page import="java.util.*" %>
<%@page import="precompute.InitState" %>
<%@page import="precompute.InitStateME" %>
<%@page import="precompute.StateUpdate" %>
<%@page import="precompute.StateUpdateME" %>
<%@page import="entity.TweetNeighborhood" %>
<%@page import="entity.AuthorInfo" %>
<%@page import="precompute.TweeterLink" %>

<%
	DBManager dbm = new DBManager();
	Integer rumor_type = Integer.parseInt(request.getParameter("RID"));			// 0 - meteor, 1 - AP

//	ArrayList<TimeStamp> ts = dbm.selectTweetTimeline(-1, -1, rumor_type);
	ArrayList<TweeterLink> tl = dbm.selectTweetLinks(rumor_type);
	
	JSONArray ret=new JSONArray();
	System.out.println("links: " + tl.size());
	for(int i = 0; i < tl.size(); i++){
		JSONObject Link = new JSONObject();
		// getTweet ID
		
		Link.put("source", new Integer(tl.get(i).source));		
		Link.put("target", new Integer(tl.get(i).target));
		
//		System.out.println();
		ret.add(Link);
	}
//	System.exit(0);
    out.print(ret);
    out.flush();
%>