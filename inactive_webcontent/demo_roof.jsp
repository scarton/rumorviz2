<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Man on the Roof Rumor</title>
    <link rel="stylesheet" href="style/base.css">
    <link rel="stylesheet" href="style/app.css">
    <link rel="stylesheet" href="style/sankey.css">
    <script> var rumorNum = 17; </script>
	<script src="http://d3js.org/d3.v3.min.js"></script>
	
	<script type="text/javascript" src="scripts/randomData.js"></script>
	<script src="scripts/dataModule.js"></script>
	<script src="scripts/sankey.js"></script>
</head>
<body onmouseup="triggerMagUpdate()" onkeydown="movePointer()">
	<div id="main">
		<div id="SankeyMode" style="position:absolute; left:740px; top:10px">
			    <form action="">
					<input type="radio" name="sm" onclick="changeSankeyMode('Passive')" checked>Exposed
					<input type="radio" name="sm" onclick="changeSankeyMode('Active')">Tweeted
				</form>	
		</div>
		<div id="barzone">
			<div class="ItemInfo" id="TweetInfo"></div>
			<table class="ItemInfo" id="AuthorInfo">
				<thead><tr><th scope="col">Author</th></tr></thead>
				<tbody><tr><td><p>  </p></td></tr></tbody>
			</table>
			<table class="ItemInfo" id="TimeInfo">
				<thead><tr><th scope="col">Time</th></tr></thead>
				<tbody><tr><td><p>  </p></td></tr></tbody>
			</table>
		</div>
	</div>
	<div id="TimelineZone" style="position:absolute; left:0px; top:595px">
		<p id="pc_label">Cursor Control</p>
        <div id="pointer_control" style="visibility:visible">
			<button id="leftButton" type="button" onclick="movePointerToLeft()"><</button>
			<button id="rightButton" type="button" onclick="movePointerToRight()">></button>
            <button id="restoreButton" type="button" onclick="restoreCursor()" disabled>Restore Cursor</button>
		</div>
		<div id="delete_window" style="visibility:hidden">
			<button id="delWindow" type="button" onclick="deleteWindow()">X</button>		
		 <script src="scripts/app2_hist.js"></script>
    <script src="scripts/TimeControl_hist.js"></script>
    <script src="scripts/tweetdisplay.js"></script>
</body>
</html>
