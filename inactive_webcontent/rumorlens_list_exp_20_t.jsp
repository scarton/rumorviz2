<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Rumor analysis</title>
    <link rel="stylesheet" href="style/base_html.css">
    <link rel="stylesheet" href="style/app.css">
    <link rel="stylesheet" href="style/timebar.css">
    <link rel="stylesheet" href="style/sankey.css">
    <link rel="stylesheet" href="style/barchart.css">
    <link rel="stylesheet" href="style/userlist.css"> 
    <link rel="stylesheet" href="style/network.css"> 
    <link rel="stylesheet" href="style/guiders.css"> 

</head>
<body>
	 <div id="main">
 	 	<div id = "central_diagram_div" style="position:absolute; left:10px; top:10px">
 		</div>
	 <div id="sankey_mode_div" style="position:absolute; left:620px; top:10px">
	 </div>
		<div id="informationPanel">
			<table class="informationBox" id ="tweetDisplayBox"> 
				<thead><tr><th>Selected Tweet</th></tr></thead>
				<tbody id = "tweet_display">
				<tr><td>
					<div id="tweet_display_info">  </div>
					
					<div id="tweet_display_content"> </div>
				</td></tr>
				</tbody>
			</table>
			<div id="selected_delete_button_div" style="visibility:hidden">
				<button id="selected_delete_button" type="button" onclick="displayAndHighlightTweets(-1);">X</button>		
			</div>
			 <table class="informationBox" id="instructionBox">
				<thead><tr><th scope="col">Help</th></tr></thead>
				<tbody><tr><td>
					<br>
					<div id = "instructions_button_div">
						<button id="show_instructions_button" type="button" onclick="showCheatSheet()">Show Cheatsheet</button>
					</div>
					<br>
					<div id = "instructions_button_div">
						<button id="replay_tutorial_button" type="button" onclick="showGuiders(true)">Replay Tutorial</button>
					</div>
				</td></tr></tbody>
			</table>
			<!--<table class="informationBox" id="highlightOptionsBox">
				<thead><tr><th scope="col">Highlighting Options</th></tr></thead>
				<tbody>
					<tr>
						<td>
							<table id="highlightOptions">  
								<tr>
									<td>
										<input type="radio" name="hm" onclick ="changeHighlightMode(highlightMode.NONE)" checked>None
									</td>
									<td>
										<input type="radio" id = "author_highlight_button" name="hm" onclick ="changeHighlightMode(highlightMode.AUTHOR)" >Self			
									</td>
								</tr>
								<tr>
									<td>
										<input type="radio" name="hm" onclick ="changeHighlightMode(highlightMode.FOLLOWER)" >Followers			
									</td>
									<td>
										<input type="radio" name="hm" onclick ="changeHighlightMode(highlightMode.FOLLOWED)" >Followed			
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table> -->
		</div>
	</div>
	<div id="timebar_div" style="position:absolute; left:0px; top:615px">
		<!-- <p id="pc_label">Cursor Control</p>
        <div id="pointer_control" style="visibility:visible">
			<button id="leftButton" type="button" ><</button>
			<button id="rightButton" type="button" >></button>
            <button id="restoreButton" type="button"  >Restore Cursor</button>
		</div> -->
		<div id="timespan_delete_button_div" style="visibility:hidden">
			<button id="delete_button" type="button" onclick="clearBrushAndMagnifiedTimeBar()">X</button>		
		</div>
	</div>
	<!--scripty scripts-->
	  
	  
	<script src = "scripts/d3_49.min.js"></script>
	<script src = "scripts/d3_force_network_2.js"></script>
	<script src = "scripts/d3_tip.js"></script>
	<script src="scripts/dataModule.js"></script>
	<script src="scripts/sankey.js"></script>
	<script src="scripts/d3_sankey.js"></script>
    <script src="scripts/timebar.js"></script>
    <script src="scripts/tweetinfo.js"></script>
    <script src="scripts/barchart.js"></script>
   	<script src="scripts/userlist.js"></script>
   	<script src="scripts/networkdiagram.js"></script>
   	<script src="scripts/tweetdisplay.js"></script>
   	<script src="scripts/jquery.min.js"></script>
   	<script src="scripts/guiders.js"></script>
   	<script src="scripts/instructions.js"></script>
    <script src="scripts/app.js"></script>  <!-- This comes last --> 
    <script> 
    	var rumorNum = 20; 
        var hitID = "list_test";
		requestExperimentDataFromServer(rumorNum,hitID);
    </script>
    
</body>
</html>
