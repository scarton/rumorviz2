<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>State Diagram </title>
    <link rel="stylesheet" href="style/base.css">
        <link rel="stylesheet" href="style/app.css">
    <link rel="stylesheet" href="style/sankey.css">
	<script src="http://d3js.org/d3.v3.min.js"></script>
	
	<script type="text/javascript" src="scripts/randomData.js"></script>
	<script src="scripts/dataModule.js"></script>
	<script src="scripts/sankey.js"></script>
</head>
<body onmouseup="triggerMagUpdate()" onkeydown="movePointer()">
	<div id="main">
		<div id="quiz">
			<div class="qheader"> <p class="qheader">Exercise) How many people were exposed to the rumor exactly twice, and then propagated it?</p></div>
			<div class="qselections">
				<form action=""><input type="radio" value="a" name="question" onclick="checkQuiz1_2('a')">a) 10 <input type="radio" value="b" name="question" onclick="checkQuiz1_2('b')">b) 100 <input type="radio" value="c" name="question" onclick="checkQuiz1_2('c')">c) 1,000 <input type="radio" value="d" name="question" onclick="checkQuiz1_2('d')">d) 10,000<br></form>
			</div>
			<p id="explanation"></p>
		</div>
		<div id="SankeyMode" style="position:absolute; left:740px; top:125px">
			    <form action="">
					<input id="PMode" type="radio" name="sm" onclick="changeSankeyMode('Exposed')" checked>Exposed
					<input id="AMode" type="radio" name="sm" onclick="changeSankeyMode('Tweeted')">Tweeted
				</form>	
		</div>
		<div id="barzone">
			<div class="ItemInfo" id="TweetInfo"></div>
			<table class="ItemInfo" id="AuthorInfo">
				<thead><tr><th scope="col">Author</th></tr></thead>
				<tbody><tr><td><p>  </p></td></tr></tbody>
			</table>
			<table class="ItemInfo" id="TimeInfo">
				<thead><tr><th scope="col">Time</th></tr></thead>
				<tbody><tr><td><p>  </p></td></tr></tbody>
			</table>
		</div>
	</div>
	<div id="TimelineZone" style="position:absolute; left:0px; top:695px">
		<p id="pc_label">Cursor Control</p>
    	<div id="pointer_control" style="visibility:visible">
			<button id="leftButton" type="button" onclick="movePointerToLeft()"><</button>
			<button id="rightButton" type="button" onclick="movePointerToRight()">></button>
		</div>
		<div id="delete_window" style="visibility:hidden">
			<button id="delWindow" type="button" onclick="deleteWindow()">X</button>		
		</div>
	</div>
   hideMagnifiedBarAndCompany()scripts/app2.js"></script>
    <script src="scripts/TimeControl.js"></script>
    <script src="scripts/tweetdisplay.js"></script>
    <script src="scripts/quizCheck.js"></script>
<!--     <script src="BarChart.js"></script>	 -->
</body>
</html>