(function()
{
	var RandomData = function(TweetInfo)
	{
		var addToLane = function(chart, item)
		{
			var name = item.lane;

			if (!chart.lanes[name])
				chart.lanes[name] = [];

			var lane = chart.lanes[name];

			var sublane = 0;
			/*
			 * while(isOverlapping(item, lane[sublane])){ // sublane++; return; }
			 */
			if (!lane[sublane])
			{
				lane[sublane] = [];
			}

			lane[sublane].push(item);
		};

		var isOverlapping = function(item, lane)
		{
			if (lane)
			{
				for (var i = 0; i < lane.length; i++)
				{
					var t = lane[i];
					if (item.start < t.end && item.end > t.start)
					{
						return true;
					}
				}
			}
			return false;
		};

		var parseData = function(data)
		{
			var i = 0, length = data.length, node;
			chart = {
				lanes : {}
			};
			// alert(chart);

			for (i; i < length; i++)
			{
				var item = data[i];

				addToLane(chart, item);
			}

			return collapseLanes(chart);
		};

		var collapseLanes = function(chart)
		{
			var lanes = [], items = [], laneId = 0;
			var now = new Date();
			var classes = [ 'rumor', 'debunk', 'overlap rumor',
					'overlap debunk' ];

			// var classes = ['rumor', 'debunk', 'overlap rumor', 'overlap
			// debunk'];
			var milisec_per_pixel = 1087322;
			var overlap_range = milisec_per_pixel / 4;

			for ( var laneName in chart.lanes)
			{
				var lane = chart.lanes[laneName];

				for (var i = 0; i < lane.length; i++)
				{
					var subLane = lane[i];

					lanes.push({
						id : laneId,
						label : i === 0 ? laneName : ''
					});

					for (var j = 0; j < subLane.length; j++)
					{
						var item = subLane[j];
						var classNum = item.Type;

						// Figure out if this tweet is close to another tweet of
						// a different type, and should be colored purple
						// instead of red or blue
						if (j > 0 && j < subLane.length - 1)
						{
							if (subLane[j - 1].start > (item.start - overlap_range)
									&& subLane[j - 1].Type != item.Type)
							{
								classNum += 2;
							}

							if (item.start + overlap_range < subLane[j + 1].start
									&& subLane[j + 1].Type != item.Type
									&& classNum <= 2)
							{
								classNum += 2;
							}
						}

						items.push({
							id : item.id,
							lane : laneId,
							start : item.start,
							end : item.end,
							class : classes[classNum - 1],
							type : item.Type,
							desc : item.desc,
							Author : item.Author
						});
					}

					laneId++;
				}
			}

			return {
				lanes : lanes,
				items : items
			};
		};

		var randomNumber = function(min, max)
		{
			return Math.floor(Math.random(0, 1) * (max - min)) + min;
		};

		var generateRandomWorkItems = function(Info)
		{
			var data = [];
			var totalWorkItems = Info.length, startMonth = 1, startDay = 15;

			var dt = new Date(2013, startMonth, startDay);
			var dateOffset = 0;
			for (var j = 0; j < totalWorkItems; j++)
			{
				// var dtS = new Date(dt.getFullYear(), dt.getMonth(),
				// dt.getDate() + dateOffset, 0, 0, 0);
				var dtS = new Date(Info[j].Date * 1000);

				// dateOffset = 1;
				// var dt = new Date(dtS.getFullYear(), dtS.getMonth(),
				// dtS.getDate(), 0, 1, 0);
				var dt = new Date(Info[j].Date * 1000 + 1000);

				var workItem = {
					id : j,
					Type : Info[j].Type,
					Author : Info[j].Author,
					desc : Info[j].Tweet,
					start : dtS,
					end : dt,
					lane : 'Timeline'
				};

				data.push(workItem);
			}

			return data;
		};

		return parseData(generateWorkItems(TweetInfo));
	};

	/**
	 * Allow library to be used within both the browser and node.js
	 */
	var root = typeof exports !== "undefined" && exports !== null ? exports
			: window;
	root.randomData = RandomData;

}).call(this);