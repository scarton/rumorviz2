var margin = {top: 20, right: 15, bottom: 118, left: 60}
, width = 1120 - margin.left - margin.right
, miniHeight = 30, m_mini_height = 1 * 12 + 50;

// sankey code starts from here 
window.svg = d3.select("#main").append("svg")
.attr("width", "860px")
.attr("height", "580px")
.attr('class', 'diagram')
.append("g");
//.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var sankey_width = 860, sankey_height = 580;
var formatNumber = d3.format(",f"),
	format = function(d) { return formatNumber(d); };
//	color = d3.scale.category20();

var chart = d3.select('#TimelineZone')
.append('svg')
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight + margin.bottom + m_mini_height + margin.top)
.attr('class', 'chart');

var m_mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 25 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', m_mini_height + margin.top)
.attr('class', 'm_mini');

var mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 110 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight)
.attr('class', 'mini');

var TweetInfoArea = chart.append('g')
//.attr('transform', 'translate(850,12)')
.attr('transform', 'translate(500,100)')
//.attr('width', width)
.attr('width', 200)
.attr('height', 20)
.attr('class', 'feedback_area');

mini.selectAll('rect.background').remove();

var window_updated = false;
var SelectedNode = -1;

function restart() {
	if(window_updated == true){
		svg.selectAll("circle.node")
			.data(NodeInfo)
			.classed("selected", function (d) { return d.focus;})
			.style("visibility", function(d) { return d.display == 1 ? "visible" : "hidden"; });
		svg.selectAll("line.link")
			.data(LinkInfo)
			.style("visibility", function(d) { return (d.source.display == 1 && d.target.display == 1) ? "visible" : "hidden"; });
	} else {
		createNT();
	}
};

function createNT(){
//	alert('start NT construction - node:' + NodeInfo.length + ' link: ' + LinkInfo.length);
    var k = Math.sqrt(NodeInfo.length / (sankey_width * sankey_height));
    
	var force = d3.layout.force()
	  .charge(-10 / k)
	  .gravity(50 * k)
      .linkDistance(80)
      .nodes(NodeInfo)
      .links(LinkInfo)
      .size([sankey_width, sankey_height])
      .start();
      
    var link = svg.selectAll('line.link')
        .data(LinkInfo)
      .enter().append('svg:line')
        .attr("class", "link")
        .style('stroke-width', 0.5)
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    var node = svg.selectAll("circle.node")
        .data(NodeInfo)
      .enter().append("svg:circle")
        .attr("class", "node")
        .attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; })
//        .attr("r", 1.5)
//        .attr("r", function(d) { if(d.size < 0) { return 2; } else {return Math.max((Math.log(d.size)/Math.LN10)*2, 2);} })
         .attr("r", function(d) { if(d.size < 0) { return 3; } else {return Math.max(Math.pow(d.size, 3/10), 3);} })
        .style("fill", function(d) { return d.type == 1 ? "blue" : (d.type == 2 ? "red" : "purple"); })
        .call(force.drag)
        .on("mouseover", function (d) {
        	svg.text.attr(
        			'transform',
        			'translate(' + d.x + ','
        					+ (d.y - 7)
        					+ ')').text(d.name + ": " + d.size + " followers").style(
        			'display', null);
        })
        .on("click", function(d, i) {
//        	d3.selectAll( 'circle.node.selected').classed( "selected", false);
//       	d3.select(this).classed("selected", true);
        	pointer_index = d.first_tweet_idx;
        	changeHighlight('author');
        	updateCursor(pointer_index);
        	focusNode();
        })
        .on("mouseout", function (d) {
        	svg.text.style('display', 'none');
        });

    svg.text = svg.append('svg:text')
	    .attr('class', 'nodetext')
	    .attr('dy', 0)
	    .attr('dx', 0)
	    .attr('text-anchor', 'middle');
    
    node.append('svg:title')
        .text(function(d) { return d.name; });

    force.on("tick", function(e) {
	      var k = .5 * e.alpha;
	      node.each(function(d) {
	          d.y += ((4 - d.type) * 100 - d.y) * k; 
	      });
	      
	      link.attr("x1", function(d) { return d.source.x; })
	          .attr("y1", function(d) { return d.source.y; })
	          .attr("x2", function(d) { return d.target.x; })
	          .attr("y2", function(d) { return d.target.y; });
	    
	      node.attr("cx", function(d) { return d.x ; })
	          .attr("cy", function(d) { return d.y; });
    });
};

console.log("requesting rumor 31");
dataRequest_NT(31);