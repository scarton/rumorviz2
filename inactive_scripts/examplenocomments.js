/**
 * Global variables and commands are defined/executed whenever the javascript
 * file is loaded into the HTML. Generally it's good to avoid having too many
 * of these, because you end up with messy spaghetti code and variable name
 * conflicts and stuff.
 */

console.log("Generally speaking, there's no 'stdout' in JavaScript. You have to "+ 
	'either send text to the javascript console available on most browsers ' + 
	'or put text in/on the containing webpage somehow.');

var mystr = 'This is a string';

//Semicolons are optional but preferred
var mynum = 20;

var myStruc = {
	str:mystr,
	num:mynum,
	desc:'This is a structure'
};

var myClass = new AClass(myStruc);


/**
 * This is a class definition. It's differentiated from a normal function
 * by the use of this.______ = _______ statements
 */
function AClass(someStruct)
{
	this.struct = someStruct;
	this.desc = 'This is a class instance';
}

/**
 * This is a function definition. The global commands above are smart enough to
 * find this, but if it were defined in another JS file, that file would
 * have to be loaded into the HTML first.
 */
function doAlert(someClass)
{
	alert('Description: ' + someClass.desc +
		'\nStructure: ' +
		'\n    Description: ' + someClass.struct.desc + 
		'\n    String: ' + someClass.struct.str +
		'\n    Num: ' + someClass.struct.num );
}