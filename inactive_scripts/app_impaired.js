var StateNames = ["Start", "RumorExp1", "RumorExp2", "RumorExp3m", "DebunkExp1", 
		"DebunkExp2", "DebunkExp3m", "BothExp", "RumorTwt", "DebunkTwt", "BothTwt"];

var TransitionNames = ["Start_RumorExp1", "Start_RumorTwt", "Start_DebunkExp1", 
			"Start_DebunkTwt", "RumorExp1_RumorExp2", "RumorExp1_BothExp", 
			"RumorExp1_RumorTwt", "RumorExp1_DebunkTwt", "RumorExp2_RumorExp3m", "RumorExp2_BothExp", 
			"RumorExp2_RumorTwt", "RumorExp2_DebunkTwt", "RumorExp3m_BothExp", "RumorExp3m_RumorTwt", 
			"RumorExp3m_DebunkTwt", "DebunkExp1_DebunkExp2", "DebunkExp1_BothExp", "DebunkExp1_RumorTwt",
			"DebunkExp1_DebunkTwt", "DebunkExp2_DebunkExp3m", "DebunkExp2_BothExp", "DebunkExp2_RumorTwt",
			"DebunkExp2_DebunkTwt", "DebunkExp3m_BothExp", "DebunkExp3m_RumorTwt", "DebunkExp3m_DebunkTwt",
			"BothExp_RumorTwt", "BothExp_DebunkTwt", "RumorTwt_BothTwt", "DebunkTwt_BothTwt"];

var PassiveSankey = {nodes: [], links : []}, ActiveSankey = {nodes: [], links: []}, PassiveSankeyBuffer = [];

PassiveSankey.nodes = [
	{ name: "Start" },
	{ name: "Rumor Exp 1"},
	{ name: "Rumor Exp 2"},
	{ name: "Rumor Exp 3+"},
	{ name: "Correction Exp 1"},
	{ name: "Correction Exp 2"},
	{ name: "Correction Exp 3+"},
	{ name: "Both Exp"}
];

ActiveSankey.nodes = [
   	{ name: "Start" },
   	{ name: "Rumor Exp 1"},
   	{ name: "Rumor Exp 2"},
   	{ name: "Rumor Exp 3+"},
   	{ name: "Correction Exp 1"},
   	{ name: "Correction Exp 2"},
   	{ name: "Correction Exp 3+"},
   	{ name: "Both Exp"},
   	{ name: "Tweet Rumor"},
   	{ name: "Tweet Correction"},
   	{ name: "Tweet Both"},
];

for(var i = 0; i < TransitionNames.length; i++){
	var s_t = TransitionNames[i].split("_");
	
	var source_id = -1, target_id = -1;
	for(var j = 0; j < StateNames.length; j++){
		if(s_t[0] == StateNames[j]) source_id = j;
		if(s_t[1] == StateNames[j]) target_id = j;
	}
	PassiveSankey.links.push({id: i, source: source_id, target: target_id, value: 0});
	ActiveSankey.links.push({id: i, source: source_id, target: target_id, value: 0});
	PassiveSankeyBuffer.push({id: i, source: source_id, target: target_id, value: 0});
//	alert('P - ' + PassiveSankey.links[i].source + ', ' + PassiveSankey.links[i].target);
//	alert('A - ' + ActiveSankey.links[i].source + ', ' + ActiveSankey.links[i].target);
}

var formatNumber = d3.format(","),
format = function(d) { return formatNumber(d); },
color = d3.scale.category20();

var margin = {top: 20, right: 15, bottom: 118, left: 60}
, width = 1120 - margin.left - margin.right
, miniHeight = 30, m_mini_height = 1 * 12 + 50;

// sankey code starts from here 
/*window.svg = d3.select("#main").append("svg")
.attr("width", "860px")
.attr("height", "580px")
.attr('class', 'diagram')
.append("g");
//.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var sankey_width = 860, sankey_height = 580;

var Passive_Sankey = d3.sankey()
.nodeWidth(15)
.nodePadding(10)
.size([sankey_width, sankey_height]);

var Passive_Sankey_path = Passive_Sankey.link();

var Active_Sankey = d3.sankey()
.nodeWidth(15)
.nodePadding(10)
.size([sankey_width, sankey_height]);

var Active_Sankey_path = Active_Sankey.link();
*/
var chart = d3.select('#TimelineZone')
.append('svg')
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight + margin.bottom + m_mini_height + margin.top)
.attr('class', 'chart');

var m_mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 15 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', m_mini_height + margin.top)
.attr('class', 'm_mini');

var mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 100 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight)
.attr('class', 'mini');

var TweetInfoArea = chart.append('g')
.attr('transform', 'translate(' + 0 + ',' + 55 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', margin.bottom)
.attr('class', 't_info');

mini.selectAll('rect.background').remove();

var bars = [], Pedge_window = new Array(TransitionNames.length), Aedge_window = new Array(TransitionNames.length);
var window_updated = false, sankey_mode_passive = true;
var LinksNotInPassive = function (elem){ return (elem.source < 8 && elem.target < 8);};

function restart() {
/*	if(window_updated == true){
		var window_width, base_width, class_string;
		if(sankey_mode_passive == true){
			for(var i = 0; i < TransitionNames.length; i++){
				PassiveSankeyBuffer[i].value = Pedge_window[i];
			}
			base_width = passive_stroke_width;
			window_width = PassiveSankeyBuffer.filter(LinksNotInPassive);
			class_string = "p_over";
		} else {
			for(var i = 0; i < TransitionNames.length; i++){
				ActiveSankey.links[i].value = Aedge_window[i];
			}
			base_width = active_stroke_width;
			window_width = ActiveSankey.links;
			class_string = "a_over";
		}
		svg.selectAll("." + class_string).data(window_width).style("stroke-width", function (d, i) {
			var new_width = base_width.dy[i] * (d.value / base_width.value[i]);
//			alert(d.id + ', source: ' + d.source.name + ', dest: ' +d.target.name);
//			alert("id: " + i + ", width: " + base_width.dy[i] + ' - new width: ' + new_width);
			return new_width;
		});
	} else {
		for(var i = 0; i < TransitionNames.length; i++){
			PassiveSankey.links[i].value = Pedge_window[i];
			ActiveSankey.links[i].value = Aedge_window[i];
		}
		PassiveSankey.links = PassiveSankey.links.filter(LinksNotInPassive);		// PassiveSankeyBuffer changes at each iteration.
		CreateSankey();
		ShowSankey("Passive");
		sankey_mode_passive = true;
	//	alert('first source: ' + PassiveSankeyBuffer[0].source + ', first target: ' + PassiveSankeyBuffer[0].target);
	}
*/
};

var passive_stroke_width = {dy: [], value: []}, active_stroke_width = {dy: [], value: []};

function CreateSankey(){
	var node_holder, dlink, link_holder;
	for(var i = 0; i < 2; i++){
		var sankey = (i == 0) ? Passive_Sankey : Active_Sankey;
		var path = (i == 0) ? Passive_Sankey_path : Active_Sankey_path;
		var SankeyData = null, link_class_string = "", node_class_string = "", hidden_class = "fake", overlay_link_class = "";
		
		if(i == 0) {
			SankeyData = PassiveSankey;
			link_class_string = "p_link", node_class_string = "p_node", overlay_link_class = "p_over"; 
		} else if(i == 1){
			SankeyData = ActiveSankey;
			link_class_string = "a_link", node_class_string = "a_node", overlay_link_class = "a_over";
		}
//		alert('Load Sankey ' + i + ' Nodes: ' + SankeyData.nodes.length + ' links: ' + SankeyData.links.length);
		sankey
		  .nodes(SankeyData.nodes)
		  .links(SankeyData.links)
		  .layout(32);
//		alert('Sankey loaded ' + i);
		
		link_holder = svg.append("g").selectAll("." + link_class_string)
		  .data(SankeyData.links)
		  .enter().append("path")
		  .attr("class", function(d) { if(d.value > 1){ return link_class_string + " link"; } else {return hidden_class + " link"; }})
		  .attr("d", path)
		  .style("stroke-width", function(d) { return Math.max(1, d.dy); })
		  .style("visibility", "hidden");
//		  .sort(function(a, b) { return b.dy - a.dy; });

		link_holder.append("title")
		  .text(function(d) { return d.source.name + " --> " + d.target.name + "\n" + format(d.value); });
//		alert('link ready ' + i);
		
		if(i == 0)
			passive_link_holder = link_holder;
		else if (i == 1)
			active_link_holder = link_holder;
		for(var temp = 0; temp < SankeyData.links.length; temp++){
			if(i == 0){
				passive_stroke_width.dy[temp] = SankeyData.links[temp].dy;
				passive_stroke_width.value[temp] = SankeyData.links[temp].value;
			} else if(i == 1){
				active_stroke_width.dy[temp] = SankeyData.links[temp].dy;
				active_stroke_width.value[temp] = SankeyData.links[temp].value;
			}
		}
		
		dlink = svg.append("g").selectAll("." + overlay_link_class)
		  .data(SankeyData.links)
		  .enter().append("path")
		  .attr("class", overlay_link_class + " dlink")
		  .attr("d", path)
		  .on("click", function (d){
			  		highlightItemOfEdge(TransitionNames[d.id]);
	    		})
		  .style("stroke-width", function(d) { return 0; })
		  .style("visibility", "hidden") ;
//		  .sort(function(a, b) { return b.dy - a.dy; });

		dlink.append("title")
		  .text(function(d) { return d.source.name + " --> " + d.target.name + "\n" + format(d.value); });

		node_holder = svg.append("g").selectAll("." + node_class_string)
		  .data(SankeyData.nodes)
		.enter().append("g")
		  .attr("class", node_class_string + " node")
//		  .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
		  .attr("transform", function(d) { return optimizeLayout(i, d); })
		  .style("visibility", "hidden")
/*		  .call(d3.behavior.drag()
		  .origin(function(d) { return d; })
		  .on("dragstart", function() { this.parentNode.appendChild(this); })
		  .on("drag", dragmove));
*/
		node_holder.append("rect")
		  .attr("height", function(d) { return d.dy; })
		  .attr("width", sankey.nodeWidth())
		  .style("fill", function(d) { return d.color = color(d.name.replace(/ .*/, "")); })
		  .style("stroke", function(d) { return d3.rgb(d.color).darker(2); })
		.append("title")
		  .text(function(d) { return d.name + "\n" + format(d.value); });

		node_holder.append("text")
		  .attr("x", -6)
		  .attr("y", function(d) { return d.dy / 2; })
		  .attr("dy", ".35em")
		  .attr("text-anchor", "end")
		  .attr("transform", null)
		  .text(function(d) { return d.name; })
		.filter(function(d) { return d.x < width / 2; })
		  .attr("x", 6 + sankey.nodeWidth())
		  .attr("text-anchor", "start");
		
		sankey.relayout();
		link_holder.attr("d", path);
		dlink.attr("d", path);
//		alert('node ready ' + i);
/*		function dragmove(d) {
			d3.select(this).attr("transform", "translate(" + d.x + "," + (d.y = Math.max(0, Math.min(sankey_height - d.dy, d3.event.y))) + ")");
			sankey.relayout();
			link_holder.attr("d", path);
		}*/
//		alert('alive ' + i);
	}
};

function optimizeLayout(type, sankey_node){
	if(type == 0){
		if(sankey_node.name == "Start"){
			sankey_node.x = 0, sankey_node.y = 8.492191470628942;
			return "translate(0,8.492191470628942)";
		} else if(sankey_node.name == "Rumor Exp 1"){
			sankey_node.x = 211.25, sankey_node.y = 4.999999999999943;
			return "translate(211.25,4.999999999999943)";
		} else if(sankey_node.name == "Rumor Exp 2"){
			sankey_node.x = 422.5, sankey_node.y = 225.98988005857956;
			return "translate(422.5,225.98988005857956)";
		} else if(sankey_node.name == "Rumor Exp 3+"){
			sankey_node.x = 633.75, sankey_node.y = 293.19924031127636;
			return "translate(633.75,293.19924031127636)";
		} else if(sankey_node.name == "Correction Exp 1"){
			sankey_node.x = 211.25, sankey_node.y = 451.5869075902176;
			return "translate(211.25,451.5869075902176)";
		} else if(sankey_node.name == "Correction Exp 2"){
			sankey_node.x = 422.5, sankey_node.y = 316.8932080481022;
			return "translate(422.5,316.8932080481022)";
		} else if(sankey_node.name == "Correction Exp 3+"){
			sankey_node.x = 633.75, sankey_node.y = 309.86357978272594;
			return "translate(633.75,309.86357978272594)";
		} else if(sankey_node.name == "Both Exp"){
			sankey_node.x = 845, sankey_node.y = 286.29270160904764;
			return "translate(845,286.29270160904764)";
		}
	} else if(type == 1){
		if(sankey_node.name == "Start"){
			sankey_node.x = 0, sankey_node.y = 4.087959271264238;
			return "translate(0,4.087959271264238)";
		} else if(sankey_node.name == "Rumor Exp 1"){
			sankey_node.x = 140.83333333333334, sankey_node.y = 73.17139245078569;
			return "translate(140.83333333333334,73.17139245078569)";
		}else if(sankey_node.name == "Rumor Exp 2"){
			sankey_node.x = 281.6666666666667, sankey_node.y = 303.8377816127512;
			return "translate(281.6666666666667,303.8377816127512)";
		}else if(sankey_node.name == "Rumor Exp 3+"){
			sankey_node.x = 422.5, sankey_node.y = 333.9213321748566;
			return "translate(422.5,333.9213321748566)";
		}else if(sankey_node.name == "Correction Exp 1"){
			sankey_node.x = 140.83333333333334, sankey_node.y = 483.18132562759615;
			return "translate(140.83333333333334,483.18132562759615)";
		}else if(sankey_node.name == "Correction Exp 2"){
			sankey_node.x = 281.6666666666667, sankey_node.y = 516.0718800794655;
			return "translate(281.6666666666667,516.0718800794655)";
		}else if(sankey_node.name == "Correction Exp 3+"){
			sankey_node.x = 422.5, sankey_node.y = 525.4397688278851;
			return "translate(422.5,525.4397688278851)";
		}else if(sankey_node.name == "Both Exp"){
			sankey_node.x = 563.3333333333334, sankey_node.y = 423.64799315577864;
			return "translate(563.3333333333334,423.64799315577864)";
		}else if(sankey_node.name == "Tweet Rumor"){
			sankey_node.x = 704.1666666666667, sankey_node.y = -2.1316282072803006e-14;
			return "translate(704.1666666666667,-2.1316282072803006e-14)";
		}else if(sankey_node.name == "Tweet Correction"){
			sankey_node.x = 704.1666666666667, sankey_node.y = 452.81289506953226;
			return "translate(704.1666666666667,452.81289506953226)";
		}else if(sankey_node.name == "Tweet Both"){
			sankey_node.x = 845, sankey_node.y = 266.0406029085497;
			return "translate(845,266.0406029085497)";
		}
	}
}

function ShowSankey(mode){
	var p_visibility = "hidden", a_visibility = "hidden";
	if(mode == "Passive"){
		p_visibility = "visible";
		a_visibility = "hidden";
	} else if(mode == "Active"){
		p_visibility = "hidden";
		a_visibility = "visible";
	}
//	alert("p_vis: " + p_visibility + ", a_vis: " + a_visibility);
	svg.selectAll(".p_node")
		.style("visibility", p_visibility);
	svg.selectAll(".p_link")
		.style("visibility", p_visibility);
	svg.selectAll(".p_over")
		.style("visibility", p_visibility);
	svg.selectAll(".a_node")
		.style("visibility", a_visibility); 
	svg.selectAll(".a_link")
		.style("visibility", a_visibility);
	svg.selectAll(".a_over")
		.style("visibility", a_visibility);
};

function changeSankeyMode(mode){
	if(mode == 'Exposed'){
		ShowSankey('Passive');
		sankey_mode_passive = true;
	}
	else if(mode == 'Tweeted'){
		ShowSankey('Active');
		sankey_mode_passive = false;
	}
	
	// copied it from updateBrush.. to maintain the window even if I swith the SankeyMode
	var minExtent = d3.time.second(brush.extent()[0])
	  , maxExtent = d3.time.second(brush.extent()[1]);
	var left_index = search_left_index(minExtent.getTime() / 1000);
	var right_index = search_right_index(maxExtent.getTime() / 1000);
	
	if(left_index <= right_index){
		initSD(TweetInfo[left_index].Date, TweetInfo[right_index].Date);
		updateNetworkInfo(left_index, false);
	} else {
		initSD(-1, -1);
	}
};

dataRequest(0);
