/*
var StateNames = ["Start", "RumorExp1", "RumorExp2", "RumorExp3m", "DebunkExp1", 
		"DebunkExp2", "DebunkExp3m", "BothExp", "RumorTwt", "DebunkTwt", "BothTwt"];
*/
var StateNames = ["Start", "RumorExp1", "RumorExp2", "RumorExp3m", "DebunkExp1", 
		"DebunkExp2", "DebunkExp3m", "BothExp", "RumorTwt1", "RumorTwt2", "RumorTwt3m", 
		"DebunkTwt1", "DebunkTwt2", "DebunkTwt3m", "BothTwt"];

/*
var TransitionNames = ["Start_RumorExp1", "Start_RumorTwt", "Start_DebunkExp1", 
			"Start_DebunkTwt", "RumorExp1_RumorExp2", "RumorExp1_BothExp", 
			"RumorExp1_RumorTwt", "RumorExp1_DebunkTwt", "RumorExp2_RumorExp3m", "RumorExp2_BothExp", 
			"RumorExp2_RumorTwt", "RumorExp2_DebunkTwt", "RumorExp3m_BothExp", "RumorExp3m_RumorTwt", 
			"RumorExp3m_DebunkTwt", "DebunkExp1_DebunkExp2", "DebunkExp1_BothExp", "DebunkExp1_RumorTwt",
			"DebunkExp1_DebunkTwt", "DebunkExp2_DebunkExp3m", "DebunkExp2_BothExp", "DebunkExp2_RumorTwt",
			"DebunkExp2_DebunkTwt", "DebunkExp3m_BothExp", "DebunkExp3m_RumorTwt", "DebunkExp3m_DebunkTwt",
			"BothExp_RumorTwt", "BothExp_DebunkTwt", "RumorTwt_BothTwt", "DebunkTwt_BothTwt"];
*/

var TransitionNames = ["Start_RumorExp1", "Start_RumorTwt1", "Start_DebunkExp1", "Start_DebunkTwt1", 
	"RumorExp1_RumorExp2", "RumorExp1_BothExp", "RumorExp1_RumorTwt1", "RumorExp1_DebunkTwt1", 
	"RumorExp2_RumorExp3m", "RumorExp2_BothExp", "RumorExp2_RumorTwt1",	"RumorExp2_DebunkTwt1", 
	"RumorExp3m_BothExp", "RumorExp3m_RumorTwt1", "RumorExp3m_DebunkTwt1", 
	"DebunkExp1_DebunkExp2", "DebunkExp1_BothExp", "DebunkExp1_RumorTwt1", "DebunkExp1_DebunkTwt1", 
	"DebunkExp2_DebunkExp3m", "DebunkExp2_BothExp", "DebunkExp2_RumorTwt1", "DebunkExp2_DebunkTwt1", 
	"DebunkExp3m_BothExp", "DebunkExp3m_RumorTwt1", "DebunkExp3m_DebunkTwt1", 
	"BothExp_RumorTwt1", "BothExp_DebunkTwt1", 
	"RumorTwt1_RumorTwt2", "RumorTwt1_BothTwt", "RumorTwt2_RumorTwt3m", "RumorTwt2_BothTwt", "RumorTwt3m_BothTwt", 
	"DebunkTwt1_DebunkTwt2", "DebunkTwt1_BothTwt", "DebunkTwt2_DebunkTwt3m", "DebunkTwt2_BothTwt", "DebunkTwt3m_BothTwt"];

var PassiveSankey = {nodes: [], links : []}, ActiveSankey = {nodes: [], links: []}, PassiveSankeyBuffer = [];

PassiveSankey.nodes = [
	{ name: "Start" },
	{ name: "Rumor Exposure 1"},
	{ name: "Rumor Exposure 2"},
	{ name: "Rumor Exposure 3+"},
	{ name: "Correction Exposure 1"},
	{ name: "Correction Exposure 2"},
	{ name: "Correction Exposure 3+"},
	{ name: "Both Exposure"}
];

ActiveSankey.nodes = [
   	{ name: "Start" },
   	{ name: "Rumor Exposure 1"},
   	{ name: "Rumor Exposure 2"},
   	{ name: "Rumor Exposure 3+"},
   	{ name: "Correction Exposure 1"},
   	{ name: "Correction Exposure 2"},
   	{ name: "Correction Exposure 3+"},
   	{ name: "Both Exposure"},
   	{ name: "Tweet Rumor 1"},
   	{ name: "Tweet Rumor 2"},
   	{ name: "Tweet Rumor 3+"},
   	{ name: "Tweet Correction 1"},
   	{ name: "Tweet Correction 2"},
   	{ name: "Tweet Correction 3+"},
   	{ name: "Tweet Both"},
];


for(var i = 0; i < TransitionNames.length; i++){
	var s_t = TransitionNames[i].split("_");
	
	var source_id = -1, target_id = -1;
	for(var j = 0; j < StateNames.length; j++){
		if(s_t[0] == StateNames[j]) source_id = j;
		if(s_t[1] == StateNames[j]) target_id = j;
	}
	PassiveSankey.links.push({id: i, source: source_id, target: target_id, value: 0});
	ActiveSankey.links.push({id: i, source: source_id, target: target_id, value: 0});
	PassiveSankeyBuffer.push({id: i, source: source_id, target: target_id, value: 0});
//	alert('P - ' + PassiveSankey.links[i].source + ', ' + PassiveSankey.links[i].target);
//	alert('A - ' + ActiveSankey.links[i].source + ', ' + ActiveSankey.links[i].target);
}

var formatNumber = d3.format(",f"),
format = function(d) { return formatNumber(d); },
color = d3.scale.category20();

var margin = {top: 20, right: 15, bottom: 118, left: 60}
, width = 1120 - margin.left - margin.right
, miniHeight = 30, m_mini_height = 1 * 12 + 50;

// sankey code starts from here 
window.svg = d3.select("#main").append("svg")
.attr("width", "860px")
.attr("height", "580px")
.attr('class', 'diagram')
.append("g");
//.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var sankey_width = 860, sankey_height = 580;

var Passive_Sankey = d3.sankey()
.nodeWidth(15)
.nodePadding(10)
.size([sankey_width, sankey_height]);

var Passive_Sankey_path = Passive_Sankey.link();

var Active_Sankey = d3.sankey()
.nodeWidth(15)
.nodePadding(10)
.size([sankey_width, sankey_height]);

var Active_Sankey_path = Active_Sankey.link();

var chart = d3.select('#TimelineZone')
.append('svg')
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight + margin.bottom + m_mini_height + margin.top)
.attr('class', 'chart');

//Magnified timebar
var m_mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 25 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', m_mini_height + margin.top)
.attr('class', 'm_mini');

//Main timebar
var mini = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 110 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight)
.attr('class', 'mini');

//Histogram alternative to main timebar
var hist = chart.append('g')
.attr('transform', 'translate(' + margin.left + ',' + 110 + ')')
//.attr('width', width)
.attr('width', width + margin.right + margin.left)
.attr('height', miniHeight)
.attr('class', 'hist');


var TweetInfoArea = chart.append('g')
.attr('transform', 'translate(850,12)')
//.attr('width', width)
.attr('width', 200)
.attr('height', 20)
.attr('class', 'feedback_area');

mini.selectAll('rect.background').remove();

var bars = [], Pedge_window = new Array(TransitionNames.length), Aedge_window = new Array(TransitionNames.length);
var window_updated = false, sankey_mode_passive = true;
var LinksNotInPassive = function (elem){ return (elem.source < 8 && elem.target < 8);};

function restart() {
	if(window_updated == true){
		var window_width, base_width, class_string;
		if(sankey_mode_passive == true){
			for(var i = 0; i < TransitionNames.length; i++){
				PassiveSankeyBuffer[i].value = Pedge_window[i];
			}
			base_width = passive_stroke_width;
			window_width = PassiveSankeyBuffer.filter(LinksNotInPassive);
			class_string = "p_over";
		} else {
			for(var i = 0; i < TransitionNames.length; i++){
				ActiveSankey.links[i].value = Aedge_window[i];
			}
			base_width = active_stroke_width;
			window_width = ActiveSankey.links;
			class_string = "a_over";
		}
		svg.selectAll("." + class_string).data(window_width).style("stroke-width", function (d, i) {
			var new_width = base_width.dy[i] * (d.value / base_width.value[i]);
//			alert(d.id + ', source: ' + d.source.name + ', dest: ' +d.target.name);
//			alert("id: " + i + ", width: " + base_width.dy[i] + ' - new width: ' + new_width);
			return new_width;
		});
		svg.selectAll("." + class_string + " title").data(window_width)
			.text(function(d) { return format(d.value); });
		
	} else {
		for(var i = 0; i < TransitionNames.length; i++){
			PassiveSankey.links[i].value = Pedge_window[i];
			ActiveSankey.links[i].value = Aedge_window[i];
		}
		PassiveSankey.links = PassiveSankey.links.filter(LinksNotInPassive);		// PassiveSankeyBuffer changes at each iteration.
		CreateSankey();
		ShowSankey("Passive");
		sankey_mode_passive = true;
	//	alert('first source: ' + PassiveSankeyBuffer[0].source + ', first target: ' + PassiveSankeyBuffer[0].target);
	}
};

var passive_stroke_width = {dy: [], value: []}, active_stroke_width = {dy: [], value: []};

function CreateSankey(){
	var node_holder, dlink, link_holder;
	for(var i = 0; i < 2; i++){
		var sankey = (i == 0) ? Passive_Sankey : Active_Sankey;
		var path = (i == 0) ? Passive_Sankey_path : Active_Sankey_path;
		var SankeyData = null, link_class_string = "", node_class_string = "", hidden_class = "fake", overlay_link_class = "";
		
		if(i == 0) {
			SankeyData = PassiveSankey;
			link_class_string = "p_link", node_class_string = "p_node", overlay_link_class = "p_over"; 
		} else if(i == 1){
			SankeyData = ActiveSankey;
			link_class_string = "a_link", node_class_string = "a_node", overlay_link_class = "a_over";
		}
//		alert('Load Sankey ' + i + ' Nodes: ' + SankeyData.nodes.length + ' links: ' + SankeyData.links.length);
		sankey
		  .nodes(SankeyData.nodes)
		  .links(SankeyData.links)
		  .layout(32);
//		alert('Sankey loaded ' + i);
		
/*		for(var temp = 0; temp < SankeyData.links.length; temp++){		// control thickness of links
			if(SankeyData.links[temp].dy > 100){
				SankeyData.links[temp].dy -= 20;
			}
		}
*/
		link_holder = svg.append("g").selectAll("." + link_class_string)
		  .data(SankeyData.links)
		  .enter().append("path")
		  .attr("class", function(d) { if(d.value > 1){ return link_class_string + " link"; } else {return hidden_class + " link"; }})
		  .attr("d", path)
		  .style("stroke-width", function(d) { return Math.max(1, d.dy); })
		  .style("visibility", "hidden")
          .on("click", function (d) { reorderLinks(d, true); })
          .on("contextmenu", function (d) { d3.event.preventDefault(); reorderLinks(d, false); });
//		  .sort(function(a, b) { return b.dy - a.dy; });

		link_holder.append("title")
		  .text(function(d) { return d.source.name + " --> " + d.target.name + "\n" + format(d.value); });
//		alert('link ready ' + i);
		
		if(i == 0)
			passive_link_holder = link_holder;
		else if (i == 1)
			active_link_holder = link_holder;
		
		for(var temp = 0; temp < SankeyData.links.length; temp++){
			if(i == 0){
				passive_stroke_width.dy[temp] = SankeyData.links[temp].dy;
				passive_stroke_width.value[temp] = SankeyData.links[temp].value;
			} else if(i == 1){
				active_stroke_width.dy[temp] = SankeyData.links[temp].dy;
				active_stroke_width.value[temp] = SankeyData.links[temp].value;
			}
		}
		
		dlink = svg.append("g").selectAll("." + overlay_link_class)
		  .data(SankeyData.links)
		  .enter().append("path")
		  .attr("class", overlay_link_class + " dlink")
		  .attr("d", path)
		  .on("click", function (d){
			  		highlightItemOfEdge(TransitionNames[d.id]);
	    		})
		  .style("stroke-width", function(d) { return 0; })
		  .style("visibility", "hidden") ;
//		  .sort(function(a, b) { return b.dy - a.dy; });

		dlink.append("title")
		  .text(function(d) { return format(d.value); });
//		alert('dlink ready ' + i);
		
		node_holder = svg.append("g").selectAll("." + node_class_string)
		  .data(SankeyData.nodes)
		.enter().append("g")
		  .attr("class", node_class_string + " node")
//		  .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
		  .attr("transform", function(d) { return optimizeLayout(i, d); })
		  .style("visibility", "hidden")
		  .call(d3.behavior.drag()
		  .origin(function(d) { return d; })
		  .on("dragstart", function() { this.parentNode.appendChild(this); })
		  .on("drag", dragmove));

		node_holder.append("rect")
		  .attr("height", function(d) { return d.dy; })
		  .attr("width", sankey.nodeWidth())
		  .style("fill", function(d) { 
			 //alert(d.name + " | " +d.name.replace(/ .*/, "") + " | " + color(d.name.replace(/ .*/, "")));
			  return d.color = customColor(d.name);})
			  
			  //return d.color = color(d.name.replace(/ .*/, "")); })
		  .style("stroke", function(d) { return d3.rgb(d.color).darker(2); })
		.append("title")
		  .text(function(d) { return d.name + "\n" + format(d.value); });

		node_holder.append("text")
		  .attr("x", -6)
		  .attr("y", function(d) { return d.dy / 2; })
		  .attr("dy", ".35em")
		  .attr("text-anchor", "end")
		  .attr("transform", null)
		  .text(function(d) { return d.name; })
		.filter(function(d) { return d.x < width / 2; })
		  .attr("x", 6 + sankey.nodeWidth())
		  .attr("text-anchor", "start");
//		alert('node ready ' + i);
		
		sankey.relayout();
		link_holder.attr("d", path);
		dlink.attr("d", path);
//		alert('node ready ' + i);
		function dragmove(d) {
			d3.select(this).attr("transform", "translate(" + d.x + "," + (d.y = Math.max(0, Math.min(sankey_height - d.dy, d3.event.y))) + ")");
			sankey.relayout();
			link_holder.attr("d", path);
		}
        
        function reorderLinks(d, LeftClick){
			var ReorderSource = true;			// reorder target links if false.
			if(d3.event.x - d.source.x < d.target.x - d3.event.x)
				ReorderSource = false;
            if(ReorderSource == true){
                sankey.shiftLinkOfNode(ReorderSource, d.target, d, LeftClick);
            }
            else
                sankey.shiftLinkOfNode(ReorderSource, d.source, d, LeftClick);

			link_holder.attr("d",path);
		}

//		alert('alive ' + i);
	}
};

function customColor(nodeName)
{
	if (nodeName.indexOf("Tweet") > -1)
	{
		if (nodeName.indexOf("Rumor") > -1)
		{
			return "#393b79";
			//return "#5254a3";
		}
		else if (nodeName.indexOf("Correction") > -1)
		{
			return "#843c39";
			//return "#ad494a";
		}
		else if (nodeName.indexOf("Both") > -1)
		{
			return "#ffbb78";
			//return "#a55194";
		}
	}
	else if (nodeName.indexOf("Start") > -1)
	{
		return "#b5cf6b";
	}
	else if (nodeName.indexOf("Rumor") > -1)
	{
		return "#6b6ecf";
	}
	else if (nodeName.indexOf("Correction") > -1)
	{
		return "#d6616b";
	}
	else if (nodeName.indexOf("Both") > -1)
	{
		//return "#ffbb78";
		return "#ce6dbd";
	}
	
	return "#e7ba52";
	
}

function optimizeLayout(type, sankey_node){
	if(type == 0){
		if(sankey_node.name == "Start"){
			sankey_node.x = 0, sankey_node.y = 8.492191470628942;
			return "translate(0,8.492191470628942)";
		} else if(sankey_node.name == "Rumor Exposure 1"){
			sankey_node.x = 211.25, sankey_node.y = 4.999999999999943;
			return "translate(211.25,4.999999999999943)";
		} else if(sankey_node.name == "Rumor Exposure 2"){
			sankey_node.x = 422.5, sankey_node.y = 225.98988005857956;
			return "translate(422.5,225.98988005857956)";
		} else if(sankey_node.name == "Rumor Exposure 3+"){
			sankey_node.x = 633.75, sankey_node.y = 293.19924031127636;
			return "translate(633.75,293.19924031127636)";
		} else if(sankey_node.name == "Correction Exposure 1"){
			sankey_node.x = 211.25, sankey_node.y = 451.5869075902176;
			return "translate(211.25,451.5869075902176)";
			//sankey_node.x = 211.25, sankey_node.y = 325.5869075902176;
			//return "translate(211.25,325.5869075902176)";
		} else if(sankey_node.name == "Correction Exposure 2"){
			sankey_node.x = 422.5, sankey_node.y = 346.8932080481022;
			return "translate(422.5,346.8932080481022)";
		} else if(sankey_node.name == "Correction Exposure 3+"){
			sankey_node.x = 633.75, sankey_node.y = 309.86357978272594;
			return "translate(633.75,309.86357978272594)";
		} else if(sankey_node.name == "Both Exposure"){
			sankey_node.x = 845, sankey_node.y = 286.29270160904764;
			return "translate(845,286.29270160904764)";
		}
	} 
	else if(type == 1){
		if(sankey_node.name == "Start"){
			sankey_node.x = 0, sankey_node.y = 43.20607375271149;
			return "translate(0,43.20607375271149)";
		} else if(sankey_node.name == "Rumor Exposure 1"){
			sankey_node.x = 105.625, sankey_node.y = 85;
			return "translate(105.625,85)";
		}else if(sankey_node.name == "Rumor Exposure 2"){
			sankey_node.x = 211.25, sankey_node.y = 293;
			return "translate(211.25,293)";
		}else if(sankey_node.name == "Rumor Exposure 3+"){
			sankey_node.x = 316.875, sankey_node.y = 322;
			return "translate(316.875,322)";
		}else if(sankey_node.name == "Correction Exposure 1"){
			sankey_node.x = 105.625, sankey_node.y = 448.09381778741863;
			return "translate(105.625,448.09381778741863)";
		}else if(sankey_node.name == "Correction Exposure 2"){
			sankey_node.x = 211.25, sankey_node.y = 465.01409978308027;
			return "translate(211.25,465.01409978308027)";
		}else if(sankey_node.name == "Correction Exposure 3+"){
			sankey_node.x = 316.875, sankey_node.y = 475.3389370932755;
			return "translate(316.875,475.3389370932755)";
		}else if(sankey_node.name == "Both Exposure"){
			sankey_node.x = 422.5, sankey_node.y = 392.00917584838413;
			return "translate(422.5,392.00917584838413)";
		}else if(sankey_node.name == "Tweet Rumor 1"){
			sankey_node.x = 528.125, sankey_node.y = 0;
			return "translate(528.125,0)";
		}else if(sankey_node.name == "Tweet Rumor 2"){
			sankey_node.x = 633.75, sankey_node.y = 266.7776865736275;
			return "translate(633.75,266.7776865736275)";
		}else if(sankey_node.name == "Tweet Rumor 3+"){
			sankey_node.x = 739.375, sankey_node.y = 271.45467791831453;
			return "translate(739.375,271.45467791831453)";
		}else if(sankey_node.name == "Tweet Correction 1"){
			sankey_node.x = 528.125, sankey_node.y = 470.7009686588376;
			return "translate(528.125,470.7009686588376)";
		} else if(sankey_node.name == "Tweet Correction 2"){
			sankey_node.x = 633.75, sankey_node.y = 363.7233575861212;
			return "translate(633.75,363.7233575861212)";
		} else if(sankey_node.name == "Tweet Correction 3+"){
			sankey_node.x = 739.375, sankey_node.y = 347.0063782049941;
			return "translate(739.375,347.0063782049941)";
		}else if(sankey_node.name == "Tweet Both"){
			sankey_node.x = 845, sankey_node.y = 272.17182661412124;
			return "translate(845,272.17182661412124)";
		}
	}

}

function ShowSankey(mode){
	var p_visibility = "hidden", a_visibility = "hidden";
	if(mode == "Passive"){
		p_visibility = "visible";
		a_visibility = "hidden";
	} else if(mode == "Active"){
		p_visibility = "hidden";
		a_visibility = "visible";
	}
//	alert("p_vis: " + p_visibility + ", a_vis: " + a_visibility);
	svg.selectAll(".p_node")
		.style("visibility", p_visibility);
	svg.selectAll(".p_link")
		.style("visibility", p_visibility);
	svg.selectAll(".p_over")
		.style("visibility", p_visibility);
	svg.selectAll(".a_node")
		.style("visibility", a_visibility); 
	svg.selectAll(".a_link")
		.style("visibility", a_visibility);
	svg.selectAll(".a_over")
		.style("visibility", a_visibility);
};

function changeSankeyMode(mode){
	if(mode == 'Exposed'){
		ShowSankey('Passive');
		sankey_mode_passive = true;
	}
	else if(mode == 'Tweeted'){
		ShowSankey('Active');
		sankey_mode_passive = false;
	}

	if(no_brush==true) 
		return;

	// copied it from updateBrush.. to maintain the window even if I swith the SankeyMode
	var minExtent = d3.time.second(brush.extent()[0])
	  , maxExtent = d3.time.second(brush.extent()[1]);
	var left_index = search_left_index(minExtent.getTime() / 1000);
	var right_index = search_right_index(maxExtent.getTime() / 1000);
	
	if(left_index <= right_index){
		initSD(TweetInfo[left_index].Date, TweetInfo[right_index].Date);
		updateNetworkInfo(left_index, false);
	} else {
		initSD(-1, -1);
	}
};
//alert("Hold.");
dataRequest(0);
