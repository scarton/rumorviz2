/**
 * D3 Sankey Diagram plugin adapted from 
 * https://github.com/d3/d3-plugins/tree/master/sankey by Mike Bostock
 */

d3.sankey = function() {
  var sankey = {},
      nodeWidth = 24,
      nodePadding = 8,
      size = [1, 1],
      nodes = [],
      links = [];

  sankey.nodeWidth = function(_) {
    if (!arguments.length) return nodeWidth;
    nodeWidth = +_;
    return sankey;
  };

  sankey.nodePadding = function(_) {
    if (!arguments.length) return nodePadding;
    nodePadding = +_;
    return sankey;
  };

  sankey.nodes = function(_) {
    if (!arguments.length) return nodes;
    nodes = _;
    return sankey;
  };

  sankey.links = function(_) {
    if (!arguments.length) return links;
    links = _;
    return sankey;
  };

  sankey.size = function(_) {
    if (!arguments.length) return size;
    size = _;
    return sankey;
  };

  sankey.layout = function(outside) {
	  //console.log("d3.sankey.layout()");
    computeNodeLinks();
    computeNodeValues();
    computeNodeBreadths();
    computeManualNodeDepths();
   // computeNodeDepths(iterations);
    computeLinkDepths(outside);
    return sankey;
  };

  sankey.relayout = function() {
    computeLinkDepths();
    return sankey;
  };

  sankey.link = function() {
    var curvature = .5;

    function link(d) {
      var x0 = d.source.x + d.source.dx,
          x1 = d.target.x,
          xi = d3.interpolateNumber(x0, x1),
          x2 = xi(curvature),
          x3 = xi(1 - curvature),
          y0 = d.source.y + d.sy + d.dy / 2,
          y1 = d.target.y + d.ty + d.dy / 2;
/*      	if (isNaN(y1))
      	{
      		console.log("d.target.name: " + d.target.name);
      		console.log("d.target.y: " + d.target.y);
      		console.log("d.ty: " + d.ty);
      		console.log("d.dy: " + d.dy);
      	}*/
      return "M" + x0 + "," + y0
           + "C" + x2 + "," + y0
           + " " + x3 + "," + y1
           + " " + x1 + "," + y1;
    }

    link.curvature = function(_) {
      if (!arguments.length) return curvature;
      curvature = +_;
      return link;
    };

    return link;
  };

  // Populate the sourceLinks and targetLinks for each node.
  // Also, if the source and target are not objects, assume they are indices.
  function computeNodeLinks() {
    nodes.forEach(function(node) {
      node.sourceLinks = [];
      node.targetLinks = [];
    });
    links.forEach(function(link) {
      var source = link.source,
          target = link.target;
      if (typeof source === "number") source = link.source = nodes[link.source];
      if (typeof target === "number") target = link.target = nodes[link.target];
      source.sourceLinks.push(link);
      target.targetLinks.push(link);
    });
  }

  // Compute the value (size) of each node by summing the associated links.
  function computeNodeValues() {
    nodes.forEach(function(node) {
      node.value = Math.max(
        d3.sum(node.sourceLinks, value),
        d3.sum(node.targetLinks, value)
      );
    });
  }

  // Iteratively assign the breadth (x-position) for each node.
  // Nodes are assigned the maximum breadth of incoming neighbors plus one;
  // nodes with no incoming links are assigned breadth zero, while
  // nodes with no outgoing links are assigned the maximum breadth.
  function computeNodeBreadths() {
    var remainingNodes = nodes,
        nextNodes,
        x = 0;

    	nodes.forEach(function(node){
    		node.x = node.position;
    		node.dx = nodeWidth;
    		x = Math.max(x,node.x);
    	});
/*    while (remainingNodes.length) {
      nextNodes = [];
      remainingNodes.forEach(function(node) {
        node.x = x;
        //console.log("Node: " + node.name + " x set to " + node.x + " in computeNodeBreadths()");
        node.dx = nodeWidth;
        node.sourceLinks.forEach(function(link) {
          nextNodes.push(link.target);
        });
      });
      remainingNodes = nextNodes;
      ++x;
    }

    moveSinksRight(x);*/
   // console.log("size[0]: " + size[0]);
   // console.log("nodeWidth: " + nodeWidth);

    scaleNodeBreadths((size[0] - nodeWidth) / (x));
  }

  function moveSourcesRight() {
    nodes.forEach(function(node) {
      if (!node.targetLinks.length) {
        node.x = d3.min(node.sourceLinks, function(d) { return d.target.x; }) - 1;
        //console.log("Node: " + node.name + " x set to " + node.x + " in moveSourcesRight()");

      }
    });
  }

  function moveSinksRight(x) {
    nodes.forEach(function(node) {
      if (!node.sourceLinks.length) {
        node.x = x - 1;
        //console.log("Node: " + node.name + " x set to " + node.x + " in moveSinksRight()");
      }
    });
  }

  function scaleNodeBreadths(kx) {
    nodes.forEach(function(node) {
      node.x *= kx;
     // console.log("Node: " + node.name + " x set to " + node.x + " in scaleNodeBreadths()");
    });
  }

  /**
   * Utility function I added to try to track appearance of NaN node y values
   */
  function checkForNaNs() {
	  console.log("Checking for NaNs...");
	  nodes.forEach(function(node){
		  if (isNaN(node.y)) console.log("\t"+node.name+".y is NaN");
	  });
  }
  
  /**
   * A layout algorithm that tries to make a very clean diagram for our particular
   * use case.
   */
  function computeManualNodeDepths()
  {
	    nodes.forEach(function(node) {
	        node.sourceLinks.sort(compareSourcePosition);
	        node.targetLinks.sort(compareTargetPosition);
	      });
	      function compareSourcePosition(a,b){
	      	return a.source_position - b.source_position;
	      }
	      
	      function compareTargetPosition(a,b){
	      	return a.target_position - b.target_position;
	      }
	      
	  //Group nodes with the same horizontal position
	    var nodesByBreadth = d3.nest()
	        .key(function(d) { return d.x; })
	        .sortKeys(d3.ascending)
	        .entries(nodes)
	        .map(function(d) { return d.values; });
	  // console.log("nodePadding: " + nodePadding);
	    //Compute ky such that node.value*ky will equal node.dy
	    var ky = d3.min(nodesByBreadth, function(nodes) {
	        return (size[1] - (nodes.length - 1) * nodePadding) / d3.sum(nodes, value);
	      });
	    
	    var first = true;
	    var centerLine; //Nodes should be centered on this y value
	    var upperGap=nodePadding/2; //Upper nodes should be this far above the centerline
	    var lowerGap=nodePadding/2; //Lower nodes should be this far below the centerline
	    
	    nodesByBreadth.forEach(function(nodes){
/*	    	console.log("upperGap: " + upperGap);
	    	console.log("lowerGap: " + lowerGap);*/
	    	//Center first set of nodes evenly across the height
	    	if (first)
	    	{
	    		var y = (size[1]-(ky*d3.sum(nodes, value)+((nodes.length - 1) * nodePadding)) )/2;
	    		nodes.forEach(function(node){
	    			node.y = y;
	    			node.dy = ky*node.value;
	    			y += node.dy+nodePadding;

	    		});
	    		first = false;
	    		
	    		if (nodes.length == 1)
	    		{
	    			centerLine = nodes[0].y;
		    		nodes[0].sourceLinks.forEach(function(link){
		    			
		    			if (link.target.upper)
		    			{
		    				centerLine += ky*link.value;
		    			}
		    		});
		    		console.log("Center line: " + centerLine);
	    		}
	    		else
	    		{
	    			centerLine = nodes[0].y;
	    			nodes.forEach(function(node){
	    				if (node.upper)
	    					centerLine = centerLine +node.dy + nodePadding;
	    			});
	    			centerLine -= nodePadding/2;
	    		}
	    	}
	    	//Compute position of subsequent nodes by the position of their 
	    	//source node and the relative position of the links connecting them
	    	else
	    	{
/*	    		console.log("non first");
*/	    		//For each node, let its position be the average of where its source nodes would like it to be
	    		nodes.forEach(function(node){
    				node.dy = ky*node.value;

	    			if (node.upper)
	    			{
	    				console.log(node.name);
	    				node.dy = ky*node.value;
	    				node.y = centerLine-upperGap - node.dy;
	    				//console.log(upperGap);
	    				//console.log(centerLine + " | " + upperGap + " | " + node.dy);
	    				//Make room for this node's links that are trying to run up the middle of the diagram
	    				var found = false;
	    				node.sourceLinks.forEach(function(link){
	    					if (found)
	    					{
	    						upperGap+=ky*link.value;
	    					}
	    					else
	    					{
	    						if (link.target.position == node.position+1)
	    							found = true;
	    					}
	    				});
	    				//upperGap += nodePadding;
	    			}
	    			else if (node.lower)
	    			{
	    				console.log(node.name);
	    				node.y = centerLine+lowerGap;
	    				var found = true;
	    				node.sourceLinks.forEach(function(link){
	    					
	    					if (link.target.position == node.position+1)
    							found = false;
	    					else if (found)
	    					{
	    						lowerGap+=ky*link.value;
	    						console.log("\tlower gap increased to " + lowerGap + " to make room for link to " + link.target.name);

	    					}

	    				});
	    			}
	    			//If we find a middle node, reset our gaps 
	    			else if (node.middle)
	    			{
/*	    				console.log("\tmiddle");
*/	    			    var first = true;
	    			    upperGap=nodePadding/2; 
	    			    lowerGap=nodePadding/2;
	    			    
	    			    node.y = centerLine;
	    			    node.targetLinks.forEach(function(link){
	    			    	if (link.source.upper)
	    			    		node.y-=ky*link.value;
	    			    });
	    			}
	    		});
	    	}
	    });

	    nodes.forEach(function(node){
	    	console.log("Node: " + node.name + " | y: " + node.y + " | dy: " +node.dy); 
	    });
	    
	      links.forEach(function(link) {
	          link.dy = link.value * ky;
	        });
  }
  
  function computeNodeDepths(iterations) {
    var nodesByBreadth = d3.nest()
        .key(function(d) { return d.x; })
        .sortKeys(d3.ascending)
        .entries(nodes)
        .map(function(d) { return d.values; });

    console.log("Nodes by breadth");
    console.log(nodesByBreadth);
    //
    initializeNodeDepth();
    resolveCollisions();
    for (var alpha = 1; iterations > 0; --iterations) {
      relaxRightToLeft(alpha *= .99);
      resolveCollisions();
      relaxLeftToRight(alpha);
      resolveCollisions();
    }

    function initializeNodeDepth() {
      var ky = d3.min(nodesByBreadth, function(nodes) {
        return (size[1] - (nodes.length - 1) * nodePadding) / d3.sum(nodes, value);
      });

      nodesByBreadth.forEach(function(nodes) {
        nodes.forEach(function(node, i) {
          node.y = i;
          //console.log("Node: " + node.name + " y set to " + node.y + " in initializeNodeDepth()");
          node.dy = node.value * ky;
         // console.log("Node: " + node.name + " dy set to " + node.dy + " in initializeNodeDepth()");

        });
      });

      nodes.forEach(function(node){
          node.sourceLinks.forEach(function(link) {
              link.dy = link.value * ky;
            });
          node.targetLinks.forEach(function(link) {
              link.dy = link.value * ky;
            });
      });

    }

    function relaxLeftToRight(alpha) {
      nodesByBreadth.forEach(function(nodes, breadth) {
        nodes.forEach(function(node) {
          if (node.targetLinks.length) {
            var y = d3.sum(node.targetLinks, weightedSource) / d3.sum(node.targetLinks, value);
            node.y += (y - center(node)) * alpha;
            if (isNaN(node.y)) node.y =0;

/*            if (isNaN(node.y))
            {
	            console.log("Node: " + node.name + " y set to " + node.y + " in relaxRightToLeft()");
	            console.log("\tcenter(node): " + center(node));
	            console.log("\talpha: " + alpha);
	            console.log("\ty: " + y);
            }*/
          }
        });
      });

      function weightedSource(link) {
        return center(link.source) * link.value;
      }
    }

    function relaxRightToLeft(alpha) {
      nodesByBreadth.slice().reverse().forEach(function(nodes) {
        nodes.forEach(function(node) {
          if (node.sourceLinks.length) {
            var y = d3.sum(node.sourceLinks, weightedTarget) / d3.sum(node.sourceLinks, value);
            node.y += (y - center(node)) * alpha;
            if (isNaN(node.y)) node.y =0;
/*            if (isNaN(node.y))
            {
	            console.log("Node: " + node.name + " y set to " + node.y + " in relaxRightToLeft()");
	            console.log("\tcenter(node): " + center(node));
	            console.log("\talpha: " + alpha);
	            console.log("\ty: " + y);
            }*/

          }
        });
      });

      function weightedTarget(link) {
        return center(link.target) * link.value;
      }
    }

    function resolveCollisions() {
      nodesByBreadth.forEach(function(nodes) {
        var node,
            dy,
            y0 = 0,
            n = nodes.length,
            i;

        // Push any overlapping nodes down.
        nodes.sort(ascendingDepth);
        for (i = 0; i < n; ++i) {
          node = nodes[i];
          dy = y0 - node.y;
          if (dy > 0) 
    	  {
        	  node.y += dy;
              //console.log("Node: " + node.name + " y set to " + node.y + " in resolveCollisions() 1");

    	  }
          y0 = node.y + node.dy + nodePadding;
        }

        // If the bottommost node goes outside the bounds, push it back up.
        dy = y0 - nodePadding - size[1];
        if (dy > 0) {
          y0 = node.y -= dy;

          // Push any overlapping nodes back up.
          for (i = n - 2; i >= 0; --i) {
            node = nodes[i];
            dy = node.y + node.dy + nodePadding - y0;
            if (dy > 0) 
            {
            	node.y -= dy;
                //console.log("Node: " + node.name + " y set to " + node.y + " in resolveCollisions() 2");

            }
            y0 = node.y;
          }
        }
      });
    }

    function ascendingDepth(a, b) {
      return a.y - b.y;
    }
  }

  function computeLinkDepths(outside) {
    
    nodes.forEach(function(node) {
    	if ((node.lower && !outside) || (node.upper && outside))
    	{
			var sy = 0, ty = 0;
	    }
	    else
	    {
			var sy = node.dy-d3.sum(node.sourceLinks,function(link){return link.dy}), ty = node.dy-d3.sum(node.targetLinks,function(link){return link.dy});
	    }
    	
		node.sourceLinks.forEach(function(link) {
			link.sy = sy;
			sy += link.dy;
		});
		node.targetLinks.forEach(function(link) {
			link.ty = ty;
			ty += link.dy;
		});
    });
  }

  function center(node) {
/*	  if (isNaN(node.y) || isNaN(node.dy))
	  {
		 console.log("\t\tcenter(): node.y: " + node.y + " | node.dy: " + node.dy); 
	  }*/
	
    return node.y + node.dy / 2;
  }

  function value(link) {
    return link.value;
  }

  return sankey;
};