
/**
 * D3 selection is based on standard CSS selector patterns
 */

var allLinks = d3.selectAll("a"); //Select all <a> </a> elements
var allWithCSSClass = d3.selectAll(".some_class"); //Select all elements with class some_class
var allWithSomeID = d3.selectAll("#some_id"); //Select all elements with id some_id
var firstDiv = d3.select("div"); //Select first div
var firstImgInsideFirstDiv = firstDiv.select("img"); //select() calls can be nested


var myData = d3.csv("example.csv", function(d) {
    return {
      year: new Date(+d.Year, 0, 1), // convert "Year" column to Date
      make: d.Make,
      model: d.Model,
      length: +d.Length // convert "Length" column to number
    };
  }, function(error, rows) {
    console.log(error);
  });

console.log("myData:");
console.log(myData);





