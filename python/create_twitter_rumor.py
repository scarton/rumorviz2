__author__ = 'Sam'
import csv
import tweepy
from time import sleep
import json
import os
import datetime
import platform
import dbs

from sqlalchemy import engine, create_engine, text

'''
This script creates a small self-contained rumor on Twitter using some bots whose details are
enumerated in a csv file.
'''

create_network = False

make_tweets = False

filename = "twitter bots.csv"

if platform.system() == 'Linux' and platform.node() == 'gauss':#for deployment on gauss

    sysname = 'gauss'
elif platform.system() == 'Windows' and platform.node() == 'Gondolin':#for local testing

    sysname = 'Gondolin'
else:
    raise Exception('Unknown system. I do not know how to assign file names')

def main():
    print "Creating a small rumor diffusion on twitter"

    print "Loading an API object for each bot"
    f = open(filename)
    dr = csv.DictReader(f)
    rs = {} #rows from csv
    for row in dr:
        rs[row["Name"]] = row

    ts = {} #twitter api objects
    for name in rs:
        r= rs[name]
        auth = tweepy.OAuthHandler(r["Consumer Key"], r["Consumer Secret"])
        auth.set_access_token(r["Access Token"], r["Access Secret Token"])

        api = tweepy.API(auth)
        ts[name] = api

    f.close()


    print ts

    if create_network:
        #Create social network
        # key: person, value: list of people who follow that person
        print "Creating small social network on Twitter"
        nk = {
            "randy":["rachel","richard","connor","christina","caitlin","rebecca","ralph"],
            "rachel":[],
            "richard":[],
            "ralph":[],
            "rebecca":[],
            "christina":["randy","richard","connor","caroline","corwin","caitlin","rebecca"],
            "connor":[],
            "caroline":[],
            "corwin":[],
            "caitlin":[]
        }

        for name in nk:
            for f_name in nk[name]:
                ts[f_name].create_friendship(ts[name].me().id)

    if make_tweets:
        #Post rumor-related tweets
        print "Making rumor tweets"


        r_msg_1 = "I heard a rumor that RumorLens is giving a demo at Tech & Check!"
        r_msg_2 = "RumorLens is going to be at Tech & Check? Great! Paul Resnick does some interesting work."
        r_msg_3 = "Looking forward to seeing RumorLens at Tech & Check!"
        c_msg_1 = "Nonsense. RumorLens isn't a fact-checking platform. It doesn't belong at Tech & Check."
        c_msg_2 = "Yeah. RumorLens doesn't really do fact-checking."
        c_msg_3 = "I guess you are right. Tech & Check really isn't the venue for Paul Resnick's RumorLens work."




        s1 = ts["randy"].update_status(r_msg_1)
        sleep(1)

        s2 = ts["rachel"].retweet(s1.id)
        sleep(1)

        s3 = ts["richard"].update_status(r_msg_2, in_reply_to_status_id = s1.id)
        sleep(1)

        s4 = ts["rebecca"].update_status(r_msg_3)
        sleep(1)

        s5 = ts["christina"].update_status(c_msg_1, in_reply_to_status_id=s1.id)
        sleep(1)


        s6 = ts["caroline"].update_status(c_msg_2, in_reply_to_status_id=s5.id)
        sleep(1)

        s7 = ts['caitlin'].retweet(s5.id)
        sleep(1)

        s8 = ts['rebecca'].update_status(c_msg_3, in_reply_to_status=s5.id)

    else:
        print "Getting latest tweet for each person"
        s1 = ts["randy"].user_timeline(count=1)[0]
        s2 = ts["rachel"].user_timeline(count=1)[0]
        s3 = ts["richard"].user_timeline(count=1)[0]
        s4 = ts["rebecca"].user_timeline(count=2)[1]
        s5 = ts["christina"].user_timeline(count=1)[0]
        s6 = ts["caroline"].user_timeline(count=1)[0]
        s7 = ts['caitlin'].user_timeline(count=1)[0]
        s8 = ts['rebecca'].user_timeline(count=1)[0]

        print


    print "Writing fake tweets to tweets.json and reduced_tweets.json"
    of = open("tweets.json",'w')
    rf = open("reduced_tweets.json",'w')
    of.write("\n")
    ss = [s1,s2,s3,s4,s5,s6,s7,s8]
    for s in ss:
        s._json["fake"] = True
        s._json["comment"] = "This tweet was manually added to the gardenhose archive to test the RumorLens pipeline. Contact scarton@umich.edu for details"
        of.write(json.dumps(s._json))
        of.write("\n")

        #Created a reduced json that can be streamed to the tweet_indexer
        r = {}

        r['text'] = s.text
        r['id'] = str(s.id)
        r['user'] = {}
        r['user']['id']= str(s.user.id)
        r['user']['location'] =""
        r['user']['screen_name'] = s.user.screen_name
        r['created_at'] = s._json['created_at']

        rf.write(json.dumps(r))
        rf.write("\n")


    of.close()
    rf.close()

    # if sysname == "gauss":
    #     opath = '/storage2/foreseer/twitter/gardenhose/raw/gardenhose.' + gettoday()
    #     print "Appending fake tweets from tweets.json to",opath
    #     os.system("cat tweets.json >> " + opath)

    cluster = [s1,s2,s3,s4]

    rid = 5000000

    #Bypass Zhe's detection system by adding a rumor directly to his DB
    # print "Manually adding rumor to Zhe's DB"

    #    # engine = create_engine('mysql+mysqlconnector://'+dbs.DETECT_DB['user']+':'+dbs.DETECT_DB['passwd']+'@'+dbs.DETECT_DB['host']+'/'+dbs.DETECT_DB['db'])
    # #Insert rumor summary
    # q1 = text("insert into rumor_summary_2 (rid, last_update, statement, num_tweets, first_update) values (:rid,:last,:text,:num,:first)")
    # engine.execute(q1, rid = rid, last=s4.created_at, text=s1.text, num = 4, first=s1.created_at)
    #
    # #Insert each tweet
    # for s in cluster:
    #     engine.execute(text("insert into rumor_tweets_2 (rid, tid, tweet, time) values (:rid, :tid, :text, :time)"), rid = rid, tid = s.id, text = s.text, time = s.created_at)
    #
    # #Upvote the rumor
    # engine.execute(text("insert into ranklist_2 (rid, score, numq, sum, update_time) values (:rid, 5, :num, :num, :time)"), rid = rid, num = 4, time = s4.created_at)

    #Bypass Zhe's whole apparatus entirely by manually adding rumor to django backing DB

    print "Entering fake rumor directly into Django database"
    engine = create_engine('postgresql://'+dbs.DJANGO_DB['user']+':'+dbs.DJANGO_DB['passwd']+'@'+dbs.DJANGO_DB['host']+':'+dbs.DJANGO_DB['port']+'/'+dbs.DJANGO_DB['db'])
    q = text("insert into allstages_rumor (rumor_number, vote_score, rank_score, title, description, sample_tweet_one, sample_tweet_two, first_update, last_update, label_progress) " \
        "values (:rumor_number, :vote_score, :rank_score, :title, :description, :sample_tweet_one, :sample_tweet_two, :first_update, :last_update, :label_progress)")

    engine.execute(q,
                   rumor_number = rid,
                   vote_score = 2,
                   rank_score=1,
                   title=s1.text,
                   description ="I heard a rumor that RumorLens is giving a demo at Tech & Check!",
                   sample_tweet_one = s1.id,
                   sample_tweet_two = s5.id,
                   first_update = s1.created_at,
                   last_update = s4.created_at,
                   label_progress = 1)


    print "Done"


def gettoday():
        year = datetime.date.today().year
        month = datetime.date.today().month
        day = datetime.date.today().day
        if month < 10:
                mstr = '0' + str(month)
        else:
                mstr = str(month)
        if day < 10:
                dstr = '0' + str(day)
        else:
                dstr = str(day)
        return str(year) + '-' + mstr + '-' + dstr


if __name__ == "__main__":
    main()