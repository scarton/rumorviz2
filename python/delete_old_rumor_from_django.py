__author__ = 'Sam'

import csv
import tweepy
from time import sleep
import json
import os
import datetime
import platform
import dbs
from sqlalchemy import engine, create_engine, text


print "Deleting manually old rumor stuff from Django DB"
# rid = 10021
rid = 10015

engine = create_engine('postgresql://'+dbs.DJANGO_DB['user']+':'+dbs.DJANGO_DB['passwd']+'@'+dbs.DJANGO_DB['host']+':'+dbs.DJANGO_DB['port']+'/'+dbs.DJANGO_DB['db'])
qs = ["delete from allstages_rumor where rumor_number = {}".format(rid),
      "delete from allstages_vote where rumor_number = {}".format(rid),
      "delete from allstages_snapshot where rumor_id = {}".format(rid),
    "delete from allstages_description where rumor_number = {}".format(rid)]

for q in qs:
    print q
    engine.execute(q)


engine = create_engine('mysql+mysqlconnector://'+dbs.RETRI_DB['user']+':'+dbs.RETRI_DB['passwd']+'@'+dbs.RETRI_DB['host']+'/'+dbs.RETRI_DB['db'])
qs = ["delete from rumorRetri where rumorID = {}".format(rid),
      "delete from rumorRetriStatus where rumorID = {}".format(rid)]

for q in qs:
    print q
    engine.execute(q)

print "Done"