__author__ = 'Sam'

import csv
import tweepy
from time import sleep
import json
import os
import datetime
import platform
import dbs

from sqlalchemy import engine, create_engine, text


# rid = 10021
# title = "Hip-Hop Rumors: Jay Z Dead? WHAT THE?!? http://t.co/kJIE5hNIXF"
# tweet1 = 398546103022260224
# tweet2 = 398596900040880128
#
# time1 = 1383828681
# time2 = 1384391445

rid = 10015
title = "Boston Bombings is a False Flag Attack gone horribly wrong, what a clumsy bunch of actors, make-up artists and FBI agents! #falseflag"
tweet1 = 331473124858146816
tweet2 = 325785546758688769

time1 = 1366060576
time2 = 1368230158

numrumor = 1743
numcorrection = 184

print "Entering old rumor directly into Django database"
engine = create_engine('postgresql://'+dbs.DJANGO_DB['user']+':'+dbs.DJANGO_DB['passwd']+'@'+dbs.DJANGO_DB['host']+':'+dbs.DJANGO_DB['port']+'/'+dbs.DJANGO_DB['db'])
q = text("insert into allstages_rumor (rumor_number, vote_score, rank_score, title, description, sample_tweet_one, sample_tweet_two, first_update, last_update, label_progress) " \
    "values (:rumor_number, :vote_score, :rank_score, :title, :description, :sample_tweet_one, :sample_tweet_two, :first_update, :last_update, :label_progress)")

engine.execute(q,
               rumor_number = rid,
               vote_score = 2,
               rank_score=1,
               title=title,
               description ="",
               sample_tweet_one = tweet1,
               sample_tweet_two = tweet2,
               first_update = datetime.datetime.fromtimestamp(time1),
               last_update = datetime.datetime.fromtimestamp(time2),
               label_progress = 1)


# Insert a rumorStatus entry for this rumor in Cheng's DB
engine = create_engine('mysql+mysqlconnector://'+dbs.RETRI_DB['user']+':'+dbs.RETRI_DB['passwd']+'@'+dbs.RETRI_DB['host']+'/'+dbs.RETRI_DB['db'])
q = text('''insert into rumorRetriStatus 
         values (
        :rumorID,
        :stage,
        :numSearches,
        :judgedCount,
        :judgedSpread,
        :judgedCorrection,
        :progress,
        :pctPosLatest,
        :stability,
        :predictedNonRel,
        :predictedRel,
        :predictedSpread,
        :predictedCorrection,
        :lastQuery,
        :searchQuery,
        :rankList,
        :sinceTime,
        :untilTime
         )''')

engine.execute(q,
        rumorID=rid,
        stage=0,
        numSearches=1,
        judgedCount=numrumor+numcorrection,
        judgedSpread=numrumor,
        judgedCorrection=numcorrection,
        progress=1,
        pctPosLatest=0,
        stability=100,
        predictedNonRel=0,
        predictedRel=0,
        predictedSpread=0,
        predictedCorrection=0,
        lastQuery=None,
        searchQuery=None,
        rankList=None,
        sinceTime=None,
        untilTime=None
    
)

print "Done"