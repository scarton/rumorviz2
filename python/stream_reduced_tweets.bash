#!/bin/bash
# Run LocalStreamer and pipe its output over ssh to instance of RemoteIngester running on Google instance.
# Look up instance IP address with inline use of "gcutil getinstance ..."
# today=$(date +"%Y-%m-%d")
# echo today: $today
now=$(date +"%s")
# echo now: $now
echo "Streaming tweets to cloud..."
ip=$(/home/scarton/gcutil/google-cloud-sdk/bin/gcutil getinstance --zone=us-central1-a tweet-indexer | grep "external-ip" | grep -o -P "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+")
cat reduced_tweets.json | \
	ssh -q -o UserKnownHostsFile=/dev/null -o CheckHostIP=no -o StrictHostKeyChecking=no \
	-i /home/scarton/.ssh/google_compute_engine -A -p 22 scarton@$ip \
	'java -cp /home/scarton/TweetIndex/bin:/home/scarton/TweetIndex/* index.GoogleCloudRemoteIngester'
echo "Done"