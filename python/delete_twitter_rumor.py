__author__ = 'Sam'
import csv
import tweepy
import dbs
from sqlalchemy import engine, create_engine


'''
This script deletes all friendships and tweets by any accounts in the twitter bots csv,
and also purges it from the various databases
'''

filename = "twitter bots.csv"

delete_friendships = False
delete_tweets_from_twitter = False
delete_author_lookups = False
delete_precomputation = False


def main():
    print "Creating a small rumor diffusion on twitter"

    print "Loading an API object for each bot"
    f = open(filename)
    dr = csv.DictReader(f)
    rs = {} #rows from csv
    for row in dr:
        rs[row["Name"]] = row

    ts = {} #twitter api objects
    for name in rs:
        r= rs[name]
        auth = tweepy.OAuthHandler(r["Consumer Key"], r["Consumer Secret"])
        auth.set_access_token(r["Access Token"], r["Access Secret Token"])

        api = tweepy.API(auth)
        ts[name] = api

    f.close()

    print "Deleting statuses and friendships for each bot"
    for tname in ts:
        print tname,'...'
        t = ts[tname]
        for id in t.friends_ids():
            if delete_friendships:
                t.destroy_friendship(id)

        for s in t.home_timeline():
            if delete_tweets_from_twitter:
                t.destroy_status(s.id)

    print "Deleting manually test stuff from Zhe's DB"

    engine = create_engine('mysql+mysqlconnector://'+dbs.DETECT_DB['user']+':'+dbs.DETECT_DB['passwd']+'@'+dbs.DETECT_DB['host']+'/'+dbs.DETECT_DB['db'])
    rid = 5000000
    qs = ["delete from rumor_summary_2 where rid = {}".format(rid),
          "delete from rumor_tweets_2 where rid = {}".format(rid),
          "delete from ranklist_2 where rid = {}".format(rid)]
    for q in qs:
        print q
        engine.execute(q)


    print "Deleting manually test stuff from Django DB"

    engine = create_engine('postgresql://'+dbs.DJANGO_DB['user']+':'+dbs.DJANGO_DB['passwd']+'@'+dbs.DJANGO_DB['host']+':'+dbs.DJANGO_DB['port']+'/'+dbs.DJANGO_DB['db'])
    qs = ["delete from allstages_rumor where rumor_number = {}".format(rid),
          "delete from allstages_vote where rumor_number = {}".format(rid),
          "delete from allstages_snapshot where rumor_id = {}".format(rid),
        "delete from allstages_description where rumor_number = {}".format(rid)]

    for q in qs:
        print q
        engine.execute(q)


    print "Manually deleting stuff from Cheng's DB"
    engine = create_engine('mysql+mysqlconnector://'+dbs.RETRI_DB['user']+':'+dbs.RETRI_DB['passwd']+'@'+dbs.RETRI_DB['host']+'/'+dbs.RETRI_DB['db'])
    qs = ["delete from rumorRetri where rumorID = {}".format(rid),
          "delete from rumorRetriStatus where rumorID = {}".format(rid)]

    for q in qs:
        print q
        engine.execute(q)


    print "Manually deleting stuff from Sam's DB"
    engine = create_engine('mysql+mysqlconnector://'+dbs.ANALYZE_DB['user']+':'+dbs.ANALYZE_DB['passwd']+'@'+dbs.ANALYZE_DB['host']+'/'+dbs.ANALYZE_DB['db'])

    qs = []
    if delete_author_lookups:
        qs.append("delete from author where rumor_id = {}".format(rid))

    if delete_precomputation:
        qs.extend(["delete from author_activity where rumor_id = {}".format(rid),
            "delete from followed where rumor_id = {}".format(rid),
            "delete from follower where rumor_id = {}".format(rid),
            "delete from neighborhood_tweets where rumor_id = {}".format(rid),
            "delete from sankey_state_update where rumor_id = {}".format(rid),
            "delete from tweet where rumor_id = {}".format(rid)])

    for q in qs:
        print q
        engine.execute(q)

    
    print "Done!"


if __name__ == "__main__":
    main()